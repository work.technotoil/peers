package com.peerbuckets.peerbucket.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInListActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleActivity;
import com.peerbuckets.peerbucket.Activity.todo.ToDoActivity;
import com.peerbuckets.peerbucket.Adapter.ActivityLatestListAdapter;
import com.peerbuckets.peerbucket.Adapter.AllEmployeAdapter;
import com.peerbuckets.peerbucket.Adapter.MinEmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.SearchDialogAdapter;
import com.peerbuckets.peerbucket.Adapter.UserListChatAdapter;
import com.peerbuckets.peerbucket.POJO.ActivityListResponce.Activity;
import com.peerbuckets.peerbucket.POJO.ActivityListResponce.CompanyActivityListResponse;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.SearchResponse.CardListing;
import com.peerbuckets.peerbucket.POJO.SearchResponse.SearchResponse;
import com.peerbuckets.peerbucket.POJO.SearchResponse.UserListing;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.AllEmployeeList;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_ANDROID;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.TRASH_CPT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class CompanyHQExpandActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private ApiRequest mApiRequest;
    RecyclerView recyclerView, memployee_icon_recycler_view, rv_latestActivity;
    private RecyclerView.LayoutManager memployee_icon_recycler_viewLayoutManager;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    private RecyclerView.Adapter memployee_icon_recycler_view_adapter;
    String mcompanyId = "", muserId = "", maddremovetype = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", mCurrentuserId = "";
    CardView mcv_search_expand, automaticCheckin, docsAndFiles, schedule, toDo, card_chat, card_message_board;
    TextView tv_description, direction_tv, tv_activity;
    Context context = this;
    RecyclerView.LayoutManager rv_latestActivityLayoutManager;
    ActivityLatestListAdapter latest_list_adapter;
    String projectId = "", name = "", discretion = "";
    int page = 0;
    AllEmployeAdapter allEmployeAdapter;
    ProgressBar pb_progress;
    Spinner sp_byAnyone, sp_everywhere;
    boolean isByAnyone = false;
    boolean isEveryWhere = false;
    ArrayList<UserListing> userListingSpinner;
    ArrayList<CardListing> cardListingSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_hqexpand);

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        mcompanyId = ShardPreferences.get(CompanyHQExpandActivity.this, share_Company_Id_key);
        muserId = ShardPreferences.get(CompanyHQExpandActivity.this, share_UserId_key);
        mCurrentuserId = ShardPreferences.get(CompanyHQExpandActivity.this, share_current_UserId_key);
        maddremovetype = ShardPreferences.get(CompanyHQExpandActivity.this, share_addd_remove_current_type);
        mcurrentProjectTeamId = ShardPreferences.get(CompanyHQExpandActivity.this, share_current_project_team_id);
        mcurrentCompanyName = ShardPreferences.get(CompanyHQExpandActivity.this, share_current_company_name);

        initUI();
        initToolbar();
        initRecyclerView();
        fetchEmployee();
        ActivityList();
    }


    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", maddremovetype);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        mApiRequest.postRequest(BASE_URL + FETCH_EMPLOYEE_ANDROID, FETCH_EMPLOYEE_ANDROID, paramsReq, Request.Method.POST);
    }


    private void initUI() {
        tv_activity = findViewById(R.id.tv_activity);
        rv_latestActivity = findViewById(R.id.rv_latestActivity);
        direction_tv = findViewById(R.id.direction_tv);
        tv_description = findViewById(R.id.tv_description);
        mcv_search_expand = findViewById(R.id.cv_search_expand);
        automaticCheckin = findViewById(R.id.card_automatic_check_in);
        docsAndFiles = findViewById(R.id.card_docs_and_files);
        schedule = findViewById(R.id.card_schedule);
        toDo = findViewById(R.id.card_todo);
        card_chat = findViewById(R.id.card_chat);
        card_message_board = findViewById(R.id.card_message_board);
        memployee_icon_recycler_view = findViewById(R.id.employee_icon_recycler_view);

        memployee_icon_recycler_view.setHasFixedSize(true);

        memployee_icon_recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        docsAndFiles.setOnClickListener(this);
        automaticCheckin.setOnClickListener(this);
        schedule.setOnClickListener(this);
        toDo.setOnClickListener(this);
        card_message_board.setOnClickListener(this);
        card_chat.setOnClickListener(this);

        rv_latestActivity.setHasFixedSize(true);
        rv_latestActivityLayoutManager = new LinearLayoutManager(context);
        rv_latestActivity.setLayoutManager(rv_latestActivityLayoutManager);

        if (getIntent().getStringExtra("projectId") != null) {
            projectId = (getIntent().getStringExtra("projectId"));
        }
        if (getIntent().getStringExtra("description") != null) {
            discretion = getIntent().getStringExtra("description");
            tv_description.setText(getIntent().getStringExtra("description"));
        }
        if (getIntent().getStringExtra("Directions") != null) {
            name = getIntent().getStringExtra("Directions");
            direction_tv.setText(getIntent().getStringExtra("Directions"));
            tv_activity.setText(getIntent().getStringExtra("Directions") + " " + getResources().getString(R.string.activity));
        }
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void initToolbar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.card_automatic_check_in:
                //  Toast.makeText(context, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                intent = new Intent(this, CheckInListActivity.class);
                startActivity(intent);
                break;

            case R.id.card_docs_and_files:
                //Toast.makeText(context, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                intent = new Intent(this, DocsAndFilesActivity.class);
                startActivity(intent);
                break;

            case R.id.card_schedule:
                //  Toast.makeText(context, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();

                intent = new Intent(this, ScheduleActivity.class);
                startActivity(intent);
                break;

            case R.id.card_todo:
                intent = new Intent(this, ToDoActivity.class);
                startActivity(intent);
                break;
            case R.id.card_message_board:
                intent = new Intent(this, Activity_MessageBoard.class);
                startActivity(intent);
                break;


            case R.id.card_chat:
                intent = new Intent(this, ChatActivity.class);
                intent.putExtra("description", discretion);
                intent.putExtra("projectId", projectId);
                intent.putExtra("name", name);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.company_expand_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_trashed:
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("companyId", mcompanyId);
                paramsReq.put("status", "trash");
                paramsReq.put("type", maddremovetype);
                paramsReq.put("projectTeamId", mcurrentProjectTeamId);
                paramsReq.put("userId", mCurrentuserId);
                mApiRequest.postRequest(BASE_URL + TRASH_CPT, TRASH_CPT, paramsReq, Request.Method.POST);
                return true;

            case R.id.search:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_search, null);
                dialogBuilder.setView(dialogView);
                ImageView tv_back;
                EditText et_searchDialog;
                Spinner sp_everything;
                RecyclerView rv_searchDialog;
                RecyclerView.LayoutManager rv_searchDialogLayoutManager;
                SearchDialogAdapter searchDialogAdapter;
                tv_back = dialogView.findViewById(R.id.tv_back);
                et_searchDialog = dialogView.findViewById(R.id.et_searchDialog);
                sp_everything = dialogView.findViewById(R.id.sp_everything);
                sp_byAnyone = dialogView.findViewById(R.id.sp_byAnyone);
                sp_everywhere = dialogView.findViewById(R.id.sp_everywhere);
                rv_searchDialog = dialogView.findViewById(R.id.rv_searchDialog);
                if (ShardPreferences.get(context, share_language).equals("2")) {
                    tv_back.setImageResource(R.drawable.ic_arrow_forward);
                }
                rv_searchDialog.setHasFixedSize(true);
                rv_searchDialogLayoutManager = new LinearLayoutManager(context);
                rv_searchDialog.setLayoutManager(rv_searchDialogLayoutManager);

                final AlertDialog alertDialog = dialogBuilder.create();
                WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.TOP;
                alertDialog.show();
                sp_byAnyone.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isByAnyone = true;
                        return false;
                    }
                });
                sp_everywhere.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isEveryWhere = true;
                        return false;
                    }
                });

                // call api of set data
                callSearch();

                tv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                et_searchDialog.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                return true;

            case R.id.im_setting:
                Intent intentSetting = new Intent(context, SettingActivity.class);
                startActivity(intentSetting);
                return true;
            case R.id.messageFull:
                callAllEmp();
                addRemovePeopleDialog();
                return true;
            case R.id.menu_addRemove:
                Intent intent = new Intent(context, AddRemovePeopleActivity.class);
                intent.putExtra("Directions", discretion);
                intent.putExtra("ProjectTeamId", projectId);
                startActivityForResult(intent, 11);
                return true;
            case R.id.menu_editDetails:
                Intent intentEdit = new Intent(context, CreateTeamActivity.class);
                intentEdit.putExtra("name", name);
                intentEdit.putExtra("description", discretion);
                intentEdit.putExtra("projectTeamId", projectId);
                intentEdit.putExtra("type", "1");
                startActivityForResult(intentEdit, 100);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void callSearch() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> resultCall = apiInterface.callSearch(map);
        resultCall.enqueue(new Callback<SearchResponse>() {

            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.body().getStatus()) {
                    userListingSpinner = (ArrayList<UserListing>) response.body().getUserListing();
                    userList();
                    cardListingSpinner = (ArrayList<CardListing>) response.body().getCardListing();
                    cardList();
                } else {
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
            }
        });

    }

    public void cardList() {
        String[] mStringArray = new String[cardListingSpinner.size()];
        String check_company_id = ShardPreferences.get(context, ShardPreferences.key_cardListingSpinner_id);
        int setposition = 0;

        for (int i = 0; i < cardListingSpinner.size(); i++) {
            mStringArray[i] = cardListingSpinner.get(i).getProjectTeamName();
            if (check_company_id.equals(cardListingSpinner.get(i).getProjectTeamId())) {
                setposition = i;
                ShardPreferences.save(context, ShardPreferences.key_cardListingSpinner_id, cardListingSpinner.get(i).getProjectTeamId());
            }
        }
        ArrayAdapter aa = new ArrayAdapter(context, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_everywhere.setAdapter(aa);
        sp_everywhere.setSelection(setposition, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (data != null) {
                projectId = data.getStringExtra("projectId");
                discretion = data.getStringExtra("description");
                tv_description.setText(data.getStringExtra("description"));
                direction_tv.setText(data.getStringExtra("name"));
                tv_activity.setText(data.getStringExtra("name"));
            }
        }
        if (requestCode == 11) {
            fetchEmployee();
        }
    }

    public void userList() {

        String[] mStringArray = new String[userListingSpinner.size()];
        String check_company_id = ShardPreferences.get(context, ShardPreferences.key_userSpinner_id);
        int setposition = 0;

        for (int i = 0; i < userListingSpinner.size(); i++) {
            mStringArray[i] = userListingSpinner.get(i).getEmployeeName();
            if (check_company_id.equals(userListingSpinner.get(i).getUserId())) {
                setposition = i;
                ShardPreferences.save(context, ShardPreferences.key_userSpinner_id, userListingSpinner.get(i).getUserId());
            }
        }

        ArrayAdapter aa = new ArrayAdapter(context, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_byAnyone.setAdapter(aa);
        sp_byAnyone.setSelection(setposition, false);
    }

    private void callAllEmp() {
        Map<String, String> map = new HashMap<>();
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));
        map.put("user_id", ShardPreferences.get(context, share_UserId_key));
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllEmployeeList> resultCall = apiInterface.callAllEmployeeList(map);
        resultCall.enqueue(new Callback<AllEmployeeList>() {
            @Override
            public void onResponse(Call<AllEmployeeList> call, Response<AllEmployeeList> response) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("true")) {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                    if (response.body().getEmployeeList().size() > 0) {
                        empList(response.body().getEmployeeList());
                    } else {
                        tv_noMember.setVisibility(View.VISIBLE);
                    }
                } else {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AllEmployeeList> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
            }
        });
    }

    private void empList(List<EmployeeList> list) {
        if (list != null && list.size() > 0) {
            allEmployeAdapter = new AllEmployeAdapter(context, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        } else {
            list = new ArrayList<>();
            allEmployeAdapter = new AllEmployeAdapter(context, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        }
    }

    RelativeLayout rl_pingChatHome;
    ShimmerFrameLayout mShimmerDialog;
    UserListChatAdapter mAddRemoveUserAdapter;
    RecyclerView rv_chaOptionHome;
    AutoCompleteTextView et_search;
    TextView tv_noMember;
    ImageView btnSend;

    public void addRemovePeopleDialog() {
        Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);
        LayoutInflater inflater = ((android.app.Activity) mContext).getLayoutInflater();
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_user_chat, null);
        dialogBuilder.setContentView(dialogView);
        tv_noMember = dialogView.findViewById(R.id.tv_noMember);
        pb_progress = dialogView.findViewById(R.id.pb_progress);
        et_search = dialogView.findViewById(R.id.et_search);
        rv_chaOptionHome = dialogView.findViewById(R.id.rv_chaOptionHome);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_chaOptionHome.setLayoutManager(mLayoutManager);
        rl_pingChatHome = dialogView.findViewById(R.id.rl_pingChatHome);
        btnSend = dialogView.findViewById(R.id.btnSend);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    allEmployeAdapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_search.getText().toString().equals("")) {
                    try {
                        allEmployeAdapter.getFilter().filter(et_search.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        dialogBuilder.show();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_EMPLOYEE_ANDROID)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        memployee_icon_recycler_view_adapter = new MinEmployeeIconAdapter(mEmployeeDetailList, this, 0, 1);
                        memployee_icon_recycler_view.setAdapter(memployee_icon_recycler_view_adapter);

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(TRASH_CPT)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    Toast.makeText(this, getResources().getString(R.string.removed_Successfully), Toast.LENGTH_SHORT).show();
                    Intent homeIntent = new Intent(CompanyHQExpandActivity.this, HomeActivity.class);
                    startActivity(homeIntent);
                    finish();
                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void ActivityList() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(context, ShardPreferences.share_current_UserId_key));
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));
        map.put("project_team_id", projectId);
        map.put("page", "0");

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CompanyActivityListResponse> resultCall = apiInterface.callCompanyActivityList(map);
        resultCall.enqueue(new Callback<CompanyActivityListResponse>() {

            @Override
            public void onResponse(Call<CompanyActivityListResponse> call, Response<CompanyActivityListResponse> response) {

                if (response.body().getStatus()) {
                    sethomeList(response.body().getActivities());
                } else {
                }
            }

            @Override
            public void onFailure(Call<CompanyActivityListResponse> call, Throwable t) {
            }
        });
    }

    private void sethomeList(List<Activity> list) {
        if (list != null && list.size() > 0) {
            latest_list_adapter = new ActivityLatestListAdapter(context, (ArrayList<Activity>) list);
            rv_latestActivity.setAdapter(latest_list_adapter);
        } else {
            list = new ArrayList<>();
            latest_list_adapter = new ActivityLatestListAdapter(context, (ArrayList<Activity>) list);
            rv_latestActivity.setAdapter(latest_list_adapter);
        }
    }
}
