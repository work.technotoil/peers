package com.peerbuckets.peerbucket.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hbisoft.pickit.PickiT;
import com.hbisoft.pickit.PickiTCallbacks;
import com.peerbuckets.peerbucket.Adapter.PingAdapter;
import com.peerbuckets.peerbucket.POJO.Chat;
import com.peerbuckets.peerbucket.POJO.OneToOneChatResponse.OneToOneNotification;
import com.peerbuckets.peerbucket.POJO.OneToOneChatResponse.ReceiverName;
import com.peerbuckets.peerbucket.POJO.OneToOneChatResponse.SenderName;
import com.peerbuckets.peerbucket.POJO.SettingListResponce.SettingListResponse;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Common;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.klinker.giphy.Giphy;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHANNEL_ID;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class PingActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, PickiTCallbacks {

    private static final String TAG = "PingActivity";
    Context context = this;
    TextView tv_document, tv_postGif;
    PopupWindow changeSortPopUp, popup;
    String otherUserId = "";
    ApiRequest apiRequest;
    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton floatingBtnAddMessage, floatingAddImage;
    EditText etMessage;
    DatabaseReference mDatabaseReference;
    FirebaseDatabase firebaseDatabase;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    PingAdapter pingAdapter;
    Boolean isChannelIdAvailable = false;
    String channelId = "", lastKey = "", firstKey = "";
    String lastTimestamp;
    Query query;
    TextView tv_hint_shlohan;
    List<Chat> chatList = new ArrayList<>();

    SenderName senderNameList = new SenderName();
    ReceiverName receiverNameList = new ReceiverName();

    Boolean loading = true;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private StorageReference mStorageReference;
    FirebaseStorage firebaseStorage;
    private Toolbar toolbar;
    ProgressBar progressBarMedia;
    ProgressBar progress_bar;
    PickiT pickiT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping);

        if (getIntent() != null) {
            otherUserId = getIntent().getStringExtra("user_id");
            if (!Utils.isStringValid(otherUserId)) {
                Toast.makeText(context, getResources().getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        inIt();
        getChannelId();
        applyPagination();
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.ping));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();

        apiRequest = new ApiRequest(context, this);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_ping);
        progress_bar = findViewById(R.id.progress_bar);
        floatingBtnAddMessage = findViewById(R.id.floating_chat);
        floatingBtnAddMessage.setOnClickListener(this);
        floatingAddImage = findViewById(R.id.floating_add_img_ping);
        floatingAddImage.setOnClickListener(this);
        etMessage = findViewById(R.id.et_message_chat);
        layoutManager = new LinearLayoutManager(context);
        ((LinearLayoutManager) layoutManager).setOrientation(RecyclerView.VERTICAL);
        recyclerView = findViewById(R.id.recycler_ping);
        recyclerView.setLayoutManager(layoutManager);

        firebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = firebaseDatabase.getReference();
        firebaseStorage = FirebaseStorage.getInstance();
        swipeRefreshLayout.setEnabled(false);
        progressBarMedia = findViewById(R.id.progress_bar_media_ping);
        tv_hint_shlohan = findViewById(R.id.tv_hint_shlohan);
        pickiT = new PickiT(this, this);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getChannelId() {
        swipeRefreshLayout.setRefreshing(true);
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("sender_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("receiver_id", otherUserId);
        apiRequest.postRequestBackground(BASE_URL + GET_CHANNEL_ID, GET_CHANNEL_ID, paramsReq, Request.Method.POST);
    }
    private void createNewMessage(String type, String message, String imageFileName) {
        Chat newMessage = new Chat(message, type, otherUserId, "", ShardPreferences.get(context, ShardPreferences.share_UserId_key), imageFileName);
        System.out.print("Message" + message);

        mDatabaseReference.push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                recyclerView.scrollToPosition(pingAdapter.getItemCount() - 1);
            }
        });
        sendNotification();

        if (Common.TYPE_TEXT.equals(type)) {
            etMessage.setText("");
        }
    }

    private void sendNotification() {
        Map<String, String> map = new HashMap<>();
        map.put("my_user_id", ShardPreferences.get(context, ShardPreferences.share_current_UserId_key));
        map.put("other_user_id", otherUserId);
        map.put("message", etMessage.getText().toString());
        map.put("message_type", "1");
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));
        map.put("channel_id", channelId);
        map.put("other_user_status", "0");

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OneToOneNotification> resultCall = apiInterface.callPingNotification(map);
        resultCall.enqueue(new Callback<OneToOneNotification>() {

            @Override
            public void onResponse(Call<OneToOneNotification> call, Response<OneToOneNotification> response) {

                if (response.body().getStatus()) {

                } else {
                }
            }

            @Override
            public void onFailure(Call<OneToOneNotification> call, Throwable t) {
            }
        });
    }

    private void getPrimaryMessage() {
        swipeRefreshLayout.setRefreshing(true);
        query = mDatabaseReference.orderByChild("timestamp");
        applyPagination();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                int size = (int) dataSnapshot.getChildrenCount();
                Log.d(TAG, "onChildAdded" + dataSnapshot.getKey() + s);
                Chat chat = dataSnapshot.getValue(Chat.class);
                chat.setPrimaryKey(dataSnapshot.getKey());
                chatList.add(chat);
                lastTimestamp = String.valueOf(chatList.get(0).getTimestamp());

                pingAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(pingAdapter.getItemCount() - 1);
                swipeRefreshLayout.setRefreshing(false);
                if (chatList.size() == 0) {
                    tv_hint_shlohan.setVisibility(View.VISIBLE);
                } else {
                    tv_hint_shlohan.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                int size = (int) dataSnapshot.getChildrenCount();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                System.out.print(TAG + "onChildRemoved" + dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                swipeRefreshLayout.setRefreshing(false);
                Log.d(TAG, databaseError.toString());
            }
        });
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_CHANNEL_ID)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONObject senderObj = jsonObject.getJSONObject("sender_name");
                    JSONObject receiverObj = jsonObject.getJSONObject("receiver_name");
                    channelId = jsonObject.getString("channel_id");
                    if (Utils.isStringValid(channelId)) {
                        isChannelIdAvailable = true;
                        senderNameList.setEmployeeName(senderObj.getString("employee_name"));
                        senderNameList.setUserImage(senderObj.getString("user_image"));
                        senderNameList.setBgColor(senderObj.getString("bgColor"));
                        senderNameList.setFontColor(senderObj.getString("fontColor"));

                        receiverNameList.setEmployeeName(receiverObj.getString("employee_name"));
                        receiverNameList.setUserImage(receiverObj.getString("user_image"));
                        receiverNameList.setBgColor(receiverObj.getString("bgColor"));
                        receiverNameList.setFontColor(receiverObj.getString("fontColor"));

                        getSupportActionBar().setTitle(Utils.unescapeJava(receiverObj.getString("employee_name")));

                        pingAdapter = new PingAdapter(context, chatList, senderNameList, receiverNameList);
                        recyclerView.setAdapter(pingAdapter);

                        mDatabaseReference = mDatabaseReference.child("message").child(channelId);
                        mStorageReference = firebaseStorage.getReference("message").child(channelId);
                        if (chatList.size() == 0) {
                            tv_hint_shlohan.setVisibility(View.VISIBLE);
                        } else {
                            tv_hint_shlohan.setVisibility(View.GONE);
                        }


                        getFirstKey();
                        getPrimaryMessage();
                    }

                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floating_chat:
                if (!etMessage.getText().toString().equals("")) {
                    if (isChannelIdAvailable) {

                        createNewMessage(Common.TYPE_TEXT, etMessage.getText().toString(), "");
                    }

                } else {
                    etMessage.setText("");
                }
                break;
            case R.id.floating_add_img_ping:
                if (!Utils.hasPermissions(this, PERMISSIONS)) {
                } else {
                    LinearLayout ll_mediaOption = findViewById(R.id.ll_mediaOption);
                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = layoutInflater.inflate(R.layout.dialog_media_option, ll_mediaOption, true);

                    tv_document = layout.findViewById(R.id.tv_document);
                    tv_postGif = layout.findViewById(R.id.tv_postGif);
                    // Creating the PopupWindow
                    popup = new PopupWindow(context);
                    popup.setContentView(layout);
                    popup.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
                    popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                    popup.setFocusable(true);
                    popup.setTouchable(true);
                    popup.setBackgroundDrawable(new BitmapDrawable());
                    hideKeyboard(PingActivity.this);
                    popup.showAtLocation(layout, Gravity.BOTTOM | Gravity.LEFT | Gravity.RIGHT, floatingAddImage.getScrollX(), floatingAddImage.getScrollY());
                    tv_document.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (Build.VERSION.SDK_INT < 19) {
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                intent.setType("*/*");
                                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                                startActivityForResult(intent, 7);
                                popup.dismiss();
                            } else {
                                Intent pdfIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                pdfIntent.setType("*/*");
                                pdfIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                                pdfIntent.addCategory(Intent.CATEGORY_OPENABLE);
                                startActivityForResult(pdfIntent, 7);
                                popup.dismiss();
                            }
                        }
                    });
                    tv_postGif.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new Giphy.Builder((Activity) context, "dc6zaTOxFJmzC")    // Giphy's BETA key
                                    .maxFileSize(5 * 1024 * 1024)               // 5 mb
                                    .start();
                            popup.dismiss();
                        }
                    });

                }
                break;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void applyPagination() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(-1)) {
                    Log.d(TAG, "Top Reached ...");
                }
            }
        });
    }

    private void getFirstKey() {
        final Query firstQuery = mDatabaseReference.orderByKey().limitToFirst(1);
        firstQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    firstKey = data.getKey(); // then it has the value "4:"
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.toString());
            }
        });
    }

    private void getPaginatedData() {

        lastKey = chatList.get(0).getPrimaryKey();
        lastTimestamp = chatList.get(0).getTimestamp().toString();
        Log.d(TAG, "Last Timestamp" + lastTimestamp);
        Log.d(TAG, "Last Key" + lastKey);
        if (lastKey.equals(firstKey)) {
            loading = false;
        }
        if (loading) {
            Query query1;
            query1 = mDatabaseReference.orderByChild("timestamp").endAt(lastTimestamp, lastKey).limitToLast(15);

            query1.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    List<Chat> newItem = new ArrayList<>();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        // TODO: handle the post
                        Chat chat = postSnapshot.getValue(Chat.class);
                        chat.setPrimaryKey(postSnapshot.getKey());
                        Log.d(TAG, chat.getMessage());
                        newItem.add(chat);

                        recyclerView.scrollToPosition(pingAdapter.getItemCount() - 1);

                    }

                    Collections.reverse(newItem);
                    newItem.remove(0);
                    for (Chat list : newItem) {
                        chatList.add(0, list);
                        pingAdapter.notifyDataSetChanged();
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                String fileName = Utils.getFileName(context, resultUri);
                progressBarMedia.setVisibility(View.VISIBLE);
                floatingAddImage.setVisibility(View.GONE);
                uploadFile(String.valueOf(resultUri), Common.TYPE_IMAGE, fileName);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();

            }

        } else if (requestCode == 7) {
            if (resultCode == RESULT_OK) {
                Uri link = data.getData();
                String PathHolder = Utils.getFileName(context, link);
                ContentResolver cr = this.getContentResolver();
                String type = getMimeType(PathHolder);


                if (type != null) {
                    if (type.contains("image")) {
                        progress_bar.setVisibility(View.VISIBLE);
                        uploadFileGiphy(String.valueOf(link), Common.TYPE_IMAGE, PathHolder);
                    } else if (type.contains("video")) {
                        progress_bar.setVisibility(View.VISIBLE);
                        uploadFileGiphy(String.valueOf(link), Common.TYPE_VIDEO, PathHolder);
                    } else {
                        progress_bar.setVisibility(View.VISIBLE);
                        pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);
                    }
                } else {
                    progress_bar.setVisibility(View.VISIBLE);
                    pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);
                }
            }
        } else if (requestCode == Giphy.REQUEST_GIPHY) {
            if (resultCode == Activity.RESULT_OK) {
                Uri gif = data.getData();
                String fileName = Utils.getFileName(context, gif);
                progress_bar.setVisibility(View.VISIBLE);
                uploadFileGiphy(String.valueOf(gif), Common.TYPE_GIPHY, fileName);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    private void uploadFileGiphy(String media, String mediaType, String fileName) {
        Random r = new Random();
        int randomId = r.nextInt((100 - 10) + 1) + 10;
        if (!Utils.isStringValid(fileName)) {
            fileName = String.valueOf(randomId);
        }
        final StorageReference storageReference = mStorageReference.child(String.valueOf(fileName));

        final UploadTask uploadTask = storageReference.putFile(Uri.parse(media));
        final String finalFileName = fileName;
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    throw task.getException();

                }
                // Continue with the task to get the download URL
                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    switch (mediaType) {
                        case "2":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_IMAGE, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "4":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_GIPHY, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "3":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_VIDEO, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "5":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_FILE, String.valueOf(downloadUri), finalFileName);
                            break;
                    }
                } else {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    Toast.makeText(context, getResources().getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Image Upload Failed");
                progressBarMedia.setVisibility(View.GONE);
                floatingAddImage.setVisibility(View.VISIBLE);
            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Image Upload in progress ");

                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                int i = (int) progress;
                Log.d(TAG, String.valueOf(i));
            }
        });

    }

    private void uploadFile(String media, String mediaType, String fileName) {
        Random r = new Random();
        int randomId = r.nextInt((100 - 10) + 1) + 10;
        if (!Utils.isStringValid(fileName)) {
            fileName = String.valueOf(randomId);
        }
        final StorageReference storageReference = mStorageReference.child(String.valueOf(fileName));

        final UploadTask uploadTask = storageReference.putFile(Uri.fromFile(new File(media)));
        final String finalFileName = fileName;
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    throw task.getException();
                }
                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    switch (mediaType) {
                        case "2":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_IMAGE, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "4":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_GIPHY, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "3":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_VIDEO, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "5":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_FILE, String.valueOf(downloadUri), finalFileName);
                            break;
                    }

                } else {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Image Upload Failed");
                progressBarMedia.setVisibility(View.GONE);
                floatingAddImage.setVisibility(View.VISIBLE);
            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Image Upload in progress ");
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                int i = (int) progress;
                Log.d(TAG, String.valueOf(i));
            }
        });
    }

    @Override
    public void PickiTonStartListener() {
    }

    @Override
    public void PickiTonProgressUpdate(int progress) {
    }

    @Override
    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason) {
        String filename = path.substring(path.lastIndexOf("/") + 1);
        uploadFile(path, Common.TYPE_FILE, filename);
    }
}
