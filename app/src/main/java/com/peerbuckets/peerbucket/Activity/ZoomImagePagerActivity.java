package com.peerbuckets.peerbucket.Activity;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.peerbuckets.peerbucket.Adapter.ZoomImagePagerAdapter;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.handler.PagerHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class ZoomImagePagerActivity extends AppCompatActivity
        implements View.OnClickListener {

    Context context = ZoomImagePagerActivity.this;
    ZoomImagePagerAdapter imagePagerAdapter;
    ViewPager viewPager;
    Integer currentPosition;
    List<String> imageList = new ArrayList<>();
    String image = "", type = "", imagePath = "";
    public static final String TYPE_SINGLE = "SINGLE";
    ImageView imgBack, imgDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image_pager);

        init();

    }

    private void init() {
        imgBack = findViewById(R.id.img_back_show_image);
        imgBack.setOnClickListener(this);

        if (ShardPreferences.get(context,share_language).equals("2")){
            imgBack.setImageResource(R.drawable.ic_arrow_forward);
        }

        imgDownload = findViewById(R.id.img_download_show_image);
        imgDownload.setOnClickListener(this);
        viewPager = findViewById(R.id.viewpager_show_image);
        if (getIntent() != null) {
            type = getIntent().getStringExtra("type");
            image = getIntent().getStringExtra("image");
            imagePath = getIntent().getStringExtra("imagePath");
            currentPosition = getIntent().getIntExtra("currentPosition", 0);
            if (type != null && type.equalsIgnoreCase("SINGLE")) {
                imageList.add(image);
            } else {
                imageList = getIntent().getStringArrayListExtra("list");
            }
            setupViewPager();
        }
    }


    private void setupViewPager() {
        //setup view pager for background images
        imagePagerAdapter = new ZoomImagePagerAdapter(context, imageList);
        viewPager.setOnPageChangeListener(new PagerHandler(viewPager));
        viewPager.setAdapter(imagePagerAdapter);
        if (currentPosition != null && !currentPosition.equals("")) {
            viewPager.setCurrentItem(currentPosition);
        }
        imagePagerAdapter.notifyDataSetChanged();

    }


    @Override
    public void onClick(final View view) {

        switch (view.getId()) {
            case R.id.img_back_show_image:
                finish();
                break;
            case R.id.img_download_show_image:
                downloadImage(imagePath, image);
                break;

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void downloadImage(String docPath, String downloadUrl) {

        if (checkFolderExist()) {
            if (checkFileExist(docPath)) {
                Log.d("FILE--", "file Exist");
                Toast.makeText(context, getResources().getString(R.string.file_exist), Toast.LENGTH_SHORT).show();
            } else {
                beginDownload(docPath, downloadUrl);
            }
        } else {

        }


    }

    private boolean checkFileExist(String path) {
        File file = new File(Environment.getExternalStorageDirectory() +
                File.separator + context.getResources().getString(R.string.app_name), path);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }

    }

    private boolean checkFolderExist() {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + context.getResources().getString(R.string.app_name));

        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            return true;
            // Do something on success
        } else {
            return false;
            // Do something else on failure
        }
    }

    private void beginDownload(String path, String url) {
        try {
            Toast.makeText(context, context.getResources().getString(R.string.downloading_started), Toast.LENGTH_SHORT).show();
            File file = new File(Environment.getExternalStorageDirectory() +
                    File.separator, context.getResources().getString(R.string.app_name));
            DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDestinationInExternalPublicDir(context.getResources().getString(R.string.app_name), path);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setTitle(getResources().getString(R.string.downloading_image));
            long queueid = dm.enqueue(request);
        } catch (IllegalArgumentException e) {
            Toast.makeText(context, context.getResources().getString(R.string.download_error), Toast.LENGTH_SHORT).show();

        }
    }
}
