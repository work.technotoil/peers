package com.peerbuckets.peerbucket.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.AddRemoveAdapter;
import com.peerbuckets.peerbucket.Adapter.AllEmployeAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.addRemoveResponse.AddEmployeeListNew;
import com.peerbuckets.peerbucket.POJO.addRemoveResponse.AddRemoveLIstNew;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.AllEmployeeList;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_PEOPLE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_ANDROID;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class AddRemovePeopleActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private ImageView closeInviteLayout, close_tcp_dialog_iv;
    private LinearLayout inviteLayout, add_layout;
    private RelativeLayout minvite_relative_layout;
    private ApiRequest mApiRequest;
    private TextView mcompany_tittle_tv, mpeople_copmay_tv, msend_invitation_tv, tv_content;
    String mcompanyId = "", muserId = "", memployeeId = "", maddremovetype = "", mcurrentProjectTeamId = "", mCurrentModule = "";
    private EditText minvite_employee_email, minvite_employee_jobTittle;
    RecyclerView mempolyeeListRecyclerView;
    private RecyclerView.Adapter mempolyeeListRecyclerViewadapter;
    private RecyclerView.LayoutManager mempolyeeListRecyclerViewManager;
    ArrayList<EmployeeDetalPOJO> mEmplyeeList;
    String inviteTypeName = "", ProjectTeamId = "";
    Context mContext;
    ProgressBar progress_bar;
    AddRemoveAdapter allEmployeAdapter;
    AllEmployeAdapter AddEmployeAdapter;
    AutoCompleteTextView minvite_employee_name;
    List<EmployeeList> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_remove_people);
        mContext = this;
        if (getIntent().getStringExtra("Directions") != null) {
            inviteTypeName = getIntent().getStringExtra("Directions");
        }
        if (getIntent().getStringExtra("ProjectTeamId") != null) {
            ProjectTeamId = getIntent().getStringExtra("ProjectTeamId");
        }
        initUI();

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        mcompanyId = ShardPreferences.get(mContext, share_Company_Id_key);
        muserId = ShardPreferences.get(mContext, share_UserId_key);
        maddremovetype = ShardPreferences.get(mContext, share_addd_remove_current_type);
        mcurrentProjectTeamId = ShardPreferences.get(mContext, share_current_project_team_id);
        mCurrentModule = ShardPreferences.get(mContext, share_current_module);

        fetchEmployee();
        callAllEmp();
    }

    private void callAllEmp() {
        Map<String, String> map = new HashMap<>();
        map.put("company_id", mcompanyId);
        map.put("user_id", muserId);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllEmployeeList> resultCall = apiInterface.callAllEmployeeList(map);
        resultCall.enqueue(new Callback<AllEmployeeList>() {
            @Override
            public void onResponse(Call<AllEmployeeList> call, Response<AllEmployeeList> response) {

                if (response.body().getStatus().equals("true")) {

                    if (response.body().getEmployeeList().size() > 0) {
                        list = response.body().getEmployeeList();
                        userList();
                    } else {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<AllEmployeeList> call, Throwable t) {

            }
        });
    }

    public void userList() {

        String[] mStringArray = new String[list.size()];
        String check_company_id = ShardPreferences.get(mContext, ShardPreferences.key_userSpinner_id);
        int setposition = 0;

        for (int i = 0; i < list.size(); i++) {
            mStringArray[i] = list.get(i).getEmployeeName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, mStringArray);
        minvite_employee_name.setThreshold(1); //will start working from first character
        minvite_employee_name.setAdapter(adapter);

        if (minvite_employee_name.getText().toString().equals("")) {
            minvite_employee_email.setText("");
            minvite_employee_jobTittle.setText("");
        }

        minvite_employee_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                if (!minvite_employee_name.getText().toString().equals("")) {
                    String User_id = list.get(position).getUserId();
                    ShardPreferences.save(mContext, ShardPreferences.key_userSpinner_id, User_id);
                    for (int i = 0; i < list.size(); i++) {
                        if (minvite_employee_name.getText().toString().equals(list.get(i).getEmployeeName())) {
                            minvite_employee_email.setText(list.get(i).getEmployeeEmail());
                            minvite_employee_jobTittle.setText(list.get(i).getEmployeeJobTitle());
                        }
                    }
                } else {
                    minvite_employee_email.setText("");
                    minvite_employee_jobTittle.setText("");
                }

            }
        });

    }


    private void fetchEmployee() {
        Map<String, String> map = new HashMap<>();
        map.put("companyId", ShardPreferences.get(mContext, ShardPreferences.key_company_Id));
        map.put("typeId", ProjectTeamId);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddRemoveLIstNew> resultCall = apiInterface.callAddRemoveList(map);
        resultCall.enqueue(new Callback<AddRemoveLIstNew>() {
            @Override
            public void onResponse(Call<AddRemoveLIstNew> call, Response<AddRemoveLIstNew> response) {

                if (response.body().getStatus().equals("true")) {
                    empList(response.body().getEmployeeList());
                } else {

                }
            }

            @Override
            public void onFailure(Call<AddRemoveLIstNew> call, Throwable t) {

            }
        });
    }

    private void empList(List<AddEmployeeListNew> list) {
        if (list != null && list.size() > 0) {
            allEmployeAdapter = new AddRemoveAdapter(mContext, (ArrayList<AddEmployeeListNew>) list, ProjectTeamId);
            mempolyeeListRecyclerView.setAdapter(allEmployeAdapter);
        } else {
            list = new ArrayList<>();
            allEmployeAdapter = new AddRemoveAdapter(mContext, (ArrayList<AddEmployeeListNew>) list, ProjectTeamId);
            mempolyeeListRecyclerView.setAdapter(allEmployeAdapter);
        }
    }

    private void initUI() {
        progress_bar = findViewById(R.id.progress_bar);
        mcompany_tittle_tv = findViewById(R.id.company_tittle_tv);
        mcompany_tittle_tv.setText(inviteTypeName);
        mpeople_copmay_tv = findViewById(R.id.people_copmay_tv);
        tv_content = findViewById(R.id.tv_content);
        msend_invitation_tv = findViewById(R.id.send_invitation_tv);
        closeInviteLayout = findViewById(R.id.close);
        close_tcp_dialog_iv = findViewById(R.id.close_tcp_dialog_iv);
        inviteLayout = findViewById(R.id.invite_layout);
        add_layout = findViewById(R.id.add_layout);
        minvite_relative_layout = findViewById(R.id.invite_relative_layout);
        minvite_employee_name = findViewById(R.id.invite_employee_name);
        minvite_employee_email = findViewById(R.id.invite_employee_email);
        minvite_employee_jobTittle = findViewById(R.id.invite_employee_jobTittle);

        mempolyeeListRecyclerView = findViewById(R.id.empolyeeListRecyclerView);

        mempolyeeListRecyclerView.setHasFixedSize(true);

        mempolyeeListRecyclerViewManager = new LinearLayoutManager(this);

        mempolyeeListRecyclerView.setLayoutManager(mempolyeeListRecyclerViewManager);

        closeInviteLayout.setOnClickListener(this);
        close_tcp_dialog_iv.setOnClickListener(this);
        inviteLayout.setOnClickListener(this);
        add_layout.setOnClickListener(this);
        msend_invitation_tv.setOnClickListener(this);
        mpeople_copmay_tv.setText(getResources().getString(R.string.Add_people_on_the) + " " + inviteTypeName);
        tv_content.setText(getResources().getString(R.string.people_you_invite_will_be_immediately_added_to_the) + " " +
                inviteTypeName + getResources().getString(R.string.and_have_full_access_to_everything_in_the) + " " + inviteTypeName);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.invite_layout:
            case R.id.close:
                if (minvite_relative_layout.getVisibility() == View.GONE) {
                    minvite_relative_layout.setVisibility(View.VISIBLE);
                } else {
                    minvite_relative_layout.setVisibility(View.GONE);
                }
                break;
            case R.id.add_layout:
                callAllAddEmp();
                addUserList();
                break;
            case R.id.close_tcp_dialog_iv:
                finish();
                break;

            case R.id.send_invitation_tv:
                if (minvite_employee_name.getText().toString().equals("")) {
                    minvite_employee_name.setFocusable(true);
                    minvite_employee_name.setError("Enter invite name");

                } else if (minvite_employee_email.getText().toString().equals("")) {
                    minvite_employee_email.setFocusable(true);
                    minvite_employee_email.setError("Enter invite email");

                } else if (!Utils.isValidEmail(minvite_employee_email.getText())) {
                    minvite_employee_email.setFocusable(true);
                    minvite_employee_email.setError("Enter valid email id");

                } /*else if (minvite_employee_jobTittle.getText().toString().equals("")) {
                    minvite_employee_jobTittle.setFocusable(true);
                    minvite_employee_jobTittle.setError("Enter invite job tittle");

                }*/ else {
                    progress_bar.setVisibility(View.VISIBLE);
                    msend_invitation_tv.setVisibility(View.GONE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("projectTeamId", mcurrentProjectTeamId);
                    paramsReq.put("employeeEmail", minvite_employee_email.getText().toString());
                    paramsReq.put("employeeName", Utils.getUnicodeString(Utils.escapeUnicodeText(minvite_employee_name.getText().toString())));
                    paramsReq.put("companyId", mcompanyId);
                    if (!minvite_employee_jobTittle.getText().toString().equals("")) {
                        paramsReq.put("employeeJobTitle", Utils.getUnicodeString(Utils.escapeUnicodeText(minvite_employee_jobTittle.getText().toString())));

                    } else {
                        paramsReq.put("employeeJobTitle", minvite_employee_jobTittle.getText().toString());

                    }
                    paramsReq.put("employeeCompanyName", ShardPreferences.get(mContext, ShardPreferences.share_current_module));
                    paramsReq.put("type", maddremovetype);
                    paramsReq.put("userId", ShardPreferences.get(this, share_UserId_key));
                    mApiRequest.postRequest(BASE_URL + ADD_PEOPLE, ADD_PEOPLE, paramsReq, Request.Method.POST);
                }
                break;
        }
    }

    RelativeLayout rl_pingChatHome;
    RecyclerView rv_chaOptionHome;
    AutoCompleteTextView et_search;
    TextView tv_noMember;
    ProgressBar pb_progress;

    private void addUserList() {

        Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_user_chat, null);
        dialogBuilder.setContentView(dialogView);
        tv_noMember = dialogView.findViewById(R.id.tv_noMember);
        pb_progress = dialogView.findViewById(R.id.pb_progress);
        et_search = dialogView.findViewById(R.id.et_search);
        rv_chaOptionHome = dialogView.findViewById(R.id.rv_chaOptionHome);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_chaOptionHome.setLayoutManager(mLayoutManager);
        rl_pingChatHome = dialogView.findViewById(R.id.rl_pingChatHome);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    AddEmployeAdapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialogBuilder.show();
    }

    private void callAllAddEmp() {
        Map<String, String> map = new HashMap<>();
        map.put("company_id", mcompanyId);
        map.put("user_id", muserId);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllEmployeeList> resultCall = apiInterface.callAllEmployeeList(map);
        resultCall.enqueue(new Callback<AllEmployeeList>() {
            @Override
            public void onResponse(Call<AllEmployeeList> call, Response<AllEmployeeList> response) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("true")) {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                    if (response.body().getEmployeeList().size() > 0) {
                        empListAdd(response.body().getEmployeeList());
                    } else {
                        tv_noMember.setVisibility(View.VISIBLE);
                    }
                } else {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AllEmployeeList> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
            }
        });
    }

    private void empListAdd(List<EmployeeList> list) {
        if (list != null && list.size() > 0) {
            AddEmployeAdapter = new AllEmployeAdapter(mContext, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(AddEmployeAdapter);
        } else {
            list = new ArrayList<>();
            AddEmployeAdapter = new AllEmployeAdapter(mContext, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(AddEmployeAdapter);
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(ADD_PEOPLE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    progress_bar.setVisibility(View.GONE);
                    msend_invitation_tv.setVisibility(View.VISIBLE);
                    finish();
                } else {
                    progress_bar.setVisibility(View.GONE);
                    msend_invitation_tv.setVisibility(View.VISIBLE);
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                progress_bar.setVisibility(View.GONE);
                msend_invitation_tv.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void RefreshAdapter(int position) {
        allEmployeAdapter.notifyItemRemoved(position);
    }
}
