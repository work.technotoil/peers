package com.peerbuckets.peerbucket.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.POJO.PicUpdateResponse;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.squareup.picasso.Picasso;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.peerbuckets.peerbucket.volley.MultipartRequest;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPLOAD_IMAGE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.clearPreference;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_profile_url;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_image_key;

public class ChangeAvatarActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    TextView changeAvatar, mupload_avatar;
    Uri picUri;
    ImageView mavater_iv, close_tcp_dialog_iv;
    Button mupload_avatar_button;
    ProgressBar progressBar;

    //Image request code
    private int PICK_IMAGE_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;
    CircleImageView avatar_iv;
    ApiRequest mApiRequest;
    //Uri to store the image uri
    private Uri filePath;

    Context context = this;
    File ImageFile;

    private static final String[] permissionsArray = {android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestStoragePermission();
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        setContentView(R.layout.activity_change_avatar);
        mavater_iv = findViewById(R.id.avatar_iv);
        progressBar = findViewById(R.id.progressBar);
        mupload_avatar_button = findViewById(R.id.upload_avatar_button);
        close_tcp_dialog_iv = findViewById(R.id.close_tcp_dialog_iv);
        changeAvatar = findViewById(R.id.change_avatar);
        mupload_avatar = findViewById(R.id.upload_avatar);
        close_tcp_dialog_iv.setOnClickListener(this);
        changeAvatar.setOnClickListener(this);
        mupload_avatar.setOnClickListener(this);
        mupload_avatar_button.setOnClickListener(this);
        loadImageFromURL(ShardPreferences.get(this, share_current_profile_url), mavater_iv);
        if(ShardPreferences.get(context, share_user_image_key)!=null && !ShardPreferences.get(context, share_user_image_key).isEmpty())
        {
            Picasso.with(context).load(ShardPreferences.get(context, share_user_image_key)).into(mavater_iv);
        }
    }

    public boolean loadImageFromURL(String fileUrl, ImageView iv) {
        if (fileUrl.isEmpty()) {

        } else {
            Picasso.with(ChangeAvatarActivity.this).load(fileUrl).into(iv);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.change_avatar:
                AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(this);

                LayoutInflater inflater1 = this.getLayoutInflater();
                View dialogView1 = inflater1.inflate(R.layout.dialog_remove_avatar, null);
                dialogBuilder1.setView(dialogView1);
                TextView close = dialogView1.findViewById(R.id.cancel);
                TextView remove = dialogView1.findViewById(R.id.remove);

                final AlertDialog alertDialog1 = dialogBuilder1.create();
                alertDialog1.show();

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog1.dismiss();
                    }
                });

                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog1.dismiss();
                    }
                });
                break;
            case R.id.upload_avatar:
                if (Utils.hasPermissions(context, permissionsArray)) {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setCropShape(CropImageView.CropShape.RECTANGLE)
                            .setAspectRatio(1, 1)
                            .start((Activity) context);

                } else {
                    Toast.makeText(context, getResources().getString(R.string.please_grant_permission_from_setting), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);

                }
                break;
            case R.id.close_tcp_dialog_iv:
                finish();
                break;

            case R.id.upload_avatar_button:
//                progressBar.setVisibility(View.VISIBLE);
//                getFileDataFromDrawable(bitmap);
//                Map<String, String> paramsReq = new HashMap<>();
//                paramsReq.put("employeeId", ShardPreferences.get(ChangeAvatarActivity.this, share_employee_Id_key));
//                final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
//                params.put("employeePic", new MultipartRequest.DataPart(ShardPreferences.get(ChangeAvatarActivity.this, share_current_UserId_key) + "ProfilePic.jpg",
//                        getFileDataFromDrawable(bitmap), "image/jpeg"));
//                mApiRequest.postRequestWithImage(BASE_URL + UPLOAD_IMAGE, UPLOAD_IMAGE, paramsReq,
//                        params, Request.Method.POST);

                againSignUp();
                Toast.makeText(context, "Image has been successfully updated", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                ImageFile = new File(Utils.getPath(context, resultUri));
                Bitmap myBitmap = BitmapFactory.decodeFile(ImageFile.getAbsolutePath());
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.PNG, 80, out);
                mavater_iv.setImageBitmap(myBitmap);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();

            }
        }
    }

    private void againSignUp() {
        progressBar.setVisibility(View.VISIBLE);
        Map<String, RequestBody> map = new HashMap<>();
        map.put("employeeId", Utils.getRequestBodyParameter(ShardPreferences.get(context, share_employee_Id_key)));

        MultipartBody.Part imageUser = null;

        if (ImageFile != null) {

            RequestBody fbody = RequestBody.create(MediaType.parse("employeePic/*"), ImageFile);
            imageUser = MultipartBody.Part.createFormData("employeePic", ImageFile.getName(), fbody);

        } else {

            Toast.makeText(context, "Upload your Profile image", Toast.LENGTH_SHORT).show();
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PicUpdateResponse> resultCall = apiInterface.CallPicUpdateResponse(map, imageUser);
        resultCall.enqueue(new Callback<PicUpdateResponse>() {

            @Override
            public void onResponse(Call<PicUpdateResponse> call, Response<PicUpdateResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body().getStatus()) {

                    if (!response.body().getUrl().equals("")) {
                        Picasso.with(context).load(response.body().getUrl()).into(mavater_iv);
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<PicUpdateResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                //Toast.makeText(context, "Error occurred check your internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, getResources().getString(R.string.Permission_granted_now_you_can_read_the_storage), Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, getResources().getString(R.string.Oops_you_just_denied_the_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        progressBar.setVisibility(View.GONE);
        if (tag_json_obj.equals(UPLOAD_IMAGE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {

                    ShardPreferences.save(ChangeAvatarActivity.this, share_current_profile_url, jsonObject.getString("url"));
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
