package com.peerbuckets.peerbucket.Activity.message;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddCommentActivity;
import com.peerbuckets.peerbucket.Activity.AddEditMessageBoardActivity;
import com.peerbuckets.peerbucket.Adapter.AddRemoveUserAdapter;
import com.peerbuckets.peerbucket.Adapter.EmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.Adapter.ToDoComSubTaskAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.ToDoSubTaskPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_REMOVE_NOTIFIED;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.EDIT_COMMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.EDIT_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_SINGLE_MESSGAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SUBSCRIBE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_image_key;

public class ExpandMessageBoardActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse, RefreshCommentInterface {

    TextView muser_email, madd_comment_tv, tv_time, tv_notified, message_count_tv, tv_people, tv_add_remove, tv_view;
    WebView message_description;
    ApiRequest mApiRequest;
    Context context = this;
    TextView tv_selectOption, tv_messageTitle;
    LinearLayout mno_comments_LL, mcomments_LL;
    ImageView muser_image, mUser_image1, imgApplause;
    String title = "", description = "", email = "", name = "", message_id, project_team_id = "", commaSeperatedID = "",
            lastName = "", firstName = "", all_users_id = "";
    LinearLayout ll_linearMaster;
    private ShimmerFrameLayout mShimmerViewContainer;
    static int REQUEST_CODE_MESSAGE_EDIT = 3;
    static int REQUEST_CODE_MESSAGE_ADD = 2;
    Button btn_add_to_do;
    private Toolbar toolbar;
    RecyclerView employeee_icon_rv, employeee_applause_rv, mcomments_rv;
    RecyclerView.LayoutManager mEmployeListanager, mApplauseManager, mcomments_rv_layout_manager;
    PopupMenu popup;
    EmployeeIconAdapter mNotifiedUserAdapter;
    MessageCommentsAdapter mcomments_rv_adapter;
    ArrayList<MessageCommentPOJO> messageCommentPOJOS;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetalPOJO = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mApplauseUserList = new ArrayList<>();
    Boolean isApplause = false, loading = true;
    Button btn_subscribe, btn_unsubscribe;
    int setApplause;
    int pastVisiblesItems, visibleItemCount, totalItemCount, page = 0;
    LinearLayoutManager layoutManagerComment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expand_message_board_layout);
        Intent intent = getIntent();
        message_id = intent.getStringExtra("message_id");
        if (getIntent().getStringExtra("project_team_id") != null) {
            project_team_id = getIntent().getStringExtra("project_team_id");
        }

        mApiRequest = new ApiRequest(this, this);
        getDetails();
        initNotifiedModule();
        initUI();
        getCommentList(page);
    }

    private void getCommentList(int cPage) {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(this, ShardPreferences.share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "message");
        if (!ShardPreferences.get(context, ShardPreferences.share_current_project_team_id).equals("")) {
            paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        } else {
            paramsReq.put("project_team_id", project_team_id);
        }
        paramsReq.put("todo_id", message_id);
        paramsReq.put("page", String.valueOf(cPage));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);
    }

    private void initUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.message_board));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        back();

        tv_selectOption = findViewById(R.id.tv_selectOption);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        mcomments_LL = findViewById(R.id.comments_LL);
        mcomments_rv = findViewById(R.id.comments_rv);
        muser_email = findViewById(R.id.user_email);
        btn_add_to_do = findViewById(R.id.btn_add_to_do);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        ll_linearMaster = findViewById(R.id.ll_linearMaster);
        mShimmerViewContainer.startShimmerAnimation();
        message_description = findViewById(R.id.message_description);
        message_description.getSettings().setJavaScriptEnabled(true);
        message_description.setLayerType(WebView.LAYER_TYPE_NONE, null);

        message_description.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

            }
        });

        tv_time = findViewById(R.id.tv_time);
        tv_notified = findViewById(R.id.tv_notified);
        message_count_tv = findViewById(R.id.message_count_tv);
        madd_comment_tv = findViewById(R.id.add_comment_tv);
        muser_image = findViewById(R.id.user_image);
        mUser_image1 = findViewById(R.id.user_image1);
        mno_comments_LL = findViewById(R.id.no_comments_LL);
        mcomments_rv_layout_manager = new LinearLayoutManager(this);
        mcomments_rv.setLayoutManager(mcomments_rv_layout_manager);
        mcomments_rv.setHasFixedSize(true);
        mno_comments_LL.setVisibility(View.GONE);
        mcomments_LL.setVisibility(View.GONE);
        madd_comment_tv.setOnClickListener(this);
        mcomments_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManagerComment.getChildCount();
                    totalItemCount = layoutManagerComment.getItemCount();
                    pastVisiblesItems = layoutManagerComment.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data
                            getCommentList(page);
                        }
                    }
                }
            }
        });

        tv_selectOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectOption(view);
            }
        });
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .bold()// height in px
                .textColor(Color.parseColor("#" + fontColor))
                .endConfig()
                .buildRound(name, Color.parseColor("#" + backColor));
        return drawable;
    }

    public void initNotifiedModule() {
        tv_add_remove = findViewById(R.id.tv_add_remove);
        tv_view = findViewById(R.id.tv_view);
        tv_people = findViewById(R.id.tv_people);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_unsubscribe = findViewById(R.id.btn_unsubscribe);
        imgApplause = findViewById(R.id.imgApplause);
        employeee_icon_rv = findViewById(R.id.employeee_icon_rv);
        mEmployeListanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_icon_rv.setLayoutManager(mEmployeListanager);
        employeee_applause_rv = findViewById(R.id.employeee_applause_rv);
        mApplauseManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_applause_rv.setLayoutManager(mApplauseManager);
        tv_add_remove.setOnClickListener(this);
        imgApplause.setOnClickListener(this);

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.VISIBLE);
                tv_view.setText(getResources().getString(R.string.you_re_subscribed));
                btn_subscribe.setVisibility(View.GONE);
                btn_unsubscribe.setVisibility(View.VISIBLE);
                actionSubscribe("1");
            }
        });

        btn_unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.GONE);
                btn_subscribe.setVisibility(View.VISIBLE);
                btn_unsubscribe.setVisibility(View.GONE);
                actionSubscribe("0");
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("id", message_id);
        intent.putExtra("isDelete", false);
        if (messageCommentPOJOS != null) {
            intent.putExtra("commentCount", messageCommentPOJOS.size() + "");
        } else {
            intent.putExtra("commentCount", "0");
        }
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    private void getDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("message_id", message_id);
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_UserId_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_SINGLE_MESSGAGE_BOARD, FETCH_SINGLE_MESSGAGE_BOARD, paramsReq, Request.Method.POST);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                intent.putExtra("id", message_id);
                intent.putExtra("message_title", tv_messageTitle.getText().toString());
                intent.putExtra("isDelete", false);
                if (messageCommentPOJOS != null) {
                    intent.putExtra("commentCount", messageCommentPOJOS.size() + "");
                } else {
                    intent.putExtra("commentCount", "0");
                }
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_comment_tv:
                Intent intent = new Intent(this, AddCommentActivity.class);
                intent.putExtra("call_type", "message");
                intent.putExtra("message_title", tv_messageTitle.getText().toString());
                intent.putExtra("message_email", email);
                intent.putExtra("name", name);
                intent.putExtra("record_id", message_id);
                startActivityForResult(intent, REQUEST_CODE_MESSAGE_ADD);
                break;

            case R.id.tv_add_remove:
                addRemovePeopleDialog();
                break;

            case R.id.imgApplause:
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause();
                break;
        }
    }

    private void SelectOption(View v) {
        popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.message_board_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit_menu:
                        Intent intent = new Intent(context, AddEditMessageBoardActivity.class);
                        intent.putExtra("message_id", message_id);
                        intent.putExtra("message_title", tv_messageTitle.getText().toString());
                        intent.putExtra("message_body", description);
                        intent.putExtra("class_type", "edit");
                        startActivityForResult(intent, REQUEST_CODE_MESSAGE_EDIT);
                        return true;
                    case R.id.menu_trash:
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("record_id", message_id);
                        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                        paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("record_type", "Message");
                        mApiRequest.postRequest(BASE_URL + DELETE_MESSAGE_BOARD, DELETE_MESSAGE_BOARD, paramsReq, Request.Method.POST);
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public void addApplause() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", message_id);
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("notification_type", "message");
        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }

        mApiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);
    }

    public void ref() {
        this.mNotifiedUserAdapter.notifyDataSetChanged();
    }

    public void refreshAdapter() {
        this.recreate();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals(FETCH_SINGLE_MESSGAGE_BOARD)) {
            Log.d("FETCH_SINGLE_MESSAGE", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {

                    isApplause = jsonObject.getBoolean("isApplause");

                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                        tv_view.setVisibility(View.VISIBLE);
                        tv_view.setText(getResources().getString(R.string.You_re_subscribed));
                        btn_subscribe.setVisibility(View.GONE);
                    } else {
                        tv_view.setVisibility(View.GONE);
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    mShimmerViewContainer.stopShimmerAnimation();
                    ll_linearMaster.setVisibility(View.VISIBLE);
                    mShimmerViewContainer.setVisibility(View.GONE);
                    JSONObject mMessageDetail = jsonObject.getJSONObject("message_details");
                    title = mMessageDetail.getString("message_title");
                    description = Utils.unescapeJava(mMessageDetail.getString("message_description"));
                    email = mMessageDetail.getString("email");
                    name = mMessageDetail.getString("employee_name");
                    String userImage = mMessageDetail.getString("user_image");
                    String MessageTitle = Utils.unescapeJava(mMessageDetail.getString("message_title"));
                    tv_messageTitle.setText(MessageTitle);
                    muser_email.setText(Utils.unescapeJava(mMessageDetail.getString("employee_name")));
                    tv_time.setText(Utils.parseDateChange(mMessageDetail.getString("postedDate")));

                    if (!mMessageDetail.getString("user_image").equals("")) {
                        Picasso.with(context).load(DEVELOPMENT_URL + mMessageDetail.getString("user_image")).into(muser_image);
                    } else {
                        muser_image.setImageDrawable(imageLatter(Utils.word(mMessageDetail.getString("employee_name")), mMessageDetail.getString("bgColor"), mMessageDetail.getString("fontColor")));
                    }

                    JSONArray mUserArray = mMessageDetail.getJSONArray("users");
                    JSONObject jsonObject1 = new JSONObject();
                    if (mUserArray != null) {
                        for (int i = 0; i < mUserArray.length(); i++) {
                            jsonObject1 = mUserArray.getJSONObject(i);

                            if (i == 0) {
                                all_users_id = jsonObject1.getString("user_id");
                            } else {
                                all_users_id = all_users_id + "," + jsonObject1.getString("user_id");
                            }

                            if (!ShardPreferences.get(context, share_user_image_key).equals("")) {
                                Picasso.with(context).load(ShardPreferences.get(context, share_user_image_key)).into(mUser_image1);
                            } else {
                                mUser_image1.setImageDrawable(imageLatter(Utils.word(jsonObject1.getString("employee_name")), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
                            }

                            //  Picasso.with(context).load(DEVELOPMENT_URL+jsonObject1.getString("user_image")).into(mUser_image1);

//                            if (!jsonObject1.getString("user_image").equals("")) {
//                            } else {
//                                mUser_image1.setImageDrawable(imageLatter(Utils.word(jsonObject1.getString("employee_name")), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
//                            }

                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));
                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);

                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                    tv_notified.setText(getResources().getString(R.string.Notified) + " " + mUserArray.length() + " " + getResources().getString(R.string.Person));
                    tv_people.setText("" + mUserArray.length() + " ");
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mApplauseUserList.add(mModel);
                        }
                        mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                        employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                    }
                    if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
                        message_description.loadData("<body dir=\"rtl\"><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + Utils.unescapeJava(mMessageDetail.getString("message_description")), "text/html; charset=utf-8", null);
                    } else {
                        message_description.loadData("<body dir=\"ltr\" ><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + Utils.unescapeJava(mMessageDetail.getString("message_description")), "text/html; charset=utf-8", null);
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.No_Data_Found), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(EDIT_MESSAGE_BOARD)) {
            Log.d("EDIT_MB", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("title", tv_messageTitle.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } catch (Exception e) {

                Log.d("EXCPTN", e.getMessage());
            }
        }

        if (tag_json_obj.equals(FETCH_COMMENT_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray mArray = jsonObject.getJSONArray("commentList");
                    if (mArray != null && mArray.length() > 0) {
                        if (page == 0 && messageCommentPOJOS != null) {
                            messageCommentPOJOS.clear();
                        }

                        messageCommentPOJOS = new Gson().fromJson(mArray.toString(), new TypeToken<List<MessageCommentPOJO>>() {
                        }.getType());

                        message_count_tv.setText(messageCommentPOJOS.size() + "");

                        if (messageCommentPOJOS != null && messageCommentPOJOS.size() > 0) {

                            mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, tv_messageTitle.getText().toString(), message_id, this, 0);
                            mcomments_rv.setAdapter(mcomments_rv_adapter);

                        }

                        page = page + 1;
                        loading = true;
                    } else {
                        message_count_tv.setText("0");
                    }
                } else {
                    message_count_tv.setText("0");
                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_subscribe.setVisibility(View.GONE);
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    tv_notified.setText(getResources().getString(R.string.Notified) + " " + jsonObject.getString(getResources().getString(R.string.userCount)) + " " + getResources().getString(R.string.Person));
                    tv_people.setText("" + jsonObject.getString(getResources().getString(R.string.userCount)) + " ");
                    JSONArray mUserArray = jsonObject.getJSONArray("update_user_list");
                    if (mUserArray != null) {
                        if (mEmployeeDetalPOJO != null) {
                            mEmployeeDetalPOJO.clear();
                        }
                        for (int i = 0; i < mUserArray.length(); i++) {
                            JSONObject jsonObject1 = mUserArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));
                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }
                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mApplauseUserList != null) {
                        mApplauseUserList.clear();
                    }
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mApplauseUserList.add(mModel);
                        }
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(EDIT_COMMENT)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    refreshAdapter();
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(DELETE_MESSAGE_BOARD)) {
            Log.d("DELETE_MB", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("id", message_id);
                    intent.putExtra("isDelete", true);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            if (mShimmerDialog != null) {
                mShimmerDialog.stopShimmerAnimation();
                mShimmerDialog.setVisibility(View.GONE);
            }
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new AddRemoveUserAdapter(all_users_id, mEmployeeDetailList, this);
                        recyclerView.setAdapter(mAddRemoveUserAdapter);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public String getAll_users_id() {
        return all_users_id;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_MESSAGE_ADD && data != null) {
                if (messageCommentPOJOS == null) {
                    messageCommentPOJOS = new ArrayList<>();
                }
                if (mcomments_rv_adapter == null) {
                    messageCommentPOJOS.add((MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, tv_messageTitle.getText().toString(), message_id, this, 0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                } else {
                    messageCommentPOJOS.add(0, (MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, tv_messageTitle.getText().toString(), message_id, this, 0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                }
            } else if (requestCode == 22) {
                finish();
                startActivity(getIntent());
            } else if (requestCode == REQUEST_CODE_MESSAGE_EDIT) {
                String MessTitle = Utils.unescapeJava(data.getStringExtra("title"));
                tv_messageTitle.setText(Html.fromHtml(MessTitle));
                Utils.setDescription(Utils.unescapeJava(data.getStringExtra("desc")), message_description, context);
            } else if (requestCode == 1) {
                ((MessageCommentsAdapter) mcomments_rv_adapter).updateData(data.getIntExtra("position", -1), data.getStringExtra("comment"));
            } else if (requestCode == 5) {

                String position = data.getStringExtra("position");
                String comment = Utils.unescapeJava(data.getStringExtra("comment"));
                messageCommentPOJOS.get(Integer.parseInt(position)).setCommentDescription(comment);
                mcomments_rv_adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void setCommentData(int commentSize) {
        message_count_tv.setText(commentSize + "");
        if (commentSize > 0) {
            mcomments_LL.setVisibility(View.GONE);
            mno_comments_LL.setVisibility(View.GONE);
        } else {
            mcomments_LL.setVisibility(View.GONE);
            mno_comments_LL.setVisibility(View.GONE);
        }
    }


    RelativeLayout relMaster;
    ShimmerFrameLayout mShimmerDialog;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    AddRemoveUserAdapter mAddRemoveUserAdapter;
    RecyclerView recyclerView;

    public void addRemovePeopleDialog() {
        final Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_remove_user, null);
        dialogBuilder.setContentView(dialogView);
        TextView selectEveryOne = dialogView.findViewById(R.id.selectEveryOne);
        TextView selectNoOne = dialogView.findViewById(R.id.selectNoOne);
        TextView tv_cancel = dialogView.findViewById(R.id.tv_cancel);
        recyclerView = dialogView.findViewById(R.id.rv_dialogAddRemove);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);


        selectEveryOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.selectAllSelections();
                    }
                }
            }
        });

        selectNoOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.clearAllSelections();
                    }
                }
            }
        });

        Button btnSave = dialogView.findViewById(R.id.btnSave);
        mShimmerDialog = dialogView.findViewById(R.id.shimmer_view_container);
        fetchEmployee();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commaSeperatedID = getAssignedUserString();
                if (commaSeperatedID.equals("")) {
                    Toast.makeText(ExpandMessageBoardActivity.this, getResources().getString(R.string.please_select_atleast_one), Toast.LENGTH_SHORT).show();
                } else {
                    all_users_id = commaSeperatedID;
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("record_id", message_id);
                    paramsReq.put("loggedIn_user_id", ShardPreferences.get(ExpandMessageBoardActivity.this, share_UserId_key));
                    paramsReq.put("company_id", ShardPreferences.get(ExpandMessageBoardActivity.this, share_Company_Id_key));
                    paramsReq.put("user_id", commaSeperatedID);
                    paramsReq.put("project_team_id", ShardPreferences.get(ExpandMessageBoardActivity.this, share_current_project_team_id));
                    paramsReq.put("notification_type", "message");
                    mApiRequest.postRequestBackground(BASE_URL + ADD_REMOVE_NOTIFIED, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
                    dialogBuilder.dismiss();
                }
            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.show();

    }

    public void refreshCompleteTask(EmployeeDetalPOJO employeeDetalPOJO) {

    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private String getAssignedUserString() {
        if (mAddRemoveUserAdapter != null) {
            return mAddRemoveUserAdapter.getAssignedUserString();
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(this, ShardPreferences.share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "message");
        if (!ShardPreferences.get(context, ShardPreferences.share_current_project_team_id).equals("")) {
            paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        } else {
            paramsReq.put("project_team_id", project_team_id);
        }
        paramsReq.put("todo_id", message_id);
        paramsReq.put("page", String.valueOf(0));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);

    }

    private void actionSubscribe(String isSubscribe) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", message_id);
        paramsReq.put("company_id", ShardPreferences.get(ExpandMessageBoardActivity.this, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(ExpandMessageBoardActivity.this, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(ExpandMessageBoardActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "message");
        paramsReq.put("isSubscribe", isSubscribe);
        mApiRequest.postRequestBackground(BASE_URL + SUBSCRIBE, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
        Intent thisIntent = getIntent();
        startActivity(thisIntent);
    }
}