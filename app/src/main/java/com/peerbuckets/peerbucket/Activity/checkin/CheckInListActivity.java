package com.peerbuckets.peerbucket.Activity.checkin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.CheckInListAdapter;
import com.peerbuckets.peerbucket.POJO.Checkin;
import com.peerbuckets.peerbucket.POJO.CheckinReply;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHECK_IN_LIST;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class CheckInListActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener {

    public static final int REQUEST_CODE_DELETE = 25;
    private static final int REQUEST_CODE_ADD = 21;
    Context context = CheckInListActivity.this;
    RecyclerView recyclerView;
    FloatingActionButton floatingAdd;
    ApiRequest apiRequest;
    List<Checkin> checkinList = new ArrayList<>();
    CheckInListAdapter checkInListAdapter;
    Toolbar toolbar;
    CardView card_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_list);
        inIt();
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.automatic_check_ins));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();
        apiRequest = new ApiRequest(context, this);
        card_info = findViewById(R.id.card_info);
        recyclerView = findViewById(R.id.recycler_check_in_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        floatingAdd = findViewById(R.id.floating_add_check_in_list);
        floatingAdd.setOnClickListener(this);
        getCheckInList();
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getCheckInList() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        apiRequest.postRequest(BASE_URL + GET_CHECK_IN_LIST, GET_CHECK_IN_LIST, paramsReq, Request.Method.POST);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_CHECK_IN_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    card_info.setVisibility(View.GONE);
                    JSONArray checkinArray = jsonObject.getJSONArray("checkins");
                    if (checkinArray.length() > 0) {
                        checkinList = new Gson().fromJson(checkinArray.toString(), new TypeToken<List<Checkin>>() {
                        }.getType());
                        if (checkinList != null && checkinList.size() > 0) {
                            checkInListAdapter = new CheckInListAdapter(context, checkinList);
                            recyclerView.setAdapter(checkInListAdapter);
                        }
                    } else {
                        card_info.setVisibility(View.VISIBLE);
                    }
                } else {
                    card_info.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floating_add_check_in_list:
                Intent intent = new Intent(context, AddCheckInActivity.class);
                intent.putExtra("type", AddCheckInActivity.TYPE_ADD);
                intent.putExtra("Added", "1");
                startActivityForResult(intent, REQUEST_CODE_ADD);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_ADD) {
                recreate();
            }
            if (requestCode == REQUEST_CODE_DELETE) {
                recreate();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        apiRequest.postRequest(BASE_URL + GET_CHECK_IN_LIST, GET_CHECK_IN_LIST, paramsReq, Request.Method.POST);

    }
}