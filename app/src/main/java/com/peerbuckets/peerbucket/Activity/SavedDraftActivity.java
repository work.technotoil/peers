package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class SavedDraftActivity extends AppCompatActivity {
    ImageView imgBack;
    LinearLayout ll_textEmpty;
    RecyclerView rv_saveDraft;
    RecyclerView.LayoutManager rv_saveDraftLayoutManager;
    Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_draft);
        rv_saveDraft = findViewById(R.id.rv_saveDraft);
        ll_textEmpty = findViewById(R.id.ll_textEmpty);
        imgBack = findViewById(R.id.imgBack);
        if (ShardPreferences.get(context,share_language).equals("2")){
            imgBack.setImageResource(R.drawable.ic_arrow_forward);
        }
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
