package com.peerbuckets.peerbucket.Activity;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.INSERT_COMPANY;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.INSERT_PROJECT_WITHOUT_DESC;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.INSERT_TEAM_WITHOUT_DESC;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.INVITE_COWORKER;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.key_backColor;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.key_fontColor;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_ISFLOW;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_name_key;

public class SetupAccountActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    LinearLayout step1Layout, step2Layout, step3Layout, step4Layout;
    TextView nextStep1, nextStep2, nextStep3, nextStep4;
    EditText mcompany_name, mjob_tittle, mcompany_admin_name, mcoworker_name, mcoworker_email;
    private ApiRequest mApiRequest;
    CheckBox mcheckbox_sales, mcheckbox_RandD, mcheckbox_marketing, mcheckbox_managers, mcheckbox_finance,
            mcheckbox_redesign_website, mcheckbox_plan_event, mcheckbox_rebrand_business, mcheckbox_launch_product;
    ProgressBar pb_progress;
    String mcompanyId = "", muserId = "", memployeeId = "", mtimezone = "";
    String isFlowKey = "";
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_account);

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        mcompany_name = findViewById(R.id.company_name);
        mjob_tittle = findViewById(R.id.job_tittle);
        pb_progress = findViewById(R.id.pb_progress);

        mcompanyId = ShardPreferences.get(SetupAccountActivity.this, share_Company_Id_key);
        muserId = ShardPreferences.get(SetupAccountActivity.this, share_UserId_key);
        memployeeId = ShardPreferences.get(SetupAccountActivity.this, share_employee_Id_key);

        mcheckbox_redesign_website = findViewById(R.id.checkbox_redesign_website);
        mcheckbox_plan_event = findViewById(R.id.checkbox_plan_event);
        mcheckbox_rebrand_business = findViewById(R.id.checkbox_rebrand_business);
        mcheckbox_launch_product = findViewById(R.id.checkbox_launch_product);
        mcompany_admin_name = findViewById(R.id.company_admin_name);
        mcoworker_name = findViewById(R.id.coworker_name);
        mcoworker_email = findViewById(R.id.coworker_email);

        mcheckbox_sales = findViewById(R.id.checkbox_sales);
        mcheckbox_RandD = findViewById(R.id.checkbox_RandD);
        mcheckbox_marketing = findViewById(R.id.checkbox_marketing);
        mcheckbox_managers = findViewById(R.id.checkbox_managers);
        mcheckbox_finance = findViewById(R.id.checkbox_finance);

        step1Layout = findViewById(R.id.step1_layout);
        step2Layout = findViewById(R.id.step2_layout);
        step3Layout = findViewById(R.id.step3_layout);
        step4Layout = findViewById(R.id.step4_layout);

        nextStep1 = findViewById(R.id.next_step1);
        nextStep2 = findViewById(R.id.next_step2);
        nextStep3 = findViewById(R.id.next_step3);
        nextStep4 = findViewById(R.id.next_step4);

        nextStep1.setOnClickListener(this);
        nextStep2.setOnClickListener(this);
        nextStep3.setOnClickListener(this);
        nextStep4.setOnClickListener(this);

        TimeZone timeZone = TimeZone.getDefault();
        mtimezone = String.valueOf(timeZone.getID());
        if (getIntent().getStringExtra("isFlowKey") != null && !getIntent().getStringExtra("isFlowKey").equals("")) {
            isFlowKey = getIntent().getStringExtra("isFlowKey");
            follow();
        }
        if (getIntent().getStringExtra("employeeId") != null) {
            memployeeId = getIntent().getStringExtra("employeeId");
        }
        if (getIntent().getStringExtra("company_id") != null) {
            ShardPreferences.save(context, ShardPreferences.share_Company_Id_key, getIntent().getStringExtra("company_id"));
        }
    }

    private void follow() {

        switch (isFlowKey) {
            case "3":
                step1Layout.setVisibility(View.GONE);
                step2Layout.setVisibility(View.VISIBLE);
                break;
            case "4":
                step1Layout.setVisibility(View.GONE);
                step3Layout.setVisibility(View.VISIBLE);
                break;
            case "5":
                step1Layout.setVisibility(View.GONE);
                step4Layout.setVisibility(View.VISIBLE);
                break;
            default:
                step1Layout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.next_step1:
                if (mcompany_admin_name.getText().toString().equals("")) {
                    mcompany_admin_name.setFocusable(true);
                    mcompany_admin_name.setError(getResources().getString(R.string.Enter_your_name));
                } else if (mcompany_name.getText().toString().equals("")) {
                    mcompany_name.setFocusable(true);
                    mcompany_name.setError(getResources().getString(R.string.Enter_company_name));
                } else if (mjob_tittle.getText().toString().equals("")) {
                    mjob_tittle.setFocusable(true);
                    mjob_tittle.setError(getResources().getString(R.string.Enter_job_title));
                } else if ((mjob_tittle.getText().toString() != null && !mjob_tittle.getText().toString().equals("")) && (mcompany_name.getText().toString() != null && !mcompany_name.getText().toString().equals("")) && (mcompany_admin_name.getText().toString() != null && !mcompany_admin_name.getText().toString().equals(""))) {
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("companyId", mcompanyId);
                    paramsReq.put("companyName", Utils.getUnicodeString(Utils.escapeUnicodeText(mcompany_name.getText().toString())));
                    paramsReq.put("companyAdminJobTitle", Utils.getUnicodeString(Utils.escapeUnicodeText(mjob_tittle.getText().toString())));
                    paramsReq.put("companyAdminName", Utils.getUnicodeString(Utils.escapeUnicodeText(mcompany_admin_name.getText().toString())));
                    paramsReq.put("userId", muserId);
                    paramsReq.put("employeeId", memployeeId);
                    paramsReq.put("timezone", mtimezone);
                    pb_progress.setVisibility(View.VISIBLE);
                    nextStep1.setVisibility(View.GONE);
                    mApiRequest.postRequest(BASE_URL + INSERT_COMPANY, INSERT_COMPANY, paramsReq, Request.Method.POST);
                }
                break;

            case R.id.next_step2:
                String send = "";
                if (mcheckbox_sales.isChecked()) {
                    if (send.equals("")) {
                        send = mcheckbox_sales.getText().toString();
                    } else {
                        send = send + "," + mcheckbox_sales.getText().toString();
                    }
                }
                if (mcheckbox_RandD.isChecked()) {
                    if (send.equals("")) {
                        send = mcheckbox_RandD.getText().toString();
                    } else {
                        send = send + "," + mcheckbox_RandD.getText().toString();
                    }
                }
                if (mcheckbox_marketing.isChecked()) {
                    if (send.equals("")) {
                        send = mcheckbox_marketing.getText().toString();
                    } else {
                        send = send + "," + mcheckbox_marketing.getText().toString();
                    }
                }
                if (mcheckbox_managers.isChecked()) {
                    if (send.equals("")) {
                        send = mcheckbox_managers.getText().toString();
                    } else {
                        send = send + "," + mcheckbox_managers.getText().toString();
                    }
                }
                if (mcheckbox_finance.isChecked()) {
                    if (send.equals("")) {
                        send = mcheckbox_finance.getText().toString();
                    } else {
                        send = send + "," + mcheckbox_finance.getText().toString();
                    }
                }
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("companyId", mcompanyId);
                paramsReq.put("userId", muserId);
                paramsReq.put("teamList", send);
                paramsReq.put("timezone", mtimezone);
                pb_progress.setVisibility(View.VISIBLE);
                nextStep2.setVisibility(View.GONE);
                mApiRequest.postRequest(BASE_URL + INSERT_TEAM_WITHOUT_DESC, INSERT_TEAM_WITHOUT_DESC, paramsReq, Request.Method.POST);
                break;

            case R.id.next_step3:
                String sendProjectNames = "";
                if (mcheckbox_redesign_website.isChecked()) {
                    if (sendProjectNames.equals("")) {
                        sendProjectNames = mcheckbox_redesign_website.getText().toString();
                    } else {
                        sendProjectNames = sendProjectNames + "," + mcheckbox_redesign_website.getText().toString();
                    }
                }
                if (mcheckbox_plan_event.isChecked()) {
                    if (sendProjectNames.equals("")) {
                        sendProjectNames = mcheckbox_plan_event.getText().toString();
                    } else {
                        sendProjectNames = sendProjectNames + "," + mcheckbox_plan_event.getText().toString();
                    }
                }
                if (mcheckbox_rebrand_business.isChecked()) {
                    if (sendProjectNames.equals("")) {
                        sendProjectNames = mcheckbox_rebrand_business.getText().toString();
                    } else {
                        sendProjectNames = sendProjectNames + "," + mcheckbox_rebrand_business.getText().toString();
                    }
                }
                if (mcheckbox_launch_product.isChecked()) {
                    if (sendProjectNames.equals("")) {
                        sendProjectNames = mcheckbox_launch_product.getText().toString();
                    } else {
                        sendProjectNames = sendProjectNames + "," + mcheckbox_launch_product.getText().toString();
                    }
                }
                mcompanyId = ShardPreferences.get(SetupAccountActivity.this, share_Company_Id_key);
                Map<String, String> paramsReqProject = new HashMap<>();
                paramsReqProject.put("companyId", mcompanyId);
                paramsReqProject.put("userId", muserId);
                paramsReqProject.put("projectList", sendProjectNames);
                paramsReqProject.put("timezone", mtimezone);
                pb_progress.setVisibility(View.VISIBLE);
                nextStep3.setVisibility(View.GONE);
                mApiRequest.postRequest(BASE_URL + INSERT_PROJECT_WITHOUT_DESC, INSERT_PROJECT_WITHOUT_DESC, paramsReqProject, Request.Method.POST);

                break;

            case R.id.next_step4:
                Map<String, String> paramsInvite = new HashMap<>();
                paramsInvite.put("companyId", mcompanyId);
                paramsInvite.put("employeeId", memployeeId);
                paramsInvite.put("employeeEmail", mcoworker_email.getText().toString());
                paramsInvite.put("employeeName", Utils.getUnicodeString(Utils.escapeUnicodeText(mcoworker_name.getText().toString())));
                paramsInvite.put("timezone", mtimezone);
                paramsInvite.put("userId", muserId);
                pb_progress.setVisibility(View.VISIBLE);
                nextStep4.setVisibility(View.GONE);
                mApiRequest.postRequest(BASE_URL + INVITE_COWORKER, INVITE_COWORKER, paramsInvite, Request.Method.POST);
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(INSERT_COMPANY)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    ShardPreferences.save(this, share_ISFLOW, jsonObject.getString("isFlow"));
                    pb_progress.setVisibility(View.GONE);
                    nextStep1.setVisibility(View.VISIBLE);
                    step1Layout.setVisibility(View.GONE);
                    step2Layout.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.Try_Again), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (tag_json_obj.equals(INSERT_TEAM_WITHOUT_DESC)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    ShardPreferences.save(this, share_ISFLOW, jsonObject.getString("isFlow"));
                    pb_progress.setVisibility(View.GONE);
                    nextStep2.setVisibility(View.VISIBLE);
                    step2Layout.setVisibility(View.GONE);
                    step3Layout.setVisibility(View.VISIBLE);
                } else {
                    pb_progress.setVisibility(View.GONE);
                    nextStep2.setVisibility(View.VISIBLE);
                    Toast.makeText(this, getResources().getString(R.string.Try_Again), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                pb_progress.setVisibility(View.GONE);
                nextStep2.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(INSERT_PROJECT_WITHOUT_DESC)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    ShardPreferences.save(this, share_ISFLOW, jsonObject.getString("isFlow"));
                    pb_progress.setVisibility(View.GONE);
                    nextStep3.setVisibility(View.VISIBLE);
                    step3Layout.setVisibility(View.GONE);
                    step4Layout.setVisibility(View.VISIBLE);
                } else {
                    pb_progress.setVisibility(View.GONE);
                    nextStep3.setVisibility(View.VISIBLE);
                    Toast.makeText(this, getResources().getString(R.string.Try_Again), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                pb_progress.setVisibility(View.GONE);
                nextStep3.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(INVITE_COWORKER)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String status = jsonObject.getString("status");
                if (status.equals("true")) {
                    ShardPreferences.save(this, share_ISFLOW, jsonObject.getString("isFlow"));
                    ShardPreferences.save(this, share_user_email_key, jsonObject.getString("user_email"));
                    ShardPreferences.save(this, share_user_name_key, jsonObject.getString("user_name"));
                    ShardPreferences.save(this, key_backColor, jsonObject.getString("bgColor"));
                    ShardPreferences.save(this, key_fontColor, jsonObject.getString("fontColor"));
                    pb_progress.setVisibility(View.GONE);
                    nextStep4.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    finishAffinity();
                } else {
                    pb_progress.setVisibility(View.GONE);
                    nextStep4.setVisibility(View.VISIBLE);
                    Toast.makeText(this, getResources().getString(R.string.Email_Already_Exist), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                pb_progress.setVisibility(View.GONE);
                nextStep4.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
