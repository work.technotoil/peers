package com.peerbuckets.peerbucket.Activity.checkin;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.CheckInDayAdapter;
import com.peerbuckets.peerbucket.Adapter.InvitedUserAdapter;
import com.peerbuckets.peerbucket.POJO.CheckinReply;
import com.peerbuckets.peerbucket.POJO.Day;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_CHECK_IN;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHECK_IN_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_INVITED_USERS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_CHECK_IN;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class AddCheckInActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    Context context = AddCheckInActivity.this;
    private RadioGroup radioGroupDay, radioGroupTime;
    private RadioButton radioBtnDay, radioBtnWeek, radioBtnOtherWeek, radioBtnMonth, radioBtnPickTime, radioBtnBeginning, radioBtnEnd;
    private RecyclerView recyclerDay, recyclerWeek, recyclerOtherWeek, recyclerMonth, recyclerUsers;
    LinearLayoutManager layoutManagerDay, layoutManagerWeek, layoutManagerOtherWeek, layoutManagerMonth;
    CheckInDayAdapter dayAdapter;
    private static final int TYPE_DAY = 1, TYPE_WEEK = 2, TYPE_OTHER_WEEK = 3, TYPE_MONTH = 4;
    private WebView webViewAssignedUsers;
    BottomSheetBehavior bottomSheetUsers;
    ImageView imgDoneBottomSheet, imgCancelBottomSheet;
    TextView tvAssignedUser, tvPickTime, tv_messageTitle;
    private InvitedUserAdapter userAdapter;
    private ApiRequest apiRequest;
    List<User> selectedUserList = new ArrayList<>(), userList = new ArrayList<>();
    String assignedUser = "", pickedTime = "", askType = "", intentType = "", checkInId = "";
    static final String TYPE_ADD = "add", TYPE_EDIT = "edit";
    EditText etQuestion;
    private Toolbar toolbar;
    Button btnAdd;
    String Addeded;
    ProgressBar pb_progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_check_in);
        if (getIntent() != null) {
            intentType = getIntent().getStringExtra("type");
            checkInId = getIntent().getStringExtra("checkInId");
            Addeded = getIntent().getStringExtra("Added");
        }
        inIt();
        checkRadioGroupDay();
        checkRadioGroupTime();
        getInvitedUsers();
        if (intentType.equals(TYPE_EDIT)) {
            getCheckInDetail();
        }
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        back();
        pb_progressBar = findViewById(R.id.pb_progressBar);
        btnAdd = findViewById(R.id.btn_add_to_do);
        btnAdd.setOnClickListener(this);

        if (checkInId != null) {
            btnAdd.setText(getResources().getString(R.string.update));
            getSupportActionBar().setTitle(getResources().getString(R.string.update_checkin));
        } else {
            btnAdd.setText(getResources().getString(R.string.add));
            getSupportActionBar().setTitle(getResources().getString(R.string.add_checkin));
        }

        apiRequest = new ApiRequest(context, this);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        radioGroupDay = findViewById(R.id.rg_day_check_in_day);
        radioBtnDay = findViewById(R.id.rb_daily_day_add_check_in);
        radioBtnWeek = findViewById(R.id.rb_week_day_add_check_in);
        radioBtnOtherWeek = findViewById(R.id.rb_other_week_day_add_check_in);
        radioBtnMonth = findViewById(R.id.rb_month_day_add_check_in);
        recyclerDay = findViewById(R.id.recycler_daily_day_add_check_in);
        recyclerWeek = findViewById(R.id.recycler_week_day_add_check_in);
        recyclerOtherWeek = findViewById(R.id.recycler_other_week_day_add_check_in);
        recyclerMonth = findViewById(R.id.recycler_month_day_add_check_in);
        layoutManagerDay = new LinearLayoutManager(context);
        layoutManagerDay.setOrientation(RecyclerView.HORIZONTAL);
        layoutManagerWeek = new LinearLayoutManager(context);
        layoutManagerWeek.setOrientation(RecyclerView.HORIZONTAL);
        layoutManagerOtherWeek = new LinearLayoutManager(context);
        layoutManagerOtherWeek.setOrientation(RecyclerView.HORIZONTAL);
        layoutManagerMonth = new LinearLayoutManager(context);
        layoutManagerMonth.setOrientation(RecyclerView.HORIZONTAL);
        recyclerDay.setLayoutManager(layoutManagerDay);
        recyclerWeek.setLayoutManager(layoutManagerWeek);
        recyclerOtherWeek.setLayoutManager(layoutManagerOtherWeek);
        recyclerMonth.setLayoutManager(layoutManagerMonth);

        webViewAssignedUsers = findViewById(R.id.webView_assigned_add_check_in);
        LinearLayout linearBottomSheet = findViewById(R.id.linear_bottom_sheet_user);
        bottomSheetUsers = BottomSheetBehavior.from(linearBottomSheet);
        imgDoneBottomSheet = findViewById(R.id.img_done_bottom_sheet_user);
        imgDoneBottomSheet.setOnClickListener(this);
        imgCancelBottomSheet = findViewById(R.id.img_close_bottom_sheet_user);
        imgCancelBottomSheet.setOnClickListener(this);
        tvAssignedUser = findViewById(R.id.tv_assignee_sub_task);
        tvAssignedUser.setOnClickListener(this);
        recyclerUsers = findViewById(R.id.recycler_bottom_sheet_user);
        recyclerUsers.setLayoutManager(new LinearLayoutManager(context));
        recyclerUsers.setHasFixedSize(true);

        tvPickTime = findViewById(R.id.tv_pick_date_add_check_in);
        tvPickTime.setOnClickListener(this);

        radioGroupTime = findViewById(R.id.rg_time_add_check_in);
        etQuestion = findViewById(R.id.et_question_add_check_in);

        radioBtnPickTime = findViewById(R.id.rb_pick_time_add_check_in);
        radioBtnBeginning = findViewById(R.id.rb_beginning_time_add_check_in);
        radioBtnEnd = findViewById(R.id.rb_end_time_add_check_in);
        tv_messageTitle.setText(ShardPreferences.get(context, ShardPreferences.share_current_module));


    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getCheckInDetail() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("checkin_id", checkInId);
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        apiRequest.postRequest(BASE_URL + GET_CHECK_IN_DETAIL, GET_CHECK_IN_DETAIL, paramsReq, Request.Method.POST);

    }

    private void checkRadioGroupDay() {
        radioGroupDay.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.rb_daily_day_add_check_in:
                        if (radioBtnDay.isPressed()) {
                            setRecyclerViewVisibility(true, TYPE_DAY);
                            setRecyclerAdapter(recyclerDay, TYPE_DAY, true);
                            askType = "daily";
                        }
                        break;
                    case R.id.rb_week_day_add_check_in:
                        if (radioBtnWeek.isPressed()) {
                            setRecyclerViewVisibility(true, TYPE_WEEK);
                            setRecyclerAdapter(recyclerWeek, TYPE_WEEK, false);
                            askType = "once_in_week";
                        }
                        break;
                    case R.id.rb_other_week_day_add_check_in:
                        if (radioBtnOtherWeek.isPressed()) {
                            setRecyclerViewVisibility(true, TYPE_OTHER_WEEK);
                            setRecyclerAdapter(recyclerOtherWeek, TYPE_OTHER_WEEK, false);
                            askType = "other_week";
                        }
                        break;
                    case R.id.rb_month_day_add_check_in:
                        if (radioBtnMonth.isPressed()) {
                            setRecyclerViewVisibility(true, TYPE_MONTH);
                            setRecyclerAdapter(recyclerMonth, TYPE_MONTH, false);
                            askType = "month";
                        }
                        break;

                }
            }
        });
    }

    private void checkRadioGroupTime() {
        radioGroupTime.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_beginning_time_add_check_in:
                        if (radioBtnBeginning.isPressed()) {
                            pickedTime = "09:00";
                            tvPickTime.setText("");
                            tvPickTime.setVisibility(View.GONE);
                        }
                        break;
                    case R.id.rb_end_time_add_check_in:
                        if (radioBtnEnd.isPressed()) {
                            pickedTime = "18:00";
                            tvPickTime.setText("");
                            tvPickTime.setVisibility(View.GONE);
                        }
                        break;
                    case R.id.rb_pick_time_add_check_in:
                        if (radioBtnPickTime.isPressed()) {
                            pickedTime = "";
                            tvPickTime.setText("");
                            tvPickTime.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });
    }

    private void setRecyclerViewVisibility(Boolean visibility, int tag) {
        if (tag == TYPE_DAY) {
            recyclerDay.setVisibility(View.VISIBLE);
            recyclerWeek.setVisibility(View.GONE);
            recyclerOtherWeek.setVisibility(View.GONE);
            recyclerMonth.setVisibility(View.GONE);
        } else if (tag == TYPE_WEEK) {
            recyclerDay.setVisibility(View.GONE);
            recyclerWeek.setVisibility(View.VISIBLE);
            recyclerOtherWeek.setVisibility(View.GONE);
            recyclerMonth.setVisibility(View.GONE);
        } else if (tag == TYPE_OTHER_WEEK) {
            recyclerDay.setVisibility(View.GONE);
            recyclerWeek.setVisibility(View.GONE);
            recyclerOtherWeek.setVisibility(View.VISIBLE);
            recyclerMonth.setVisibility(View.GONE);
        } else if (tag == TYPE_MONTH) {
            recyclerDay.setVisibility(View.GONE);
            recyclerWeek.setVisibility(View.GONE);
            recyclerOtherWeek.setVisibility(View.GONE);
            recyclerMonth.setVisibility(View.VISIBLE);
        }
    }

    private void setRecyclerAdapter(RecyclerView recycler, int type, Boolean isMultiple) {
        List<Day> daysList = new ArrayList<>();
        List<String> daysNameList;
        daysNameList = Arrays.asList(getResources().getStringArray(R.array.day_array));
        for (String dayName : daysNameList) {
            daysList.add(new Day(dayName));
        }
        dayAdapter = new CheckInDayAdapter(context, daysList, type, isMultiple);
        recycler.setAdapter(dayAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_done_bottom_sheet_user:
                assignedUser = "";
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                if (userAdapter != null) {
                    selectedUserList.clear();
                    selectedUserList = userAdapter.getSelectedUsers();
                    if (selectedUserList != null && selectedUserList.size() > 0) {
                        for (User selectedUser : selectedUserList) {
                            assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                        }
                        webViewAssignedUsers.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUser, "text/html", "utf-8", null);
                    } else {
                        webViewAssignedUsers.loadData("", "text/html", "utf-8");
                    }
                }
                break;
            case R.id.tv_assignee_sub_task:

                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                } else {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
            case R.id.img_close_bottom_sheet_user:
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                selectedUserList.clear();
                userAdapter.clearAllSelections();
                webViewAssignedUsers.loadData("", "text/html", "utf-8");
                break;
            case R.id.tv_pick_date_add_check_in:
                selectTime();
                break;
            case R.id.btn_add_to_do:
                validateDetails();
                break;
        }
    }

    private void getInvitedUsers() {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        apiRequest.postRequestBackground(BASE_URL + GET_INVITED_USERS, GET_INVITED_USERS, paramsReq, Request.Method.POST);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_INVITED_USERS)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray usersArray = jsonObject.getJSONArray("user_list");

                    if (usersArray.length() > 0) {
                        userList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                        }.getType());
                        if (userList != null && userList.size() > 0) {
                            userAdapter = new InvitedUserAdapter(userList, context);
                            recyclerUsers.setAdapter(userAdapter);
                        }
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(ADD_CHECK_IN)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    pb_progressBar.setVisibility(View.GONE);
                    btnAdd.setVisibility(View.VISIBLE);
                    CheckinReply checkinReply = new CheckinReply();
                    Intent intent = new Intent(context, CheckInDetailActivity.class);
                    String comment = Utils.unescapeJava(jsonObject.getString("answer"));
                    checkinReply.setAnswer(comment);
                    checkinReply.setCheckinId(jsonObject.getString("record_id"));
                    checkinReply.setCreatedAt(jsonObject.getString("date"));
                    checkinReply.setAnswereDate(jsonObject.getString("date"));
                    checkinReply.setBgColor(ShardPreferences.get(context, ShardPreferences.key_backColor));
                    checkinReply.setFontColor(ShardPreferences.get(context, ShardPreferences.key_fontColor));
                    checkinReply.setEmployeeName(ShardPreferences.get(context, ShardPreferences.share_user_name_key));
                    checkinReply.setUserImage(ShardPreferences.get(context, ShardPreferences.share_current_profile_url));
                    checkinReply.setUserId(ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                    intent.putExtra("reply_list", checkinReply);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    pb_progressBar.setVisibility(View.GONE);
                    btnAdd.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                pb_progressBar.setVisibility(View.GONE);
                btnAdd.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(UPDATE_CHECK_IN)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    pb_progressBar.setVisibility(View.GONE);
                    btnAdd.setVisibility(View.VISIBLE);
                    CheckinReply checkinReply = new CheckinReply();
                    Intent intent = new Intent(context, CheckInDetailActivity.class);
                    String comment = Utils.unescapeJava(jsonObject.getString("answer"));
                    checkinReply.setAnswer(comment);
                    checkinReply.setCheckinId(jsonObject.getString("record_id"));
                    checkinReply.setCreatedAt(jsonObject.getString("date"));
                    checkinReply.setAnswereDate(jsonObject.getString("date"));
                    checkinReply.setBgColor(ShardPreferences.get(context, ShardPreferences.key_backColor));
                    checkinReply.setFontColor(ShardPreferences.get(context, ShardPreferences.key_fontColor));
                    checkinReply.setEmployeeName(ShardPreferences.get(context, ShardPreferences.share_user_name_key));
                    checkinReply.setUserImage(ShardPreferences.get(context, ShardPreferences.share_current_profile_url));
                    checkinReply.setUserId(ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                    intent.putExtra("reply_list", checkinReply);
                    setResult(RESULT_OK, getIntent());
                    finish();
                } else {
                    pb_progressBar.setVisibility(View.GONE);
                    btnAdd.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                pb_progressBar.setVisibility(View.GONE);
                btnAdd.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(GET_CHECK_IN_DETAIL)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONObject checkInObject = jsonObject.getJSONObject("checkins");
                    updateUI(checkInObject);

                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (JSONException e) {
                Toast.makeText(context, e + "", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5) {
            Intent intent = getIntent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }


    private void validateDetails() {

        if (!Utils.isStringValid(etQuestion.getText().toString().trim())) {
            Toast.makeText(context, getResources().getString(R.string.please_enter_question), Toast.LENGTH_SHORT).show();
        } else if (!radioBtnDay.isChecked() && !radioBtnWeek.isChecked() && !radioBtnOtherWeek.isChecked() && !radioBtnMonth.isChecked()) {
            Toast.makeText(context, getResources().getString(R.string.please_select_ask_type), Toast.LENGTH_SHORT).show();
        } else if (!radioBtnBeginning.isChecked() && !radioBtnEnd.isChecked() && !radioBtnPickTime.isChecked()) {
            Toast.makeText(context, getResources().getString(R.string.please_select_time_of_the_day), Toast.LENGTH_SHORT).show();
        } else if (!Utils.isStringValid(getSelectedDays("all"))) {
            Toast.makeText(context, getResources().getString(R.string.please_select_day), Toast.LENGTH_SHORT).show();
        } else if (radioBtnPickTime.isChecked() && !Utils.isStringValid(pickedTime)) {
            Toast.makeText(context, getResources().getString(R.string.please_select_time), Toast.LENGTH_SHORT).show();
        } else {
            pb_progressBar.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.GONE);
            Map<String, String> paramsReq = new HashMap<String, String>();
            paramsReq.put("question", Utils.getUnicodeString(Utils.escapeUnicodeText(etQuestion.getText().toString().trim())));
            paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
            paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
            paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
            paramsReq.put("ask_type", askType);
            paramsReq.put("day", getSelectedDays("daily"));
            paramsReq.put("once_in_week_day", getSelectedDays("once_in_week"));
            paramsReq.put("other_week_day", getSelectedDays("other_week"));
            paramsReq.put("month_day", getSelectedDays("month"));
            paramsReq.put("time", radioBtnPickTime.isChecked() ? "custom" : pickedTime);
            paramsReq.put("time_select", pickedTime);
            paramsReq.put("users", getAssignedUserString());
            if (intentType.equals(TYPE_ADD)) {
                apiRequest.postRequest(BASE_URL + ADD_CHECK_IN, ADD_CHECK_IN, paramsReq, Request.Method.POST);
            } else if (intentType.equals(TYPE_EDIT)) {
                paramsReq.put("checkin_id", checkInId);
                apiRequest.postRequest(BASE_URL + UPDATE_CHECK_IN, UPDATE_CHECK_IN, paramsReq, Request.Method.POST);
            }
        }
    }

    private String getAssignedUserString() {
        List<String> usersIdList = new ArrayList<>();
        if (selectedUserList != null && selectedUserList.size() > 0) {
            for (User userId : selectedUserList) {
                usersIdList.add(userId.getUserId());
            }
            return android.text.TextUtils.join(",", usersIdList);

        } else {
            return "";
        }

    }

    private void selectTime() {
        //Open Time Picker for choose BirthTime
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                }
                tvPickTime.setText(pad(selectedHour) + ":" + pad(selectedMinute));
                pickedTime = pad(selectedHour) + ":" + pad(selectedMinute);
            }
        }, hour, minute, true);
        mTimePicker.show();

    }

    public String pad(int input) {
        // for converting time value for time picker
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }

    private String getSelectedDays(String type) {

        if (!askType.equals(type) && !type.equals("all")) {
            return "";
        } else {
            JSONArray dayArray = new JSONArray();

            if (dayAdapter != null) {
                dayArray = dayAdapter.getSelectedArray();
                if (dayArray != null && dayArray.length() > 0) {
                    return dayArray.toString();
                }
            } else {
                return "";
            }
            return "";
        }

    }

    private void updateUI(JSONObject object) {
        try {
            String assignedUser = "";
            etQuestion.setText(Utils.unescapeJava(Utils.checkStringWithHash(object.getString("question"))));

            if (ShardPreferences.get(context, ShardPreferences.share_language).equals("") || ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
                etQuestion.setGravity(Gravity.LEFT);

            } else {
            }

            if (Utils.isStringValid(object.getString("ask_type"))) {
                switch (object.getString("ask_type")) {
                    case "daily":
                        radioBtnDay.setChecked(true);
                        setRecyclerAdapter(recyclerDay, TYPE_DAY, true);
                        askType = "daily";
                        if (dayAdapter != null && Utils.isStringValid(object.getString("day"))) {
                            String selectedDays = object.getString("day");
                            dayAdapter.updateDaysList(selectedDays);
                        }
                        break;
                    case "once_in_week":
                        radioBtnWeek.setChecked(true);
                        setRecyclerAdapter(recyclerWeek, TYPE_WEEK, false);
                        askType = "once_in_week";
                        break;
                    case "other_week":
                        radioBtnOtherWeek.setChecked(true);
                        setRecyclerAdapter(recyclerOtherWeek, TYPE_OTHER_WEEK, false);
                        askType = "other_week";
                        break;
                    case "month":
                        radioBtnMonth.setChecked(true);
                        setRecyclerAdapter(recyclerMonth, TYPE_MONTH, false);
                        askType = "month";
                        break;
                }
            }

            if (Utils.isStringValid(object.getString("time"))) {
                if (object.getString("time").equals("09:00 am")) {
                    radioBtnBeginning.setChecked(true);
                    pickedTime = "09:00";
                    tvPickTime.setText("");
                    tvPickTime.setVisibility(View.GONE);
                } else if (object.getString("time").equals("06:00 pm")) {
                    radioBtnEnd.setChecked(true);
                    pickedTime = "18:00";
                    tvPickTime.setText("");
                    tvPickTime.setVisibility(View.GONE);
                } else {
                    radioBtnPickTime.setChecked(true);
                    pickedTime = object.getString("time").substring(0, 5);
                    tvPickTime.setText(pickedTime);
                    tvPickTime.setVisibility(View.VISIBLE);

                }
            }

            JSONArray userArray = object.getJSONArray("users");
            List<User> userList;
            if (userArray.length() > 0) {

                selectedUserList = new Gson().fromJson(userArray.toString(), new TypeToken<List<User>>() {
                }.getType());
                if (selectedUserList != null && selectedUserList.size() > 0) {
                    for (User selectedUser : selectedUserList) {
                        assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                    }
                    if (userAdapter != null) {
                        userAdapter.updateSelectedUser(selectedUserList);
                    }
                    webViewAssignedUsers.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUser, "text/html", "utf-8", null);
                } else {
                    webViewAssignedUsers.loadData("", "text/html", "utf-8");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}