package com.peerbuckets.peerbucket.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.peerbuckets.peerbucket.volley.MultipartRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_COMPANY_LOGO;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPLOAD_IMAGE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.key_company_logo;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_profile_url;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

public class ChangeCompnayLogoActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    Uri picUri;
    ImageView mavater_iv, close_tcp_dialog_iv;
    Button mupload_avatar_button;
    ProgressBar progressBar;
    RelativeLayout rel_change_logo;
    TextView select_image;
    Context context = this;
    //Image request code
    private int PICK_IMAGE_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;
    ApiRequest mApiRequest;
    //Uri to store the image uri
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestStoragePermission();
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        setContentView(R.layout.activity_change_logo);
        select_image = findViewById(R.id.select_image);
        rel_change_logo = findViewById(R.id.rel_change_logo);
        close_tcp_dialog_iv = findViewById(R.id.close_tcp_dialog_iv);
        mavater_iv = findViewById(R.id.avatar_iv);
        progressBar = findViewById(R.id.progressBar);
        mupload_avatar_button = findViewById(R.id.upload_avatar_button);
        mupload_avatar_button.setVisibility(View.GONE);
        rel_change_logo.setOnClickListener(this);
        close_tcp_dialog_iv.setOnClickListener(this);
        mupload_avatar_button.setOnClickListener(this);
        select_image.setOnClickListener(this);
        loadImageFromURL(ShardPreferences.get(context, ShardPreferences.key_company_logo), mavater_iv);

    }

    public boolean loadImageFromURL(String fileUrl, ImageView iv) {
        if (fileUrl != null && !fileUrl.equals("")) {
            if (!fileUrl.isEmpty()) {
                Picasso.with(ChangeCompnayLogoActivity.this).load(fileUrl).into(iv);
            } else {
                Picasso.with(ChangeCompnayLogoActivity.this).load(fileUrl).into(iv);
            }
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.close_tcp_dialog_iv:
                finish();
                break;
            case R.id.rel_change_logo:
                showFileChooser();
                break;
            case R.id.select_image:
                showFileChooser();
                break;

            case R.id.upload_avatar_button:
                progressBar.setVisibility(View.VISIBLE);
                getFileDataFromDrawable(bitmap);
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("company_id", ShardPreferences.get(ChangeCompnayLogoActivity.this, share_Company_Id_key));
                final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
                params.put("company_logo", new MultipartRequest.DataPart(ShardPreferences.get(ChangeCompnayLogoActivity.this, share_current_UserId_key) + "ProfilePic.jpg",
                        getFileDataFromDrawable(bitmap), "image/jpeg"));
                mApiRequest.postRequestWithImage(BASE_URL + UPDATE_COMPANY_LOGO, UPDATE_COMPANY_LOGO, paramsReq,
                        params, Request.Method.POST);
                break;
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null && !data.getData().equals("")) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                mupload_avatar_button.setVisibility(View.VISIBLE);
                mavater_iv.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, getResources().getString(R.string.Permission_granted_now_you_can_read_the_storage), Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, getResources().getString(R.string.Oops_you_just_denied_the_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        progressBar.setVisibility(View.GONE);
        if (tag_json_obj.equals(UPDATE_COMPANY_LOGO)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {

                    ShardPreferences.save(ChangeCompnayLogoActivity.this, key_company_logo, jsonObject.getString("url"));
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent i = getIntent();
                    i.putExtra("company_logo", jsonObject.getString("url"));
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    Toast.makeText(this, jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
