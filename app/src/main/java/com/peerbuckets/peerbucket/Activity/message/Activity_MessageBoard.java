package com.peerbuckets.peerbucket.Activity.message;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.EditProfileActivity;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageListAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageTypeAdapter;
import com.peerbuckets.peerbucket.POJO.MessageList;
import com.peerbuckets.peerbucket.POJO.MessageTypePOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_MESSAGE_BOARD_DETAILS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;


public class Activity_MessageBoard extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    TextView mmessage_board_sort_end, mmessage_board_sort_type, mmessage_board_start;
    Typeface fontTF;
    LinearLayout mmessage_sort_LL;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "";
    ApiRequest mApiRequest;
    FloatingActionButton madd_message;
    RecyclerView mmessage_type_rv, recyclerrMessageList;
    RecyclerView.Adapter mmessage_type_rv_adapter;
    MessageListAdapter mMessageListAdapter;
    ArrayList<MessageTypePOJO> messageTypePOJOArrayList;
    ArrayList<MessageList> mMessageList;
    LinearLayoutManager mmessage_type_rv_layout_manager;
    AlertDialog alertDialog;
    CardView card_info;
    LinearLayout ll_linearMaster;
    private ShimmerFrameLayout mShimmerViewContainer;
    private Toolbar toolbar;
    public static int REQ_CODE_EXPAND_MESSAGE = 2;
    int page = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private Boolean loading = true;
    ProgressBar progress;
    Context context = this;
    String direct = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_board);
        mApiRequest = new ApiRequest(this, this);
        if (getIntent().getStringExtra("direct") != null) {
            direct = getIntent().getStringExtra("direct");
        }
        if (getIntent().getStringExtra("message_id") != null) {
            if (direct.equals("direct")) {
                Intent intent = new Intent(context, ExpandMessageBoardActivity.class);
                intent.putExtra("message_id", getIntent().getStringExtra("message_id"));
                mcurrentProjectTeamId = getIntent().getStringExtra("project_team_id");
                intent.putExtra("project_team_id", getIntent().getStringExtra("project_team_id"));
                startActivity(intent);
            }
        }
        initUI();
        getPrefrences();
        loadDetails(page);
        applyPagination();
    }

    private void loadDetails(int page) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", mCurrentuserId);
        if (!ShardPreferences.get(Activity_MessageBoard.this, share_current_project_team_id).equals("")) {
            paramsReq.put("team_id", ShardPreferences.get(Activity_MessageBoard.this, share_current_project_team_id));
        } else {
            paramsReq.put("team_id", mcurrentProjectTeamId);
        }
        paramsReq.put("company_id", mcompanyId);
        paramsReq.put("type", mcurrent_type);
        paramsReq.put("page", page + "");
        mApiRequest.postRequestBackground(BASE_URL + GET_MESSAGE_BOARD_DETAILS, GET_MESSAGE_BOARD_DETAILS, paramsReq, Request.Method.POST);
    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(Activity_MessageBoard.this, share_Company_Id_key);
        mcurrent_type = ShardPreferences.get(Activity_MessageBoard.this, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(Activity_MessageBoard.this, share_current_UserId_key);
    }

    private void initUI() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        if (ShardPreferences.get(context,share_language).equals("2")){
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();

        progress = findViewById(R.id.progress);
        card_info = findViewById(R.id.card_info);
        mmessage_board_sort_end = findViewById(R.id.message_board_sort_end);
        mmessage_sort_LL = findViewById(R.id.message_sort_LL);
        mmessage_board_sort_type = findViewById(R.id.messgae_board_sort_type);
        mmessage_board_start = findViewById(R.id.message_board_start);
        madd_message = findViewById(R.id.add_message);
        recyclerrMessageList = findViewById(R.id.recyclerrMessageList);
        ll_linearMaster = findViewById(R.id.ll_linearMaster);

        fontTF = Typeface.createFromAsset(getAssets(), "font/fawsmsolid.ttf");

        mmessage_board_sort_end.setTypeface(fontTF);

        mmessage_board_start.setTypeface(fontTF);

        mmessage_board_sort_end.setText("\uF0DC");
        toolbar.setTitle(getResources().getString(R.string.message_board));
        mmessage_sort_LL.setOnClickListener(this);
        madd_message.setOnClickListener(this);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void applyPagination() {
        // Add Pagination on recycler view android
        recyclerrMessageList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mmessage_type_rv_layout_manager.getChildCount();
                    totalItemCount = mmessage_type_rv_layout_manager.getItemCount();
                    pastVisiblesItems = mmessage_type_rv_layout_manager.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data
                            loadDetails(page);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.message_sort_LL:
                showMessageTypeDialog();
                break;
            case R.id.add_message:
                Intent intent = new Intent(Activity_MessageBoard.this, AddMessageBoardActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        ll_linearMaster.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
        if (tag_json_obj.equals(GET_MESSAGE_BOARD_DETAILS)) {
            Log.d("GET_MESSAGE_BOARD", response);
            try {

                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray jsonArray_types = jsonObject.getJSONArray("types");
                    JSONArray message_list = jsonObject.getJSONArray("message_list");

                    if (message_list != null && message_list.length() > 0) {
                        if (page == 0 && mMessageList != null) {
                            mMessageList.clear();
                        }
                        messageTypePOJOArrayList = new Gson().fromJson(jsonArray_types.toString(), new TypeToken<List<MessageTypePOJO>>() {
                        }.getType());

                        mMessageList = new Gson().fromJson(message_list.toString(), new TypeToken<List<MessageList>>() {
                        }.getType());

                        if (mMessageList != null && mMessageList.size() > 0) {
                            card_info.setVisibility(View.GONE);
                            if (page == 0) {
                                setMessageTypeRecyclerView();
                            } else {
                                recyclerrMessageList.post(new Runnable() {
                                    public void run() {
                                        mMessageListAdapter.setNotifyData(mMessageList);
                                    }
                                });
                            }
                        }
                        page = page + 1;
                        loading = true;
                    } else {
                        if (page == 0) {
                            card_info.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    if (page == 0) {
                        card_info.setVisibility(View.VISIBLE);
                    }
                }

            } catch (Exception e) {
                if (page == 0) {
                    card_info.setVisibility(View.VISIBLE);
                }
                Log.d("EXCPTN", e.toString());
            }
        }
    }

    private void setMessageTypeRecyclerView() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_message_type, null);
        dialogBuilder.setView(dialogView);

        mmessage_type_rv = dialogView.findViewById(R.id.message_type_rv);
        mmessage_type_rv.setHasFixedSize(true);

        mmessage_type_rv_layout_manager = new LinearLayoutManager(this);
        mmessage_type_rv_layout_manager.setOrientation(RecyclerView.VERTICAL);
        recyclerrMessageList.setLayoutManager(mmessage_type_rv_layout_manager);

        alertDialog = dialogBuilder.create();
        mmessage_type_rv_adapter = new MessageTypeAdapter(messageTypePOJOArrayList, this, "MessageBoard");
        mmessage_type_rv.setAdapter(mmessage_type_rv_adapter);
        recyclerrMessageList.setLayoutManager(mmessage_type_rv_layout_manager);
        mMessageListAdapter = new MessageListAdapter(mMessageList, this);
        recyclerrMessageList.setAdapter(mMessageListAdapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_CODE_EXPAND_MESSAGE) {
                mMessageListAdapter.removeMessageAndUpdateComment(data.getBooleanExtra("isDelete", false), data.getStringExtra("id"), data.getStringExtra("commentCount"),data.getStringExtra("message_title"));
            }
        }
    }

    public void showMessageTypeDialog() {
        if (alertDialog != null)
            alertDialog.show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("EXCPTN", error.toString());
    }
}