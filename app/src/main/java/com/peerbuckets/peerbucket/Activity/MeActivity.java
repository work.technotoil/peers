package com.peerbuckets.peerbucket.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleActivity;
import com.peerbuckets.peerbucket.Activity.todo.ToDoActivity;
import com.peerbuckets.peerbucket.Adapter.AllEmployeAdapter;
import com.peerbuckets.peerbucket.Adapter.SearchDialogAdapter;
import com.peerbuckets.peerbucket.Adapter.UserListChatAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.SearchResponse.CardListing;
import com.peerbuckets.peerbucket.POJO.SearchResponse.SearchResponse;
import com.peerbuckets.peerbucket.POJO.SearchResponse.UserListing;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.AllEmployeeList;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Common;
import com.peerbuckets.peerbucket.Utils.UserDialog;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.localization.LocaleHelper;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.prefs.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.klinker.giphy.Giphy;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_USER_DETAIL_BY_ID;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.REMOVE_PEOPLE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_PROFILE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_image_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_name_key;

public class MeActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {
    Context context = this;
    public static final int REQUEST_CODE = 7;
    private FloatingActionButton floatingActionButton;
    private LinearLayout mtab_start_hey_LL, closeFabLayout,
            mtab_start_activity_LL, mtab_start_home_LL, versionLayout, userNameLayout, editProfileLayout, assignmentLayout,
            scheduleLayout, changeAvatarLayout, registeredDevicesLayout,
            bookmarkLayout, recentActivityLayout, notificationSettingLayout, savedDraftLayout, boostLayout, llLogout;
    LinearLayout llTodo, llEvents, llMessage, llDocs, ll_tool_bookmark, ll_language;
    private CardView fabLayout, mcv_search_expand;
    private Typeface fontTF, fontTFRegular;
    private TextView bookmark, schedule, tv_email, set_user_name;
    Context mContext;
    ApiRequest mApiRequest;
    boolean doubleBackToExitPressedOnce = false;
    boolean isByAnyone = false;
    boolean isEveryWhere = false;
    ArrayList<UserListing> userListingSpinner;
    ArrayList<CardListing> cardListingSpinner;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    Spinner sp_byAnyone, sp_everywhere;
    ImageView img_me_userImage;
    String textcolor = "", backcolor = "", name = "", userID = "", timezone = "", userImage = "";
    ProgressBar pb_progress;
    AllEmployeAdapter allEmployeAdapter;
    TextView tv_CompanyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        mContext = this;
        mApiRequest = new ApiRequest(mContext, this);
        initToolbar();
        initUI();
        userDetails();
    }

    private void userDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", ShardPreferences.get(mContext, share_Company_Id_key));
        paramsReq.put("userId", ShardPreferences.get(mContext, share_UserId_key));
        mApiRequest.postRequest(BASE_URL + GET_USER_DETAIL_BY_ID, GET_USER_DETAIL_BY_ID, paramsReq, Request.Method.POST);
    }

    private void setApplicationLanguage(String newLanguage) {
        Resources activityRes = getResources();
        Configuration activityConf = activityRes.getConfiguration();
        Locale newLocale = new Locale(newLanguage);
        activityConf.setLocale(newLocale);
        activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

        Resources applicationRes = getApplicationContext().getResources();
        Configuration applicationConf = applicationRes.getConfiguration();
        applicationConf.setLocale(newLocale);
        applicationRes.updateConfiguration(applicationConf,
                applicationRes.getDisplayMetrics());
    }

    private void initUI() {

        img_me_userImage = findViewById(R.id.img_me_userImage);

        llTodo = findViewById(R.id.llTodo);
        llEvents = findViewById(R.id.llEvents);
        llMessage = findViewById(R.id.llMessage);
        llDocs = findViewById(R.id.llDocs);
        llLogout = findViewById(R.id.llLogout);
        // ll_tool_bookmark = findViewById(R.id.ll_tool_bookmark);
        ll_language = findViewById(R.id.ll_language);

        fabLayout = findViewById(R.id.fab_layout);
        tv_CompanyName = findViewById(R.id.tv_CompanyName);
        closeFabLayout = findViewById(R.id.close_fab_layout);
        mcv_search_expand = findViewById(R.id.cv_search_expand);
        versionLayout = findViewById(R.id.version);
        userNameLayout = findViewById(R.id.user_name);
        set_user_name = findViewById(R.id.tv_username);
        bookmark = findViewById(R.id.bookmark);
        schedule = findViewById(R.id.schedule);
        editProfileLayout = findViewById(R.id.edit_profile);
        assignmentLayout = findViewById(R.id.assignment);
        scheduleLayout = findViewById(R.id.schedule_layout);
        changeAvatarLayout = findViewById(R.id.change_avatar_layout);
        registeredDevicesLayout = findViewById(R.id.registered_devices_layout);
        bookmarkLayout = findViewById(R.id.bookmark_layout);
        recentActivityLayout = findViewById(R.id.recent_activity_layout);
        notificationSettingLayout = findViewById(R.id.notification_setting_layout);
        savedDraftLayout = findViewById(R.id.saved_draft_layout);
        boostLayout = findViewById(R.id.boost_layout);

        tv_email = findViewById(R.id.tv_email);

        mcv_search_expand.setOnClickListener(this);
        mtab_start_hey_LL = findViewById(R.id.tab_start_hey_LL);
        mtab_start_home_LL = findViewById(R.id.tab_start_home_LL);
        mtab_start_activity_LL = findViewById(R.id.tab_start_activity_LL);

//        ll_tool_bookmark.setOnClickListener(this);
        ll_language.setOnClickListener(this);
        boostLayout.setOnClickListener(this);
        savedDraftLayout.setOnClickListener(this);
        notificationSettingLayout.setOnClickListener(this);
        recentActivityLayout.setOnClickListener(this);
        bookmarkLayout.setOnClickListener(this);
        registeredDevicesLayout.setOnClickListener(this);
        changeAvatarLayout.setOnClickListener(this);
        scheduleLayout.setOnClickListener(this);
        assignmentLayout.setOnClickListener(this);
        editProfileLayout.setOnClickListener(this);
        schedule.setOnClickListener(this);
        bookmark.setOnClickListener(this);
        userNameLayout.setOnClickListener(this);
        versionLayout.setOnClickListener(this);
        closeFabLayout.setOnClickListener(this);
        mtab_start_hey_LL.setOnClickListener(this);
        mtab_start_activity_LL.setOnClickListener(this);
        mtab_start_home_LL.setOnClickListener(this);
        llLogout.setOnClickListener(this);
        llTodo.setOnClickListener(this);
        llEvents.setOnClickListener(this);
        llMessage.setOnClickListener(this);
        llDocs.setOnClickListener(this);

        tv_CompanyName.setText(ShardPreferences.get(context, ShardPreferences.share_current_company_name));

    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    private void initToolbar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_search, null);
                dialogBuilder.setView(dialogView);
                ImageView tv_back;
                EditText et_searchDialog;
                Spinner sp_everything;
                RecyclerView rv_searchDialog;
                RecyclerView.LayoutManager rv_searchDialogLayoutManager;
                SearchDialogAdapter searchDialogAdapter;
                tv_back = dialogView.findViewById(R.id.tv_back);
                et_searchDialog = dialogView.findViewById(R.id.et_searchDialog);
                sp_everything = dialogView.findViewById(R.id.sp_everything);
                sp_byAnyone = dialogView.findViewById(R.id.sp_byAnyone);
                sp_everywhere = dialogView.findViewById(R.id.sp_everywhere);
                rv_searchDialog = dialogView.findViewById(R.id.rv_searchDialog);
                if (ShardPreferences.get(context, share_language).equals("2")) {
                    tv_back.setImageResource(R.drawable.ic_arrow_forward);
                }
                rv_searchDialog.setHasFixedSize(true);
                rv_searchDialogLayoutManager = new LinearLayoutManager(mContext);
                rv_searchDialog.setLayoutManager(rv_searchDialogLayoutManager);

                final AlertDialog alertDialog = dialogBuilder.create();
                WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.TOP;
                alertDialog.show();
                sp_byAnyone.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isByAnyone = true;
                        return false;
                    }
                });
                sp_everywhere.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isEveryWhere = true;
                        return false;
                    }
                });

                // call api of set data
                callSearch();

                tv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                et_searchDialog.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                return true;

            /*case R.id.admin_land:
                Intent intent = new Intent(this, AdminLandActivity.class);
                startActivity(intent);
                return true;
*/
            case R.id.im_setting:
                Intent intentSetting = new Intent(mContext, SettingActivity.class);
                startActivity(intentSetting);
                return true;

            case R.id.im_logout:
                AlertDialog logoutDialog = new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.logout))
                        .setMessage(getResources().getString(R.string.logout_info))
                        .setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent10 = new Intent(context, DeciderActivity.class);
                                startActivity(intent10);
                                ShardPreferences.clearPreference(context);
                                finishAffinity();
                            }
                        }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                logoutDialog.show();
                return true;
            case R.id.message:
                callAllEmp();
                addRemovePeopleDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callAllEmp() {
        Map<String, String> map = new HashMap<>();
        map.put("company_id", ShardPreferences.get(mContext, ShardPreferences.key_company_Id));
        map.put("user_id", ShardPreferences.get(mContext, share_UserId_key));
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllEmployeeList> resultCall = apiInterface.callAllEmployeeList(map);
        resultCall.enqueue(new Callback<AllEmployeeList>() {
            @Override
            public void onResponse(Call<AllEmployeeList> call, Response<AllEmployeeList> response) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("true")) {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                    if (response.body().getEmployeeList().size() > 0) {
                        empList(response.body().getEmployeeList());
                    } else {
                        tv_noMember.setVisibility(View.VISIBLE);
                    }
                } else {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AllEmployeeList> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
            }
        });
    }

    private void empList(List<EmployeeList> list) {
        if (list != null && list.size() > 0) {
            allEmployeAdapter = new AllEmployeAdapter(mContext, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        } else {
            list = new ArrayList<>();
            allEmployeAdapter = new AllEmployeAdapter(mContext, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.please_click_BACK_again_to_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llTodo:
                Intent startTodo = new Intent(this, ToDoActivity.class);
                startActivity(startTodo);
                break;

            case R.id.llEvents:

            case R.id.schedule_layout:
                Intent startEvent = new Intent(this, ScheduleActivity.class);
                startActivity(startEvent);
                break;

            case R.id.llMessage:
                Intent startMessage = new Intent(this, Activity_MessageBoard.class);
                startActivity(startMessage);
                break;

            case R.id.llDocs:
                Intent startDOc = new Intent(this, DocsAndFilesActivity.class);
                startActivity(startDOc);
                break;

            case R.id.tab_start_hey_LL:
                Intent startHeyActivity = new Intent(this, HeyActivity.class);
                startActivity(startHeyActivity);
                finish();
                break;

            case R.id.tab_start_activity_LL:
                Intent activity = new Intent(this, ActivityScreen.class);
                startActivity(activity);
                finish();
                break;

            case R.id.tab_start_home_LL:
                Intent activity1 = new Intent(this, HomeActivity.class);
                startActivity(activity1);
                finish();
                break;


            case R.id.close_fab_layout:
                fabLayout.setVisibility(View.GONE);
                floatingActionButton.setVisibility(View.VISIBLE);
                break;

            case R.id.edit_profile:
                Intent intent = new Intent(this, EditProfileActivity.class);
                startActivity(intent);
                break;

            case R.id.version:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_version, null);
                dialogBuilder.setView(dialogView);

                TextView mDialogCancel = (TextView) dialogView.findViewById(R.id.textview_close);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;

            case R.id.user_name:
                UserDialog.show(mContext, userID, name, userImage, timezone, backcolor, textcolor);
                break;

            case R.id.assignment:
                Intent intent1 = new Intent(this, MyAssignmentActivity.class);
                startActivity(intent1);
                break;

            case R.id.change_avatar_layout:
                Intent intent3 = new Intent(this, ChangeAvatarActivity.class);
                startActivity(intent3);
                break;

            case R.id.registered_devices_layout:
                Intent intent4 = new Intent(this, MyDevicesActivity.class);
                startActivity(intent4);
                break;

/*
            case R.id.ll_tool_bookmark:
*/
            case R.id.bookmark_layout:
                Intent intent5 = new Intent(this, BookmarkActivity.class);
                startActivity(intent5);
                break;

            case R.id.recent_activity_layout:
                Intent intent6 = new Intent(this, RecentActivity.class);
                startActivity(intent6);
                break;

            case R.id.notification_setting_layout:
                Intent intent7 = new Intent(this, NotificationSettingActivity.class);
                startActivity(intent7);
                break;

            case R.id.saved_draft_layout:
                Intent intent8 = new Intent(this, SavedDraftActivity.class);
                startActivity(intent8);
                break;
            case R.id.ll_language:
                OpenChangeLanguage();
                break;

            case R.id.boost_layout:
                Intent intent9 = new Intent(this, BoostActivity.class);
                startActivity(intent9);
                break;
            case R.id.llLogout:
                AlertDialog logoutDialog = new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.logout))
                        .setMessage(getResources().getString(R.string.logout_info))
                        .setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent10 = new Intent(context, DeciderActivity.class);
                                startActivity(intent10);
                                ShardPreferences.clearPreference(context);
                                finishAffinity();
                            }
                        }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                logoutDialog.show();
                break;
        }
    }

    private void OpenChangeLanguage() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_language, null);
        dialogBuilder.setView(dialogView);
        ImageView img_close;
        RadioGroup rg_language;
        RadioButton rb_english, rb_arabick;
        rg_language = dialogView.findViewById(R.id.rg_language);
        img_close = dialogView.findViewById(R.id.img_close);
        rb_english = dialogView.findViewById(R.id.rb_english);
        rb_arabick = dialogView.findViewById(R.id.rb_arabick);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
            rb_arabick.setChecked(true);
            rb_english.setChecked(false);
        } else {
            rb_arabick.setChecked(false);
            rb_english.setChecked(true);
        }
        rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == rb_english.getId()) {
                    LocaleHelper.setLocale(context, "en");
                    ShardPreferences.save(context, ShardPreferences.share_language, "1");
                    setApplicationLanguage("en");
                    return;

                } else if (checkedId == rb_arabick.getId()) {
                    LocaleHelper.setLocale(context, "ar");
                    ShardPreferences.save(context, ShardPreferences.share_language, "2");
                    setApplicationLanguage("ar");
                    return;
                }
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HomeActivity.class);
                startActivity(intent);
                finish();
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_USER_DETAIL_BY_ID)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    set_user_name.setText(Utils.unescapeJava(jsonObjec.getString("employee_name")));
                    name = Utils.unescapeJava(jsonObjec.getString("employee_name"));
                    tv_email.setText(jsonObjec.getString("employee_email"));
                    textcolor = jsonObjec.getString("fontColor");
                    backcolor = jsonObjec.getString("bgColor");

                    timezone = jsonObjec.getString("timezone");
                    userImage = jsonObjec.getString("user_image");
                    userID = jsonObjec.getString("user_id");

                    if (!jsonObjec.getString("user_image").equals("")) {
                        Picasso.with(context).load(jsonObjec.getString("user_image")).into(img_me_userImage);
                    } else {
                        img_me_userImage.setImageDrawable(imageLatter(Utils.word(Utils.unescapeJava(jsonObjec.getString("employee_name"))),
                                jsonObjec.getString("bgColor"),
                                jsonObjec.getString("fontColor")));
                    }

                } else {
                    Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            mShimmerDialog.stopShimmerAnimation();
            mShimmerDialog.setVisibility(View.GONE);
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new UserListChatAdapter(mEmployeeDetailList, this);
                        rv_chaOptionHome.setAdapter(mAddRemoveUserAdapter);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void callSearch() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
        map.put("company_id", ShardPreferences.get(mContext, ShardPreferences.key_company_Id));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> resultCall = apiInterface.callSearch(map);
        resultCall.enqueue(new Callback<SearchResponse>() {

            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.body().getStatus()) {
                    userListingSpinner = (ArrayList<UserListing>) response.body().getUserListing();
                    userList();
                    cardListingSpinner = (ArrayList<CardListing>) response.body().getCardListing();
                    cardList();
                } else {
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
            }
        });

    }

    public void userList() {

        String[] mStringArray = new String[userListingSpinner.size()];
        String check_company_id = ShardPreferences.get(mContext, ShardPreferences.key_userSpinner_id);
        int setposition = 0;

        for (int i = 0; i < userListingSpinner.size(); i++) {
            mStringArray[i] = userListingSpinner.get(i).getEmployeeName();
            if (check_company_id.equals(userListingSpinner.get(i).getUserId())) {
                setposition = i;
                ShardPreferences.save(mContext, ShardPreferences.key_userSpinner_id, userListingSpinner.get(i).getUserId());
            }
        }

        ArrayAdapter aa = new ArrayAdapter(mContext, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_byAnyone.setAdapter(aa);
        sp_byAnyone.setSelection(setposition, false);
    }

    public void cardList() {
        String[] mStringArray = new String[cardListingSpinner.size()];
        String check_company_id = ShardPreferences.get(mContext, ShardPreferences.key_cardListingSpinner_id);
        int setposition = 0;

        for (int i = 0; i < cardListingSpinner.size(); i++) {
            mStringArray[i] = cardListingSpinner.get(i).getProjectTeamName();
            if (check_company_id.equals(cardListingSpinner.get(i).getProjectTeamId())) {
                setposition = i;
                ShardPreferences.save(mContext, ShardPreferences.key_cardListingSpinner_id, cardListingSpinner.get(i).getProjectTeamId());
            }
        }

        ArrayAdapter aa = new ArrayAdapter(mContext, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_everywhere.setAdapter(aa);
        sp_everywhere.setSelection(setposition, false);
    }

    RelativeLayout rl_pingChatHome;
    ShimmerFrameLayout mShimmerDialog;
    UserListChatAdapter mAddRemoveUserAdapter;
    RecyclerView rv_chaOptionHome;
    AutoCompleteTextView et_search;
    TextView tv_noMember;
    ImageView btnSend;

    public void addRemovePeopleDialog() {
        Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_user_chat, null);
        dialogBuilder.setContentView(dialogView);
        tv_noMember = dialogView.findViewById(R.id.tv_noMember);
        pb_progress = dialogView.findViewById(R.id.pb_progress);
        et_search = dialogView.findViewById(R.id.et_search);
        rv_chaOptionHome = dialogView.findViewById(R.id.rv_chaOptionHome);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_chaOptionHome.setLayoutManager(mLayoutManager);
        rl_pingChatHome = dialogView.findViewById(R.id.rl_pingChatHome);
        btnSend = dialogView.findViewById(R.id.btnSend);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    allEmployeAdapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_search.getText().toString().equals("")) {
                    try {
                        allEmployeAdapter.getFilter().filter(et_search.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        dialogBuilder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE) {
                recreate();
                userDetails();
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", ShardPreferences.get(mContext, share_Company_Id_key));
        paramsReq.put("userId", ShardPreferences.get(mContext, share_UserId_key));
        mApiRequest.postRequest(BASE_URL + GET_USER_DETAIL_BY_ID, GET_USER_DETAIL_BY_ID, paramsReq, Request.Method.POST);
    }
}
