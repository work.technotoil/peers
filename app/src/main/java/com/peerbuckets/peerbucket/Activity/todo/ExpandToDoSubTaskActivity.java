package com.peerbuckets.peerbucket.Activity.todo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddCommentActivity;
import com.peerbuckets.peerbucket.Adapter.AddRemoveUserAdapter;
import com.peerbuckets.peerbucket.Adapter.EmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_REMOVE_NOTIFIED;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_TODO_TASK;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_SINGLE_TODO_SUB_TASK;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.MASK_SUB_TO_DO_COMPLETE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SUBSCRIBE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class ExpandToDoSubTaskActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, RefreshCommentInterface {
    Context context = this;
    WebView msub_task_title, msub_task_desc, webViewAssignedTo;
    String title, sub_task_desc = "", sub_task_date, record_id, email, assignedUser, all_users_id, assignedUsersHTML = "", name = "",
            user_id = "", commaSeperatedID = "";
    PopupMenu popup;
    Intent intent;
    ApiRequest mApiRequest;
    TextView tvAssignee, tv_messageTitle, tvDate, message_count_tv, tv_add_remove, add_comment_tv, tv_people, tv_view, tv_selectOption;
    LinearLayout linearDate;
    LinearLayoutManager commentLayoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount, page = 0;
    private boolean checkedType;
    CheckBox checkBox;
    private LinearLayout mno_comments_LL, mcomments_LL;
    private int REQUEST_SUB_TO_DO_EDIT = 103;
    private Toolbar toolbar;
    public static int REQ_CODE_ADD_COMMENT = 2;
    RecyclerView employeee_icon_rv, employeee_applause_rv, rv_dialogAddRemove, mcomments_rv;
    RecyclerView.LayoutManager mEmployeListanager, mApplauseManager;
    AddRemoveUserAdapter mAddRemoveUserAdapter;
    EmployeeIconAdapter mNotifiedUserAdapter;
    MessageCommentsAdapter mcomments_rv_adapter;
    ArrayList<MessageCommentPOJO> messageCommentPOJOS;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    List<User> assignedUsersList = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mEmployeeDetalPOJO = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mApplauseUserList = new ArrayList<>();
    ImageView imgApplause, user_image;
    Boolean isApplause = false, loading = true;
    Button btn_subscribe, btn_unsubscribe;
    RelativeLayout relMaster, rel_comment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expand_todo_sub_task);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        intent = getIntent();

        initUI();
        initNotifiedModule();
        getDetails();
        getCommentList(page);
        applyPagination();
        fetchEmployee();

    }

    public static boolean isRTL() {
        return isRTL(Locale.getDefault());
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }

    private void initUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(getResources().getString(R.string.subto_dos));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();

        if (getIntent().getStringExtra("sub_task_title") != null) {
            title = getIntent().getStringExtra("sub_task_title");
        }
        if (getIntent().getStringExtra("sub_task_id") != null) {
            record_id = getIntent().getStringExtra("sub_task_id");
        }
        if (getIntent().getStringExtra("sub_task_desc") != null) {
            sub_task_desc = getIntent().getStringExtra("sub_task_desc");
        }
        if (getIntent().getStringExtra("sub_task_start_date") != null) {
            sub_task_date = getIntent().getStringExtra("sub_task_start_date");
        }
        if (getIntent().getStringExtra("email") != null) {
            email = getIntent().getStringExtra("email");
        }
        if (getIntent().getStringExtra("assignedUser") != null) {
            assignedUser = getIntent().getStringExtra("assignedUser");
        }
        if (getIntent().getStringExtra("users") != null) {
            all_users_id = getIntent().getStringExtra("users");
        }
        if (getIntent().getStringExtra("user_id") != null) {
            user_id = getIntent().getStringExtra("user_id");
        }
        if (getIntent().getBooleanExtra("checked_type", false)) {
            checkedType = getIntent().getBooleanExtra("checked_type", false);
        }


        tv_selectOption = findViewById(R.id.tv_selectOption);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        tv_people = findViewById(R.id.tv_people);
        tv_view = findViewById(R.id.tv_view);
        user_image = findViewById(R.id.user_image);
        msub_task_title = findViewById(R.id.sub_task_title);
        msub_task_desc = findViewById(R.id.sub_task_desc);
        tvAssignee = findViewById(R.id.tv_assignee_sub_task);
        tvDate = findViewById(R.id.tv_dates_sub_task);
        message_count_tv = findViewById(R.id.message_count_tv);
        add_comment_tv = findViewById(R.id.add_comment_tv);
        linearDate = findViewById(R.id.linear_date_sub_task);
        webViewAssignedTo = findViewById(R.id.webView_assigned_to_sub_task);

        checkBox = findViewById(R.id.checkbox_time_add_schedule);

        rel_comment = findViewById(R.id.rel_comment);

        mcomments_rv = findViewById(R.id.comments_rv);

        commentLayoutManager = new LinearLayoutManager(context);
        commentLayoutManager.setOrientation(RecyclerView.VERTICAL);
        mcomments_rv.setLayoutManager(commentLayoutManager);
        mcomments_rv.setHasFixedSize(true);
        checkBox.setOnCheckedChangeListener(this);
        mno_comments_LL = findViewById(R.id.no_comments_LL);

        mcomments_LL = findViewById(R.id.comments_LL);
        mno_comments_LL.setVisibility(View.GONE);
        mcomments_LL.setVisibility(View.GONE);
        add_comment_tv.setOnClickListener(this);

        tv_selectOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectOption(view);
            }
        });
    }

    public void initNotifiedModule() {
        tv_add_remove = findViewById(R.id.tv_add_remove);
        tv_people = findViewById(R.id.tv_people);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_unsubscribe = findViewById(R.id.btn_unsubscribe);
        imgApplause = findViewById(R.id.imgApplause);
        employeee_icon_rv = findViewById(R.id.employeee_icon_rv);
        mEmployeListanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_icon_rv.setLayoutManager(mEmployeListanager);

        employeee_applause_rv = findViewById(R.id.employeee_applause_rv);
        mApplauseManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_applause_rv.setLayoutManager(mApplauseManager);

        tv_add_remove.setOnClickListener(this);
        imgApplause.setOnClickListener(this);
        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.VISIBLE);
                btn_subscribe.setVisibility(View.GONE);
                btn_unsubscribe.setVisibility(View.VISIBLE);
                actionSubscribe("1");
            }
        });

        btn_unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.GONE);
                btn_subscribe.setVisibility(View.VISIBLE);
                btn_unsubscribe.setVisibility(View.GONE);
                actionSubscribe("0");
            }
        });

    }

    private void actionSubscribe(String isSubscribe) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", record_id);
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        paramsReq.put("loggedIn_user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("user_id", commaSeperatedID);
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("notification_type", "todo_list");
        paramsReq.put("isSubscribe", isSubscribe);
        mApiRequest.postRequestBackground(BASE_URL + SUBSCRIBE, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);

    }


    private void getCommentList(int cPage) {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "sub-todo");
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("todo_id", record_id);
        paramsReq.put("page", String.valueOf(cPage));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);

    }

    private void applyPagination() {
        mcomments_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = commentLayoutManager.getChildCount();
                    totalItemCount = commentLayoutManager.getItemCount();
                    pastVisiblesItems = commentLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            getCommentList(page);

                        }
                    }
                }
            }
        });
    }

    private void SelectOption(View v) {
        popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.message_board_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit_menu:
                        Intent intent = new Intent(context, ActivityAddEditToDoTask.class);
                        intent.putExtra("to_do_sub_id", record_id);
                        intent.putExtra("to_do_sub_title", Utils.getUnicodeString(Utils.escapeUnicodeText(title)));
                        intent.putExtra("to_do_title", Utils.getUnicodeString(Utils.escapeUnicodeText(title)));
                        intent.putExtra("sub_task_desc", Utils.getUnicodeString(Utils.escapeUnicodeText(sub_task_desc)));
                        intent.putExtra("class_type", ActivityAddEditToDoTask.TYPE_EDIT);
                        startActivityForResult(intent, REQUEST_SUB_TO_DO_EDIT);
                        return true;
                    case R.id.menu_trash:
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("record_id", record_id);
                        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                        paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("record_type", "Sub-Todo");
                        mApiRequest.postRequest(BASE_URL + DELETE_TODO_TASK, DELETE_TODO_TASK, paramsReq, Request.Method.POST);
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    private void getDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("todo_list_task_id", record_id);
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequest(BASE_URL + FETCH_SINGLE_TODO_SUB_TASK, FETCH_SINGLE_TODO_SUB_TASK, paramsReq, Request.Method.POST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add_remove:
                addRemovePeopleDialog();
                break;
            case R.id.imgApplause:
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause();
                break;
            case R.id.add_comment_tv:
                Intent intent1 = new Intent(this, AddCommentActivity.class);
                intent1.putExtra("call_type", "sub-todo");
                intent1.putExtra("message_title", title);
                intent1.putExtra("message_email", email);
                intent1.putExtra("name", name);
                intent1.putExtra("record_id", record_id);
                intent1.putExtra("all_users_id", all_users_id);
                startActivityForResult(intent1, REQ_CODE_ADD_COMMENT);
                break;
        }
    }

    public void addApplause() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", record_id);
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("notification_type", "sub-todo");
        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }

        mApiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);
    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .bold()// height in px
                .textColor(Color.parseColor("#" + fontColor))
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));
        return drawable;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_SINGLE_TODO_SUB_TASK)) {
            Log.d("SINGLE_SUBTASK", response);
            try {
                JSONObject jsonObject = new JSONObject(response);

                isApplause = jsonObject.getBoolean("isApplause");

                if (jsonObject.getBoolean("isSubscribe")) {
                    btn_unsubscribe.setVisibility(View.VISIBLE);
                    btn_subscribe.setVisibility(View.GONE);
                    tv_view.setVisibility(View.VISIBLE);
                } else {
                    tv_view.setVisibility(View.GONE);
                    btn_subscribe.setVisibility(View.VISIBLE);
                    btn_unsubscribe.setVisibility(View.GONE);
                }

                JSONObject jsonObjectDetail = jsonObject.getJSONObject("subtodo_details");

                name = Utils.unescapeJava(jsonObjectDetail.getString("employee_name"));
                if (!jsonObjectDetail.getString("user_image").equals("")) {
                    Picasso.with(this).load(DEVELOPMENT_URL + jsonObjectDetail.getString("user_image")).into(user_image);
                } else {
                    user_image.setImageDrawable(imageLatter(Utils.word(name), jsonObjectDetail.getString("bgColor"), jsonObjectDetail.getString("fontColor")));
                }

                Utils.setDescription(Utils.unescapeJava(jsonObjectDetail.getString("todo_list_task_title")), msub_task_title, context);
                title = Utils.unescapeJava(jsonObjectDetail.getString("todo_list_task_title"));
                user_id = Utils.unescapeJava(jsonObjectDetail.getString("user_id"));
                checkBox.setChecked(checkedType);
                if (jsonObjectDetail.getString("todo_list_task_description").equals("")) {
                    Utils.setDescription(Utils.unescapeJava(jsonObjectDetail.getString("todo_list_task_description")), msub_task_desc, context);
                    sub_task_desc = Utils.unescapeJava(jsonObjectDetail.getString("todo_list_task_description"));
                }
                tvAssignee.setText(getResources().getString(R.string.added_by, Utils.bindTwoString(Utils.unescapeJava(jsonObjectDetail.getString("employee_name")), jsonObjectDetail.getString("created_at"), null)));
                JSONArray usersArray = jsonObjectDetail.getJSONArray("users");

                if (usersArray.length() > 0) {
                    assignedUsersList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                    }.getType());

                    if (assignedUsersList != null && assignedUsersList.size() > 0) {
                        for (User assignedUser : assignedUsersList) {
                            assignedUsersHTML = assignedUsersHTML + Utils.createUserHtml(assignedUser.getEmployeeName(), assignedUser.getUserImage(), assignedUser.getBgColor(), assignedUser.getFontColor());
                            webViewAssignedTo.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUsersHTML, "text/html", "utf-8", null);
                        }
                    } else {
                        webViewAssignedTo.loadData(assignedUsersHTML, "text/html", "utf-8");
                    }
                }

                if (Utils.isStringValid(jsonObjectDetail.getString("selector"))) {
                    if (jsonObjectDetail.getString("selector").equals("no_due_date")) {
                        linearDate.setVisibility(View.GONE);
                    } else if (jsonObjectDetail.getString("selector").equals("due_date")) {
                        tvDate.setText(Utils.checkStringWithEmpty(jsonObjectDetail.getString("todo_list_task_start_date_show")));
                    } else {
                        tvDate.setText(Utils.bindTwoStringWithData(jsonObjectDetail.getString("todo_list_task_start_date_show"), jsonObjectDetail.getString("todo_list_task_end_date_show"), "-", "-"));
                    }
                } else {
                    linearDate.setVisibility(View.GONE);
                }

                JSONObject jsonObject1 = new JSONObject();
                if (usersArray != null) {
                    for (int i = 0; i < usersArray.length(); i++) {
                        jsonObject1 = usersArray.getJSONObject(i);
                        if (i == 0) {
                            all_users_id = jsonObject1.getString("user_id");
                        } else {
                            all_users_id = all_users_id + "," + jsonObject1.getString("user_id");
                        }
                        EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                        mModel.setUserId(jsonObject1.getString("user_id"));
                        mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                        mModel.setUserImage(jsonObject1.getString("user_image"));
                        mModel.setFontColor(jsonObject1.getString("fontColor"));
                        mModel.setBgColor(jsonObject1.getString("bgColor"));

                        mEmployeeDetalPOJO.add(mModel);
                        Log.d("ALL_USERS", all_users_id);
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                    employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                }

                JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");

                if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                    for (int i = 0; i < mGetApplauseArray.length(); i++) {
                        JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                        EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                        mModel.setUserId(jsonObject2.getString("user_id"));
                        mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                        mModel.setUserImage(jsonObject2.getString("user_image"));
                        mModel.setFontColor(jsonObject2.getString("fontColor"));
                        mModel.setBgColor(jsonObject2.getString("bgColor"));
                        mApplauseUserList.add(mModel);
                    }

                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }


            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_subscribe.setVisibility(View.GONE);
                        tv_view.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        tv_view.setVisibility(View.GONE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    tv_people.setText("" + jsonObject.getString("userCount") + " ");
                    JSONArray mUserArray = jsonObject.getJSONArray("update_user_list");
                    if (mUserArray != null) {
                        if (mEmployeeDetalPOJO != null) {
                            mEmployeeDetalPOJO.clear();
                        }
                        for (int i = 0; i < mUserArray.length(); i++) {
                            JSONObject jsonObject1 = mUserArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));

                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mApplauseUserList != null) {
                        mApplauseUserList.clear();
                    }
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));

                            mApplauseUserList.add(mModel);
                        }
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(FETCH_COMMENT_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    rel_comment.setVisibility(View.VISIBLE);
                    JSONArray mArray = jsonObject.getJSONArray("commentList");
                    if (mArray != null && mArray.length() > 0) {
                        if (page == 0 && messageCommentPOJOS != null) {
                            messageCommentPOJOS.clear();
                        }

                        messageCommentPOJOS = new Gson().fromJson(mArray.toString(), new TypeToken<List<MessageCommentPOJO>>() {
                        }.getType());

                        message_count_tv.setText(messageCommentPOJOS.size() + "");

                        if (messageCommentPOJOS != null && messageCommentPOJOS.size() > 0) {
                            if (page == 0) {
                                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, context, title, record_id, this, 0);
                                mcomments_rv.setAdapter(mcomments_rv_adapter);
                            } else {
                                mcomments_rv.post(new Runnable() {
                                    public void run() {
                                        mcomments_rv_adapter.setNotifyData(messageCommentPOJOS);
                                    }
                                });
                            }
                        }

                        page = page + 1;
                        loading = true;
                    }

                } else {
                    if (page == 0) {
                        rel_comment.setVisibility(View.GONE);
                    }

                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(DELETE_TODO_TASK)) {
            Log.d("DELETE_TODO_TASK", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    Intent i = getIntent();
                    setResult(ExpandToDoActivity.RESULT_OK, i);
                    finish();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }
        if (tag_json_obj.equals(MASK_SUB_TO_DO_COMPLETE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {

                    checkBox.setEnabled(true);
                } else {
                    checkBox.setEnabled(true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                checkBox.setEnabled(true);
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        if (rv_dialogAddRemove != null) {
                            mAddRemoveUserAdapter = new AddRemoveUserAdapter(all_users_id, mEmployeeDetailList, this);
                            rv_dialogAddRemove.setAdapter(mAddRemoveUserAdapter);
                        }

                    } else {
                        Toast.makeText(this, "No employee found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Server side issue, code error is :" + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {

                    tv_people.setText("" + jsonObject.getString("userCount"));
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == REQ_CODE_ADD_COMMENT && data != null) {
            if (messageCommentPOJOS == null) {
                messageCommentPOJOS = new ArrayList<>();
            }

            if (mcomments_rv_adapter == null) {
                messageCommentPOJOS.add((MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, title, record_id, this, 0);
                mcomments_rv.setAdapter(mcomments_rv_adapter);
                mcomments_rv_adapter.notifyDataSetChanged();
            } else {
                messageCommentPOJOS.add(0, (MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, title, record_id, this, 0);
                mcomments_rv.setAdapter(mcomments_rv_adapter);
                mcomments_rv_adapter.notifyDataSetChanged();
            }
        }
        if (requestCode == 22) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 1) {
            ((MessageCommentsAdapter) mcomments_rv_adapter).updateData(data.getIntExtra("position", -1), data.getStringExtra("comment"));
        }
        if (requestCode == REQUEST_SUB_TO_DO_EDIT) {
            recreate();
        }
        if (requestCode == 5) {

            String position = data.getStringExtra("position");
            String comment = Utils.unescapeJava(data.getStringExtra("comment"));
            messageCommentPOJOS.get(Integer.parseInt(position)).setCommentDescription(comment);

            mcomments_rv_adapter.notifyDataSetChanged();
        }
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, intent);
                onBackPressed();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.checkbox_time_add_schedule) {
            if (buttonView.isPressed()) {
                if (isChecked) {
                    checkBox.setEnabled(false);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("todo_task_id", record_id);
                    paramsReq.put("status", "1");
                    paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                    paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                    paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));

                    mApiRequest.postRequestBackground(BASE_URL + MASK_SUB_TO_DO_COMPLETE, MASK_SUB_TO_DO_COMPLETE, paramsReq, Request.Method.POST);
                } else {
                    checkBox.setEnabled(false);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("todo_task_id", record_id);
                    paramsReq.put("status", "0");
                    paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                    paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                    paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                    mApiRequest.postRequestBackground(BASE_URL + MASK_SUB_TO_DO_COMPLETE, MASK_SUB_TO_DO_COMPLETE, paramsReq, Request.Method.POST);
                }
            }
        }
    }

    @Override
    public void setCommentData(int commentSize) {
        message_count_tv.setText(commentSize + "");
        if (commentSize > 0) {
            mcomments_LL.setVisibility(View.GONE);
            mno_comments_LL.setVisibility(View.GONE);
        } else {
            mcomments_LL.setVisibility(View.GONE);
            mno_comments_LL.setVisibility(View.VISIBLE);
        }
    }


    public void addRemovePeopleDialog() {
        final Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_remove_user, null);
        dialogBuilder.setContentView(dialogView);
        TextView selectEveryOne = dialogView.findViewById(R.id.selectEveryOne);
        TextView selectNoOne = dialogView.findViewById(R.id.selectNoOne);
        TextView tv_cancel = dialogView.findViewById(R.id.tv_cancel);
        rv_dialogAddRemove = dialogView.findViewById(R.id.rv_dialogAddRemove);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_dialogAddRemove.setLayoutManager(mLayoutManager);
        ;

        selectEveryOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.selectAllSelections();
                    }
                }
            }
        });

        try {
            selectNoOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                        if (mAddRemoveUserAdapter != null) {
                            mAddRemoveUserAdapter.clearAllSelections();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        fetchEmployee();

        Button btnSave = dialogView.findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commaSeperatedID = getAssignedUserString();
                if (commaSeperatedID.equals("")) {
                    Toast.makeText(context, "Please select atleast one", Toast.LENGTH_SHORT).show();
                } else {
                    all_users_id = commaSeperatedID;
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("record_id", record_id);
                    paramsReq.put("loggedIn_user_id", ShardPreferences.get(context, share_UserId_key));
                    paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                    paramsReq.put("user_id", commaSeperatedID);
                    paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                    paramsReq.put("notification_type", "todo_list");
                    mApiRequest.postRequestBackground(BASE_URL + ADD_REMOVE_NOTIFIED, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
                    dialogBuilder.dismiss();
                }
            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.show();

    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);

    }

    private String getAssignedUserString() {
        if (mAddRemoveUserAdapter != null) {
            return mAddRemoveUserAdapter.getAssignedUserString();
        }
        return "";
    }
}
