package com.peerbuckets.peerbucket.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hbisoft.pickit.PickiT;
import com.hbisoft.pickit.PickiTCallbacks;
import com.peerbuckets.peerbucket.Adapter.ChatAdapter;
import com.peerbuckets.peerbucket.POJO.Chat;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Common;
import com.peerbuckets.peerbucket.Utils.PicassoImageGetter;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.view_holder.MentoningViewHolder;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.teamwork.autocomplete.MultiAutoComplete;
import com.teamwork.autocomplete.adapter.AutoCompleteTypeAdapter;
import com.teamwork.autocomplete.adapter.OnTokensChangedListener;
import com.teamwork.autocomplete.filter.HandleTokenFilter;
import com.teamwork.autocomplete.tokenizer.PrefixTokenizer;
import com.teamwork.autocomplete.util.SpannableUtils;
import com.teamwork.autocomplete.view.AutoCompleteViewBinder;
import com.teamwork.autocomplete.view.AutoCompleteViewHolder;
import com.teamwork.autocomplete.view.MultiAutoCompleteEditText;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import xyz.klinker.giphy.Giphy;

import static com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity.STORAGE_PERMISSION_CODE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_ANDROID;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_INVITED_USERS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.Utils.Utils.checkStringWithEmpty;
import static com.peerbuckets.peerbucket.Utils.Utils.hasPermissions;
import static com.peerbuckets.peerbucket.Utils.Utils.unescapeJava;

public class ChatActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, PickiTCallbacks {

    private static final String TAG = "ChatActivity";
    Context context = this;
    ImageView img_priority_high;
    ApiRequest apiRequest;
    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton floatingBtnAddMessage, floatingAddImage;
    DatabaseReference mDatabaseReference;
    FirebaseDatabase firebaseDatabase;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ChatAdapter chatAdapter;
    Boolean isTeamMembersAvailable = false;
    String channelId = "", lastKey = "", firstKey = "", lastName = "", firstName = "";
    String lastTimestamp;
    private Query query;
    MultiAutoComplete customMultiAutoComplete;
    ProgressBar progress_bar;
    List<Chat> chatList = new ArrayList<>();
    Boolean loading = true;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    String name = "";
    private StorageReference mStorageReference;
    FirebaseStorage firebaseStorage;
    private Toolbar toolbar;
    ProgressBar progressBarMedia;
    List<User> teamMembersList = new ArrayList<>();
    String subjectString = "";
    MultiAutoCompleteEditText etMessage;
    TextView tv_document, tv_postGif;
    String image;
    String source;
    PopupWindow changeSortPopUp, popup;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    PickiT pickiT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping);

        channelId = ShardPreferences.get(context, share_current_project_team_id);
        if (getIntent().getStringExtra("projectId") != null) {
            subjectString = getIntent().getStringExtra("projectId");
        }
        if (getIntent().getStringExtra("name") != null) {
            name = getIntent().getStringExtra("name");
        }
        pickiT = new PickiT(this, this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        inIt();
        getInvitedUsers();

        if (!hasPermissions(context, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, STORAGE_PERMISSION_CODE);
        }

        applyPagination();
        fetchEmployee();
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(name);

        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        back();

        apiRequest = new ApiRequest(context, this);

        progress_bar = findViewById(R.id.progress_bar);
        img_priority_high = findViewById(R.id.img_priority_high);
        img_priority_high.setVisibility(View.VISIBLE);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_ping);
        floatingBtnAddMessage = findViewById(R.id.floating_chat);
        floatingBtnAddMessage.setOnClickListener(this);
        floatingAddImage = findViewById(R.id.floating_add_img_ping);
        floatingAddImage.setOnClickListener(this);
        img_priority_high.setOnClickListener(this);

        etMessage = findViewById(R.id.et_message_chat);

        layoutManager = new LinearLayoutManager(context);
        ((LinearLayoutManager) layoutManager).setOrientation(RecyclerView.VERTICAL);
        recyclerView = findViewById(R.id.recycler_ping);
        recyclerView.setLayoutManager(layoutManager);
        chatAdapter = new ChatAdapter(context, chatList, teamMembersList);
        recyclerView.setAdapter(chatAdapter);
        recyclerView.setAdapter(chatAdapter);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = firebaseDatabase.getReference();
        firebaseStorage = FirebaseStorage.getInstance();
        swipeRefreshLayout.setEnabled(false);
        progressBarMedia = findViewById(R.id.progress_bar_media_ping);
        mDatabaseReference = mDatabaseReference.child("message").child(channelId);
        mStorageReference = firebaseStorage.getReference("message").child(channelId);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (customMultiAutoComplete != null) {
            customMultiAutoComplete.onViewDetached();
        }
        super.onDestroy();
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", ShardPreferences.get(context, ShardPreferences.key_company_Id));
        paramsReq.put("type", "test_hq");
        paramsReq.put("typeId", subjectString);
        apiRequest.postRequest(BASE_URL + FETCH_EMPLOYEE_ANDROID, FETCH_EMPLOYEE_ANDROID, paramsReq, Request.Method.POST);
    }

    private void getInvitedUsers() {
        swipeRefreshLayout.setRefreshing(true);
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        apiRequest.postRequestBackground(BASE_URL + GET_INVITED_USERS, GET_INVITED_USERS, paramsReq, Request.Method.POST);
    }

    private void createNewMessage(String type, String message, String imageFileName) {
        //Create a new message on send click
        message = addHTMLonMention(message);
        Chat newMessage = new Chat(message, type, "", "", ShardPreferences.get(context, ShardPreferences.share_UserId_key), imageFileName);
        System.out.print("Message" + message);

        mDatabaseReference.push().setValue(newMessage)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        recyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);
                    }

                });

        switch (type) {
            case Common.TYPE_TEXT:
                etMessage.setText("");
                break;
        }
    }

    public String addHTMLonMention(String msg) {
        String message = msg;
        if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {

            for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                if (msg.contains("@" + mEmployeeDetailList.get(i).getEmployeeName())) {
                    message = message.replace("@" + Utils.unescapeJava(mEmployeeDetailList.get(i).getEmployeeName()), "<font color=#0000FF >@" + Utils.unescapeJava(mEmployeeDetailList.get(i).getEmployeeName()) + "</font>");
                }
            }
        }
        return message;
    }

    private void getPrimaryMessage() {
        swipeRefreshLayout.setRefreshing(true);
        query = mDatabaseReference.orderByChild("timestamp");
        applyPagination();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //Add data to list when data added to database.
                Log.d(TAG, "onChildAdded" + dataSnapshot.getKey() + s);
                Chat chat = dataSnapshot.getValue(Chat.class);
                chat.setPrimaryKey(dataSnapshot.getKey());
                chatList.add(chat);
                lastTimestamp = String.valueOf(chatList.get(0).getTimestamp());

                chatAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                System.out.print(TAG + "onChildRemoved" + dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                swipeRefreshLayout.setRefreshing(false);
                Log.d(TAG, databaseError.toString());

            }
        });

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_INVITED_USERS)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray usersArray = jsonObject.getJSONArray("user_list");
                    getFirstKey();
                    getPrimaryMessage();
                    swipeRefreshLayout.setRefreshing(false);
                    if (usersArray.length() > 0) {
                        isTeamMembersAvailable = true;
                        teamMembersList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                        }.getType());
                        if (teamMembersList != null && teamMembersList.size() > 0) {
                            chatAdapter.updateTeamMemberList(teamMembersList);
                        }
                    }
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            } catch (JSONException e) {
                swipeRefreshLayout.setRefreshing(false);
                e.printStackTrace();
            }
        } else if (tag_json_obj.equals(FETCH_EMPLOYEE_ANDROID)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");
                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());

                        Handler uiHandler = new Handler(Looper.getMainLooper());
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                setupCustomAutoComplete();
                                customMultiAutoComplete.onViewAttached(etMessage);
                            }
                        });


                    } else {
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floating_chat:
                if (!etMessage.getText().toString().equals("")) {
                    if (isTeamMembersAvailable) {
                        createNewMessage(Common.TYPE_TEXT, Utils.unescapeJava(etMessage.getText().toString()), "");
                    }
                } else {
                    etMessage.setText("");
                }
                break;

            case R.id.img_priority_high:
                Intent intent = new Intent(context, ChatingUserListActivity.class);
                intent.putExtra("list", (Serializable) teamMembersList);
                startActivity(intent);
                break;
            case R.id.floating_add_img_ping:

                LinearLayout ll_mediaOption = findViewById(R.id.ll_mediaOption);
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = layoutInflater.inflate(R.layout.dialog_media_option, ll_mediaOption, true);

                tv_document = layout.findViewById(R.id.tv_document);
                tv_postGif = layout.findViewById(R.id.tv_postGif);
                // Creating the PopupWindow
                popup = new PopupWindow(context);
                popup.setContentView(layout);
                popup.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
                popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popup.setFocusable(true);
                popup.setTouchable(true);

                // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.

                // Clear the default translucent background
                popup.setBackgroundDrawable(new BitmapDrawable());
                hideKeyboard(ChatActivity.this);
                // Displaying the popup at the specified location, + offsets.
                popup.showAtLocation(layout, Gravity.BOTTOM | Gravity.LEFT | Gravity.RIGHT, floatingAddImage.getScrollX(), floatingAddImage.getScrollY());
                tv_document.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (Build.VERSION.SDK_INT < 19) {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("*/*");
                            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                            startActivityForResult(intent, 7);
                            popup.dismiss();
                        } else {
                            Intent pdfIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            pdfIntent.setType("*/*");
                            pdfIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                            pdfIntent.addCategory(Intent.CATEGORY_OPENABLE);
                            startActivityForResult(pdfIntent, 7);
                            popup.dismiss();
                        }
                    }
                });
                tv_postGif.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new Giphy.Builder((Activity) context, "dc6zaTOxFJmzC")    // Giphy's BETA key
                                .maxFileSize(5 * 1024 * 1024)               // 5 mb
                                .start();
                        popup.dismiss();
                    }
                });

                break;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void applyPagination() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(-1)) {
                    Log.d(TAG, "Top Reached ...");
                }
            }
        });
    }

    private void getFirstKey() {
        final Query firstQuery = mDatabaseReference.orderByKey().limitToFirst(1);
        firstQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    //if you call methods on dataSnapshot it gives you the required values
                    firstKey = data.getKey(); // then it has the value "4:"
                    //as per your given snapshot of firebase database data
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.toString());
            }
        });
    }

    private void getPaginatedData() {

        lastKey = chatList.get(0).getPrimaryKey();
        lastTimestamp = chatList.get(0).getTimestamp().toString();
        Log.d(TAG, "Last Timestamp" + lastTimestamp);
        Log.d(TAG, "Last Key" + lastKey);
        if (lastKey.equals(firstKey)) {
            loading = false;
        }
        if (loading) {
            Query query1;
            query1 = mDatabaseReference.orderByChild("timestamp").endAt(lastTimestamp, lastKey).limitToLast(15);

            query1.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    List<Chat> newItem = new ArrayList<>();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        // TODO: handle the post
                        Chat chat = postSnapshot.getValue(Chat.class);
                        chat.setPrimaryKey(postSnapshot.getKey());
                        Log.d(TAG, chat.getMessage());
                        newItem.add(chat);

                        recyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);
                    }

                    Collections.reverse(newItem);
                    newItem.remove(0);
                    for (Chat list : newItem) {
                        chatList.add(0, list);
                        chatAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }

            });
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                String fileName = Utils.getFileName(context, resultUri);
                progressBarMedia.setVisibility(View.VISIBLE);
                floatingAddImage.setVisibility(View.GONE);
                uploadFile(String.valueOf(resultUri), Common.TYPE_IMAGE, fileName);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();

            }

        } else if (requestCode == 7) {
            if (resultCode == RESULT_OK) {
                Uri link = data.getData();
                String PathHolder = Utils.getFileName(context, link);
                ContentResolver cr = this.getContentResolver();
                String type = getMimeType(PathHolder);


                if (type != null) {
                    if (type.contains("image")) {
                        progress_bar.setVisibility(View.VISIBLE);
                        uploadFileGiphy(String.valueOf(link), Common.TYPE_IMAGE, PathHolder);
                    } else if (type.contains("video")) {
                        progress_bar.setVisibility(View.VISIBLE);
                        uploadFileGiphy(String.valueOf(link), Common.TYPE_VIDEO, PathHolder);
                    } else {
                        progress_bar.setVisibility(View.VISIBLE);
                        pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);

                    }
                } else {
                    progress_bar.setVisibility(View.VISIBLE);
                    pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);

                }
            }
        } else if (requestCode == Giphy.REQUEST_GIPHY) {
            if (resultCode == Activity.RESULT_OK) {
                Uri gif = data.getData();
                String fileName = Utils.getFileName(context, gif);
                progress_bar.setVisibility(View.VISIBLE);
                uploadFileGiphy(String.valueOf(gif), Common.TYPE_GIPHY, fileName);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    private void uploadFile(String media, String mediaType, String fileName) {
        Random r = new Random();
        int randomId = r.nextInt((100 - 10) + 1) + 10;
        if (!Utils.isStringValid(fileName)) {
            fileName = String.valueOf(randomId);
        }
        final StorageReference storageReference = mStorageReference.child(String.valueOf(fileName));

        final UploadTask uploadTask = storageReference.putFile(Uri.fromFile(new File(media)));
        final String finalFileName = fileName;
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    switch (mediaType) {
                        case "2":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_IMAGE, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "4":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_GIPHY, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "3":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_VIDEO, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "5":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_FILE, String.valueOf(downloadUri), finalFileName);
                            break;
                    }

                } else {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    // Handle failures
                    // ...
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Image Upload Failed");
                progressBarMedia.setVisibility(View.GONE);
                floatingAddImage.setVisibility(View.VISIBLE);
            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Image Upload in progress ");
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                int i = (int) progress;
                Log.d(TAG, String.valueOf(i));
            }
        });
    }

    private void uploadFileGiphy(String media, String mediaType, String fileName) {
        Random r = new Random();
        int randomId = r.nextInt((100 - 10) + 1) + 10;
        if (!Utils.isStringValid(fileName)) {
            fileName = String.valueOf(randomId);
        }
        final StorageReference storageReference = mStorageReference.child(String.valueOf(fileName));

        final UploadTask uploadTask = storageReference.putFile(Uri.parse(media));
        final String finalFileName = fileName;
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    throw task.getException();

                }
                // Continue with the task to get the download URL
                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    switch (mediaType) {
                        case "2":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_IMAGE, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "4":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_GIPHY, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "3":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_VIDEO, String.valueOf(downloadUri), finalFileName);
                            break;
                        case "5":
                            progress_bar.setVisibility(View.GONE);
                            createNewMessage(Common.TYPE_FILE, String.valueOf(downloadUri), finalFileName);
                            break;
                    }
                } else {
                    progressBarMedia.setVisibility(View.GONE);
                    floatingAddImage.setVisibility(View.VISIBLE);
                    // Handle failures
                    // ...
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Image Upload Failed");
                progressBarMedia.setVisibility(View.GONE);
                floatingAddImage.setVisibility(View.VISIBLE);
            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "Image Upload in progress ");

                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                int i = (int) progress;
                Log.d(TAG, String.valueOf(i));
            }
        });
    }


    private void setupCustomAutoComplete() {
        // type adapter to match country codes prefixed with '@'
        // note: the data type here could be different (i.e. a Region object)
        AutoCompleteTypeAdapter<EmployeeDetalPOJO> codeTypeAdapter =
                AutoCompleteTypeAdapter.Build.from(new CountryViewBinder(), new CountryCodeTokenFilter());


        // setting items synchronously since we already have the list
        // this could be also done later on in the likely case the data set comes from network or disk
        codeTypeAdapter.setItems(mEmployeeDetailList);

        // build the custom MultiAutoComplete by passing the required Tokenizer and type adapters
        customMultiAutoComplete = new MultiAutoComplete.Builder()
                .tokenizer(new PrefixTokenizer('@'))
                .addTypeAdapter(codeTypeAdapter)
                .delayer(new TestDelayer())
                .build();

    }


    @Override
    public void PickiTonStartListener() {
    }

    @Override
    public void PickiTonProgressUpdate(int progress) {

    }

    @Override
    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason) {
        String filename = path.substring(path.lastIndexOf("/") + 1);
        uploadFile(path, Common.TYPE_FILE, filename);
    }


    private class CountryCodeTokenFilter extends HandleTokenFilter<EmployeeDetalPOJO> /*implements Html.ImageGetter*/ {

        CountryCodeTokenFilter() {
            super('@');
        }

        @Override
        protected boolean matchesConstraint(@NonNull EmployeeDetalPOJO country, @NonNull CharSequence constraint) {
            return country.getEmployeeName().toLowerCase().contains(constraint.toString().toLowerCase());
        }

        @Override
        public @NonNull
        CharSequence toTokenString(@NonNull EmployeeDetalPOJO item) {
            if (!item.getUserImage().equals("") && item.getUserImage() != null) {
                image = item.getUserImage();
            } else {
                image = String.valueOf(imageLatter(Utils.word(item.getEmployeeName()), item.getBgColor(), item.getFontColor()));
            }
            Spannable html;
            return Html.fromHtml("<font color=#0000FF >@" + item.getEmployeeName() + "</font>");
        }


        private String singleName(String firstName) {
            String[] nameFirst = firstName.split(" ");
            return nameFirst[0];
        }


    }

    private class CountryViewBinder implements AutoCompleteViewBinder<EmployeeDetalPOJO> {
        @Override
        public long getItemId(@NonNull EmployeeDetalPOJO item) {
            return item.getEmployeeName().hashCode();
        }

        @Override
        public int getItemLayoutId() {
            return R.layout.mentoning_item_employee_icons;
        }

        @NonNull
        @Override
        public AutoCompleteViewHolder getViewHolder(@NonNull View view) {
            return new MentoningViewHolder(view);
        }

        @Override
        public void bindData(@NonNull AutoCompleteViewHolder viewHolder, @NonNull EmployeeDetalPOJO item, @Nullable CharSequence constraint) {
            MentoningViewHolder itemViewHolder = (MentoningViewHolder) viewHolder;
            CharSequence countryLabel;
            if (constraint == null) {
                countryLabel = item.getEmployeeName();
            } else {
                countryLabel = new SpannableStringBuilder()
                        .append(SpannableUtils.setBoldSubText(item.getEmployeeName(), constraint))
                        .append(" ");
            }
//            if (ShardPreferences.get(context, ShardPreferences.share_language).equals("") || ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
//                itemViewHolder.tv_memberName.setGravity(Gravity.LEFT);
//
//            } else {
//            }

            itemViewHolder.tv_memberName.setText(countryLabel);
            if (!item.getUserImage().equals("") && item.getUserImage() != null) {
                Picasso.with(context).load(item.getUserImage()).into(itemViewHolder.employee_icon_iv);
            } else {
                itemViewHolder.employee_icon_iv.setImageDrawable(imageLatter(Utils.word(item.getEmployeeName()), item.getBgColor(), item.getFontColor()));
            }
        }
    }

    public TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(35)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }
}