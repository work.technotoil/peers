package com.peerbuckets.peerbucket.Activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.ToDoList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_TODO;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_TO_DRIVE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_DOCUMENT_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_TODO;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class GoogleDriveActivity extends AppCompatActivity implements View.OnClickListener,
        IApiResponse, EditorHtmlInterface {

    Button btn_add_to_do;
    EditText et_title, et_drive_link;
    RadioGroup radioGrp;
    WebView wv_editor;
    ApiRequest mApiRequest;
    Toolbar toolbar;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    Context context;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "", groupMembersUserId = "";
    String extension = "";
    RadioButton rb_doc, rb_sheet, rb_slide, rb_other;

    ProgressBar pb_progressBar;
    String messageTitle = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;

    String editorData = "", record_id = "", class_type = "", parent_id = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_drive);
        mApiRequest = new ApiRequest(this, this);
        context = this;
        initUI();
        initEditor();
        if (getIntent().getStringExtra("record_id") != null) {
            record_id = getIntent().getStringExtra("record_id");
            class_type = getIntent().getStringExtra("class_type");
            btn_add_to_do.setText(getResources().getString(R.string.update));
            loadDetails();
        }
        parent_id = getIntent().getStringExtra("parent_id");
    }

    private void initUI() {
        btn_add_to_do = findViewById(R.id.btn_add_to_do);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        back();
        pb_progressBar = findViewById(R.id.pb_progressBar);
        et_title = findViewById(R.id.et_title);
        et_drive_link = findViewById(R.id.et_drive_link);
        radioGrp = findViewById(R.id.radioGrp);
        rb_doc = findViewById(R.id.rb_doc);
        rb_other = findViewById(R.id.rb_other);
        rb_slide = findViewById(R.id.rb_slide);
        rb_sheet = findViewById(R.id.rb_sheet);
        getPrefrences();

        btn_add_to_do.setOnClickListener(this);

        if (record_id != null && !record_id.equals("")) {
            btn_add_to_do.setText(getResources().getString(R.string.update));
            getSupportActionBar().setTitle(getResources().getString(R.string.update_drive_document));
        } else {
            btn_add_to_do.setText(getResources().getString(R.string.add));
            getSupportActionBar().setTitle(getResources().getString(R.string.add_drive_document));
        }

    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void loadDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("file_id", record_id);
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequest(BASE_URL + GET_DOCUMENT_DETAIL, GET_DOCUMENT_DETAIL, paramsReq, Request.Method.POST);
    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
        mcurrentProjectTeamId = ShardPreferences.get(context, share_current_project_team_id);
        mcurrent_type = ShardPreferences.get(context, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
    }

    private void initEditor() {
        wv_editor = findViewById(R.id.wv_editor);

        wv_editor.getSettings().setJavaScriptEnabled(true);
        wv_editor.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);

        wv_editor.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });

        wv_editor.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        wv_editor.setWebContentsDebuggingEnabled(true);
        wv_editor.getSettings().setDomStorageEnabled(true);
        wv_editor.loadUrl("file:///android_asset/index1.html");

        wv_editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                wv_editor.loadUrl("javascript:copyContent()");
                wv_editor.loadUrl("javascript:hello()");
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        wv_editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_TO_DRIVE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {

                    JSONArray mArray = jsonObject.getJSONArray("documents");

                    Intent intent = getIntent();
                    intent.putExtra("documentData", mArray.toString());
                    intent.putExtra("title", et_title.getText().toString().trim());
                    intent.putExtra("desc", editorData);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(GET_DOCUMENT_DETAIL)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONObject jsonObjectDetail = jsonObject.getJSONObject("document");
                    updateUI(jsonObjectDetail);
                } else {

                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("EXCPTN", e.toString());
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void setData(String htmlData, String mention_uid) {
        editorData = htmlData;
        messageTitle = et_title.getText().toString();
        int selectedId = radioGrp.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(selectedId);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", mCurrentuserId);
        paramsReq.put("project_team_id", mcurrentProjectTeamId);
        paramsReq.put("company_id", mcompanyId);
        paramsReq.put("title", Utils.getUnicodeString(Utils.escapeUnicodeText(messageTitle)));
        paramsReq.put("description", Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData)));
        paramsReq.put("users", groupMembersUserId);
        paramsReq.put("extension", radioButton.getText().toString());
        paramsReq.put("drive_link", et_drive_link.getText().toString().trim());
        paramsReq.put("platform", "mobile");
        paramsReq.put("record_id", record_id);
        paramsReq.put("parent_id", parent_id);
        mApiRequest.postRequest(BASE_URL + ADD_TO_DRIVE, ADD_TO_DRIVE, paramsReq, Request.Method.POST);
    }


    private void updateUI(JSONObject jsonObject) {
        try {
            et_title.setText(Utils.checkStringWithEmpty(jsonObject.getString("document_title")));
            et_drive_link.setText(jsonObject.getString("name_on_disk"));

            if (jsonObject.getString("extension").equals("Doc")) {
                rb_doc.setChecked(true);
            } else if (jsonObject.getString("extension").equals("Sheet")) {
                rb_sheet.setChecked(true);
            } else if (jsonObject.getString("extension").equals("Other")) {
                rb_other.setChecked(true);
            } else if (jsonObject.getString("extension").equals("Slide")) {
                rb_slide.setChecked(true);
            }

            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
            String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
            String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
            String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
            String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
            String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
            String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
            String messageBody = jsonObject.getString("document_description");
            messageBody = messageBody.replace("\"", "'");

            String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                    "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                    "<script src=" + mentionJsPath + "></script>" +
                    "</head><body>";
            String editorHtml = "<form>" +
                    "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                    "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                    "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                    "</form>";
            String pas = "<script>" +
                    "function hello()" +
                    "{" +
                    "   var _id = document.getElementById('x').value;" +
                    "   var mention_uid = document.getElementById('mention_uid').value;" +
                    "    Android.nextScreen(_id,mention_uid);" +
                    "}" +
                    "</script></body></html>";
            String myHtmlString = pish + editorHtml + pas;
            wv_editor.loadData(myHtmlString, "text/html; charset=utf-8", null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
