package com.peerbuckets.peerbucket.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.message.AddMessageBoardActivity;
import com.peerbuckets.peerbucket.Adapter.InvitedUserAdapter;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_TODO_TASK;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_SINGLE_TODO_SUB_TASK;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_INVITED_USERS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_TODO_TASK;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;


public class ActivityAddEditNewToDoTask extends Activity implements IApiResponse, View.OnClickListener, EditorHtmlInterface {

    BottomSheetBehavior bottomSheetUsers;
    ApiRequest apiRequest;
    private Context context = this;
    private List<User> userList = new ArrayList<>();
    private InvitedUserAdapter userAdapter;
    RecyclerView recyclerUsers;
    WebView webViewAssignedUsers, webViewDesc;
    ImageView imgDoneBottomSheet, imgCancelBottomSheet;
    List<User> selectedUserList = new ArrayList<>();
    TextView tvAssignedTo, tvDueDate, tvStartDate, tvEndDate;
    String class_type = "", to_do_id = "", to_do_sub_id = "", to_do_title, date_on_selector = "", groupMembersUserId = "", dueDate = "", startDate = "", endDate = "";
    public static String TYPE_ADD = "add", TYPE_EDIT = "edit";
    RadioGroup radioGroupDate;
    RadioButton radioBtnNoDate, radionBtnDueDate, radioBtnMultiDate;
    private EditText etTitle;
    Button btnAddToDo;
    ProgressBar pb_progress;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    private String taskDesc = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    String assignedUser = "";
    TextView tvTitleToDo;
    String todo_master_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_todo_task);
        if (getIntent() != null) {
            class_type = getIntent().getStringExtra("class_type");
            to_do_id = getIntent().getStringExtra("to_do_id");
            to_do_sub_id = getIntent().getStringExtra("to_do_sub_id");
            to_do_title = getIntent().getStringExtra("to_do_title");

        }
        inIt();
        getInvitedUsers();
        if (class_type.equals(TYPE_EDIT)) {
            getSubToDoTaskDetails();
        }
    }

    private void inIt() {
        apiRequest = new ApiRequest(context, this);

        LinearLayout linearBottomSheet = findViewById(R.id.linear_bottom_sheet_user);
        bottomSheetUsers = BottomSheetBehavior.from(linearBottomSheet);
        recyclerUsers = findViewById(R.id.recycler_bottom_sheet_user);
        recyclerUsers.setLayoutManager(new LinearLayoutManager(context));

        recyclerUsers.setHasFixedSize(true);

        webViewAssignedUsers = findViewById(R.id.web_view_assigned_add_schedule);
        imgDoneBottomSheet = findViewById(R.id.img_done_bottom_sheet_user);
        imgCancelBottomSheet = findViewById(R.id.img_close_bottom_sheet_user);
        tvAssignedTo = findViewById(R.id.tv_assigned_add_to_do_task);

        btnAddToDo = findViewById(R.id.btn_add_to_do);
        pb_progress = findViewById(R.id.pb_progressBar);

        imgCancelBottomSheet.setOnClickListener(this);
        tvAssignedTo.setOnClickListener(this);
        imgDoneBottomSheet.setOnClickListener(this);
        btnAddToDo.setOnClickListener(this);

        tvTitleToDo = findViewById(R.id.tv_title_add_sub_to_do_task);
        radioGroupDate = findViewById(R.id.radio_group_add_to_do_task);
        radioBtnNoDate = findViewById(R.id.radio_btn_no_due_add_to_do_task);
        radionBtnDueDate = findViewById(R.id.radio_btn_due_date_add_to_do_task);
        radioBtnMultiDate = findViewById(R.id.radio_btn_multi_date_add_to_do_task);
        tvDueDate = findViewById(R.id.tv_pick_due_date_add_to_do_task);
        tvEndDate = findViewById(R.id.tv_end_date_add_to_do_task);
        tvStartDate = findViewById(R.id.tv_start_date_add_to_do_task);

        tvDueDate.setOnClickListener(this);
        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);

        etTitle = findViewById(R.id.et_title_add_to_do_task);

        radioGroupDate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == radioBtnNoDate.getId()) {
                    if (!radioBtnNoDate.isPressed()) {
                        return;
                    }
                    tvDueDate.setText("");
                    endDate = "";
                    startDate = "";
                    endDate = "";
                    tvStartDate.setText("");
                    tvEndDate.setText("");
                    tvDueDate.setVisibility(View.GONE);
                    tvStartDate.setVisibility(View.GONE);
                    tvEndDate.setVisibility(View.GONE);
                    date_on_selector = "no_due_date";
                } else if (checkedId == radionBtnDueDate.getId()) {
                    if (!radionBtnDueDate.isPressed()) {
                        return;
                    }
                    tvDueDate.setText("");
                    tvStartDate.setText("");
                    tvEndDate.setText("");
                    endDate = "";
                    startDate = "";
                    endDate = "";
                    tvDueDate.setVisibility(View.VISIBLE);
                    tvStartDate.setVisibility(View.GONE);
                    tvEndDate.setVisibility(View.GONE);
                    date_on_selector = "due_date";
                } else if (checkedId == radioBtnMultiDate.getId()) {
                    if (!radioBtnMultiDate.isPressed()) {
                        return;
                    }
                    tvDueDate.setText("");
                    tvStartDate.setText("");
                    tvEndDate.setText("");
                    endDate = "";
                    startDate = "";
                    endDate = "";
                    tvDueDate.setVisibility(View.GONE);
                    tvStartDate.setVisibility(View.VISIBLE);
                    tvEndDate.setVisibility(View.VISIBLE);
                    date_on_selector = "due_date_range";
                }
            }
        });

        webViewDesc = findViewById(R.id.webview_desc_add_schedule);
        webViewDesc.getSettings().setJavaScriptEnabled(true);
        webViewDesc.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webViewDesc.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
            }
        });

        webViewDesc.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        webViewDesc.setWebContentsDebuggingEnabled(true);
        webViewDesc.getSettings().setDomStorageEnabled(true);
        if (class_type.equals(TYPE_ADD)) {
            webViewDesc.loadUrl("file:///android_asset/index1.html");
        }
        webViewDesc.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", ShardPreferences.get(context, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(context, share_addd_remove_current_type));
        paramsReq.put("typeId", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        apiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }


    private void getSubToDoTaskDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("todo_list_task_id", to_do_sub_id);
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        apiRequest.postRequest(BASE_URL + FETCH_SINGLE_TODO_SUB_TASK, FETCH_SINGLE_TODO_SUB_TASK, paramsReq, Request.Method.POST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_done_bottom_sheet_user:
                assignedUser = "";
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                if (userAdapter != null) {
                    selectedUserList.clear();
                    selectedUserList = userAdapter.getSelectedUsers();
                    if (selectedUserList != null && selectedUserList.size() > 0) {
                        for (User selectedUser : selectedUserList) {
                            assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                        }
                        webViewAssignedUsers.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUser, "text/html", "utf-8", null);
                    } else {
                        webViewAssignedUsers.loadData("", "text/html", "utf-8");
                    }
                }
                break;
            case R.id.tv_assigned_add_to_do_task:

                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                } else {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
            case R.id.img_close_bottom_sheet_user:
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                }

                selectedUserList.clear();
                userAdapter.clearAllSelections();
                webViewAssignedUsers.loadData("", "text/html", "utf-8");
                break;

            case R.id.btn_add_to_do:
                pb_progress.setVisibility(View.VISIBLE);
                btnAddToDo.setVisibility(View.GONE);
                webViewDesc.loadUrl("javascript:copyContent()");
                webViewDesc.loadUrl("javascript:hello()");
                break;

            case R.id.tv_pick_due_date_add_to_do_task:
                selectDate(tvDueDate);
                break;
            case R.id.tv_start_date_add_to_do_task:
                selectDate(tvStartDate);
                break;
            case R.id.tv_end_date_add_to_do_task:
                selectDate(tvEndDate);
                break;
        }
    }

    private void getInvitedUsers() {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        apiRequest.postRequestBackground(BASE_URL + GET_INVITED_USERS, GET_INVITED_USERS, paramsReq, Request.Method.POST);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_INVITED_USERS)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray usersArray = jsonObject.getJSONArray("user_list");

                    if (usersArray.length() > 0) {
                        userList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                        }.getType());
                        if (userList != null && userList.size() > 0) {
                            userAdapter = new InvitedUserAdapter(userList, context);
                            recyclerUsers.setAdapter(userAdapter);
                        }
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(FETCH_SINGLE_TODO_SUB_TASK)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {

                    JSONObject jsonObjectDetail = jsonObject.getJSONObject("subtodo_details");

                } else {

                }

            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }


        if (tag_json_obj.equals(ADD_TODO_TASK)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                pb_progress.setVisibility(View.GONE);
                btnAddToDo.setVisibility(View.VISIBLE);
                if (jsonObject.getString("status").equals("1")) {
                    Intent intent = getIntent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                }
            } catch (JSONException e) {
                pb_progress.setVisibility(View.GONE);
                btnAddToDo.setVisibility(View.VISIBLE);
                e.printStackTrace();
                Toast.makeText(context, getResources().getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag_json_obj.equals(UPDATE_TODO_TASK)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                pb_progress.setVisibility(View.GONE);
                btnAddToDo.setVisibility(View.VISIBLE);
                if (jsonObject.getString("status").equals("1")) {
                    Intent intent = getIntent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                }
            } catch (JSONException e) {
                pb_progress.setVisibility(View.GONE);
                btnAddToDo.setVisibility(View.VISIBLE);
                e.printStackTrace();
                Toast.makeText(context, getResources().getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
            }
        }
        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        webViewDesc.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }

                        //recyclerEmployee

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void selectDate(final TextView tv) {
        int mYear, mMonth, mDay;

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(context, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                String cDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                String cTime = Utils.parseDateWithFormat(currentDateTimeString, "MMM dd, yyyy", "HH:mm:ss");
                tv.setText(cDate);
                if (tv == tvDueDate) {
                    dueDate = Utils.parseDateWithFormat(cDate + " " + cTime, "dd-MM-yyyy", "yyyy-MM-dd HH:mm:ss");
                } else if (tv == tvStartDate) {
                    startDate = Utils.parseDateWithFormat(cDate + " " + cTime, "dd-MM-yyyy", "yyyy-MM-dd HH:mm:ss");
                } else if (tv == tvEndDate) {
                    endDate = Utils.parseDateWithFormat(cDate + " " + cTime, "dd-MM-yyyy", "yyyy-MM-dd HH:mm:ss");
                }

            }
        }, mYear, mMonth, mDay);
        mDatePicker.show();
    }

    private String getAssignedUserString() {
        List<String> usersIdList = new ArrayList<>();
        if (selectedUserList != null && selectedUserList.size() > 0) {
            for (User userId : selectedUserList) {
                usersIdList.add(userId.getUserId());
            }
            return android.text.TextUtils.join(",", usersIdList);

        } else {
            return "";
        }

    }


    @Override
    public void setData(String htmlData, String mention_uid) {
        taskDesc = htmlData;
        if (!Utils.isStringValid(etTitle.getText().toString())) {
            Toast.makeText(context, getResources().getString(R.string.please_enter_title), Toast.LENGTH_SHORT).show();
        } else if (!radioBtnNoDate.isChecked() && !radionBtnDueDate.isChecked() && !radioBtnMultiDate.isChecked()) {
            Toast.makeText(context, getResources().getString(R.string.please_select_due_on), Toast.LENGTH_SHORT).show();
        } else if (radionBtnDueDate.isChecked() && !Utils.isStringValid(tvDueDate.getText().toString())) {
            Toast.makeText(context, getResources().getString(R.string.please_select_due_date), Toast.LENGTH_SHORT).show();
        } else if (radioBtnMultiDate.isChecked() && !Utils.isStringValid(tvStartDate.getText().toString()) && !Utils.isStringValid(tvEndDate.getText().toString())) {
            Toast.makeText(context, getResources().getString(R.string.please_select_start_end_date), Toast.LENGTH_SHORT).show();
        } else {
            if (class_type.equals(TYPE_ADD)) {

                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("todo_id", to_do_id);
                paramsReq.put("project_id", ShardPreferences.get(this, share_current_project_team_id));
                paramsReq.put("todo_list_task_title", Utils.getUnicodeString(Utils.escapeUnicodeText(etTitle.getText().toString())));
                paramsReq.put("new_todo_desc", Utils.getUnicodeString(Utils.escapeUnicodeText(taskDesc)));
                paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
                paramsReq.put("selector", date_on_selector);
                paramsReq.put("due_date", dueDate);
                paramsReq.put("start_date", startDate);
                paramsReq.put("end_date", endDate);
                paramsReq.put("timezone", "gmt");
                paramsReq.put("assign_users", groupMembersUserId);
                paramsReq.put("mention_user_id", mention_uid);
                apiRequest.postRequest(BASE_URL + ADD_TODO_TASK, ADD_TODO_TASK, paramsReq, Request.Method.POST);
            } else if (class_type.equals(TYPE_EDIT)) {

                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("task_id", to_do_sub_id);
                paramsReq.put("todo_id", todo_master_id);
                paramsReq.put("project_id", ShardPreferences.get(this, share_current_project_team_id));
                paramsReq.put("todo_list_task_title", Utils.getUnicodeString(Utils.escapeUnicodeText(etTitle.getText().toString())));
                paramsReq.put("new_todo_desc", Utils.getUnicodeString(Utils.escapeUnicodeText(taskDesc)));
                paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
                paramsReq.put("selector", date_on_selector);
                paramsReq.put("due_date", tvDueDate.getText().toString());
                paramsReq.put("start_date", tvStartDate.getText().toString());
                paramsReq.put("end_date", tvEndDate.getText().toString());
                paramsReq.put("timezone", "gmt");
                paramsReq.put("assign_users", groupMembersUserId);
                paramsReq.put("mention_user_id", mention_uid);
                apiRequest.postRequest(BASE_URL + UPDATE_TODO_TASK, UPDATE_TODO_TASK, paramsReq, Request.Method.POST);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
// Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
// Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = intent == null || resultCode != AddMessageBoardActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else
            Toast.makeText(getApplicationContext(), "Failed to Upload Image", Toast.LENGTH_LONG).show();
    }

    private void updateUI(JSONObject jsonObject) {
        try {
            todo_master_id = jsonObject.getString("list_id");
            etTitle.setText(Utils.checkStringWithEmpty(jsonObject.getString("todo_list_task_title")));
            JSONArray usersArray = jsonObject.getJSONArray("users");
            if (usersArray.length() > 0) {
                selectedUserList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                }.getType());

                if (selectedUserList != null && selectedUserList.size() > 0) {
                    for (User selectedUser : selectedUserList) {
                        assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                        if (userAdapter != null) {

                        }
                    }
                    if (userAdapter != null) {
                        userAdapter.updateSelectedUser(selectedUserList);
                    }
                }
                webViewAssignedUsers.loadData(assignedUser, "text/html", "utf-8");


            }

            if (Utils.isStringValid(jsonObject.getString("selector"))) {
                if (jsonObject.getString("selector").equals("no_due_date")) {
                    radioGroupDate.check(radioBtnNoDate.getId());
                    dueDate = "";
                    startDate = "";
                    endDate = "";
                    date_on_selector = "no_due_date";

                } else if (jsonObject.getString("selector").equals("due_date")) {
                    tvDueDate.setText(Utils.checkStringWithEmpty(jsonObject.getString("todo_list_task_start_date")));
                    radioGroupDate.check(radionBtnDueDate.getId());
                    tvDueDate.setVisibility(View.VISIBLE);

                    date_on_selector = "due_date";
                } else {
                    tvStartDate.setText(Utils.checkStringWithEmpty(jsonObject.getString("todo_list_task_start_date")));
                    tvEndDate.setText(Utils.checkStringWithEmpty(jsonObject.getString("todo_list_task_end_date")));
                    tvStartDate.setVisibility(View.VISIBLE);
                    tvEndDate.setVisibility(View.VISIBLE);
                    radioGroupDate.check(radioBtnMultiDate.getId());
                    date_on_selector = "due_date_range";
                }

            }

            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
            String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
            String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
            String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
            String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
            String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
            String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
            String messageBody = jsonObject.getString("todo_list_task_description");
            messageBody = messageBody.replace("\"", "'");

            String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                    "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                    "<script src=" + mentionJsPath + "></script>" +
                    "</head><body>";
            String editorHtml = "<form>" +
                    "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                    "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                    "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                    "</form>";
            String pas = "<script>" +
                    "function hello()" +
                    "{" +
                    "   var _id = document.getElementById('x').value;" +
                    "   var mention_uid = document.getElementById('mention_uid').value;" +
                    "    Android.nextScreen(_id,mention_uid);" +
                    "}" +
                    "</script></body></html>";
            String myHtmlString = pish + editorHtml + pas;
            webViewDesc.loadData(myHtmlString, "text/html; charset=utf-8", null);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
