package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class BookmarkActivity extends AppCompatActivity {
    ImageView imgBack;
    Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        imgBack = findViewById(R.id.imgBack);
        if (ShardPreferences.get(context,share_language).equals("2")) {
            imgBack.setImageResource(R.drawable.ic_arrow_forward);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
