package com.peerbuckets.peerbucket.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Adapter.ActivityScreenAdapter;
import com.peerbuckets.peerbucket.Adapter.AllEmployeAdapter;
import com.peerbuckets.peerbucket.Adapter.HeyListAdapter;
import com.peerbuckets.peerbucket.Adapter.SearchDialogAdapter;
import com.peerbuckets.peerbucket.Adapter.UserListChatAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.LatestActivityPOJO;
import com.peerbuckets.peerbucket.POJO.SearchResponse.CardListing;
import com.peerbuckets.peerbucket.POJO.SearchResponse.SearchResponse;
import com.peerbuckets.peerbucket.POJO.SearchResponse.UserListing;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.AllEmployeeList;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.LATEST_ACTIVITIES;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;


public class ActivityScreen extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private RecyclerView mactivity_recyclerView;
    private FloatingActionButton floatingActionButton;
    private LinearLayout mtab_start_hey_LL, closeFabLayout, mtab_start_me_LL, mtab_start_home_LL;
    private CardView fabLayout, mcv_search_expand;
    ApiRequest mApiRequest;
    boolean doubleBackToExitPressedOnce = false;
    int page = 0;
    private Boolean loading = true;
    SwipyRefreshLayout mSwipyRefreshLayout;
    String mCurrentuserId = "", mcompanyId = "";
    LinearLayoutManager mactivity_recyclerView_linearLayoutManager;
    ActivityScreenAdapter mactivity_recyclerView_adapter;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    ProgressBar progressBarPagination, pb_progress;
    TextView mempty_activity_tv;
    ArrayList<LatestActivityPOJO> mLatestActivityList;
    NestedScrollView nestedScrollView;
    boolean isByAnyone = false;
    boolean isEveryWhere = false;
    ArrayList<UserListing> userListingSpinner;
    ArrayList<CardListing> cardListingSpinner;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    Spinner sp_byAnyone, sp_everywhere;
    Context context = this;
    AllEmployeAdapter allEmployeAdapter;
    TextView tv_CompanyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);
        nestedScrollView = findViewById(R.id.LatestActivity_NestedScrollView);
        tv_CompanyName = findViewById(R.id.tv_CompanyName);
        progressBarPagination = findViewById(R.id.progress_bar_pagination);
        mSwipyRefreshLayout = findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                page = 0;
                fetchActivities(page);
            }
        });
        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        initUI();
        initToolbar();
        mCurrentuserId = ShardPreferences.get(ActivityScreen.this, share_current_UserId_key);
        mcompanyId = ShardPreferences.get(ActivityScreen.this, share_Company_Id_key);
        initRecyclerView();
        fetchActivities(page);
        applyPagination();
        tv_CompanyName.setText(ShardPreferences.get(context, share_current_company_name));

    }

    private void applyPagination() {

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            progressBarPagination.setVisibility(View.VISIBLE);
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data

                            fetchActivities(page);

                        }
                    }
                }
            }
        });
    }

    private void fetchActivities(int page) {
        progressBarPagination.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("userId", mCurrentuserId);
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("page", page + "");
        mApiRequest.postRequest(BASE_URL + LATEST_ACTIVITIES, LATEST_ACTIVITIES, paramsReq, Request.Method.POST);

    }

    private void initRecyclerView() {
        mactivity_recyclerView = findViewById(R.id.activity_recyclerView);
        mactivity_recyclerView.setHasFixedSize(true);

        mactivity_recyclerView_linearLayoutManager = new LinearLayoutManager(this);
        mactivity_recyclerView.setLayoutManager(mactivity_recyclerView_linearLayoutManager);
    }

    private void initUI() {

        floatingActionButton = findViewById(R.id.fab);
        fabLayout = findViewById(R.id.fab_layout);
        closeFabLayout = findViewById(R.id.close_fab_layout);

        mtab_start_hey_LL = (LinearLayout) findViewById(R.id.tab_start_hey_LL);
        mtab_start_home_LL = (LinearLayout) findViewById(R.id.tab_start_home_LL);
        mtab_start_me_LL = (LinearLayout) findViewById(R.id.tab_start_me_LL);
        mcv_search_expand = (CardView) findViewById(R.id.cv_search_expand);
        mempty_activity_tv = findViewById(R.id.empty_activity_tv);

        mcv_search_expand.setOnClickListener(this);
        closeFabLayout.setOnClickListener(this);
        floatingActionButton.setOnClickListener(this);
        mtab_start_hey_LL.setOnClickListener(this);
        mtab_start_me_LL.setOnClickListener(this);
        mtab_start_home_LL.setOnClickListener(this);

    }

    private void initToolbar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(ShardPreferences.get(this, share_current_company_name));

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_search, null);
                dialogBuilder.setView(dialogView);
                ImageView tv_back;
                EditText et_searchDialog;
                Spinner sp_everything;
                RecyclerView rv_searchDialog;
                RecyclerView.LayoutManager rv_searchDialogLayoutManager;
                SearchDialogAdapter searchDialogAdapter;
                tv_back = dialogView.findViewById(R.id.tv_back);
                et_searchDialog = dialogView.findViewById(R.id.et_searchDialog);
                sp_everything = dialogView.findViewById(R.id.sp_everything);
                sp_byAnyone = dialogView.findViewById(R.id.sp_byAnyone);
                sp_everywhere = dialogView.findViewById(R.id.sp_everywhere);
                rv_searchDialog = dialogView.findViewById(R.id.rv_searchDialog);
                if (ShardPreferences.get(context,share_language).equals("2")){
                    tv_back.setImageResource(R.drawable.ic_arrow_forward);
                }
                rv_searchDialog.setHasFixedSize(true);
                rv_searchDialogLayoutManager = new LinearLayoutManager(context);
                rv_searchDialog.setLayoutManager(rv_searchDialogLayoutManager);

                final AlertDialog alertDialog = dialogBuilder.create();
                WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.TOP;
                alertDialog.show();
                sp_byAnyone.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isByAnyone = true;
                        return false;
                    }
                });
                sp_everywhere.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isEveryWhere = true;
                        return false;
                    }
                });

                // call api of set data
                callSearch();

                tv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                et_searchDialog.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                return true;

           /* case R.id.admin_land:
                Intent intent = new Intent(this, AdminLandActivity.class);
                startActivity(intent);
                return true;*/
            case R.id.im_logout:
                AlertDialog logoutDialog = new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.logout))
                        .setMessage(getResources().getString(R.string.logout_info))
                        .setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent10 = new Intent(context, DeciderActivity.class);
                                startActivity(intent10);
                                ShardPreferences.clearPreference(context);
                                finishAffinity();
                            }
                        }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                logoutDialog.show();
                return true;

            case R.id.im_setting:
                Intent intentSetting = new Intent(context, SettingActivity.class);
                startActivity(intentSetting);
                return true;
            case R.id.message:
                callAllEmp();
                addRemovePeopleDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.please_click_BACK_again_to_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void callAllEmp() {
        Map<String, String> map = new HashMap<>();
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));
        map.put("user_id", ShardPreferences.get(context, share_UserId_key));
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllEmployeeList> resultCall = apiInterface.callAllEmployeeList(map);
        resultCall.enqueue(new Callback<AllEmployeeList>() {
            @Override
            public void onResponse(Call<AllEmployeeList> call, Response<AllEmployeeList> response) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("true")) {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                    if (response.body().getEmployeeList().size() > 0) {
                        empList(response.body().getEmployeeList());
                    } else {
                        tv_noMember.setVisibility(View.VISIBLE);
                    }
                } else {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AllEmployeeList> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
            }
        });
    }

    private void empList(List<EmployeeList> list) {
        if (list != null && list.size() > 0) {
            allEmployeAdapter = new AllEmployeAdapter(context, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        } else {
            list = new ArrayList<>();
            allEmployeAdapter = new AllEmployeAdapter(context, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_start_hey_LL:
                Intent startHeyActivity = new Intent(this, HeyActivity.class);
                startActivity(startHeyActivity);
                finish();
                break;

            case R.id.tab_start_me_LL:
                Intent activity = new Intent(this, MeActivity.class);
                startActivity(activity);
                finish();
                break;

            case R.id.tab_start_home_LL:
                Intent activity1 = new Intent(this, HomeActivity.class);
                startActivity(activity1);
                finish();
                break;

            case R.id.fab:
                floatingActionButton.setVisibility(View.GONE);
                fabLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.close_fab_layout:
                fabLayout.setVisibility(View.GONE);
                floatingActionButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(LATEST_ACTIVITIES)) {
            mSwipyRefreshLayout.setRefreshing(false);
            progressBarPagination.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    JSONArray jsonArrayActivity = jsonObject.getJSONArray("activities");
                    if (jsonArrayActivity.length() > 0) {

                        if (page == 0 && mLatestActivityList != null) {
                            mLatestActivityList.clear();
                        }

                        mLatestActivityList = new Gson().fromJson(jsonArrayActivity.toString(), new TypeToken<List<LatestActivityPOJO>>() {
                        }.getType());
                        if (mLatestActivityList != null && mLatestActivityList.size() > 0) {
                            mactivity_recyclerView.setVisibility(View.VISIBLE);
                            if (page == 0) {
                                mactivity_recyclerView_adapter = new ActivityScreenAdapter(mLatestActivityList, this);
                                mactivity_recyclerView.setAdapter(mactivity_recyclerView_adapter);

                            } else {
                                mactivity_recyclerView.post(new Runnable() {
                                    public void run() {
                                        mactivity_recyclerView_adapter.setNotofyData(mLatestActivityList);
                                    }
                                });
                            }
                            page = page + 1;
                            loading = true;
                            progressBarPagination.setVisibility(View.GONE);
                        } else {
                            mempty_activity_tv.setVisibility(View.GONE);
                        }
                    } else {
                        if (page == 0) {
                        }
                    }
                } else {
                    loading = false;
                    progressBarPagination.setVisibility(View.GONE);
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                progressBarPagination.setVisibility(View.GONE);
                e.printStackTrace();
            }

        } else if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            mShimmerDialog.stopShimmerAnimation();
            mShimmerDialog.setVisibility(View.GONE);
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new UserListChatAdapter(mEmployeeDetailList, this);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void callSearch() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> resultCall = apiInterface.callSearch(map);
        resultCall.enqueue(new Callback<SearchResponse>() {

            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.body().getStatus()) {
                    userListingSpinner = (ArrayList<UserListing>) response.body().getUserListing();
                    userList();
                    cardListingSpinner = (ArrayList<CardListing>) response.body().getCardListing();
                    cardList();
                } else {
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
            }
        });

    }

    public void userList() {

        String[] mStringArray = new String[userListingSpinner.size()];
        String check_company_id = ShardPreferences.get(context, ShardPreferences.key_userSpinner_id);
        int setposition = 0;

        for (int i = 0; i < userListingSpinner.size(); i++) {
            mStringArray[i] = userListingSpinner.get(i).getEmployeeName();
            if (check_company_id.equals(userListingSpinner.get(i).getUserId())) {
                setposition = i;
                ShardPreferences.save(context, ShardPreferences.key_userSpinner_id, userListingSpinner.get(i).getUserId());
            }
        }

        ArrayAdapter aa = new ArrayAdapter(context, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_byAnyone.setAdapter(aa);
        sp_byAnyone.setSelection(setposition, false);
    }

    public void cardList() {
        String[] mStringArray = new String[cardListingSpinner.size()];
        String check_company_id = ShardPreferences.get(context, ShardPreferences.key_cardListingSpinner_id);
        int setposition = 0;

        for (int i = 0; i < cardListingSpinner.size(); i++) {
            mStringArray[i] = cardListingSpinner.get(i).getProjectTeamName();
            if (check_company_id.equals(cardListingSpinner.get(i).getProjectTeamId())) {
                setposition = i;
                ShardPreferences.save(context, ShardPreferences.key_cardListingSpinner_id, cardListingSpinner.get(i).getProjectTeamId());
            }
        }
        ArrayAdapter aa = new ArrayAdapter(context, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_everywhere.setAdapter(aa);
        sp_everywhere.setSelection(setposition, false);
    }

    RelativeLayout rl_pingChatHome;
    ShimmerFrameLayout mShimmerDialog;
    UserListChatAdapter mAddRemoveUserAdapter;
    RecyclerView rv_chaOptionHome;
    AutoCompleteTextView et_search;
    TextView tv_noMember;
    ImageView btnSend;

    public void addRemovePeopleDialog() {
        Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_user_chat, null);
        dialogBuilder.setContentView(dialogView);
        tv_noMember = dialogView.findViewById(R.id.tv_noMember);
        pb_progress = dialogView.findViewById(R.id.pb_progress);
        et_search = dialogView.findViewById(R.id.et_search);
        rv_chaOptionHome = dialogView.findViewById(R.id.rv_chaOptionHome);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_chaOptionHome.setLayoutManager(mLayoutManager);
        rl_pingChatHome = dialogView.findViewById(R.id.rl_pingChatHome);
        btnSend = dialogView.findViewById(R.id.btnSend);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    allEmployeAdapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_search.getText().toString().equals("")) {
                    try {
                        allEmployeAdapter.getFilter().filter(et_search.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        dialogBuilder.show();
    }
}
