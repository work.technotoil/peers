package com.peerbuckets.peerbucket.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInListActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleActivity;
import com.peerbuckets.peerbucket.Activity.todo.ToDoActivity;
import com.peerbuckets.peerbucket.Adapter.AllEmployeAdapter;
import com.peerbuckets.peerbucket.Adapter.CompnyEmpolyAdapter;
import com.peerbuckets.peerbucket.Adapter.HomeAdapter;
import com.peerbuckets.peerbucket.Adapter.IteamShowAdapter;
import com.peerbuckets.peerbucket.Adapter.MinEmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.SearchDialogAdapter;
import com.peerbuckets.peerbucket.Adapter.UserListChatAdapter;
import com.peerbuckets.peerbucket.Hpojo.CompanyList;
import com.peerbuckets.peerbucket.Hpojo.HomeModule;
import com.peerbuckets.peerbucket.Hpojo.HomePojo;
import com.peerbuckets.peerbucket.Hpojo.ModuleList;
import com.peerbuckets.peerbucket.POJO.AddModulePojo;
import com.peerbuckets.peerbucket.POJO.AllCompanyListPOJO;
import com.peerbuckets.peerbucket.POJO.CompanyListPOJO;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.SearchResponse.CardListing;
import com.peerbuckets.peerbucket.POJO.SearchResponse.SearchResponse;
import com.peerbuckets.peerbucket.POJO.SearchResponse.UserListing;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.AllEmployeeList;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.localization.ActivityHockeyApp;
import com.peerbuckets.peerbucket.localization.LocaleHelper;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.COMPANY_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_ANDROID;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_EMPLOYEE_PROFILE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.get;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.getInt;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.key_company_logo;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_profile_url;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, IApiResponse, RefreshCommentInterface {
    Context context = this;
    SwipyRefreshLayout mSwipyRefreshLayout;
    RecyclerView memployee_icon_recycler_view;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    RecyclerView.Adapter memployee_icon_recycler_view_adapter;
    ArrayList<CompanyListPOJO> list;
    ArrayList<ModuleList> ModList;
    ArrayList<HomeModule> HomeModList;
    boolean doubleBackToExitPressedOnce = false;
    ArrayList<AllCompanyListPOJO> mAllCompanylist;
    ArrayList<UserListing> userListingSpinner;
    ArrayList<CardListing> cardListingSpinner;
    RecyclerView rv_addedTeam_projectLit;
    boolean isSpinnerTouched = false;
    boolean isByAnyone = false;
    boolean isEveryWhere = false;
    HomeAdapter ListAdapter;
    IteamShowAdapter iteamShowAdapter;
    AutoCompleteTextView et_searchDialog;
    PopupMenu popup;
    AllEmployeAdapter allEmployeAdapter;
    CompnyEmpolyAdapter compnyEmpolyAdapter;
    Button upgradeAccount;
    List<CompanyList> companyLists;
    CardView mcv_search_expand, fabLayout, cv_company_hq;
    ProgressBar progressBar, pb_progress;
    private ApiRequest mApiRequest;
    LinearLayout mtab_start_hey_LL, mtab_start_activity_LL, mtab_start_me_LL,
            mdialog_tcp_chat, mdialog_tcp_message_board, mdialog_tcp_automatic_checkins, mdialog_tcp_docs_and_files,
            mdialog_tcp_todos, mdialog_tcp_schedule, mcompany_LL, mno_company_LL, ll_cmpnay;
    String mcompanyId = "", muserId = "", memployeeId = "", mcurrentProjectTeamId = "",
            mcurrentCompanyName = "", maddremovetype = "test_hq", mCurrentuserId = "", jump = "", record_id = "",
            record_type = "", project_team_id = "", message_id = "", todo_id = "";
    Spinner mtoolbar_spinner;
    TextView mcompany_name_tv, mcompany_desc_tv, mdelete_tv;
    LinearLayout expired_company;
    NestedScrollView nestedScrollView;
    CardView bottomTab, changeCompanyLogo;
    RecyclerView.LayoutManager rv_addedTeam_projectLitLayoutManager;
    ImageView img_logo;
    LinearLayout ll_image_logo;
    FrameLayout fl_addNewModule;
    EditText et_newModule;
    Spinner sp_byAnyone, sp_everywhere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
            LocaleHelper.setLocale(context, "ar");
            setApplicationLanguage("ar");
            LocaleHelper.getLanguage(context);
        } else {
            LocaleHelper.setLocale(context, "en");
            setApplicationLanguage("en");
        }

        setContentView(R.layout.activity_home);

        mApiRequest = new ApiRequest(context, this);
        initUI();

        if (getIntent().getStringExtra("jump") != null) {
            jump = getIntent().getStringExtra("jump");
        }
        if (getIntent().getStringExtra("record_type") != null) {
            record_type = getIntent().getStringExtra("record_type");
        }
        if (getIntent().getStringExtra("todo_id") != null) {
            todo_id = getIntent().getStringExtra("todo_id");
        }

        if (getIntent().getStringExtra("message_id") != null) {
            message_id = getIntent().getStringExtra("message_id");
            if (jump.equals("2")) {
                Intent intent = new Intent(context, Activity_MessageBoard.class);
                intent.putExtra("message_id", getIntent().getStringExtra("message_id"));
                intent.putExtra("direct", "direct");
                startActivity(intent);
            }
        }

        if (getIntent().getStringExtra("pingId") != null) {
            if (jump.equals("1")) {
                Intent intent = new Intent(context, PingActivity.class);
                intent.putExtra("user_id", getIntent().getStringExtra("pingId"));
                startActivity(intent);
            }
        }
        if (getIntent().getStringExtra("project_team_id") != null) {
            project_team_id = getIntent().getStringExtra("project_team_id");

        }
        if (getIntent().getStringExtra("record_id") != null) {
            Intent intent;
            switch (record_type) {
                case "message":
                    intent = new Intent(context, Activity_MessageBoard.class);
                    intent.putExtra("direct", "direct");
                    intent.putExtra("project_team_id", project_team_id);
                    intent.putExtra("message_id", getIntent().getStringExtra("record_id"));
                    startActivity(intent);
                    break;
                case "task":
                case "todo_list":
                case "todo":
                case "sub-todo":
                    intent = new Intent(context, ToDoActivity.class);
                    intent.putExtra("record_id", getIntent().getStringExtra("record_id"));
                    intent.putExtra("project_team_id", project_team_id);
                    intent.putExtra("todo_id", todo_id);
                    startActivity(intent);
                    break;
            }
        }

        getPreferences();
        getEmployeeDetails();
        callApi();

        mtoolbar_spinner.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isSpinnerTouched = true;
                return false;
            }
        });

        et_searchDialog.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    ListAdapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setApplicationLanguage(String newLanguage) {
        Resources activityRes = getResources();
        Configuration activityConf = activityRes.getConfiguration();
        Locale newLocale = new Locale(newLanguage);
        activityConf.setLocale(newLocale);
        activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

        Resources applicationRes = getApplicationContext().getResources();
        Configuration applicationConf = applicationRes.getConfiguration();
        applicationConf.setLocale(newLocale);
        applicationRes.updateConfiguration(applicationConf,
                applicationRes.getDisplayMetrics());
    }

    private void search(String search) {
        for (int i = 0; i < ModList.size(); i++) {
            if (search.equals(ModList.get(i).getModuleName())) {
                ModList.get(i).setSelected(true);
                ListAdapter.check();
            } else {
                for (int j = 0; j < HomeModList.size(); j++) {
                    if (search.equals(HomeModList.get(j).getProjectTeamName())) {

                        HomeModList.get(j).setSelected(true);
                        ListAdapter.check();
                    }
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.please_click_BACK_again_to_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void getEmployeeDetails() {
        Map<String, String> paramsReqCL = new HashMap<>();
        paramsReqCL.put("user_id", mCurrentuserId);
        mApiRequest.postRequestBackground(BASE_URL + GET_EMPLOYEE_PROFILE, GET_EMPLOYEE_PROFILE, paramsReqCL, Request.Method.POST);
    }

    private void getPreferences() {
        mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
        muserId = ShardPreferences.get(context, share_UserId_key);
        mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
        memployeeId = ShardPreferences.get(context, share_employee_Id_key);
    }

    private void initUI() {
        et_searchDialog = findViewById(R.id.et_searchDialog);
        progressBar = findViewById(R.id.pb_progress);
        ll_image_logo = findViewById(R.id.ll_image_logo);

        mSwipyRefreshLayout = findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                callApi();
                getEmployeeDetails();
            }
        });
        cv_company_hq = findViewById(R.id.cv_company_hq);
        img_logo = findViewById(R.id.img_logo);
        nestedScrollView = findViewById(R.id.nestedScrollView);

        progressBar.setVisibility(View.VISIBLE);
        nestedScrollView.setVisibility(View.GONE);

        fl_addNewModule = findViewById(R.id.fl_addNewModule);
        expired_company = findViewById(R.id.expired_company);
        upgradeAccount = findViewById(R.id.upgradeAccount);
        upgradeAccount.setOnClickListener(this);

        bottomTab = findViewById(R.id.bottomTab);
        changeCompanyLogo = findViewById(R.id.changeCompanyLogo);
        rv_addedTeam_projectLit = findViewById(R.id.rv_addedTeam_projectLit);
        rv_addedTeam_projectLit.setHasFixedSize(true);
        rv_addedTeam_projectLitLayoutManager = new LinearLayoutManager(context);
        rv_addedTeam_projectLit.setLayoutManager(rv_addedTeam_projectLitLayoutManager);

        GridLayoutManager mGrid = new GridLayoutManager(this, 2);
        mGrid.setOrientation(RecyclerView.VERTICAL);
        GridLayoutManager mGridProject = new GridLayoutManager(this, 2);
        mGridProject.setOrientation(RecyclerView.VERTICAL);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mtoolbar_spinner = toolbar.findViewById(R.id.toolbar_spinner);
        mcv_search_expand = findViewById(R.id.cv_search_expand);
        mtab_start_hey_LL = findViewById(R.id.tab_start_hey_LL);
        mtab_start_me_LL = findViewById(R.id.tab_start_me_LL);
        mcompany_LL = findViewById(R.id.company_LL);
        mno_company_LL = findViewById(R.id.no_company_LL);
        ll_cmpnay = findViewById(R.id.ll_cmpnay);

        mdelete_tv = findViewById(R.id.delete_tv);

        mcompany_name_tv = findViewById(R.id.company_name_tv);
        mcompany_desc_tv = findViewById(R.id.company_desc_tv);


        mtab_start_activity_LL = findViewById(R.id.tab_start_activity_LL);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsingToolbarLayoutTitleColor);

        fabLayout = findViewById(R.id.fab_layout);

        memployee_icon_recycler_view = findViewById(R.id.employee_icon_recycler_view);

        memployee_icon_recycler_view.setHasFixedSize(true);

        memployee_icon_recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mcompany_LL.setOnClickListener(this);
        mcv_search_expand.setOnClickListener(this);
        mtab_start_hey_LL.setOnClickListener(this);
        mtab_start_activity_LL.setOnClickListener(this);
        fl_addNewModule.setOnClickListener(this);
        mtab_start_me_LL.setOnClickListener(this);
        mtoolbar_spinner.setOnItemSelectedListener(this);
        mdelete_tv.setOnClickListener(this);
        changeCompanyLogo.setOnClickListener(this);
        img_logo.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        Map<String, String> paramsReqCL = new HashMap<>();
        paramsReqCL.put("user_id", mCurrentuserId);
        mApiRequest.postRequestBackground(BASE_URL + GET_EMPLOYEE_PROFILE, GET_EMPLOYEE_PROFILE, paramsReqCL, Request.Method.POST);
        paramsReqCL.put("userId", mCurrentuserId);
        mApiRequest.postRequestBackground(BASE_URL + COMPANY_LIST, COMPANY_LIST, paramsReqCL, Request.Method.POST);
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", maddremovetype);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_ANDROID, FETCH_EMPLOYEE_ANDROID, paramsReq, Request.Method.POST);

    }

    private void callApi() {
        Map<String, String> paramsReqCL = new HashMap<>();
        paramsReqCL.put("userId", mCurrentuserId);
        mApiRequest.postRequestBackground(BASE_URL + COMPANY_LIST, COMPANY_LIST, paramsReqCL, Request.Method.POST);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection

        switch (item.getItemId()) {

            case R.id.search:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_search, null);
                dialogBuilder.setView(dialogView);
                ImageView tv_back;
                EditText et_searchDialog;
                Spinner sp_everything;
                RecyclerView rv_searchDialog;
                RecyclerView.LayoutManager rv_searchDialogLayoutManager;
                SearchDialogAdapter searchDialogAdapter;

                tv_back = dialogView.findViewById(R.id.tv_back);
                et_searchDialog = dialogView.findViewById(R.id.et_searchDialog);
                sp_everything = dialogView.findViewById(R.id.sp_everything);
                sp_byAnyone = dialogView.findViewById(R.id.sp_byAnyone);
                sp_everywhere = dialogView.findViewById(R.id.sp_everywhere);
                rv_searchDialog = dialogView.findViewById(R.id.rv_searchDialog);
                if (ShardPreferences.get(context, share_language).equals("2")) {
                    tv_back.setImageResource(R.drawable.ic_arrow_forward);
                }
                rv_searchDialog.setHasFixedSize(true);
                rv_searchDialogLayoutManager = new LinearLayoutManager(context);
                rv_searchDialog.setLayoutManager(rv_searchDialogLayoutManager);

                final AlertDialog alertDialog = dialogBuilder.create();
                WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.TOP;
                alertDialog.show();
                sp_byAnyone.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isByAnyone = true;
                        return false;
                    }
                });
                sp_everywhere.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        isEveryWhere = true;
                        return false;
                    }
                });
                // call api of set data
                callSearch();
                tv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                et_searchDialog.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                return true;
            case R.id.message:
                callAllEmp();
                addRemovePeopleDialog();
                return true;
            /*case R.id.im_manageTemplates:
                View view = new View(this);
                item.setActionView(view);
                view.setPadding(16, 0, 16, 0);
                Toast.makeText(context, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.admin_land:
                Intent intent = new Intent(this, AdminLandActivity.class);
                startActivity(intent);
                return true;*/
            case R.id.im_setting:
                Intent intentSetting = new Intent(context, SettingActivity.class);
                startActivityForResult(intentSetting, 102);
                return true;
            case R.id.im_logout:
                AlertDialog logoutDialog = new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.logout))
                        .setMessage(getResources().getString(R.string.logout_info))
                        .setPositiveButton(getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent10 = new Intent(context, DeciderActivity.class);
                                startActivity(intent10);
                                ShardPreferences.clearPreference(context);
                                finishAffinity();
                            }
                        }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                logoutDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callAllEmp() {
        Map<String, String> map = new HashMap<>();
        map.put("company_id", mcompanyId);
        map.put("user_id", muserId);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AllEmployeeList> resultCall = apiInterface.callAllEmployeeList(map);
        resultCall.enqueue(new Callback<AllEmployeeList>() {
            @Override
            public void onResponse(Call<AllEmployeeList> call, Response<AllEmployeeList> response) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("true")) {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                    if (response.body().getEmployeeList().size() > 0) {
                        empList(response.body().getEmployeeList());
                    } else {
                        tv_noMember.setVisibility(View.VISIBLE);
                    }
                } else {
                    pb_progress.setVisibility(View.GONE);
                    rl_pingChatHome.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<AllEmployeeList> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
                rl_pingChatHome.setVisibility(View.VISIBLE);
            }
        });
    }

    private void empList(List<EmployeeList> list) {
        if (list != null && list.size() > 0) {
            allEmployeAdapter = new AllEmployeAdapter(context, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        } else {
            list = new ArrayList<>();
            allEmployeAdapter = new AllEmployeAdapter(context, (ArrayList<EmployeeList>) list);
            rv_chaOptionHome.setAdapter(allEmployeAdapter);
        }
    }

    RelativeLayout rl_pingChatHome;
    RecyclerView rv_chaOptionHome;
    AutoCompleteTextView et_search;
    TextView tv_noMember;
    ImageView btnSend;

    public void addRemovePeopleDialog() {
        Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_user_chat, null);
        dialogBuilder.setContentView(dialogView);
        tv_noMember = dialogView.findViewById(R.id.tv_noMember);
        pb_progress = dialogView.findViewById(R.id.pb_progress);
        et_search = dialogView.findViewById(R.id.et_search);
        rv_chaOptionHome = dialogView.findViewById(R.id.rv_chaOptionHome);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rv_chaOptionHome.setLayoutManager(mLayoutManager);
        rl_pingChatHome = dialogView.findViewById(R.id.rl_pingChatHome);
        btnSend = dialogView.findViewById(R.id.btnSend);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    allEmployeAdapter.getFilter().filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_search.getText().toString().equals("")) {
                    try {
                        allEmployeAdapter.getFilter().filter(et_search.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        dialogBuilder.show();
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.company_LL:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();
                @SuppressLint("InflateParams")
                View dialogView = inflater.inflate(R.layout.dialog_tcp_expand, null);
                dialogBuilder.setView(dialogView);

                ImageView imageView = dialogView.findViewById(R.id.expand_company_hq);
                TextView addorRemtextView = dialogView.findViewById(R.id.add_remove);
                TextView mcompany_expand_name_tv = dialogView.findViewById(R.id.company_expand_name_tv);
                TextView mcompany_desc_expand_tv = dialogView.findViewById(R.id.company_desc_expand_tv);
                mcompany_expand_name_tv.setText(Utils.unescapeJava(companyLists.get(0).getCompanyName()));
                mcompany_desc_expand_tv.setText(Utils.unescapeJava(companyLists.get(0).getProjectTeamDescription()));

                mdialog_tcp_chat = dialogView.findViewById(R.id.dialog_tcp_chat);
                mdialog_tcp_message_board = dialogView.findViewById(R.id.dialog_tcp_message_board);
                mdialog_tcp_automatic_checkins = dialogView.findViewById(R.id.dialog_tcp_automatic_checkins);
                mdialog_tcp_docs_and_files = dialogView.findViewById(R.id.dialog_tcp_docs_and_files);
                mdialog_tcp_todos = dialogView.findViewById(R.id.dialog_tcp_todos);
                mdialog_tcp_schedule = dialogView.findViewById(R.id.dialog_tcp_schedule);
                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                addorRemtextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        Intent intent = new Intent(context, AddRemovePeopleActivity.class);
                        intent.putExtra("Directions", Utils.unescapeJava(companyLists.get(0).getCompanyName()));
                        intent.putExtra("ProjectTeamId", companyLists.get(0).getProjectTeamId());
                        startActivityForResult(intent, 11);
                        alertDialog.dismiss();
                    }
                });


                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        Intent intent = new Intent(context, CompanyHQExpandActivity.class);
                        intent.putExtra("description", Utils.unescapeJava(companyLists.get(0).getProjectTeamDescription()));
                        intent.putExtra("Directions", Utils.unescapeJava(companyLists.get(0).getCompanyName()));
                        intent.putExtra("projectId", companyLists.get(0).getProjectTeamId());
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra("name", Utils.unescapeJava(companyLists.get(0).getCompanyName()));
                        intent.putExtra("description", Utils.unescapeJava(companyLists.get(0).getProjectTeamDescription()));
                        intent.putExtra("projectId", companyLists.get(0).getProjectTeamId());
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_message_board.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, Activity_MessageBoard.class);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_automatic_checkins.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(HomeActivity.this, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, CheckInListActivity.class);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_docs_and_files.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Toast.makeText(HomeActivity.this, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, DocsAndFilesActivity.class);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_todos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_module, Utils.unescapeJava(mcurrentCompanyName));
                        Intent intent = new Intent(context, ToDoActivity.class);
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_schedule.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Toast.makeText(HomeActivity.this, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                        ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        Intent intent = new Intent(context, ScheduleActivity.class);
                        startActivity(intent);
                        alertDialog.dismiss();
                    }
                });
                break;

            case R.id.tab_start_hey_LL:
                Intent startHeyActivity = new Intent(context, HeyActivity.class);
                startActivity(startHeyActivity);
                finish();
                break;

            case R.id.upgradeAccount:
                Toast.makeText(context, getResources().getString(R.string.work_on_way), Toast.LENGTH_SHORT).show();
                break;

            case R.id.tab_start_activity_LL:
                Intent activity = new Intent(context, ActivityScreen.class);
                startActivity(activity);
                finish();
                break;
            case R.id.fl_addNewModule:
                AlertDialog.Builder ModuleDialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater ModuleInflater = this.getLayoutInflater();
                View ModuleView = ModuleInflater.inflate(R.layout.dialog_add_new_module, null);
                ModuleDialogBuilder.setView(ModuleView);
                final ProgressBar pb_progress;
                pb_progress = ModuleView.findViewById(R.id.pb_progress);
                TextView tv_addModuleDialog = ModuleView.findViewById(R.id.tv_addModuleDialog);
                ImageView imgClose = ModuleView.findViewById(R.id.imgClose);
                et_newModule = ModuleView.findViewById(R.id.et_newModule);
                Button btnClose = ModuleView.findViewById(R.id.btnClose);
                final Button btnAdd = ModuleView.findViewById(R.id.btnAdd);
                tv_addModuleDialog.setText(getResources().getString(R.string.add_new_module));
                btnAdd.setText(getResources().getString(R.string.add));
                final AlertDialog addDialog = ModuleDialogBuilder.create();
                addDialog.show();

                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addDialog.dismiss();
                    }
                });

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addDialog.dismiss();
                    }
                });

                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pb_progress.setVisibility(View.VISIBLE);
                        btnAdd.setVisibility(View.GONE);
                        Map<String, String> map = new HashMap<>();
                        map.put("user_id", mCurrentuserId);
                        map.put("company_id", mcompanyId);
                        map.put("title", Utils.getUnicodeString(Utils.escapeUnicodeText(et_newModule.getText().toString())));
                        map.put("module_id", "");

                        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<AddModulePojo> resultCall = apiInterface.CallCreateModule(map);
                        resultCall.enqueue(new Callback<AddModulePojo>() {

                            @Override
                            public void onResponse(Call<AddModulePojo> call, Response<AddModulePojo> response) {
                                pb_progress.setVisibility(View.GONE);
                                btnAdd.setVisibility(View.VISIBLE);
                                if (response.body().getStatus().equals("true")) {
                                    pb_progress.setVisibility(View.GONE);
                                    btnAdd.setVisibility(View.VISIBLE);
                                    addDialog.dismiss();
                                    homeApi(mcompanyId);
                                } else {
                                    pb_progress.setVisibility(View.GONE);
                                    btnAdd.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onFailure(Call<AddModulePojo> call, Throwable t) {
                                pb_progress.setVisibility(View.GONE);
                                btnAdd.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });


                break;

            case R.id.tab_start_me_LL:
                ShardPreferences.save(context, share_current_project_team_id, mcurrentProjectTeamId);
                ShardPreferences.save(context, share_addd_remove_current_type, "test_hq");
                Intent activity1 = new Intent(context, MeActivity.class);
                startActivity(activity1);
                finish();
                break;

            case R.id.delete_tv:
                Intent trashIntent = new Intent(context, TrashActivity.class);
                startActivityForResult(trashIntent, 101);
                break;
            case R.id.changeCompanyLogo:
            case R.id.img_logo:
                Intent changeLogoIntetn = new Intent(context, ChangeCompnayLogoActivity.class);
                changeLogoIntetn.putExtra("image", img_logo + "");
                startActivityForResult(changeLogoIntetn, 108);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        if (isSpinnerTouched) {
            String company_id = mAllCompanylist.get(position).getCompanyId();
            getIntent().removeExtra("jump");
            getIntent().removeExtra("record_type");
            getIntent().removeExtra("message_id");
            getIntent().removeExtra("pingId");
            getIntent().removeExtra("project_team_id");
            getIntent().removeExtra("record_id");
            ShardPreferences.save(context, share_Company_Id_key, company_id);
            startActivity(getIntent());
        }
        if (isByAnyone) {
            String User_id = userListingSpinner.get(position).getUserId();
            ShardPreferences.save(context, ShardPreferences.key_userSpinner_id, User_id);
            startActivity(getIntent());
        }
        if (isEveryWhere) {
            String User_id = cardListingSpinner.get(position).getProjectTeamId();
            ShardPreferences.save(context, ShardPreferences.key_cardListingSpinner_id, User_id);
            startActivity(getIntent());
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(COMPANY_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    mSwipyRefreshLayout.setRefreshing(false);
                    JSONArray jsonArrayAlllCompany = jsonObject.getJSONArray("data");
                    mAllCompanylist = new Gson().fromJson(jsonArrayAlllCompany.toString(), new TypeToken<List<AllCompanyListPOJO>>() {
                    }.getType());
                    String[] mStringArray = new String[mAllCompanylist.size()];
                    String check_company_id = ShardPreferences.get(this, share_Company_Id_key);
                    int setposition = 0;

                    for (int i = 0; i < mAllCompanylist.size(); i++) {
                        mStringArray[i] = Utils.unescapeJava(mAllCompanylist.get(i).getCompanyName());
                        if (check_company_id.equals(mAllCompanylist.get(i).getCompanyId())) {
                            setposition = i;
                            if (mAllCompanylist.get(i).getIsCompanyExpired().equals("false")) {
                                homeApi(mAllCompanylist.get(i).getCompanyId());
                                ShardPreferences.save(context, ShardPreferences.key_company_Id, mAllCompanylist.get(i).getCompanyId());
                            } else {
                                expired_company.setVisibility(View.VISIBLE);
                                nestedScrollView.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                bottomTab.setVisibility(View.GONE);
                            }
                        }
                    }

                    if (setposition == 0 && (!check_company_id.equals(mAllCompanylist.get(0).getCompanyId()))) {
                        homeApi(mAllCompanylist.get(0).getCompanyId());
                    }

                    ArrayAdapter aa = new ArrayAdapter(this, R.layout.custom_spinner, mStringArray);
                    aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
                    mtoolbar_spinner.setAdapter(aa);
                    mtoolbar_spinner.setSelection(setposition, false);
                    ShardPreferences.save(context, share_current_company_name, mtoolbar_spinner.getSelectedItem().toString());

                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (tag_json_obj.equals(FETCH_EMPLOYEE_ANDROID)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        memployee_icon_recycler_view_adapter = new MinEmployeeIconAdapter(mEmployeeDetailList, this, 0, 0);
                        memployee_icon_recycler_view.setAdapter(memployee_icon_recycler_view_adapter);
                    } else {
                    }
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                    } else {
                    }
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (tag_json_obj.equals(GET_EMPLOYEE_PROFILE)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String status = jsonObjec.getString("status");
                if (status.equals("true")) {
                    mSwipyRefreshLayout.setRefreshing(false);
                    ShardPreferences.save(context, ShardPreferences.share_user_image_key, jsonObjec.getString("employee_pic"));
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 22) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 108) {
            if (data != null) {
                finish();
                startActivity(getIntent());
                Picasso.with(context).load(data.getStringExtra("company_logo")).into(img_logo);
            }
        }
        if (requestCode == 102) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 11) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 101) {
            finish();
            startActivity(getIntent());
        }

    }

    private void homeApi(String companyID) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", mCurrentuserId);
        map.put("companyId", companyID);
        map.put("type", "active");

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<HomePojo> resultCall = apiInterface.Callhome(map);
        resultCall.enqueue(new Callback<HomePojo>() {

            @Override
            public void onResponse(Call<HomePojo> call, Response<HomePojo> response) {
                progressBar.setVisibility(View.GONE);
                nestedScrollView.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals("true")) {
                    ShardPreferences.save(context, share_current_profile_url, response.body().getUrl());
                    ShardPreferences.save(context, share_employee_Id_key, response.body().getEmployeeID());
                    sethomeList(response.body().getModuleList());
                    ModList = (ArrayList<ModuleList>) response.body().getModuleList();
                    for (int i = 0; i < ModList.size(); i++) {
                        HomeModList = (ArrayList<HomeModule>) ModList.get(i).getModuleList();
                    }

                    companyLists = response.body().getCompanyList();
                    if (companyLists.size() > 0) {
                        ll_cmpnay.setVisibility(View.VISIBLE);
                        mcompany_name_tv.setText(Utils.unescapeJava(companyLists.get(0).getCompanyName()));
                        mcompany_desc_tv.setText(Utils.unescapeJava(companyLists.get(0).getProjectTeamDescription()));
                        mcurrentProjectTeamId = companyLists.get(0).getProjectTeamId();
                        mcurrentCompanyName = Utils.unescapeJava(companyLists.get(0).getCompanyName());
                        ShardPreferences.save(context, share_current_module, mcurrentCompanyName);
                        if (!companyLists.get(0).getCompanyLogo().equals("")) {
                            ll_image_logo.setVisibility(View.GONE);
                            img_logo.setVisibility(View.VISIBLE);
                            Picasso.with(context).load(companyLists.get(0).getCompanyLogo()).into(img_logo);
                            ShardPreferences.save(context, key_company_logo, companyLists.get(0).getCompanyLogo());
                            changeCompanyLogo.setVisibility(View.GONE);

                        } else {
                            changeCompanyLogo.setVisibility(View.VISIBLE);
                        }
                        ShardPreferences.save(context, share_current_company_name, mcurrentCompanyName);

                    } else {
                        ll_cmpnay.setVisibility(View.GONE);
                    }
                    fetchEmployee();
                } else {
                }
            }

            @Override
            public void onFailure(Call<HomePojo> call, Throwable t) {
            }
        });
    }

    private void sethomeList(List<ModuleList> list) {
        if (list != null && list.size() > 0) {
            ListAdapter = new HomeAdapter(context, (ArrayList<ModuleList>) list);
            rv_addedTeam_projectLit.setAdapter(ListAdapter);
        } else {
            list = new ArrayList<>();
            ListAdapter = new HomeAdapter(context, (ArrayList<ModuleList>) list);
            rv_addedTeam_projectLit.setAdapter(ListAdapter);
        }
    }

    public void callSearch() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
        map.put("company_id", ShardPreferences.get(context, ShardPreferences.key_company_Id));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> resultCall = apiInterface.callSearch(map);
        resultCall.enqueue(new Callback<SearchResponse>() {

            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.body().getStatus()) {
                    userListingSpinner = (ArrayList<UserListing>) response.body().getUserListing();
                    userList();
                    cardListingSpinner = (ArrayList<CardListing>) response.body().getCardListing();
                    cardList();
                } else {
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
            }
        });

    }

    public void userList() {

        String[] mStringArray = new String[userListingSpinner.size()];
        String check_company_id = ShardPreferences.get(context, ShardPreferences.key_userSpinner_id);
        int setposition = 0;

        for (int i = 0; i < userListingSpinner.size(); i++) {
            mStringArray[i] = userListingSpinner.get(i).getEmployeeName();
            if (check_company_id.equals(userListingSpinner.get(i).getUserId())) {
                setposition = i;


                ShardPreferences.save(context, ShardPreferences.key_userSpinner_id, userListingSpinner.get(i).getUserId());
            }
        }

        ArrayAdapter aa = new ArrayAdapter(context, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_byAnyone.setAdapter(aa);
        sp_byAnyone.setSelection(setposition, false);
    }

    public void cardList() {
        String[] mStringArray = new String[cardListingSpinner.size()];
        String check_company_id = ShardPreferences.get(context, ShardPreferences.key_cardListingSpinner_id);
        int setposition = 0;

        for (int i = 0; i < cardListingSpinner.size(); i++) {
            mStringArray[i] = cardListingSpinner.get(i).getProjectTeamName();
            if (check_company_id.equals(cardListingSpinner.get(i).getProjectTeamId())) {
                setposition = i;
                ShardPreferences.save(context, ShardPreferences.key_cardListingSpinner_id, cardListingSpinner.get(i).getProjectTeamId());
            }
        }
        ArrayAdapter aa = new ArrayAdapter(context, R.layout.custom_spinner, mStringArray);
        aa.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        //Setting the ArrayAdapter data on the Spinner
        sp_everywhere.setAdapter(aa);
        sp_everywhere.setSelection(setposition, false);
    }

    @Override
    public void setCommentData(int commentSize) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}

