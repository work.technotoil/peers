package com.peerbuckets.peerbucket.Activity.schedule;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.ScheduleListAdapter;
import com.peerbuckets.peerbucket.POJO.SchdeuleMonthListModel;
import com.peerbuckets.peerbucket.POJO.ScheduleListModel;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard.REQ_CODE_EXPAND_MESSAGE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SCHEDULE_LIST_BY_DATE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SCHEDULE_LIST_MONTH;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class ScheduleActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener {

    private static final String TAG = "ScheduleActivity";
    public static final int REQUEST_CODE_ADD_SCHEDULE = 2, REQUEST_CODE_EDIT_SCHEDULE = 3, REQUEST_CODE_DELETE_SCHEDULE = 455;

    private RecyclerView dateEventRecyclerView;
    private CompactCalendarView compactCalendarView;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private boolean shouldShow = false;
    ApiRequest mApiRequest;
    ArrayList<SchdeuleMonthListModel> mSchdeuleMonthListModel = new ArrayList<>();
    ArrayList<ScheduleListModel> mScheduleListModel = new ArrayList<>();
    ScheduleListAdapter mAdapter;
    String inviteTypeName = "";
    Button btnAddSchedule;
    private Toolbar toolbar;
    public static int REQ_CODE_EXPAND_MESSAGES = 2;
    public static int REQ_CODE_EXPAND_MESSAGESss = 58;
    public static int REQUEST_CODE_ADD_SCHEDULEss = 52;
    private Context context = this;
    LinearLayout ll_add_event;
    TextView current_date;
    Button btnAddEvent;
    ProgressBar progress_bar_pagination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        inviteTypeName = getIntent().getStringExtra("inviteTypeName");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.schedule));
        back();

        progress_bar_pagination = findViewById(R.id.progress_bar_pagination);
        current_date = findViewById(R.id.current_date);
        btnAddEvent = findViewById(R.id.btnAddEvent);
        ll_add_event = findViewById(R.id.ll_add_event);
        btnAddSchedule = findViewById(R.id.btn_add_to_do);
        btnAddSchedule.setOnClickListener(this);
        btnAddEvent.setOnClickListener(this);
        initRecyclerView();
        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        SimpleDateFormat df1 = new SimpleDateFormat("EEE, MMM dd ");
        Date cDate = Calendar.getInstance().getTime();
        String formattedDate1 = df1.format(cDate);
        current_date.setText(formattedDate1);
        getDataFromDate();
    }

    public void getDataFromDate() {
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
        progress_bar_pagination.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(ScheduleActivity.this, share_current_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(ScheduleActivity.this, share_current_project_team_id));
        paramsReq.put("company_id", ShardPreferences.get(ScheduleActivity.this, share_Company_Id_key));
        paramsReq.put("month", (month + 1) + "");
        mApiRequest.postRequestBackground(BASE_URL + SCHEDULE_LIST_MONTH, SCHEDULE_LIST_MONTH, paramsReq, Request.Method.POST);

        Date cDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String formattedDate = df.format(cDate);
        paramsReq.put("date", formattedDate);
        mApiRequest.postRequestBackground(BASE_URL + SCHEDULE_LIST_BY_DATE, SCHEDULE_LIST_BY_DATE, paramsReq, Request.Method.POST);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initRecyclerView() {
        dateEventRecyclerView = findViewById(R.id.date_event_recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        dateEventRecyclerView.setLayoutManager(linearLayoutManager);
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        compactCalendarView.setIsRtl(false);
        compactCalendarView.displayOtherMonthDays(false);
        compactCalendarView.invalidate();

        logEventsByMonth(compactCalendarView);

        final List<String> mutableBookings = new ArrayList<>();
        final ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mutableBookings);


        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                String formattedDate = df.format(dateClicked);

                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("date", formattedDate);
                paramsReq.put("user_id", ShardPreferences.get(ScheduleActivity.this, share_current_UserId_key));
                paramsReq.put("project_team_id", ShardPreferences.get(ScheduleActivity.this, share_current_project_team_id));
                paramsReq.put("company_id", ShardPreferences.get(ScheduleActivity.this, share_Company_Id_key));
                mApiRequest.postRequest(BASE_URL + SCHEDULE_LIST_BY_DATE, SCHEDULE_LIST_BY_DATE, paramsReq, Request.Method.POST);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                //toolbar.setTitle(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
    }

    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
        Log.d(TAG, "Events for Aug with simple date formatter: " + dates);
        Log.d(TAG, "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(currentCalender.getTime()));
    }

    private void addEvents(int dotCount, int mDate) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        currentCalender.setTime(firstDayOfMonth);
        currentCalender.add(Calendar.DATE, mDate);
        long timeInMillis = currentCalender.getTimeInMillis();
        List<Event> events = getEvents(timeInMillis, dotCount);
        compactCalendarView.addEvents(events);
    }

    private List<Event> getEvents(long timeInMillis, int dotCount) {

        if (dotCount < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if (dotCount > 2 && dotCount <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(SCHEDULE_LIST_MONTH)) {
            Log.d("GET_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray schdeule_list = jsonObject.getJSONArray("schedule_list");
                    mSchdeuleMonthListModel = new Gson().fromJson(schdeule_list.toString(), new TypeToken<List<SchdeuleMonthListModel>>() {
                    }.getType());
                    if (mSchdeuleMonthListModel != null && mSchdeuleMonthListModel.size() > 0) {
                        for (int i = 0; i < mSchdeuleMonthListModel.size(); i++) {
                            addEvents(Integer.parseInt(mSchdeuleMonthListModel.get(i).getMydate()), Integer.parseInt(mSchdeuleMonthListModel.get(i).getDot_days()));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(SCHEDULE_LIST_BY_DATE)) {
            progress_bar_pagination.setVisibility(View.GONE);
            Log.d("GET_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    ll_add_event.setVisibility(View.GONE);
                    dateEventRecyclerView.setVisibility(View.VISIBLE);
                    JSONArray schdeule_list = jsonObject.getJSONArray("schedule_list");
                    mScheduleListModel = new Gson().fromJson(schdeule_list.toString(), new TypeToken<List<ScheduleListModel>>() {
                    }.getType());
                    mAdapter = new ScheduleListAdapter(mScheduleListModel, this);
                    dateEventRecyclerView.setAdapter(mAdapter);
                } else {
                    ll_add_event.setVisibility(View.VISIBLE);
                    dateEventRecyclerView.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                Intent intent = new Intent(context, AddScheduleActivity.class);
                intent.putExtra("type", AddScheduleActivity.TYPE_ADD);
                startActivityForResult(intent, REQUEST_CODE_ADD_SCHEDULEss);
                break;
            case R.id.btnAddEvent:
                Intent mintent = new Intent(context, AddScheduleActivity.class);
                mintent.putExtra("type", AddScheduleActivity.TYPE_ADD);
                startActivityForResult(mintent, REQUEST_CODE_ADD_SCHEDULEss);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_CODE_EXPAND_MESSAGES) {
              mAdapter.removeMessageAndUpdateComment(data.getBooleanExtra("isDelete", false), data.getStringExtra("id"));
            } else if (requestCode == REQUEST_CODE_EDIT_SCHEDULE) {
                recreate();
            }
            if (requestCode == REQ_CODE_EXPAND_MESSAGES) {
                mAdapter.removeMessageAndUpdateComment(data.getBooleanExtra("isDelete", false), data.getStringExtra("id"));
            } else if (requestCode == REQUEST_CODE_DELETE_SCHEDULE) {
                try {
                    if (mAdapter != null && mScheduleListModel != null) {
                        int position = data.getIntExtra("position", -1);
                        if (position != -1) {
                            mScheduleListModel.remove(position);
                            if (mScheduleListModel != null && mScheduleListModel.size() == 0) {
                                ll_add_event.setVisibility(View.VISIBLE);
                            }
                            mAdapter.notifyDataSetChanged();
                        } else {
                            getDataFromDate();
                        }
                    } else {
                        getDataFromDate();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(ScheduleActivity.this, share_current_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(ScheduleActivity.this, share_current_project_team_id));
        paramsReq.put("company_id", ShardPreferences.get(ScheduleActivity.this, share_Company_Id_key));
        paramsReq.put("month", (month + 1) + "");
        mApiRequest.postRequestBackground(BASE_URL + SCHEDULE_LIST_MONTH, SCHEDULE_LIST_MONTH, paramsReq, Request.Method.POST);

        Date cDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String formattedDate = df.format(cDate);
        paramsReq.put("date", formattedDate);
        mApiRequest.postRequestBackground(BASE_URL + SCHEDULE_LIST_BY_DATE, SCHEDULE_LIST_BY_DATE, paramsReq, Request.Method.POST);
    }
}