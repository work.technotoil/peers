package com.peerbuckets.peerbucket.Activity.checkin;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.peerbuckets.peerbucket.Adapter.CheckInListAdapter;
import com.peerbuckets.peerbucket.Adapter.ReplyAdapter;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.CheckinReply;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.Activity.checkin.CheckInDetailActivity.REQUEST_CODE_ADD_REPLY;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_CHECK_IN_REPLY;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_CHECK_IN_REPLY;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_CHECK_IN_REPLY;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.Utils.Utils.imageLatter;

public class AddEditReplyActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, EditorHtmlInterface {

    public static final String TYPE_ADD = "add", TYPE_EDIT = "edit", TYPE_DELETE = "delete";
    String name = "", email = "", title = "", all_users_id = "", record_id = "";
    TextView mmessage_title, memail, madd_comment_tv;
    WebView editor;
    Context context = this;
    ApiRequest mApiRequest;
    Button btn_add_to_dos;
    ProgressBar pb_progressBar;
    ImageView user_image;
    private ShimmerFrameLayout mShimmerViewContainer;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    TextView tv_cancel;
    String intentType = "", replyId = "", userName = "";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_checkinreply_layout);

        mApiRequest = new ApiRequest(this, this);
        Intent intent = getIntent();

        if (getIntent() != null) {
            intentType = intent.getStringExtra("type");
            name = intent.getStringExtra("name");
            title = intent.getStringExtra("message_title");
            email = intent.getStringExtra("message_email");
            record_id = intent.getStringExtra("check_in_id");
            all_users_id = intent.getStringExtra("all_users_id");
            userName = intent.getStringExtra("userName");
            replyId = intent.getStringExtra("reply_id");
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        back();
        initUI();

        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });

        btn_add_to_dos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb_progressBar.setVisibility(View.VISIBLE);
                btn_add_to_dos.setVisibility(View.GONE);
                editor.loadUrl("javascript:copyContent()");
                editor.loadUrl("javascript:hello()");
                onBackPressed();
            }
        });
    }

    private void initUI() {
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mmessage_title = findViewById(R.id.message_title);
        memail = findViewById(R.id.email);
        editor = findViewById(R.id.add_comment);
        btn_add_to_dos = findViewById(R.id.btn_add_to_do);
        pb_progressBar = findViewById(R.id.pb_progressBar);
        user_image = findViewById(R.id.user_image);
        editor.getSettings().setJavaScriptEnabled(true);
        editor.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        editor.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });

        editor.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);

        if (intentType.equals(TYPE_ADD)) {
            editor.loadUrl("file:///android_asset/index1.html");
        } else if (intentType.equals(TYPE_EDIT)) {
            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
            String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
            String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
            String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
            String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
            String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
            String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
            String messageBody = getIntent().getStringExtra("description");
            if (Utils.isStringValid(messageBody)) {
                messageBody = messageBody.replace("\"", "'");
            }

            //messageBody = TextUtils.htmlEncode(messageBody);


            String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                    "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                    "<script src=" + mentionJsPath + "></script>" +
                    "</head><body>";
            String editorHtml = "<form>" +
                    "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                    "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                    "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                    "</form>";
            String pas = "<script>" +
                    "function hello()" +
                    "{" +
                    "   var _id = document.getElementById('x').value;" +
                    "   var mention_uid = document.getElementById('mention_uid').value;" +
                    "    Android.nextScreen(_id,mention_uid);" +
                    "}" +
                    "</script></body></html>";
            String myHtmlString = pish + editorHtml + pas;
            editor.loadData(myHtmlString, "text/html; charset=utf-8", null);
        }
        madd_comment_tv = findViewById(R.id.add_comment_tv);
        mmessage_title.setText(title);
     ////   memail.setText(Utils.unescapeJava(userName));

        if (!ShardPreferences.get(context, ShardPreferences.share_user_image_key).equals("") && user_image != null) {
            Picasso.with(context).load(ShardPreferences.get(context, ShardPreferences.share_user_image_key)).into(user_image);
        } else {
            // user_image.setImageDrawable(imageLatter(Utils.word(name), ShardPreferences.get(context, ShardPreferences.key_backColor), ShardPreferences.get(context, ShardPreferences.key_fontColor)));
        }
        madd_comment_tv.setOnClickListener(this);
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_comment_tv:
                editor.loadUrl("javascript:copyContent()");
                editor.loadUrl("javascript:hello()");
                break;
        }
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(ADD_CHECK_IN_REPLY)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    pb_progressBar.setVisibility(View.GONE);
                    madd_comment_tv.setVisibility(View.VISIBLE);
                    CheckinReply checkinReply = new CheckinReply();
                    String comment = Utils.unescapeJava(jsonObject.getString("answer"));
                    checkinReply.setAnswer(comment);
                    checkinReply.setCheckinId(jsonObject.getString("record_id"));
                    checkinReply.setCreatedAt(jsonObject.getString("date"));
                    checkinReply.setAnswereDate(jsonObject.getString("date"));
                    checkinReply.setBgColor(ShardPreferences.get(context, ShardPreferences.key_backColor));
                    checkinReply.setFontColor(ShardPreferences.get(context, ShardPreferences.key_fontColor));
                    checkinReply.setEmployeeName(ShardPreferences.get(context, ShardPreferences.share_user_name_key));
                    checkinReply.setUserImage(ShardPreferences.get(context, ShardPreferences.share_current_profile_url));
                    checkinReply.setUserId(ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                    Intent intent = getIntent();
                    intent.putExtra("reply_list", checkinReply);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    pb_progressBar.setVisibility(View.GONE);
                    madd_comment_tv.setVisibility(View.VISIBLE);
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(UPDATE_CHECK_IN_REPLY)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    CheckinReply checkinReply = new CheckinReply();
                    String comment = Utils.unescapeJava(jsonObject.getString("reply"));
                    checkinReply.setAnswer(comment);
                    checkinReply.setCheckinId(jsonObject.getString("record_id"));
                    checkinReply.setCreatedAt(jsonObject.getString("date"));
                    checkinReply.setAnswereDate(jsonObject.getString("date"));
                    checkinReply.setBgColor(ShardPreferences.get(context, ShardPreferences.key_backColor));
                    checkinReply.setFontColor(ShardPreferences.get(context, ShardPreferences.key_fontColor));
                    checkinReply.setEmployeeName(ShardPreferences.get(context, ShardPreferences.share_user_name_key));
                    checkinReply.setUserImage(ShardPreferences.get(context, ShardPreferences.share_current_profile_url));
                    checkinReply.setUserId(ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                    Intent intent = getIntent();
                    intent.putExtra("reply_list", checkinReply);
                    setResult(REQUEST_CODE_ADD_REPLY, intent);
                    finish();
                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(UPDATE_CHECK_IN_REPLY)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    setResult(RESULT_OK, getIntent());
                    finish();
                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            mShimmerViewContainer.stopShimmerAnimation();
            editor.setVisibility(View.VISIBLE);
            mShimmerViewContainer.setVisibility(View.GONE);

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 22) {
            finish();
            startActivity(getIntent());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
// Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
// Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = data == null || resultCode != AddEditReplyActivity.RESULT_OK ? null : data.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void setData(String htmlData, String mention_uid) {

      /*  if (htmlData == null || htmlData.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.please_enter_description), Toast.LENGTH_SHORT).show();
        } else {*/
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("record_id", record_id);
        paramsReq.put("type", ShardPreferences.get(this, ShardPreferences.share_addd_remove_current_type));
        paramsReq.put("record_type", "checkin_reply");
        paramsReq.put("mention_uid", mention_uid);
        paramsReq.put("comment", Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData)));
        paramsReq.put("checkin_reply_description", Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData)));
        paramsReq.put("users", "");
        paramsReq.put("timezone", "GMT");
        Date cDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(cDate);
        paramsReq.put("question_date", formattedDate);
        if (intentType.equals(TYPE_ADD)) {
            mApiRequest.postRequest(BASE_URL + ADD_CHECK_IN_REPLY, ADD_CHECK_IN_REPLY, paramsReq, Request.Method.POST);
        } else if (intentType.equals(TYPE_EDIT)) {
            paramsReq.put("checkin_reply_id", replyId);
            paramsReq.put("checkin_id", record_id);
            mApiRequest.postRequest(BASE_URL + UPDATE_CHECK_IN_REPLY, UPDATE_CHECK_IN_REPLY, paramsReq, Request.Method.POST);
        } else if (intentType.equals(TYPE_DELETE)) {
            mApiRequest.postRequest(BASE_URL + DELETE_CHECK_IN_REPLY, DELETE_CHECK_IN_REPLY, paramsReq, Request.Method.POST);
        }
        //}
    }



    class WebAppInterfaceEdit {
        Context mContext;
        EditorHtmlInterface mEditorHtmlInterface;

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterfaceEdit(Context c, EditorHtmlInterface mEditorHtmlInterface) {
            mContext = c;
            this.mEditorHtmlInterface = mEditorHtmlInterface;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void nextScreen(String htmlData, String mention_uid) {
            mEditorHtmlInterface.setData(htmlData, mention_uid);

        }
    }
}


