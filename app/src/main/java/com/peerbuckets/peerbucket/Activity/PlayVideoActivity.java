package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.view_holder.ChatSenderVideoViewHolder;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class PlayVideoActivity extends AppCompatActivity {
    VideoView vv_playVideo;
    Context context = this;
    ImageView imgBack, imgDownload;
    Uri vUri;
    String image = "", type = "", imagePath = "";
    public static final String TYPE_SINGLE = "SINGLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);

        vv_playVideo = findViewById(R.id.vv_playVideo);
        imgBack = findViewById(R.id.img_back_show_image);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        MediaController mc = new MediaController(context);
        mc.setAnchorView(vv_playVideo);
        mc.setMediaPlayer(vv_playVideo);

        if (getIntent() != null) {
            type = getIntent().getStringExtra("type");
            image = getIntent().getStringExtra("image");
            imagePath = getIntent().getStringExtra("imagePath");
        }
        vUri = Uri.parse(image);
        vv_playVideo.setMediaController(mc);
        vv_playVideo.setVideoURI(vUri);
        vv_playVideo.start();

        if (ShardPreferences.get(context,share_language).equals("2")){
            imgBack.setImageResource(R.drawable.ic_arrow_forward);
        }

    }
}