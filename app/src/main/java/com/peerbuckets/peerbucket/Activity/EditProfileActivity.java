package com.peerbuckets.peerbucket.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;


import org.json.JSONException;
import org.json.JSONObject;


import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import static com.chinalwb.are.Util.toast;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_USER_DETAIL_BY_ID;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_PROFILE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private RelativeLayout timezoneLayout, weekLayout;
    ApiRequest mApiRequest;
    Context mContext;
    EditText et_username, et_title, et_shortbio, et_email;
    TextView tv_timezone, tv_ColorGrey, tv_ColorOrange, tv_ColorGreen, tv_ColorBlue, tv_ColorPurple;
    public static int REQUEST_CODE = 100;
    String timezoneValue = "", employeeId = "", backcolor = "", textcolor = "", firstName = "", lastName = "", name = "";
    Button btnSave;
    ImageView imgUserName, cp_backColore;
    ImageView close_tcp_dialog_iv;
    ProgressBar pb_progress;
    ImageView cp_textColor, imgColorPurple, imgColorBlue, imgColorGreen, imgColorOrange, imgColorGrey;
    private int currentBackgroundColor = 0xffffffff;
    private View root;
    StringBuilder backSb;
    RelativeLayout rl_imgColorGrey, rl_imgColorOrange, rl_imgColorGreen, rl_imgColorBlue, rl_imgColorPurple;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_layout);

        cp_backColore = findViewById(R.id.cp_background);
        cp_textColor = findViewById(R.id.cp_text);
        imgUserName = findViewById(R.id.imgUserName);

        imgColorPurple = findViewById(R.id.imgColorPurple);
        imgColorBlue = findViewById(R.id.imgColorBlue);
        imgColorGreen = findViewById(R.id.imgColorGreen);
        imgColorOrange = findViewById(R.id.imgColorOrange);
        imgColorGrey = findViewById(R.id.imgColorGrey);

        rl_imgColorOrange = findViewById(R.id.rl_imgColorOrange);
        rl_imgColorPurple = findViewById(R.id.rl_imgColorPurple);
        rl_imgColorBlue = findViewById(R.id.rl_imgColorBlue);
        rl_imgColorGreen = findViewById(R.id.rl_imgColorGreen);
        rl_imgColorGrey = findViewById(R.id.rl_imgColorGrey);

        tv_ColorGreen = findViewById(R.id.tv_ColorGreen);
        tv_ColorBlue = findViewById(R.id.tv_ColorBlue);
        tv_ColorPurple = findViewById(R.id.tv_ColorPurple);
        tv_ColorGrey = findViewById(R.id.tv_ColorGrey);
        tv_ColorOrange = findViewById(R.id.tv_ColorOrange);

        pb_progress = findViewById(R.id.pb_progress);
        btnSave = findViewById(R.id.btnSave);
        close_tcp_dialog_iv = findViewById(R.id.close_tcp_dialog_iv);
        et_username = findViewById(R.id.et_username);
        et_title = findViewById(R.id.et_title);
        et_shortbio = findViewById(R.id.et_shortbio);
        et_email = findViewById(R.id.et_email);
        tv_timezone = findViewById(R.id.tv_timezone);

        timezoneLayout = findViewById(R.id.relative_layout1);
        timezoneLayout.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        mContext = this;

        mApiRequest = new ApiRequest(this, this);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", ShardPreferences.get(mContext, share_Company_Id_key));
        paramsReq.put("userId", ShardPreferences.get(mContext, share_UserId_key));
        mApiRequest.postRequest(BASE_URL + GET_USER_DETAIL_BY_ID, GET_USER_DETAIL_BY_ID, paramsReq, Request.Method.POST);

        close_tcp_dialog_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rl_imgColorPurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backcolor = "745dad";
                imgUserName.setImageDrawable(imageLatter(Utils.word(name), textcolor, backcolor));
                tv_ColorPurple.setVisibility(View.VISIBLE);
                tv_ColorGreen.setVisibility(View.GONE);
                tv_ColorGrey.setVisibility(View.GONE);
                tv_ColorBlue.setVisibility(View.GONE);
                tv_ColorOrange.setVisibility(View.GONE);
            }
        });

        rl_imgColorBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backcolor = "1c68ca";
                imgUserName.setImageDrawable(imageLatter(Utils.word(name),
                        textcolor,
                        backcolor));
                tv_ColorPurple.setVisibility(View.GONE);
                tv_ColorGreen.setVisibility(View.GONE);
                tv_ColorGrey.setVisibility(View.GONE);
                tv_ColorBlue.setVisibility(View.VISIBLE);
                tv_ColorOrange.setVisibility(View.GONE);
            }
        });
        rl_imgColorGrey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backcolor = "5a6567";
                imgUserName.setImageDrawable(imageLatter(Utils.word(name),
                        textcolor,
                        backcolor));
                tv_ColorPurple.setVisibility(View.GONE);
                tv_ColorGreen.setVisibility(View.GONE);
                tv_ColorGrey.setVisibility(View.VISIBLE);
                tv_ColorBlue.setVisibility(View.GONE);
                tv_ColorOrange.setVisibility(View.GONE);
            }
        });
        rl_imgColorGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backcolor = "3bb471";
                imgUserName.setImageDrawable(imageLatter(Utils.word(name),
                        textcolor,
                        backcolor));
                tv_ColorPurple.setVisibility(View.GONE);
                tv_ColorGreen.setVisibility(View.VISIBLE);
                tv_ColorGrey.setVisibility(View.GONE);
                tv_ColorBlue.setVisibility(View.GONE);
                tv_ColorOrange.setVisibility(View.GONE);
            }
        });
        rl_imgColorOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backcolor = "ec5945";
                imgUserName.setImageDrawable(imageLatter(Utils.word(name),
                        textcolor,
                        backcolor));
                tv_ColorPurple.setVisibility(View.GONE);
                tv_ColorGreen.setVisibility(View.GONE);
                tv_ColorGrey.setVisibility(View.GONE);
                tv_ColorBlue.setVisibility(View.GONE);
                tv_ColorOrange.setVisibility(View.VISIBLE);
            }
        });


        cp_backColore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorPickerDialogBuilder
                        .with(mContext)
                        .setTitle(getResources().getString(R.string.Choose_color))
                        .initialColor(currentBackgroundColor)
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                            }
                        })
                        .setPositiveButton(getResources().getString(R.string.okS), new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                                changeBackgroundColor(selectedColor);
                                backcolor = Integer.toHexString(selectedColor);
                                if (allColors != null) {
                                    backSb = null;

                                    for (Integer color : allColors) {
                                        if (color == null)
                                            continue;
                                        if (backSb == null)
                                            backSb = new StringBuilder("Color List:");
                                        backSb.append(Integer.toHexString(color).toUpperCase());
                                    }

                                }
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .build()
                        .show();
            }
        });

        cp_textColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorPickerDialogBuilder
                        .with(mContext)
                        .setTitle(getResources().getString(R.string.Choose_color))
                        .initialColor(currentBackgroundColor)
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                                //  toast(mContext, "onColorSelected: 0x" + Integer.toHexString(selectedColor));
                            }
                        })
                        .setPositiveButton(getResources().getString(R.string.okS), new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                                changeTextColor(selectedColor);
                                textcolor = String.valueOf(selectedColor);
                                textcolor = Integer.toHexString(selectedColor);
                                //Toast.makeText(mContext, textcolor, Toast.LENGTH_SHORT).show();
                                if (allColors != null) {
                                    StringBuilder sb = null;

                                    for (Integer color : allColors) {
                                        if (color == null)
                                            continue;
                                        if (sb == null)
                                            sb = new StringBuilder("Color List:");
                                        sb.append("\r\n#" + Integer.toHexString(color).toUpperCase());
                                    }

                                    //  if (sb != null)
                                    // Toast.makeText(getApplicationContext(), sb.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .build()
                        .show();
            }
        });


    }

    private TextDrawable imageLatter(String name, String fontcolor, String bgcolor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(45)
                .bold()
                .textColor(Color.parseColor("#" + fontcolor))/* size in px */
                //.bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + bgcolor));

        return drawable;
    }

    private void changeBackgroundColor(int selectedColor) {
        currentBackgroundColor = selectedColor;

        TextDrawable drawable = TextDrawable.builder()
                .buildRect("", selectedColor);

        cp_backColore.setImageDrawable(drawable);
        backcolor = Integer.toHexString(selectedColor);
        imgUserName.setImageDrawable(imageLatter(Utils.word(name),
                textcolor,
                backcolor));
    }

    private void changeTextColor(int selectedColor) {
        currentBackgroundColor = selectedColor;
        TextDrawable drawable = TextDrawable.builder()
                .buildRound("", selectedColor);

        cp_textColor.setImageDrawable(drawable);

        textcolor = Integer.toHexString(selectedColor);
        imgUserName.setImageDrawable(imageLatter(Utils.word(name),
                textcolor,
                backcolor));
    }

    void popup(View v) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.relative_layout1:
                Intent i = new Intent(this, TimezoneListActivity.class);
                startActivityForResult(i, REQUEST_CODE);
                break;

            case R.id.btnSave:
                if (et_username.getText().toString().trim().equals("")) {
                    et_username.setError(getString(R.string.please_enter_name));
                    et_username.requestFocus();
                } else if (et_title.getText().toString().trim().equals("")) {
                    et_title.setError(getString(R.string.please_enter_title));
                    et_title.requestFocus();
                } else if (tv_timezone.getText().toString().trim().equals("")) {
                    tv_timezone.setError(getString(R.string.please_select_timezone));
                    tv_timezone.requestFocus();
                } else {
                    btnSave.setVisibility(View.GONE);
                    pb_progress.setVisibility(View.VISIBLE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("employee_id", employeeId);
                    paramsReq.put("user_id", ShardPreferences.get(mContext, ShardPreferences.share_current_UserId_key));
                    paramsReq.put("employee_name", Utils.getUnicodeString(Utils.escapeUnicodeText(et_username.getText().toString().trim())));
                    paramsReq.put("employee_job_title", Utils.getUnicodeString(Utils.escapeUnicodeText(et_title.getText().toString().trim())));
                    paramsReq.put("shortbio", Utils.getUnicodeString(Utils.escapeUnicodeText(et_shortbio.getText().toString().trim())));
                    paramsReq.put("timezone", timezoneValue);
                    paramsReq.put("bgColor", backcolor);
                    paramsReq.put("fontColor", textcolor);
                    ShardPreferences.save(mContext, ShardPreferences.key_backColor, backcolor);
                    ShardPreferences.save(mContext, ShardPreferences.key_fontColor, textcolor);
                    mApiRequest.postRequest(BASE_URL + UPDATE_PROFILE, UPDATE_PROFILE, paramsReq, Request.Method.POST);
                }
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_USER_DETAIL_BY_ID)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    et_username.setText(Utils.unescapeJava(jsonObjec.getString("employee_name")));
                    name = Utils.unescapeJava(jsonObjec.getString("employee_name"));
                    et_title.setText(Utils.unescapeJava(jsonObjec.getString("employee_job_title")));
                    if (jsonObjec.getString("shortbio") != null && !jsonObjec.getString("shortbio").equals("")) {
                        et_shortbio.setText(Utils.unescapeJava(jsonObjec.getString("shortbio")));
                    }
                    et_email.setText(Utils.unescapeJava(jsonObjec.getString("employee_email")));
                    tv_timezone.setText(jsonObjec.getString("timezone_value"));
                    timezoneValue = jsonObjec.getString("timezone_value");
                    employeeId = jsonObjec.getString("employee_id");

//                    if (!jsonObjec.getString("bgColor").equals("")) {
//                        TextDrawable drawable = TextDrawable.builder()
//                                .buildRect("", Color.parseColor("#" + jsonObjec.getString("bgColor")));
//
//                        cp_backColore.setImageDrawable(drawable);
//
//                    }

                    TextDrawable drawableFont = TextDrawable.builder()
                            .buildRound("", Color.parseColor("#" + jsonObjec.getString("fontColor")));

                    cp_textColor.setImageDrawable(drawableFont);
                    textcolor = jsonObjec.getString("fontColor");
                    backcolor = jsonObjec.getString("bgColor");

                    switch (backcolor) {
                        case "5a6567":
                            //Gray
                            tv_ColorPurple.setVisibility(View.GONE);
                            tv_ColorGreen.setVisibility(View.GONE);
                            tv_ColorGrey.setVisibility(View.VISIBLE);
                            tv_ColorBlue.setVisibility(View.GONE);
                            tv_ColorOrange.setVisibility(View.GONE);
                            break;
                        case "ec5945":
                            //Orange
                            tv_ColorPurple.setVisibility(View.GONE);
                            tv_ColorGreen.setVisibility(View.GONE);
                            tv_ColorGrey.setVisibility(View.GONE);
                            tv_ColorBlue.setVisibility(View.GONE);
                            tv_ColorOrange.setVisibility(View.VISIBLE);
                            break;
                        case "3bb471":
                            //Green
                            tv_ColorPurple.setVisibility(View.GONE);
                            tv_ColorGreen.setVisibility(View.VISIBLE);
                            tv_ColorGrey.setVisibility(View.GONE);
                            tv_ColorBlue.setVisibility(View.GONE);
                            tv_ColorOrange.setVisibility(View.GONE);
                            break;
                        case "1c68ca":
                            //Blue
                            tv_ColorPurple.setVisibility(View.GONE);
                            tv_ColorGreen.setVisibility(View.GONE);
                            tv_ColorGrey.setVisibility(View.GONE);
                            tv_ColorBlue.setVisibility(View.VISIBLE);
                            tv_ColorOrange.setVisibility(View.GONE);
                            break;
                        case "745dad":
                            //Purple
                            tv_ColorPurple.setVisibility(View.VISIBLE);
                            tv_ColorGreen.setVisibility(View.GONE);
                            tv_ColorGrey.setVisibility(View.GONE);
                            tv_ColorBlue.setVisibility(View.GONE);
                            tv_ColorOrange.setVisibility(View.GONE);
                            break;
                    }
                    if (!jsonObjec.getString("bgColor").equals("") &&  jsonObjec.getString("bgColor")!=null) {
                        imgUserName.setImageDrawable(imageLatter(Utils.word(Utils.unescapeJava(jsonObjec.getString("employee_name"))),
                                jsonObjec.getString("fontColor"),
                                jsonObjec.getString("bgColor")));
                    }

                    ShardPreferences.save(mContext, ShardPreferences.key_backColor, jsonObjec.getString("bgColor"));
                    ShardPreferences.save(mContext, ShardPreferences.key_fontColor, jsonObjec.getString("fontColor"));
                    ShardPreferences.save(mContext, ShardPreferences.key_EmpName, jsonObjec.getString("employee_name"));
                } else {
                    Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else if (tag_json_obj.equals(UPDATE_PROFILE)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    btnSave.setVisibility(View.VISIBLE);
                    pb_progress.setVisibility(View.GONE);
                    Toast.makeText(this, jsonObjec.getString("message"), Toast.LENGTH_SHORT).show();
                    imgUserName.setImageDrawable(imageLatter(Utils.word(et_username.getText().toString()), textcolor, backcolor));
                } else {
                    btnSave.setVisibility(View.VISIBLE);
                    pb_progress.setVisibility(View.GONE);
                    Toast.makeText(this, jsonObjec.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                btnSave.setVisibility(View.VISIBLE);
                pb_progress.setVisibility(View.GONE);
                Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (RESULT_OK == resultCode) {
            if (requestCode == REQUEST_CODE) {
                // tv_timezone.setText(data.getStringExtra("timezoneName"));
                tv_timezone.setText(data.getStringExtra("value"));
                timezoneValue = data.getStringExtra("value");

            }
        }
    }
}
