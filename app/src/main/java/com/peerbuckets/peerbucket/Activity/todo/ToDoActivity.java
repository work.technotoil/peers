package com.peerbuckets.peerbucket.Activity.todo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.ToDoAdapter;
import com.peerbuckets.peerbucket.POJO.ToDoList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_ALL_TODO;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class ToDoActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private static final String TAG = "ToDoActivity";
    public static int REQUEST_CODE_DELETE = 2;
    private RecyclerView recyclerView;
    private FloatingActionButton fabAdd;
    ImageView imgBack;
    TextView tv_HQ;
    Context mContext;
    ApiRequest apiRequest;
    Context context = ToDoActivity.this;
    ToDoAdapter toDoAdapter;
    int page = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    List<ToDoList> toDoList = new ArrayList<>();
    private Boolean loading = true;
    LinearLayoutManager mLayoutManager;
    ProgressBar progressBarPagination;
    NestedScrollView nestedScrollView;
    private int REQUEST_CODE_ADD = 1;
    private Toolbar toolbar;
    CardView card_info;
    String project_team_id = "", record_id = "", todo_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);
        mContext = this;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.to_dos));
        if (ShardPreferences.get(context,share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();
        fabAdd = findViewById(R.id.fab);
        fabAdd.setOnClickListener(this);
        apiRequest = new ApiRequest(context, this);
        card_info = findViewById(R.id.card_info);
        progressBarPagination = findViewById(R.id.progress_bar_pagination);
        recyclerView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        nestedScrollView = findViewById(R.id.nestedScrollViewToDo);
        if (getIntent().getStringExtra("project_team_id") != null) {
            project_team_id = getIntent().getStringExtra("project_team_id");
        }
        if (getIntent().getStringExtra("record_id") != null) {
            record_id = getIntent().getStringExtra("record_id");
        }
        if (getIntent().getStringExtra("todo_id") != null) {
            todo_id = getIntent().getStringExtra("todo_id");
            Intent intent = new Intent(context, ExpandToDoActivity.class);
            intent.putExtra("SubTodo_id", todo_id);
            intent.putExtra("to_do_id", record_id);
            startActivity(intent);
        }

        getToDoList(page);
        applyPagination();
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getToDoList(int cPage) {
        progressBarPagination.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(mContext, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(mContext, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(mContext, ShardPreferences.share_addd_remove_current_type));
        if (ShardPreferences.get(mContext, ShardPreferences.share_current_project_team_id).equals("")) {
            paramsReq.put("project_team_id", project_team_id);
        } else {
            paramsReq.put("project_team_id", ShardPreferences.get(mContext, ShardPreferences.share_current_project_team_id));
        }
        paramsReq.put("page", String.valueOf(cPage));
        apiRequest.postRequestBackground(BASE_URL + FETCH_ALL_TODO, FETCH_ALL_TODO, paramsReq, Request.Method.POST);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(context, AddToDoActivity.class);
                intent.putExtra("classType", AddToDoActivity.TYPE_ADD);
                startActivity(intent);
                finish();
                break;

        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_ALL_TODO)) {
            progressBarPagination.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    card_info.setVisibility(View.GONE);
                    JSONArray mArray = jsonObject.getJSONArray("todo_list");
                    if (mArray != null && mArray.length() > 0) {

                        if (page == 0 && toDoList != null) {
                            toDoList.clear();
                        }

                        toDoList = new Gson().fromJson(mArray.toString(), new TypeToken<List<ToDoList>>() {
                        }.getType());

                        if (toDoList != null && toDoList.size() > 0) {
                            recyclerView.setVisibility(View.VISIBLE);

                            if (page == 0) {
                                toDoAdapter = new ToDoAdapter(toDoList, mContext);
                                recyclerView.setAdapter(toDoAdapter);

                            } else {
                                recyclerView.post(new Runnable() {
                                    public void run() {
                                        toDoAdapter.setNotifyData(toDoList);
                                    }
                                });
                            }

                            page = page + 1;
                            loading = true;
                            progressBarPagination.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);

                        } else {
                            recyclerView.setVisibility(View.GONE);
                        }

                    } else {
                        if (page == 0) {
                        }
                    }
                } else {
                    if (page == 0) {
                        card_info.setVisibility(View.VISIBLE);
                    }

                    loading = false;
                    progressBarPagination.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                progressBarPagination.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_ADD) {
                card_info.setVisibility(View.GONE);
                if (toDoList == null) {
                    toDoList = new ArrayList<>();
                }
                if (toDoAdapter != null) {
                    toDoAdapter.addNewItem((ToDoList) data.getSerializableExtra("todo"));
                    recyclerView.scrollToPosition(0);
                } else {
                    toDoList.add(0, (ToDoList) data.getSerializableExtra("todo"));
                    toDoAdapter = new ToDoAdapter(toDoList, context);
                    recyclerView.setAdapter(toDoAdapter);
                }
            }
            if (requestCode == REQUEST_CODE_DELETE) {
                try {
                    if (toDoAdapter != null && toDoList != null) {
                        int position = data.getIntExtra("position", -1);
                        if (position != -1) {
                            toDoList.remove(position);
                            if (toDoList != null && toDoList.size() == 0) {
                                card_info.setVisibility(View.VISIBLE);
                            }
                            toDoAdapter.notifyDataSetChanged();
                        } else {
                            page = 0;
                            getToDoList(page);
                        }
                    } else {
                        page = 0;
                        getToDoList(page);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void applyPagination() {

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            progressBarPagination.setVisibility(View.VISIBLE);
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data

                            getToDoList(page);

                        }
                    }
                }
            }
        });
    }
}
