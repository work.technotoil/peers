package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.ActivityScreenAdapter;
import com.peerbuckets.peerbucket.Adapter.CardViewCommonAdapter;
import com.peerbuckets.peerbucket.Adapter.CheckInDayAdapter;
import com.peerbuckets.peerbucket.POJO.Day;
import com.peerbuckets.peerbucket.POJO.HoursModel;
import com.peerbuckets.peerbucket.POJO.LatestActivityPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.RecyclerItemClickListener;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHECK_IN_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_DEFAULT_NOTIFICATION_SETTING;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.LATEST_ACTIVITIES;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.TUEN_ON_NOTIFICTION;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.WORK_CAN_WAIT;

public class NotificationSettingActivity extends AppCompatActivity implements IApiResponse {

    RecyclerView recyclerDay;
    LinearLayoutManager layoutManagerDay;
    Context context;
    CheckInDayAdapter dayAdapter;
    private static final int TYPE_DAY = 1;
    RadioGroup radioGroupWhat, radioGroupWhen;
    RadioButton radioNotify, radioOnlyMention, radioAlways, radioWork;
    String askTypeWhen = "", askTypeWhat = "", askHow = "none";
    CheckBox chkEmail, chkPush;
    ArrayList<HoursModel> mTimeList = new ArrayList<>();
    TextView tv_from_time, tv_to_time, tv_notification_on, tv_notification_onn, tv_notification_off, tv_notification_offf;
    LinearLayout llSelectTime;
    ApiRequest mApiRequest;
    Button btnSave;
    String isNotificationTurnOn = "true";
    ImageView close_tcp_dialog_iv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_notification_setting);
        close_tcp_dialog_iv = findViewById(R.id.close_tcp_dialog_iv);
        radioGroupWhat = findViewById(R.id.radioGroupWhat);
        radioGroupWhen = findViewById(R.id.radioGroupWhen);
        radioNotify = findViewById(R.id.radioNotify);
        radioOnlyMention = findViewById(R.id.radioOnlyMention);
        radioAlways = findViewById(R.id.radioAlways);
        radioWork = findViewById(R.id.radioWork);
        btnSave = findViewById(R.id.btnSave);
        recyclerDay = findViewById(R.id.recycler_daily_day_add_check_in);
        llSelectTime = findViewById(R.id.llSelectTime);
        chkEmail = findViewById(R.id.chkEmail);
        chkPush = findViewById(R.id.chkPush);
        tv_from_time = findViewById(R.id.tv_from_time);
        tv_to_time = findViewById(R.id.tv_to_time);
        tv_notification_on = findViewById(R.id.tv_notification_on);
        tv_notification_onn = findViewById(R.id.tv_notification_onn);
        tv_notification_off = findViewById(R.id.tv_notification_off);
        tv_notification_offf = findViewById(R.id.tv_notification_offf);
        layoutManagerDay = new LinearLayoutManager(context);
        layoutManagerDay.setOrientation(RecyclerView.HORIZONTAL);
        recyclerDay.setLayoutManager(layoutManagerDay);
        setTimeList();

        tv_notification_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnOnNotification("false");
            }
        });

        tv_notification_offf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnOnNotification("false");
            }
        });


        tv_notification_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnOnNotification("true");
            }
        });

        tv_notification_onn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnOnNotification("true");
            }
        });

        mApiRequest = new ApiRequest(this, this);

        setRecyclerAdapter(recyclerDay, TYPE_DAY, true);

        radioGroupWhat.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioNotify:
                        if (radioNotify.isPressed()) {
                            askTypeWhat = "all";
                        }
                        break;
                    case R.id.radioOnlyMention:
                        if (radioOnlyMention.isPressed()) {
                            askTypeWhat = "mention";
                        }
                        break;
                }
            }
        });

        radioGroupWhen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radioAlways:
                        if (radioAlways.isPressed()) {
                            recyclerDay.setVisibility(View.GONE);
                            llSelectTime.setVisibility(View.GONE);
                            askTypeWhen = "all";
                        }
                        break;
                    case R.id.radioWork:
                        if (radioWork.isPressed()) {
                            askTypeWhen = "mention";
                            recyclerDay.setVisibility(View.VISIBLE);
                            llSelectTime.setVisibility(View.VISIBLE);
                            setRecyclerAdapter(recyclerDay, TYPE_DAY, true);
                        }
                        break;
                }
            }
        });

        tv_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog(tv_from_time);
            }
        });

        tv_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog(tv_to_time);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWorkCanWait();
            }
        });

        close_tcp_dialog_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getDefaultWorkCanWait();


    }

    private void turnOnNotification(String isTurnOn) {
        isNotificationTurnOn = isTurnOn;
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("isTurnON", isTurnOn);
        mApiRequest.postRequestBackground(BASE_URL + TUEN_ON_NOTIFICTION, TUEN_ON_NOTIFICTION, paramsReq, Request.Method.POST);
    }

    private void getDefaultWorkCanWait() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        mApiRequest.postRequest(BASE_URL + GET_DEFAULT_NOTIFICATION_SETTING, GET_DEFAULT_NOTIFICATION_SETTING, paramsReq, Request.Method.POST);

    }

    private void setWorkCanWait() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("what", askTypeWhat);
        paramsReq.put("how", getCheckBox());
        paramsReq.put("when", askTypeWhen);
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("from_schedule", checkHoursTime(tv_from_time));
        paramsReq.put("to_schedule", checkHoursTime(tv_to_time));
        paramsReq.put("day", getSelectedDays());
        mApiRequest.postRequest(BASE_URL + WORK_CAN_WAIT, WORK_CAN_WAIT, paramsReq, Request.Method.POST);

    }

    private String getSelectedDays() {
        JSONArray dayArray = new JSONArray();
        if (dayAdapter != null) {
            dayArray = dayAdapter.getSelectedArray();
            if (dayArray != null && dayArray.length() > 0) {
                return dayArray.toString();
            }
        } else {
            return "";
        }
        return "";
    }

    public String getCheckBox() {
        if (chkEmail.isChecked() && chkPush.isChecked()) {
            askHow = "both";
        } else if (chkEmail.isChecked()) {
            askHow = "email";
        } else if (chkPush.isChecked()) {
            askHow = "push";
        }
        return askHow;
    }

    public String checkHoursTime(TextView tv_check) {
        if (tv_check.getText().toString().trim().equals("")) {
            return "";
        } else {
            for (int i = 0; i < mTimeList.size(); i++) {
                if (tv_check.getText().toString().trim().equalsIgnoreCase(mTimeList.get(i).getHourFormat())) {
                    return mTimeList.get(i).getHours();
                }
            }
            return "";
        }
    }

    public String getHoursFormatTime(String tv_check) {
        if (tv_check.toString().trim().equals("")) {
            return "";
        } else {
            for (int i = 0; i < mTimeList.size(); i++) {
                if (tv_check.toString().trim().equalsIgnoreCase(mTimeList.get(i).getHourFormat())) {
                    return mTimeList.get(i).getHours();
                }
            }
            return "";
        }
    }

    private void setRecyclerAdapter(RecyclerView recycler, int type, Boolean isMultiple) {
        List<Day> daysList = new ArrayList<>();
        List<String> daysNameList;
        daysNameList = Arrays.asList(getResources().getStringArray(R.array.day_array));
        for (String dayName : daysNameList) {
            daysList.add(new Day(dayName));
        }
        dayAdapter = new CheckInDayAdapter(context, daysList, type, isMultiple);
        recycler.setAdapter(dayAdapter);
    }


    public void setTimeList() {

        for (int i = 1; i <= 12; i++) {
            HoursModel mModel = new HoursModel();
            mModel.setHourFormat(i + "");
            mModel.setHours(i + " am");
            mTimeList.add(mModel);
        }

        for (int i = 1; i <= 12; i++) {
            HoursModel mModel = new HoursModel();
            mModel.setHourFormat((i + 12) + "");
            mModel.setHours(i + " pm");
            mTimeList.add(mModel);
        }
    }


    @SuppressLint("WrongConstant")
    public void showTimeDialog(final TextView mTextView) {

        // TODO Auto-generated method stub
        final Dialog dialogEdit;
        dialogEdit = new Dialog(context);
        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEdit.setContentView(R.layout.common_dialog);
        RecyclerView dialog_recyclerList = (RecyclerView) dialogEdit.findViewById(R.id.dialog_recyclerList);
        TextView txtCancel = (TextView) dialogEdit.findViewById(R.id.txtCancel);
        TextView txtTitle = (TextView) dialogEdit.findViewById(R.id.txtTitle);
        txtTitle.setText("Select Time");

        LinearLayoutManager mManager = new LinearLayoutManager(context);
        mManager.setOrientation(LinearLayoutManager.VERTICAL);
        dialog_recyclerList.setLayoutManager(mManager);
        CardViewCommonAdapter mCardViewCommonAdapter = new CardViewCommonAdapter(mTimeList, context);
        dialog_recyclerList.setAdapter(mCardViewCommonAdapter);
        dialogEdit.show();
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEdit.dismiss();
            }
        });

        dialog_recyclerList.addOnItemTouchListener(
                new RecyclerItemClickListener(context, dialog_recyclerList,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                mTextView.setTextColor(Color.BLACK);
                                mTextView.setText(mTimeList.get(position).getHours());
                                mTextView.setError(null);
                                dialogEdit.dismiss();
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {
                            }

                        })
        );
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(TUEN_ON_NOTIFICTION)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    if (isNotificationTurnOn.equals("true")) {

                        tv_notification_on.setVisibility(View.GONE);
                        tv_notification_onn.setVisibility(View.GONE);
                        tv_notification_off.setVisibility(View.VISIBLE);
                        tv_notification_offf.setVisibility(View.VISIBLE);

                    } else {
                        tv_notification_on.setVisibility(View.VISIBLE);
                        tv_notification_onn.setVisibility(View.VISIBLE);
                        tv_notification_off.setVisibility(View.GONE);
                        tv_notification_offf.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (tag_json_obj.equals(WORK_CAN_WAIT)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (tag_json_obj.equals(GET_DEFAULT_NOTIFICATION_SETTING)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    JSONObject mObj = jsonObject.getJSONObject("data");

                    askHow = mObj.getString("how");
                    askTypeWhat = mObj.getString("what");
                    askTypeWhen = mObj.getString("when");

                    if (askTypeWhat.equals("all")) {
                        radioNotify.setChecked(true);
                        radioOnlyMention.setChecked(false);
                    } else if (askTypeWhat.equals("mention")) {
                        radioNotify.setChecked(false);
                        radioOnlyMention.setChecked(true);
                    } else {
                        radioNotify.setChecked(false);
                        radioOnlyMention.setChecked(false);
                    }

                    if (askTypeWhen.equals("all")) {
                        radioAlways.setChecked(true);
                        radioWork.setChecked(false);
                        recyclerDay.setVisibility(View.GONE);
                        llSelectTime.setVisibility(View.GONE);
                    } else if (askTypeWhen.equals("mention")) {
                        radioAlways.setChecked(false);
                        radioWork.setChecked(true);
                        recyclerDay.setVisibility(View.VISIBLE);
                        llSelectTime.setVisibility(View.VISIBLE);

                        tv_from_time.setText(getHoursFormatTime(mObj.getString("not_from_time")));
                        tv_to_time.setText(getHoursFormatTime(mObj.getString("not_to_time")));

                        if (dayAdapter != null && Utils.isStringValid(mObj.getString("not_day"))) {
                            String selectedDays = mObj.getString("not_day");
                            dayAdapter.updateDaysList(selectedDays);
                        }
                    } else {
                        radioAlways.setChecked(false);
                        radioWork.setChecked(false);
                        recyclerDay.setVisibility(View.VISIBLE);
                        llSelectTime.setVisibility(View.VISIBLE);
                    }

                    if (askHow.equals("both")) {
                        chkEmail.setChecked(true);
                        chkPush.setChecked(true);
                    } else if (askHow.equals("email")) {
                        chkEmail.setChecked(true);
                        chkPush.setChecked(false);
                    } else if (askHow.equals("push")) {
                        chkEmail.setChecked(false);
                        chkPush.setChecked(true);
                    } else if (askHow.equals("")) {
                        chkEmail.setChecked(false);
                        chkPush.setChecked(false);
                    }

                    isNotificationTurnOn = mObj.getString("is_notification");
                    if (isNotificationTurnOn.equals("true")) {

                        tv_notification_on.setVisibility(View.GONE);
                        tv_notification_onn.setVisibility(View.GONE);
                        tv_notification_off.setVisibility(View.VISIBLE);
                        tv_notification_offf.setVisibility(View.VISIBLE);

                    } else {
                        tv_notification_on.setVisibility(View.VISIBLE);
                        tv_notification_onn.setVisibility(View.VISIBLE);
                        tv_notification_off.setVisibility(View.GONE);
                        tv_notification_offf.setVisibility(View.GONE);
                    }


                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
