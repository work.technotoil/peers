package com.peerbuckets.peerbucket.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.internal.Util;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.INSERT_SINGLE_PROJECT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;

public class CreateProjectActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {
    TextView mclose_tcp_dialog_tv, mheader_tcp_dialog_tv;
    EditText mtcp_name_et, moptional_desc_et;
    Button msave_team_button;
    private ApiRequest mApiRequest;
    Typeface typeface;
    String mcompanyId = "", muserId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tcp);
        typeface = Typeface.createFromAsset(getAssets(), "font/fawsmsolid.ttf");
        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        mclose_tcp_dialog_tv = findViewById(R.id.close_tcp_dialog_tv);
        mheader_tcp_dialog_tv = findViewById(R.id.header_tcp_dialog_tv);
        mtcp_name_et = findViewById(R.id.tcp_name_et);
        moptional_desc_et = findViewById(R.id.optional_desc_et);
        msave_team_button = findViewById(R.id.save_team_button);
        mcompanyId = ShardPreferences.get(CreateProjectActivity.this, share_Company_Id_key);
        muserId = ShardPreferences.get(CreateProjectActivity.this, share_UserId_key);

        mheader_tcp_dialog_tv.setText(R.string.tcp_project_dialog_header);

        msave_team_button.setOnClickListener(this);
        mclose_tcp_dialog_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_team_button:
                if (mtcp_name_et.getText() == null || mtcp_name_et.getText().toString().equals("")
                        || mtcp_name_et.getText().toString().isEmpty() || moptional_desc_et.getText() == null || moptional_desc_et.getText().toString().equals("")
                        || moptional_desc_et.getText().toString().isEmpty()) {
                    Toast.makeText(this, getResources().getString(R.string.please_Fill_all_the_req_data_first), Toast.LENGTH_SHORT).show();
                } else {
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("projectTeamName", Utils.getUnicodeString(Utils.escapeUnicodeText(mtcp_name_et.getText().toString())));
                    paramsReq.put("projectTeamDescription", Utils.getUnicodeString(Utils.escapeUnicodeText(moptional_desc_et.getText().toString())));
                    paramsReq.put("companyId", mcompanyId);
                    paramsReq.put("userId", muserId);
                    mApiRequest.postRequest(BASE_URL + INSERT_SINGLE_PROJECT, INSERT_SINGLE_PROJECT, paramsReq, Request.Method.POST);
                }

                break;

            case R.id.close_tcp_dialog_tv:
                finish();
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(INSERT_SINGLE_PROJECT)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    Toast.makeText(this, getResources().getString(R.string.team_created_successfully), Toast.LENGTH_SHORT).show();
                    Intent intentnewT = new Intent(CreateProjectActivity.this, HomeActivity.class);
                    startActivity(intentnewT);
                    finish();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
