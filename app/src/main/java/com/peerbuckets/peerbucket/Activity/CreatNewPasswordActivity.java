package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.CHECK_EMAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FORGOT_PASSWORD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.USER_LOGIN;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.USER_PASSWORD;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.get;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_name_key;

public class CreatNewPasswordActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {
    EditText create_password_text, confirm_password_text;
    Button mcreate_password_button;
    ApiRequest mApiRequest;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creat_new_password);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        confirm_password_text = findViewById(R.id.confirm_password_text);
        create_password_text = findViewById(R.id.create_password_text);
        mcreate_password_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.navigate_back:
                finish();
                break;

            case R.id.contact_support_tv:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_support_team, null);
                dialogBuilder.setView(dialogView);

                TextView mDialogCancel = (TextView) dialogView.findViewById(R.id.contact_support_cancel_tv);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;
            case R.id.create_password_button:
                String mpassword = create_password_text.getText().toString();
                String rpassword = confirm_password_text.getText().toString();
                if (mpassword.equals(rpassword)) {
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("userId", get(this, share_UserId_key));
                    paramsReq.put("password", mpassword);
                    Toast.makeText(this, share_UserId_key, Toast.LENGTH_SHORT).show();
                    mApiRequest.postRequest(BASE_URL + USER_PASSWORD, USER_PASSWORD, paramsReq, Request.Method.POST);
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.Both_entered_password_are_different), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals(USER_PASSWORD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                String message = jsonObject.getString("message");
                if (code.equals("200")) {
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

}
