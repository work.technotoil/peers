package com.peerbuckets.peerbucket.Activity;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.ProjectListAdapter;
import com.peerbuckets.peerbucket.Adapter.TrashProjectListAdapter;
import com.peerbuckets.peerbucket.POJO.ProjectListPOJO;
import com.peerbuckets.peerbucket.POJO.TeamListPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.TRASH_DETAIL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class TrashActivity extends AppCompatActivity implements IApiResponse {

    private ApiRequest mApiRequest;

    public ArrayList<ProjectListPOJO> mTrashICreated;
    public ArrayList<ProjectListPOJO> mTrashOthers;
    RecyclerView trash_item_created, trash_items_others_rv;
    CardView cardview_empty;
    String muserId = "", mcompanyId = "", mCurrentuserId = "";
    private TrashProjectListAdapter mteam_recyclerViewadapter;
    TextView empty_others_trash_tv, empty_my_trash_tv;
    private Toolbar toolbar;
    ProgressBar pb_process;
    Context context = this;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash_archeive_del);

        toolbar = findViewById(R.id.toolbar);
        pb_process = findViewById(R.id.pb_process);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.trash));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        back();

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        empty_my_trash_tv = findViewById(R.id.empty_my_trash_tv);
        empty_others_trash_tv = findViewById(R.id.empty_others_trash_tv);
        trash_items_others_rv = findViewById(R.id.trash_items_others_rv);
        trash_item_created = findViewById(R.id.trash_item_created);

        GridLayoutManager mGrid = new GridLayoutManager(this, 2);
        mGrid.setOrientation(RecyclerView.VERTICAL);
        trash_items_others_rv.setLayoutManager(mGrid);

        GridLayoutManager mGrid2 = new GridLayoutManager(this, 2);
        mGrid2.setOrientation(RecyclerView.VERTICAL);
        trash_item_created.setLayoutManager(mGrid2);

        cardview_empty = findViewById(R.id.cardview_empty);

        mcompanyId = ShardPreferences.get(TrashActivity.this, share_Company_Id_key);
        muserId = ShardPreferences.get(TrashActivity.this, share_UserId_key);
        mCurrentuserId = ShardPreferences.get(TrashActivity.this, share_current_UserId_key);


        callTrashApi();
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void callTrashApi() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("userId", mCurrentuserId);
        paramsReq.put("companyId", mcompanyId);
        mApiRequest.postRequest(BASE_URL + TRASH_DETAIL, TRASH_DETAIL, paramsReq, Request.Method.POST);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(TRASH_DETAIL)) {
            pb_process.setVisibility(View.GONE);
            try {
                int check = 0;
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    JSONArray jsoTrashICreated = jsonObject.getJSONArray("trashICreated");
                    JSONArray jsoTrashCreatedOther = jsonObject.getJSONArray("trashCreatedOther");
                    if (jsoTrashICreated.length() == 0 && jsoTrashCreatedOther.length() == 0) {
                        cardview_empty.setVisibility(View.VISIBLE);
                    } else {
                        cardview_empty.setVisibility(View.GONE);

                        if (jsoTrashICreated.length() > 0) {
                            mTrashICreated = new Gson().fromJson(jsoTrashICreated.toString(), new TypeToken<List<ProjectListPOJO>>() {
                            }.getType());

                            mteam_recyclerViewadapter = new TrashProjectListAdapter(mTrashICreated, this, true);
                            trash_item_created.setAdapter(mteam_recyclerViewadapter);
                            empty_my_trash_tv.setVisibility(View.GONE);
                        } else {
                            empty_my_trash_tv.setVisibility(View.VISIBLE);
                        }


                        if (jsoTrashCreatedOther.length() > 0) {
                            mTrashOthers = new Gson().fromJson(jsoTrashCreatedOther.toString(), new TypeToken<List<ProjectListPOJO>>() {
                            }.getType());

                            mteam_recyclerViewadapter = new TrashProjectListAdapter(mTrashOthers, this, false);
                            trash_items_others_rv.setAdapter(mteam_recyclerViewadapter);
                            empty_others_trash_tv.setVisibility(View.GONE);
                        } else {
                            empty_others_trash_tv.setVisibility(View.VISIBLE);
                        }
                    }

                } else {
                    cardview_empty.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void RefreshProjectAdapter(int position, Boolean isICreated) {

        if (isICreated) {
            mTrashICreated.remove(position);
            if (mTrashICreated.size() == 0) {
                empty_my_trash_tv.setVisibility(View.VISIBLE);
                mteam_recyclerViewadapter = new TrashProjectListAdapter(mTrashICreated, this, true);
                trash_item_created.setAdapter(mteam_recyclerViewadapter);
            } else {
                empty_my_trash_tv.setVisibility(View.GONE);
                mteam_recyclerViewadapter.notifyDataSetChanged();
            }
        } else {
            mTrashOthers.remove(position);
            if (mTrashICreated != null && mTrashICreated.size() == 0) {
                empty_others_trash_tv.setVisibility(View.VISIBLE);
                mteam_recyclerViewadapter = new TrashProjectListAdapter(mTrashOthers, this, false);
                trash_items_others_rv.setAdapter(mteam_recyclerViewadapter);
            } else {
                empty_others_trash_tv.setVisibility(View.GONE);
                mteam_recyclerViewadapter.notifyDataSetChanged();
            }
        }

    }


}
