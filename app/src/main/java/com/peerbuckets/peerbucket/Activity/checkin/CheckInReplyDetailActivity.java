package com.peerbuckets.peerbucket.Activity.checkin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddCommentActivity;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHECKIN_REPLY_DETAIL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class CheckInReplyDetailActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, RefreshCommentInterface {
    private static final int REQUEST_CODE_MESSAGE_ADD = 2;
    private static final int REQUEST_CODE_MESSAGE_EDIT = 3;
    Context context = this;
    private Toolbar toolbar;
    TextView tvReply, tvUserName, tvCommentCount, tvAddComment;
    ApiRequest apiRequest;
    String check_in_reply_id = "", checkInQuestion = "", all_users_id = "", userName = "",
            userEmail = "", firstName = "", lastName = "";
    CircleImageView imgUser;
    WebView webViewReply;

    int page = 0;
    private Boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    ArrayList<MessageCommentPOJO> messageCommentPOJOS;
    MessageCommentsAdapter mcomments_rv_adapter;
    private RecyclerView mcomments_rv;
    LinearLayoutManager layoutManagerComment;
    RelativeLayout rel_comment;
    private LinearLayout mcomments_LL;
    ImageView user_image;
    View view18;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_reply_detail);
        if (getIntent().getStringExtra("check_in_reply_id") != null) {
            check_in_reply_id = getIntent().getStringExtra("check_in_reply_id");
        }
        inIt();
        getReplyDetail();
        getCommentList(page);
        applyPagination();
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.Reply_to_do));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();
        apiRequest = new ApiRequest(context, this);
        view18 = findViewById(R.id.view18);
        rel_comment = findViewById(R.id.rel_comment);
        user_image = findViewById(R.id.user_image);
        tvReply = findViewById(R.id.tv_reply_detail);
        tvUserName = findViewById(R.id.tv_user_name_reply_detail);
        imgUser = findViewById(R.id.img_user_reply_detail);
        webViewReply = findViewById(R.id.webview_reply_detail);
        tvCommentCount = findViewById(R.id.message_count_tv);
        mcomments_rv = findViewById(R.id.recycler_reply_comment);
        tvAddComment = findViewById(R.id.btn_add_reply_check_in_detail);
        tvAddComment.setOnClickListener(this);
        layoutManagerComment = new LinearLayoutManager(context);
        layoutManagerComment.setOrientation(RecyclerView.VERTICAL);
        mcomments_rv.setLayoutManager(layoutManagerComment);
        mcomments_rv.setHasFixedSize(true);
        mcomments_LL = findViewById(R.id.comments_LL);
        mcomments_LL.setVisibility(View.VISIBLE);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getReplyDetail() {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_current_UserId_key));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        paramsReq.put("checkin_reply_id", check_in_reply_id);
        apiRequest.postRequest(BASE_URL + GET_CHECKIN_REPLY_DETAIL, GET_CHECKIN_REPLY_DETAIL, paramsReq, Request.Method.POST);

    }

    private void getCommentList(int cPage) {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "checkin_reply");
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("todo_id", check_in_reply_id);
        paramsReq.put("page", String.valueOf(cPage));
        apiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);

    }

    private TextDrawable imageLatter(String name, String backColr, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColr));

        return drawable;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_CHECKIN_REPLY_DETAIL)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONObject detailObject = jsonObject.getJSONObject("checkins_reply_detail");
                    updateUI(detailObject);
                    JSONArray mUserArray = detailObject.getJSONArray("users");
                    JSONObject jsonObject1 = new JSONObject();

                    if (!jsonObject1.getString("user_image").equals("")) {
                        Picasso.with(context).load(jsonObject1.getString("user_image")).into(user_image);
                        Picasso.with(context).load(jsonObject1.getString("user_image")).into(imgUser);
                    } else {
                        user_image.setImageDrawable(imageLatter(Utils.word(jsonObject1.getString("employee_name")), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
                        imgUser.setImageDrawable(imageLatter(Utils.word(jsonObject1.getString("employee_name")), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
                    }
                    if (mUserArray != null) {

                        for (int i = 0; i < mUserArray.length(); i++) {
                            jsonObject1 = mUserArray.getJSONObject(i);
                            if (i == 0) {
                                all_users_id = jsonObject1.getString("user_id");
                            } else {
                                all_users_id = all_users_id + "," + jsonObject1.getString("user_id");
                            }
                            Log.d("ALL_USERS", all_users_id);
                        }
                    }
                } else {


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(FETCH_COMMENT_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray mArray = jsonObject.getJSONArray("commentList");
                    if (mArray != null && mArray.length() > 0) {
                        rel_comment.setVisibility(View.GONE);
                        if (page == 0 && messageCommentPOJOS != null) {
                            messageCommentPOJOS.clear();
                        }

                        messageCommentPOJOS = new Gson().fromJson(mArray.toString(), new TypeToken<List<MessageCommentPOJO>>() {
                        }.getType());

                        tvCommentCount.setText(messageCommentPOJOS.size() + "");

                        if (messageCommentPOJOS != null && messageCommentPOJOS.size() > 0) {
                            if (page == 0) {
                                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, context, "", check_in_reply_id, this,0);
                                mcomments_rv.setAdapter(mcomments_rv_adapter);
                            } else {

                                mcomments_rv.post(new Runnable() {
                                    public void run() {
                                        mcomments_rv_adapter.setNotifyData(messageCommentPOJOS);
                                    }
                                });
                            }
                        }

                        page = page + 1;
                        loading = true;
                    } else {
                        if (page == 0) {
                            rel_comment.setVisibility(View.GONE);
                        }
                    }

                } else {
                    if (page == 0) {
                        rel_comment.setVisibility(View.GONE);
                    }
                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @SuppressLint("NewApi")
    private void updateUI(JSONObject detailObject) {
        try {
            userName = detailObject.getString("employee_name");
            userEmail = "";
            Utils.setDescription(Utils.unescapeJava(detailObject.getString("answer")), webViewReply, context);
            tvReply.setText("Answer to " + checkInQuestion + " ");
            tvUserName.setText(Utils.unescapeJava(Utils.checkStringWithHash(detailObject.getString("employee_name"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_reply_check_in_detail:
                Intent intent = new Intent(this, AddCommentActivity.class);
                intent.putExtra("call_type", "checkin_reply");
                intent.putExtra("message_title", checkInQuestion);
                intent.putExtra("message_email", userEmail);
                intent.putExtra("name", userName);
                intent.putExtra("record_id", check_in_reply_id);
                intent.putExtra("all_users_id", all_users_id);
                startActivityForResult(intent, REQUEST_CODE_MESSAGE_ADD);
                break;
        }

    }

    private void applyPagination() {
        // Add Pagination on recycler view android
        mcomments_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManagerComment.getChildCount();
                    totalItemCount = layoutManagerComment.getItemCount();
                    pastVisiblesItems = layoutManagerComment.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data

                            getCommentList(page);

                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_MESSAGE_ADD) {
                rel_comment.setVisibility(View.GONE);

                if (messageCommentPOJOS == null) {
                    messageCommentPOJOS = new ArrayList<>();
                }
                if (mcomments_rv_adapter == null) {
                    messageCommentPOJOS.add((MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, "", check_in_reply_id, this,0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                } else {
                    messageCommentPOJOS.add(0, (MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter.notifyDataSetChanged();
                }
                ((MessageCommentsAdapter) mcomments_rv_adapter).addCommentSize();
            } else if (requestCode == 1) {
                ((MessageCommentsAdapter) mcomments_rv_adapter).updateData(data.getIntExtra("position", -1), data.getStringExtra("comment"));
            }
        }
    }

    @Override
    public void setCommentData(int commentSize) {
        tvCommentCount.setText(commentSize + "");
        if (commentSize > 0) {
            mcomments_LL.setVisibility(View.VISIBLE);
        } else {
            mcomments_LL.setVisibility(View.GONE);
        }
    }
}
