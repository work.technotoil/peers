package com.peerbuckets.peerbucket.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.localization.LocaleHelper;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import java.util.Locale;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.APP_PREF;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;

public class DeciderActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private Button mLoginButton, mFreeTrial;
    private TextView mContactSupportTv;
    SharedPreferences sharedPref;
    private static final int STORAGE_PERMISSION_CODE = 123;
    LinearLayout ll_language;
    String pingId = "", jump = "", language = "";

    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decider);
        initUI();
        sessionCheck();
        requestStoragePermission();
        if (getIntent().getStringExtra("pingId") != null) {
            pingId = getIntent().getStringExtra("pingId");
        }
        if (getIntent().getStringExtra("jump") != null) {
            jump = getIntent().getStringExtra("jump");
        }
    }

    private void initUI() {
        language = Locale.getDefault().getDisplayLanguage();

        ll_language = findViewById(R.id.ll_language);
        mLoginButton = findViewById(R.id.login_button);
        mContactSupportTv = findViewById(R.id.contact_support_tv);
        mFreeTrial = findViewById(R.id.free_trial_button);

        ll_language.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
        mContactSupportTv.setOnClickListener(this);
        mFreeTrial.setOnClickListener(this);
    }

    private void sessionCheck() {
        /*share_Company_Id_key*/
        sharedPref = getSharedPreferences(APP_PREF, MODE_PRIVATE);
        String value = ShardPreferences.get(this, share_Company_Id_key);
        if (value != null) {
            if (sharedPref.contains(share_Company_Id_key)) {
                Intent intent = new Intent(DeciderActivity.this, HomeActivity.class);
                intent.putExtra("pingId", pingId);
                intent.putExtra("jump", jump);
                startActivity(intent);
                finish();

            }
        }
    }

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, STORAGE_PERMISSION_CODE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.login_button:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.contact_support_tv:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_support_team, null);
                dialogBuilder.setView(dialogView);
                TextView mDialogCancel = dialogView.findViewById(R.id.contact_support_cancel_tv);
                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;
            case R.id.free_trial_button:
                Intent ft_intent = new Intent(this, SignUpActivity.class);
                startActivity(ft_intent);
                break;

            case R.id.ll_language:
                OpenChangeLanguage();
                break;

        }

    }

    private void OpenChangeLanguage() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_language, null);
        dialogBuilder.setView(dialogView);
        ImageView img_close;
        RadioGroup rg_language;
        RadioButton rb_english, rb_arabick;
        rg_language = dialogView.findViewById(R.id.rg_language);
        img_close = dialogView.findViewById(R.id.img_close);
        rb_english = dialogView.findViewById(R.id.rb_english);
        rb_arabick = dialogView.findViewById(R.id.rb_arabick);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
            rb_arabick.setChecked(true);
            rb_english.setChecked(false);
        } else {
            rb_arabick.setChecked(false);
            rb_english.setChecked(true);
        }
        rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == rb_english.getId()) {
                    LocaleHelper.setLocale(context, "en");
                    ShardPreferences.save(context, ShardPreferences.share_language, "1");
                    setApplicationLanguage("en");
                    return;

                } else if (checkedId == rb_arabick.getId()) {
                    LocaleHelper.setLocale(context, "ar");
                    ShardPreferences.save(context, ShardPreferences.share_language, "2");
                    setApplicationLanguage("ar");
                    return;
                }
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SplachScreenActivity.class);
                startActivity(intent);
                finish();
                alertDialog.dismiss();
            }
        });
    }

    private void setApplicationLanguage(String newLanguage) {
        Resources activityRes = getResources();
        Configuration activityConf = activityRes.getConfiguration();
        Locale newLocale = new Locale(newLanguage);
        activityConf.setLocale(newLocale);
        activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

        Resources applicationRes = getApplicationContext().getResources();
        Configuration applicationConf = applicationRes.getConfiguration();
        applicationConf.setLocale(newLocale);
        applicationRes.updateConfiguration(applicationConf,
                applicationRes.getDisplayMetrics());
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
