package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;

import java.util.prefs.Preferences;

public class SplachScreenActivity extends AppCompatActivity {
    Context context = this;
    String id = "", channel_id = "", todo_id = "", to_user_id = "", from_user_id = "", jump = "0", message_id = "", record_id = "", project_team_id = "", record_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splach_screen);
        if (getIntent().getStringExtra("channel_id") != null) {
            channel_id = getIntent().getStringExtra("channel_id");
        }
        if (getIntent().getStringExtra("todo_id") != null) {
            todo_id = getIntent().getStringExtra("todo_id");
        }
        if (getIntent().getStringExtra("to_user_id") != null) {
            to_user_id = getIntent().getStringExtra("to_user_id");
        }
        if (getIntent().getStringExtra("message_id") != null) {
            message_id = getIntent().getStringExtra("message_id");
            jump = "2";
        }
        if (getIntent().getStringExtra("record_type") != null) {
            record_type = getIntent().getStringExtra("record_type");
        }
        if (getIntent().getStringExtra("project_team_id") != null) {
            project_team_id = getIntent().getStringExtra("project_team_id");
        }
        if (getIntent().getStringExtra("record_id") != null) {
            record_id = getIntent().getStringExtra("record_id");
        }
        if (getIntent().getStringExtra("from_user_id") != null) {
            from_user_id = getIntent().getStringExtra("from_user_id");
            jump = "1";
        }
        SplashThread splashThread = new SplashThread();
        splashThread.start();


    }

    class SplashThread extends Thread {
        public void run() {
            try {
                Thread.sleep(2200);
                if (ShardPreferences.get(context, ShardPreferences.share_ISFLOW).equals("6")) {
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("project_team_id", project_team_id);
                    intent.putExtra("pingId", from_user_id);
                    intent.putExtra("record_type", record_type);
                    intent.putExtra("message_id", message_id);
                    intent.putExtra("record_id", record_id);
                    intent.putExtra("todo_id", todo_id);
                    intent.putExtra("jump", jump);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(context, DeciderActivity.class);
                    intent.putExtra("pingId", from_user_id);
                    intent.putExtra("jump", jump);
                    startActivity(intent);
                    finish();
                }
            } catch (Exception e) {
                if (ShardPreferences.get(context, ShardPreferences.share_ISFLOW).equals("6")) {
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("project_team_id", project_team_id);
                    intent.putExtra("pingId", from_user_id);
                    intent.putExtra("record_type", record_type);
                    intent.putExtra("message_id", message_id);
                    intent.putExtra("record_id", record_id);
                    intent.putExtra("jump", jump);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(context, DeciderActivity.class);
                    intent.putExtra("pingId", from_user_id);
                    intent.putExtra("jump", jump);
                    startActivity(intent);
                    finish();
                }
            }
        }
    }
}
