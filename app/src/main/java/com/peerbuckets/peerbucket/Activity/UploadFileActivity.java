package com.peerbuckets.peerbucket.Activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.peerbuckets.peerbucket.volley.MultipartRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.EDIT_COMMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_SINGLE_TODO_SUB_TASK;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_DOCUMENT_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_DOCUMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPLOAD_DOCUMENT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class UploadFileActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, EditorHtmlInterface {

    EditText upload_file_title;
    Button mpost_message;
    ApiRequest mApiRequest;
    WebView editor;
    RecyclerView recyclerEmployee;
    RecyclerView.LayoutManager mLayoutManager;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    LinearLayout ll_linearMaster;
    private ShimmerFrameLayout mShimmerViewContainer;
    private Toolbar toolbar;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "", message_desc = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    String groupMembersUserId = "",filename="",pathholder="";
    String editorData="",record_id="0",class_type="";
    Context mContext;
    ProgressBar progressBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_upload_file);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        mContext = this;
        getPrefrences();

        if(getIntent().getStringExtra("PathHolder") != null){
            pathholder = getIntent().getStringExtra("PathHolder");
            filename=pathholder.substring(pathholder.lastIndexOf("/")+1);
        }

        initUI();

        if(getIntent().getStringExtra("record_id") != null){
            record_id = getIntent().getStringExtra("record_id");
            class_type = getIntent().getStringExtra("class_type");
            if(!class_type.equals("add")){
                mpost_message.setText(getResources().getString(R.string.update));
                loadDetails();
            }
        }

        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", mcurrent_type);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(UploadFileActivity.this, share_Company_Id_key);
        mcurrentProjectTeamId = ShardPreferences.get(UploadFileActivity.this, share_current_project_team_id);
        mcurrent_type = ShardPreferences.get(UploadFileActivity.this, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(UploadFileActivity.this, share_current_UserId_key);
    }

    private void loadDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("file_id", record_id);
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequest(BASE_URL + GET_DOCUMENT_DETAIL, GET_DOCUMENT_DETAIL, paramsReq, Request.Method.POST);
    }


    private void initUI() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(ShardPreferences.get(mContext, share_current_module));
        getSupportActionBar().setSubtitle(getResources().getString(R.string.upload_document));


        progressBar = findViewById(R.id.progressBar);
        recyclerEmployee = findViewById(R.id.recyclerEmployee);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerEmployee.setLayoutManager(mLayoutManager);
        upload_file_title = findViewById(R.id.upload_file_title);
        upload_file_title.setText(filename);
        mpost_message = findViewById(R.id.btn_add_to_do);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        ll_linearMaster = findViewById(R.id.ll_linearMaster);
        mShimmerViewContainer.startShimmerAnimation();

        editor = findViewById(R.id.editor_add_to_do);
        editor.getSettings().setJavaScriptEnabled(true);
        editor.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        editor.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
            }
        });
        editor.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);
        editor.loadUrl("file:///android_asset/index1.html");
        mpost_message.setOnClickListener(this);
        back();
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                if (upload_file_title.getText().toString().trim().equals("")) {
                    Toast.makeText(this, getString(R.string.please_enter_title), Toast.LENGTH_SHORT).show();
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    editor.loadUrl("javascript:copyContent()");
                    editor.loadUrl("javascript:hello()");
                }
                break;
        }
    }

    @Override
    public void setData(String htmlData, String mention_uid) {
        editorData = htmlData;
        if(class_type.equals("edit")){

            Map<String, String> mapparamsReq = new HashMap<>();
            mapparamsReq.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
            mapparamsReq.put("project_team_id", ShardPreferences.get(mContext, share_current_project_team_id));
            mapparamsReq.put("company_id", ShardPreferences.get(mContext, share_Company_Id_key));
            mapparamsReq.put("file_name", upload_file_title.getText().toString().trim());
            mapparamsReq.put("mention_uid", mention_uid);
            mapparamsReq.put("doc_description", htmlData);
            mapparamsReq.put("record_id", record_id);
            mApiRequest.postRequestBackground(BASE_URL + UPDATE_DOCUMENT, UPDATE_DOCUMENT, mapparamsReq, Request.Method.POST);

        }else{
            if (htmlData.equals("")) {
                Toast.makeText(this, getString(R.string.please_enter_description), Toast.LENGTH_SHORT).show();
            } else {
                if(pathholder.equals("")){
                    Toast.makeText(mContext, getResources().getString(R.string.please_select_file), Toast.LENGTH_SHORT).show();
                }else{

                    Map<String, String> mapparamsReq = new HashMap<>();
                    mapparamsReq.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
                    mapparamsReq.put("project_team_id", ShardPreferences.get(mContext, share_current_project_team_id));
                    mapparamsReq.put("company_id", ShardPreferences.get(mContext, share_Company_Id_key));
                    mapparamsReq.put("file_name", upload_file_title.getText().toString().trim());
                    mapparamsReq.put("parent_id", record_id);
                    mapparamsReq.put("timezone", "GMT");
                    mapparamsReq.put("mention_uid", mention_uid);
                    mapparamsReq.put("doc_description", htmlData);
                    mapparamsReq.put("users", groupMembersUserId);
                    final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
                    params.put("doc", new MultipartRequest.DataPart(filename, getVideoByteData(pathholder), getMimeType(filename)));
                    mApiRequest.postRequestWithImage(BASE_URL + UPLOAD_DOCUMENT, UPLOAD_DOCUMENT, mapparamsReq, params, Request.Method.POST);
                }
            }
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        progressBar.setVisibility(View.GONE);
        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            mShimmerViewContainer.stopShimmerAnimation();
            ll_linearMaster.setVisibility(View.VISIBLE);
            mShimmerViewContainer.setVisibility(View.GONE);

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(UPLOAD_DOCUMENT)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                if (jsonObjec.getString("status").equals("1")) {
                    Toast.makeText(this, jsonObjec.getString("message"), Toast.LENGTH_SHORT).show();

                    JSONArray mArray = jsonObjec.getJSONArray("documents");
                    if(mArray.length()>0){
                        Intent i = getIntent();
                        i.putExtra("documentData",mArray.toString());
                        setResult(Activity.RESULT_OK,i);
                        finish();
                    }else{
                        Toast.makeText(this, getResources().getString(R.string.something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(this, getResources().getString(R.string.something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(UPDATE_DOCUMENT)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                if (jsonObjec.getString("status").equals("1")) {
                    Toast.makeText(this, jsonObjec.getString("message"), Toast.LENGTH_SHORT).show();

                    JSONArray mArray = jsonObjec.getJSONArray("documents");
                    if(mArray.length()>0){
                        Intent i = getIntent();
                        i.putExtra("desc",editorData);
                        i.putExtra("title",upload_file_title.getText().toString().trim());
                        setResult(Activity.RESULT_OK,i);
                        finish();
                    }else{
                        Toast.makeText(this, getResources().getString(R.string.something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(this, getResources().getString(R.string.something_went_wrong_please_try_again_later), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }




        if (tag_json_obj.equals(GET_DOCUMENT_DETAIL)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONObject jsonObjectDetail = jsonObject.getJSONObject("document");
                    updateUI(jsonObjectDetail);
                } else {

                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("EXCPTN", e.toString());
            }
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public byte[] getVideoByteData(String filepath) {

        byte[] videoBytes = null;
        try{
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileInputStream fis = new FileInputStream(new File(filepath));

            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = fis.read(buf)))
                baos.write(buf, 0, n);

            videoBytes = baos.toByteArray();

        }catch (Exception e){
            e.printStackTrace();
        }
        return  videoBytes;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }


    private void updateUI(JSONObject jsonObject) {
        try {
            upload_file_title.setText(Utils.checkStringWithEmpty(jsonObject.getString("document_title")));

            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
            String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
            String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
            String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
            String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
            String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
            String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
            String messageBody = jsonObject.getString("document_description");
            messageBody = messageBody.replace("\"", "'");

            String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                    "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                    "<script src=" + mentionJsPath + "></script>" +
                    "</head><body>";
            String editorHtml = "<form>" +
                    "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                    "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                    "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                    "</form>";
            String pas = "<script>" +
                    "function hello()" +
                    "{" +
                    "   var _id = document.getElementById('x').value;" +
                    "   var mention_uid = document.getElementById('mention_uid').value;" +
                    "    Android.nextScreen(_id,mention_uid);" +
                    "}" +
                    "</script></body></html>";
            String myHtmlString = pish + editorHtml + pas;
            editor.loadData(myHtmlString, "text/html; charset=utf-8", null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
