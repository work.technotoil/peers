package com.peerbuckets.peerbucket.Activity;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class AdminLandActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout addRemovePeopleLayout, addRemoveAdministratorsLayout, addRemoveAccountOwnerLayout, renameAccountLayout;
    ImageView img_back;
    Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_land);

        initUI();
    }

    private void initUI() {
        img_back = findViewById(R.id.img_back);
        addRemovePeopleLayout = findViewById(R.id.add_remove);
        addRemoveAdministratorsLayout = findViewById(R.id.add_remove_administrators);
        addRemoveAccountOwnerLayout = findViewById(R.id.add_remove_account_owner);
        renameAccountLayout = findViewById(R.id.rename_account);
        if (ShardPreferences.get(context,share_language).equals("2")) {
            img_back.setImageResource(R.drawable.ic_arrow_forward);
        }

        renameAccountLayout.setOnClickListener(this);
        addRemoveAccountOwnerLayout.setOnClickListener(this);
        addRemoveAdministratorsLayout.setOnClickListener(this);
        addRemovePeopleLayout.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.add_remove:
                Intent intent = new Intent(this, AddRemovePeopleChangeAccessActivity.class);
                startActivity(intent);
                break;

            case R.id.add_remove_administrators:
                intent = new Intent(this, AddRemoveAdministratorsActivity.class);
                startActivity(intent);
                break;

            case R.id.add_remove_account_owner:
                intent = new Intent(this, AddRemoveAccoutOwnerActivity.class);
                startActivity(intent);
                break;

            case R.id.rename_account:
                intent = new Intent(this, RenameAccountActivity.class);
                startActivity(intent);
                break;
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}
