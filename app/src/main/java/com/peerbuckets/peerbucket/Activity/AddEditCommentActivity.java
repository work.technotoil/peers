package com.peerbuckets.peerbucket.Activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.message.AddMessageBoardActivity;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.EDIT_COMMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;


public class AddEditCommentActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, EditorHtmlInterface {

    WebView editor;
    Button mpost_message;
    TextView mmessage_title, memail;
    ApiRequest mApiRequest;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "";
    String groupMembersUserId = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    ImageView user_image;
    AlertDialog alertDialog;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    LinearLayout ll_linearMaster;
    private ShimmerFrameLayout mShimmerViewContainer;
    ProgressBar pb_progressBar, progress;
    private String updatedHtml = "";
    Toolbar toolbar;
    Context context = this;
    String comment_id = "", comment_description = "", fount = "", back = "", name = "", imageUser = "", position = "", record_id = "", message_title = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_comment);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        if (getIntent().getStringExtra("comment_id") != null) {
            comment_id = getIntent().getStringExtra("comment_id");
        }
        if (getIntent().getStringExtra("comment_description") != null) {
            comment_description = getIntent().getStringExtra("comment_description");
        }
        if (getIntent().getStringExtra("message_title") != null) {
            message_title = getIntent().getStringExtra("message_title");
        }
        if (getIntent().getStringExtra("position") != null) {
            position = getIntent().getStringExtra("position");
        }
        if (getIntent().getStringExtra("record_id") != null) {
            record_id = getIntent().getStringExtra("record_id");
        }
        if (getIntent().getStringExtra("user_image") != null) {
            imageUser = getIntent().getStringExtra("user_image");
        }
        if (getIntent().getStringExtra("name") != null) {
            name = getIntent().getStringExtra("name");
        }
        if (getIntent().getStringExtra("back") != null) {
            back = getIntent().getStringExtra("back");
        }
        if (getIntent().getStringExtra("fount") != null) {
            fount = getIntent().getStringExtra("fount");
        }

        initUI();
        getPrefrences();

        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                pb_progressBar.setVisibility(View.VISIBLE);
                mpost_message.setVisibility(View.GONE);
                editor.loadUrl("javascript:copyContent()");
                editor.loadUrl("javascript:hello()");
                break;
        }
    }

    private void initUI() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(getResources().getString(R.string.update_Comment));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mpost_message = findViewById(R.id.btn_add_to_do);
        mpost_message.setText(R.string.update);
        mmessage_title = findViewById(R.id.message_title);
        user_image = findViewById(R.id.user_image);
        memail = findViewById(R.id.email);
        editor = findViewById(R.id.editor_add_to_do);
        editor.getSettings().setJavaScriptEnabled(true);
        mmessage_title.setText(Utils.unescapeJava(message_title));
        progress = findViewById(R.id.progress);
        ll_linearMaster = findViewById(R.id.ll_linearMaster);
        pb_progressBar = findViewById(R.id.pb_progressBar);
        editor.setWebChromeClient(new WebChromeClient() {

            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });

        editor.addJavascriptInterface(new WebAppInterfaceEdit(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);
        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
        String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
        String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
        String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
        String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
        String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
        String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
        String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
        String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
        String messageBody = comment_description;
        messageBody = messageBody.replace("\"", "'");

        //messageBody = TextUtils.htmlEncode(messageBody);


        String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                "<script src=" + mentionJsPath + "></script>" +
                "</head><body>";
        String editorHtml = "<form>" +
                "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                "</form>";
        String pas = "<script>" +
                "function hello()" +
                "{" +
                "   var _id = document.getElementById('x').value;" +
                "   var mention_uid = document.getElementById('mention_uid').value;" +
                "    Android.nextScreen(_id,mention_uid);" +
                "}" +
                "</script></body></html>";
        String myHtmlString = pish + editorHtml + pas;
        editor.loadData(myHtmlString, "text/html; charset=utf-8", null);
        mpost_message.setOnClickListener(this);

        if (!imageUser.equals("")) {
            Picasso.with(context).load(DEVELOPMENT_URL + imageUser).into(user_image);
        } else {
            user_image.setImageDrawable(Utils.imageLatter(Utils.word(name), ShardPreferences.get(context, ShardPreferences.key_backColor), ShardPreferences.get(context, ShardPreferences.key_fontColor)));
        }
        memail.setText(name);
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", mcurrent_type);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);

    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(this, share_Company_Id_key);
        mcurrentProjectTeamId = ShardPreferences.get(this, share_current_project_team_id);
        mcurrent_type = ShardPreferences.get(this, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(this, share_current_UserId_key);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            ll_linearMaster.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(EDIT_COMMENT)) {
            Log.d("ADD_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                pb_progressBar.setVisibility(View.GONE);
                mpost_message.setVisibility(View.VISIBLE);
                if (status == 1) {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent intent = getIntent();
                    intent.putExtra("message_id", record_id);
                    intent.putExtra("position", position);
                    intent.putExtra("comment", Utils.unescapeJava(updatedHtml));
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } catch (Exception e) {
                pb_progressBar.setVisibility(View.GONE);
                mpost_message.setVisibility(View.VISIBLE);
                Log.d("EXCPTN", e.getMessage());
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void setData(String htmlData, String mention_uid) {
        updatedHtml = htmlData;
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("mention_uid", mention_uid);
        paramsReq.put("users", groupMembersUserId);
        paramsReq.put("platform", "mobile");
        paramsReq.put("edit_comment", Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData)));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("record_type", "message");
        paramsReq.put("comment_id", comment_id);
        paramsReq.put("record_id", record_id);
        mApiRequest.postRequest(BASE_URL + EDIT_COMMENT, EDIT_COMMENT, paramsReq, Request.Method.POST);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
            Uri result = intent == null || resultCode != AddMessageBoardActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
    }
}


