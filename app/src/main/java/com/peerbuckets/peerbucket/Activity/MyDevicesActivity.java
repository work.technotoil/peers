package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.DeviceListAdapter;
import com.peerbuckets.peerbucket.Adapter.TimezoneAdapter;
import com.peerbuckets.peerbucket.POJO.DeviceDatum;
import com.peerbuckets.peerbucket.POJO.MessageList;
import com.peerbuckets.peerbucket.POJO.MessageTypePOJO;
import com.peerbuckets.peerbucket.POJO.TimezoneListModel;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.CONNECTED_DEVICE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_MESSAGE_BOARD_DETAILS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class MyDevicesActivity extends AppCompatActivity implements IApiResponse {

    RecyclerView recyclerDevice;
    DeviceListAdapter mAdapter;
    ArrayList<DeviceDatum> mDeviceDatumList = new ArrayList<>();
    LinearLayoutManager mLaayoutManager;
    ImageView ic_close_lg;
    ApiRequest mApiRequest;
    Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_devices);

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        ic_close_lg = findViewById(R.id.close_tcp_dialog_iv);
        if (ShardPreferences.get(context, share_language).equals("2")) {
            ic_close_lg.setImageResource(R.drawable.ic_arrow_forward);
        }
        ic_close_lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerDevice = findViewById(R.id.recyclerDevice);
        recyclerDevice.setHasFixedSize(true);
        mLaayoutManager = new LinearLayoutManager(this);
        mLaayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerDevice.setLayoutManager(mLaayoutManager);

        Map<String, String> paramsReq = new HashMap<>();
        mApiRequest.postRequest(BASE_URL + CONNECTED_DEVICE + "/" + ShardPreferences.get(this, share_UserId_key), CONNECTED_DEVICE, paramsReq, Request.Method.POST);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(CONNECTED_DEVICE)) {
            Log.d("CONNECTED_DEVICE", response);
            try {

                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    JSONArray mdeviceDataArray = jsonObject.getJSONArray("deviceData");
                    mDeviceDatumList = new Gson().fromJson(mdeviceDataArray.toString(), new TypeToken<List<DeviceDatum>>() {
                    }.getType());
                    mAdapter = new DeviceListAdapter(mDeviceDatumList, this);
                    recyclerDevice.setAdapter(mAdapter);
                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();

    }
}
