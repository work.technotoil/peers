package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.peerbuckets.peerbucket.Adapter.ProiortyEmployeeIconAdapter;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;

import java.util.ArrayList;
import java.util.List;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class ChatingUserListActivity extends AppCompatActivity {
    RecyclerView rv_chating_userList;
    RecyclerView.LayoutManager rv_chating_userListLayoutManager;
    Toolbar toolbar;
    Context context = this;
    List<User> userList;
    ProiortyEmployeeIconAdapter proiortyEmployeeIconAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chating_user_list);

        rv_chating_userList = findViewById(R.id.rv_chating_userList);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent().getSerializableExtra("list") != null) {
            Intent i = getIntent();
            userList = (List<User>) i.getSerializableExtra("list");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.Group_User));
        if (ShardPreferences.get(context,share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rv_chating_userList.setHasFixedSize(true);
        rv_chating_userListLayoutManager = new LinearLayoutManager(context);
        rv_chating_userList.setLayoutManager(rv_chating_userListLayoutManager);

        proiortyEmployeeIconAdapter = new ProiortyEmployeeIconAdapter(context, (ArrayList<User>) userList, 1);
        rv_chating_userList.setAdapter(proiortyEmployeeIconAdapter);
    }
}
