package com.peerbuckets.peerbucket.Activity.schedule;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.message.AddMessageBoardActivity;
import com.peerbuckets.peerbucket.Adapter.InvitedUserAdapter;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_NEW_SCHEDULE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_INVITED_USERS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_SCHEDULE_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_SCHEDULE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class AddScheduleActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse, EditorHtmlInterface {
    Context context = this;
    EditText etTitle;
    CheckBox checkBoxTime;
    TextView tvStartDate, tvStartTime, tvEndDate, tvEndTime, tvAssignedUser, tv_messageTitle;
    WebView webViewAssignedUsers, webViewDesc;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    String assignedUser = "", groupMembersUserId = "", taskDesc = "", startDate = "", startTime = "", endDate = "", endTime = "", type = "", scheduleId = "";
    ApiRequest apiRequest;
    private List<User> userList = new ArrayList<>();
    private InvitedUserAdapter userAdapter;
    RecyclerView recyclerUsers;
    BottomSheetBehavior bottomSheetUsers;
    List<User> selectedUserList = new ArrayList<>();
    Toolbar toolbar;
    ImageView imgDoneBottomSheet, imgCancelBottomSheet;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    Button btnAddSchedule;
    public static final String TYPE_ADD = "add", TYPE_EDIT = "edit";
    ProgressBar pb_progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        if (getIntent().getStringExtra("schedule_id") != null) {
            scheduleId = getIntent().getStringExtra("schedule_id");
        }

        if (getIntent().getStringExtra("type") != null) {
            type = getIntent().getStringExtra("type");
        }

        inIt();
        getInvitedUsers();
    }

    private void inIt() {

        apiRequest = new ApiRequest(this, this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        back();
        btnAddSchedule = findViewById(R.id.btn_add_to_do);

        if (scheduleId != null && !scheduleId.equals("")) {
            btnAddSchedule.setText(getResources().getString(R.string.update));
            getSupportActionBar().setTitle(getResources().getString(R.string.update_schedule));
        } else {
            btnAddSchedule.setText(getResources().getString(R.string.add));
            getSupportActionBar().setTitle(getResources().getString(R.string.add_schedule));
        }

        pb_progressBar = findViewById(R.id.pb_progressBar);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        etTitle = findViewById(R.id.et_title_add_schedule);
        checkBoxTime = findViewById(R.id.checkbox_time_add_schedule);
        tvStartDate = findViewById(R.id.tv_start_date_add_schedule);
        tvStartTime = findViewById(R.id.tv_start_time_add_schedule);
        tvEndDate = findViewById(R.id.tv_end_date_add_schedule);
        tvEndTime = findViewById(R.id.tv_end_time_add_schedule);
        tvAssignedUser = findViewById(R.id.tv_assigned_add_schedule);
        webViewAssignedUsers = findViewById(R.id.web_view_assigned_add_schedule);
        webViewDesc = findViewById(R.id.webview_desc_add_schedule);
        imgDoneBottomSheet = findViewById(R.id.img_done_bottom_sheet_user);
        imgCancelBottomSheet = findViewById(R.id.img_close_bottom_sheet_user);

        tvStartDate.setOnClickListener(this);
        tvStartTime.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        tvEndTime.setOnClickListener(this);
        tvAssignedUser.setOnClickListener(this);
        imgDoneBottomSheet.setOnClickListener(this);
        imgCancelBottomSheet.setOnClickListener(this);
        btnAddSchedule.setOnClickListener(this);
        tv_messageTitle.setText(ShardPreferences.get(context, ShardPreferences.share_current_module));
        recyclerUsers = findViewById(R.id.recycler_bottom_sheet_user);
        recyclerUsers.setLayoutManager(new LinearLayoutManager(context));
        recyclerUsers.setHasFixedSize(true);

        LinearLayout linearBottomSheet = findViewById(R.id.linear_bottom_sheet_user);
        bottomSheetUsers = BottomSheetBehavior.from(linearBottomSheet);

        webViewDesc.getSettings().setJavaScriptEnabled(true);
        webViewDesc.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webViewDesc.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });
        webViewDesc.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        webViewDesc.setWebContentsDebuggingEnabled(true);
        webViewDesc.getSettings().setDomStorageEnabled(true);
        if (type.equals(TYPE_ADD)) {
            webViewDesc.loadUrl("file:///android_asset/index1.html");
        } else if (type.equals(TYPE_EDIT)) {
            getScheduleDetail();
        }

        checkBoxTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvStartTime.setVisibility(View.INVISIBLE);
                    tvStartTime.setText("");
                    tvEndTime.setVisibility(View.INVISIBLE);
                    tvEndTime.setText("");
                } else {
                    tvStartTime.setVisibility(View.VISIBLE);
                    tvStartTime.setText("");
                    tvEndTime.setVisibility(View.VISIBLE);
                    tvEndTime.setText("");
                }

            }
        });


    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getScheduleDetail() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        paramsReq.put("schedule_id", scheduleId);
        apiRequest.postRequest(BASE_URL + GET_SCHEDULE_DETAIL, GET_SCHEDULE_DETAIL, paramsReq, Request.Method.POST);
    }

    private void getInvitedUsers() {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, share_addd_remove_current_type));
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        apiRequest.postRequestBackground(BASE_URL + GET_INVITED_USERS, GET_INVITED_USERS, paramsReq, Request.Method.POST);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                boolean check = CheckDates(startDate, endDate);
                if (check) {
                    btnAddSchedule.setVisibility(View.GONE);
                    pb_progressBar.setVisibility(View.VISIBLE);
                    webViewDesc.loadUrl("javascript:copyContent()");
                    webViewDesc.loadUrl("javascript:hello()");
                } else {
                    Toast.makeText(context, getResources().getString(R.string.select_wrong_date), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.tv_start_date_add_schedule:
                selectDate(tvStartDate);
                break;
            case R.id.tv_start_time_add_schedule:
                selectTime(tvStartTime);
                break;
            case R.id.tv_end_date_add_schedule:
                selectDate(tvEndDate);
                break;
            case R.id.tv_end_time_add_schedule:
                selectTime(tvEndTime);
                break;
            case R.id.tv_assigned_add_schedule:
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                } else {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
            case R.id.img_close_bottom_sheet_user:
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                selectedUserList.clear();
                userAdapter.clearAllSelections();
                webViewAssignedUsers.loadData("", "text/html", "utf-8");
                break;
            case R.id.img_done_bottom_sheet_user:
                assignedUser = "";
                if (bottomSheetUsers.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetUsers.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
                if (userAdapter != null) {
                    selectedUserList.clear();
                    selectedUserList = userAdapter.getSelectedUsers();
                    if (selectedUserList != null && selectedUserList.size() > 0) {
                        for (User selectedUser : selectedUserList) {
                            assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                        }
                        webViewAssignedUsers.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUser, "text/html", "utf-8", null);
                    } else {
                        webViewAssignedUsers.loadData("", "text/html", "utf-8");
                    }
                }
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_INVITED_USERS)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray usersArray = jsonObject.getJSONArray("user_list");

                    if (usersArray.length() > 0) {
                        userList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                        }.getType());
                        if (userList != null && userList.size() > 0) {
                            userAdapter = new InvitedUserAdapter(userList, context);
                            recyclerUsers.setAdapter(userAdapter);
                        }
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        webViewDesc.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }

                        //recyclerEmployee

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(GET_SCHEDULE_DETAIL)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONObject detailObject = jsonObject.getJSONObject("schedule_detail");
                    updateUI(detailObject);
                } else {


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(ADD_NEW_SCHEDULE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    btnAddSchedule.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    Intent intent = getIntent();
                    intent.putExtra("id", scheduleId);
                    setResult(RESULT_OK);
                    finish();
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(UPDATE_SCHEDULE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    btnAddSchedule.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    setResult(RESULT_OK);
                    finish();
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


    @Override
    public void setData(String htmlData, String mention_uid) {
        taskDesc = htmlData;
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        paramsReq.put("startTime", startTime);
        paramsReq.put("endTime", endTime);
        paramsReq.put("title", Utils.getUnicodeString(Utils.escapeUnicodeText(etTitle.getText().toString())));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("selector", checkBoxTime.isChecked() ? "all_day" : "");
        paramsReq.put("startDate", startDate);
        paramsReq.put("endDate", endDate);
        paramsReq.put("timezone", "GMT");
        paramsReq.put("description", Utils.getUnicodeString(Utils.escapeUnicodeText(taskDesc)));
        paramsReq.put("users", groupMembersUserId);
        paramsReq.put("mention_uid", mention_uid);
        if (type.equals(TYPE_ADD)) {
            apiRequest.postRequest(BASE_URL + ADD_NEW_SCHEDULE, ADD_NEW_SCHEDULE, paramsReq, Request.Method.POST);
        } else if (type.equals(TYPE_EDIT)) {
            paramsReq.put("schedule_id", scheduleId);
            apiRequest.postRequest(BASE_URL + UPDATE_SCHEDULE, UPDATE_SCHEDULE, paramsReq, Request.Method.POST);
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
// Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
// Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = intent == null || resultCode != AddMessageBoardActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
    }


    public static boolean CheckDates(String d1, String d2) {
        boolean b = false;
        SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
        try {
            if (dfDate.parse(d1).before(dfDate.parse(d2))) {
                b = true;//If start date is before end date
            } else if (dfDate.parse(d1).equals(dfDate.parse(d2))) {
                b = true;//If two dates are equal
            } else {
                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    private void selectDate(final TextView tv) {
        int mYear, mMonth, mDay;

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(context, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                String cDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                String cTime = Utils.parseDateWithFormat(currentDateTimeString, "MMM dd, yyyy", "HH:mm:ss");

                if (tv == tvStartDate) {
                    startDate = cDate;
                    // startDate = Utils.parseDateWithFormat(cDate, "dd-MM-yyyy", "yyyy-MM-dd");
                } else if (tv == tvEndDate) {
                    endDate = cDate;
                    //endDate = Utils.parseDateWithFormat(cDate, "dd-MM-yyyy", "yyyy-MM-dd");
                }
                cDate = Utils.parseDateWithFormat(cDate, "dd-MM-yyyy", "yyyy-MMM-dd");
                tv.setText(cDate);
            }
        }, mYear, mMonth, mDay);
//                Calendar calendar = Calendar.getInstance();
//                calendar.add(Calendar.YEAR, -18);
//                mDatePicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());

        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());

        mDatePicker.show();
    }

    private void selectTime(final TextView tvTime) {
        //Open Time Picker for choose BirthTime
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                }
                tvTime.setText(pad(selectedHour) + ":" + pad(selectedMinute));
//                pickedTime = Utils.parseDateWithFormat(cDate + " " + cTime, "dd-MM-yyyy", "yyyy-MM-dd HH:mm:ss");

                if (tvTime == tvStartTime) {
                    startTime = pad(selectedHour) + ":" + pad(selectedMinute);
                } else if (tvTime == tvEndTime) {
                    endTime = pad(selectedHour) + ":" + pad(selectedMinute);
                }

            }
        }, hour, minute, true);
        mTimePicker.show();

    }

    public String pad(int input) {
        // for converting time value for time picker
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }

    private String getAssignedUserString() {
        List<String> usersIdList = new ArrayList<>();
        if (selectedUserList != null && selectedUserList.size() > 0) {
            for (User userId : selectedUserList) {
                usersIdList.add(userId.getUserId());
            }
            return android.text.TextUtils.join(",", usersIdList);

        } else {
            return "";
        }

    }

    private Boolean isTimeSelected() {
        if (!checkBoxTime.isChecked()) {
            if (!Utils.isStringValid(startTime) || !Utils.isStringValid(endTime)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }

    }

    private void updateUI(JSONObject detailObject) {
        try {

            etTitle.setText(Utils.checkStringWithHash(Utils.unescapeJava(detailObject.getString("title"))));
            String messageBody = detailObject.getString("description");
            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
            String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
            String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
            String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
            String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
            String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
            String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

            if (Utils.isStringValid(messageBody)) {
                messageBody = messageBody.replace("\"", "'");
            }

            //messageBody = TextUtils.htmlEncode(messageBody);

            String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                    "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                    "<script src=" + mentionJsPath + "></script>" +
                    "</head><body>";
            String editorHtml = "<form>" +
                    "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                    "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                    "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                    "</form>";
            String pas = "<script>" +
                    "function hello()" +
                    "{" +
                    "   var _id = document.getElementById('x').value;" +
                    "   var mention_uid = document.getElementById('mention_uid').value;" +
                    "    Android.nextScreen(_id,mention_uid);" +
                    "}" +
                    "</script></body></html>";
            String myHtmlString = pish + editorHtml + pas;
            webViewDesc.loadData(Utils.unescapeJava("<style>img{display: inline;height: auto;max-width: 100%;}</style>" + myHtmlString), "text/html; charset=utf-8", null);

            if (Utils.isStringValid(detailObject.getString("start_date_time"))) {
                tvStartDate.setText(Utils.parseDateWithFormat(detailObject.getString("start_date_time"), "yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy"));
                startDate = Utils.parseDateWithFormat(detailObject.getString("start_date_time"), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
                if (Utils.isStringValid(detailObject.getString("selector")) && detailObject.getString("selector").equals("all_day")) {
                    checkBoxTime.setChecked(true);
                } else {
                    tvStartTime.setText(Utils.parseDateWithFormat(detailObject.getString("start_date_time"), "yyyy-MM-dd HH:mm:ss", "HH:mm"));
                    startTime = Utils.parseDateWithFormat(detailObject.getString("start_date_time"), "yyyy-MM-dd HH:mm:ss", "HH:mm");
                }

            }

            if (Utils.isStringValid(detailObject.getString("end_date_time"))) {
                tvEndDate.setText(Utils.parseDateWithFormat(detailObject.getString("end_date_time"), "yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy"));
                endDate = Utils.parseDateWithFormat(detailObject.getString("end_date_time"), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
                if (Utils.isStringValid(detailObject.getString("selector")) && detailObject.getString("selector").equals("all_day")) {
                    checkBoxTime.setChecked(true);
                } else {
                    tvEndTime.setText(Utils.parseDateWithFormat(detailObject.getString("end_date_time"), "yyyy-MM-dd HH:mm:ss", "HH:mm"));
                    endTime = Utils.parseDateWithFormat(detailObject.getString("end_date_time"), "yyyy-MM-dd HH:mm:ss", "HH:mm");
                }
            }

            JSONArray usersArray = detailObject.getJSONArray("users");
            if (usersArray.length() > 0) {
                selectedUserList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                }.getType());

                if (selectedUserList != null && selectedUserList.size() > 0) {
                    for (User selectedUser : selectedUserList) {
                        assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                    }
                    if (userAdapter != null) {
                        userAdapter.updateSelectedUser(selectedUserList);
                    }
                }
                webViewAssignedUsers.loadData(assignedUser, "text/html", "utf-8");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
