package com.peerbuckets.peerbucket.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FORGOT_PASSWORD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.USER_LOGIN;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.get;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_name_key;

public class EnterPasswordActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {
    ApiRequest mApiRequest;
    Context context = this;
    EditText password_text;
    TextView tv_forgotPassword, contact_support_tv;
    Button btn_next;
    ImageView img_back;
    ProgressBar pb_enterPassword;
    private String mtimezone = "", uuid = "";
    private static final int READ_PHONE_STATE_PERMISSION = 1253;
    String isFlow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_password);

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        pb_enterPassword = findViewById(R.id.pb_enterPassword);
        img_back = findViewById(R.id.img_back);
        if (ShardPreferences.get(context, share_language).equals("2")) {
            img_back.setImageResource(R.drawable.ic_arrow_forward);
        }

        tv_forgotPassword = findViewById(R.id.tv_forgotPassword);
        password_text = findViewById(R.id.password_text);
        contact_support_tv = findViewById(R.id.contact_support_tv);
        btn_next = findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        contact_support_tv.setOnClickListener(this);
        img_back.setOnClickListener(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = task.getResult().getToken();
                        ShardPreferences.save(context, ShardPreferences.share_current_user_token, token);
                    }
                });


        /*TIMEZONE CALCULATION*/
        TimeZone timeZone = TimeZone.getDefault();
        mtimezone = String.valueOf(timeZone.getID());

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            uuid = tManager.getDeviceId();
            return;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PHONE_STATE_PERMISSION);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                finish();
                break;

            case R.id.btn_next:

                if (password_text.getText().toString().equals("")) {
                    password_text.setFocusable(true);
                    password_text.setError(getResources().getString(R.string.Please_Enter_your_Passwords));
                    // Toast.makeText(this, getResources().getString(R.string.Please_Enter_your_Password), Toast.LENGTH_SHORT).show();
                } else {
                    pb_enterPassword.setVisibility(View.VISIBLE);
                    btn_next.setVisibility(View.GONE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("email", get(this, share_user_email_key));
                    paramsReq.put("password", password_text.getText().toString());
                    paramsReq.put("timezone", mtimezone);
                    paramsReq.put("device_token", ShardPreferences.get(this, ShardPreferences.share_current_user_token));
                    paramsReq.put("device_id", uuid);
                    String device_name = android.os.Build.MODEL;
                    Field[] fields = Build.VERSION_CODES.class.getFields();
                    String versionName = "";
                    try {
                        versionName = Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
                    } catch (Exception e) {
                        versionName = "";
                    }
                    paramsReq.put("device_name", device_name);
                    paramsReq.put("os_version", versionName);
                    paramsReq.put("type", "Android");

                    mApiRequest.postRequest(BASE_URL + USER_LOGIN, USER_LOGIN, paramsReq, Request.Method.POST);
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                break;


            case R.id.contact_support_tv:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_support_team, null);
                dialogBuilder.setView(dialogView);
                TextView mDialogCancel = dialogView.findViewById(R.id.contact_support_cancel_tv);
                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;


            case R.id.tv_forgotPassword:
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("user_email", ShardPreferences.get(context, share_user_email_key));
                mApiRequest.postRequest(BASE_URL + FORGOT_PASSWORD, FORGOT_PASSWORD, paramsReq, Request.Method.POST);
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals(FORGOT_PASSWORD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");

                if (code.equals("200")) {
                    Intent intent = new Intent(context, CreatNewPasswordActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(USER_LOGIN)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    pb_enterPassword.setVisibility(View.GONE);
                    btn_next.setVisibility(View.VISIBLE);
                    isFlow = jsonObject.getString("isFlow");
                    if (isFlow.equals("6")) {
                        ShardPreferences.save(context, ShardPreferences.share_Company_Id_key, jsonObject.getString("company_id"));
                        ShardPreferences.save(context, ShardPreferences.share_user_name_key, jsonObject.getString("employee_name"));
                        ShardPreferences.save(context, ShardPreferences.share_ISFLOW, jsonObject.getString("isFlow"));
                        ShardPreferences.save(context, ShardPreferences.key_backColor, jsonObject.getString("bgColor"));
                        ShardPreferences.save(context, ShardPreferences.key_fontColor, jsonObject.getString("fontColor"));
                        Intent intent = new Intent(context, HomeActivity.class);
                        startActivity(intent);
                        finishAffinity();
                    } else {
                        Intent intent = new Intent(context, SetupAccountActivity.class);
                        intent.putExtra("isFlowKey", isFlow);
                        intent.putExtra("employeeId", jsonObject.getString("user_id"));
                        intent.putExtra("company_id", jsonObject.getString("company_id"));
                        ShardPreferences.save(context, share_user_name_key, jsonObject.getString("employee_name"));
                        startActivity(intent);
                        finish();
                    }
                } else {
                    pb_enterPassword.setVisibility(View.GONE);
                    btn_next.setVisibility(View.VISIBLE);
                    if (code.equals("404")) {
                        Toast.makeText(this, getResources().getString(R.string.please_fill_all_the_req_fields), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (JSONException e) {
                pb_enterPassword.setVisibility(View.GONE);
                btn_next.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
