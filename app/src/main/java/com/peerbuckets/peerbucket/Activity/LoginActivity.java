package com.peerbuckets.peerbucket.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.CHECK_EMAIL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private ImageView navigateBack;
    private LinearLayout mainLayout, forgotPasswordLayout;
    private EditText emailText;
    private Button navigateNext;
    private TextView mContactSupportTv;
    private ApiRequest mApiRequest;
    private String mUserIdValue = "", mUserEmail = "", mtimezone = "", mImage = "";
    String token = "",language="";
    Context context = this;
    FrameLayout fl_google_login;
    ProgressBar pg_progressBar;
    String isFlow = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        language= Locale.getDefault().getDisplayLanguage();
        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        navigateBack = findViewById(R.id.navigate_back);
        if (language.equals("???????")){
            navigateBack.setImageResource(R.drawable.ic_arrow_forward);
        }

        navigateBack.setOnClickListener(this);

        mainLayout = findViewById(R.id.main_layout);

        forgotPasswordLayout = findViewById(R.id.forgot_password_layout);
        emailText = findViewById(R.id.email);
        navigateNext = findViewById(R.id.navigate_next);

        fl_google_login = findViewById(R.id.fl_google_login);
        pg_progressBar = findViewById(R.id.pg_progressBar);

        fl_google_login.setOnClickListener(this);
        navigateNext.setOnClickListener(this);


        mContactSupportTv = findViewById(R.id.contact_support_tv);


        mContactSupportTv.setOnClickListener(this);

        emailText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isEmailValid(emailText.getText().toString())) {
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("email", emailText.getText().toString());
                        mApiRequest.postRequest(BASE_URL + CHECK_EMAIL, CHECK_EMAIL, paramsReq, Request.Method.POST);
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    } else {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_is_not_valid), Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = task.getResult().getToken();
                        ShardPreferences.save(LoginActivity.this, ShardPreferences.share_current_user_token, token);
                    }
                });


        /*TIMEZONE CALCULATION*/
        TimeZone timeZone = TimeZone.getDefault();
        mtimezone = String.valueOf(timeZone.getID());
    }

    private static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.navigate_back:
                finish();
                break;
            case R.id.fl_google_login:
                Toast.makeText(context, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                break;

            case R.id.navigate_next:
                if (isEmailValid(emailText.getText().toString())) {
                    pg_progressBar.setVisibility(View.VISIBLE);
                    navigateNext.setVisibility(View.GONE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("email", emailText.getText().toString());
                    mApiRequest.postRequest(BASE_URL + CHECK_EMAIL, CHECK_EMAIL, paramsReq, Request.Method.POST);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.email_is_not_valid), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.contact_support_tv:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_support_team, null);
                dialogBuilder.setView(dialogView);
                TextView mDialogCancel = dialogView.findViewById(R.id.contact_support_cancel_tv);
                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;

        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(CHECK_EMAIL)) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    String code = jsonObject.getString("code");
                    String passwordStatus = jsonObject.getString("password");
                    if (code.equals("200")) {
                        pg_progressBar.setVisibility(View.GONE);
                        navigateNext.setVisibility(View.VISIBLE);
                        mUserIdValue = jsonObject.getString("user_id");
                        mUserEmail = emailText.getText().toString();
                        ShardPreferences.save(LoginActivity.this, share_UserId_key, mUserIdValue);
                        ShardPreferences.save(LoginActivity.this, share_current_UserId_key, mUserIdValue);
                        ShardPreferences.save(LoginActivity.this, share_user_email_key, mUserEmail);
                            if (passwordStatus.equals("true")) {
                                Intent intent = new Intent(context, EnterPasswordActivity.class);
                                startActivity(intent);
                            }
                    }
                } else {
                    pg_progressBar.setVisibility(View.GONE);
                    navigateNext.setVisibility(View.VISIBLE);
                    String message = jsonObject.getString("message");
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    Intent toSignUp = new Intent(this, SignUpActivity.class);
                    startActivity(toSignUp);
                    finish();
                }
            } catch (Exception e) {
                pg_progressBar.setVisibility(View.GONE);
                navigateNext.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


}
