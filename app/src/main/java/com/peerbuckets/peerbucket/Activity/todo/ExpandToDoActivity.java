package com.peerbuckets.peerbucket.Activity.todo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddCommentActivity;
import com.peerbuckets.peerbucket.Adapter.AddRemoveUserAdapter;
import com.peerbuckets.peerbucket.Adapter.EmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.Adapter.ToDoComSubTaskAdapter;
import com.peerbuckets.peerbucket.Adapter.ToDoSubTaskAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.ToDoList;
import com.peerbuckets.peerbucket.POJO.ToDoSubTaskPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_REMOVE_NOTIFIED;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_TODO;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_SINGLE_TODO;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SUBSCRIBE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class ExpandToDoActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, RefreshCommentInterface {

    Context context = ExpandToDoActivity.this;
    TextView tv_messageTitle;
    RelativeLayout rel_comment;
    LinearLayout madd_todo_ll;
    TextView madd_comment_tv, message_count_tv, tv_add_remove, tv_people, tv_view;
    String to_do_desc = "", SubTodo_id = "", lastName = "";
    ApiRequest mApiRequest;
    RecyclerView sub_task_rv, completed_subtask_rv, mcomments_rv;
    ToDoSubTaskAdapter mToDoSubTaskAdapter;
    ToDoComSubTaskAdapter mToDoComSubTaskAdapter;
    MessageCommentsAdapter mcomments_rv_adapter;
    ArrayList<ToDoSubTaskPOJO> mToDoSubTaskPOJOS;
    ArrayList<ToDoSubTaskPOJO> mCompletedSubTask = new ArrayList<>();
    ArrayList<ToDoSubTaskPOJO> mUnCompleteSubTask = new ArrayList<>();
    ArrayList<MessageCommentPOJO> messageCommentPOJOS;
    private String all_users_id;
    String email, name;
    // private LinearLayout mcomments_LL;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 0;
    private Boolean loading = true;
    LinearLayoutManager layoutManagerComment;
    WebView mto_do_description;
    public static int REQUEST_SUB_TO_DO_CHANGES = 105;
    public static int REQUEST_SUB_TO_DO_ADD = 100;
    private int REQUEST_CODE_EDIT = 2050;
    private int REQUEST_ADD_COMMENT = 552;
    private int toDoPosition = -1;
    String title = "", record_id = "", user_id = "", commaSeperatedID = "";
    private Toolbar toolbar;
    ProgressBar progressBarPagination;

    RecyclerView employeee_icon_rv, employeee_applause_rv;
    RecyclerView.LayoutManager mEmployeListanager, mApplauseManager;
    EmployeeIconAdapter mNotifiedUserAdapter;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetalPOJO = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mApplauseUserList = new ArrayList<>();
    ImageView imgApplause, user_image;
    Boolean isApplause = false;
    Button btn_subscribe, btn_unsubscribe;
    TextView tv_selectOption;
    PopupMenu popup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expand_to_do_layout);
        mApiRequest = new ApiRequest(this, this);
        Intent intent = getIntent();


        if (intent.getStringExtra("to_do_desc") != null) {
            to_do_desc = intent.getStringExtra("to_do_desc");
        }
        if (intent.getStringExtra("SubTodo_id") != null) {
            SubTodo_id = intent.getStringExtra("SubTodo_id");
        }
        if (intent.getStringExtra("to_do_title") != null) {
            title = intent.getStringExtra("to_do_title");
        }

        if (intent.getStringExtra("to_do_id") != null) {
            record_id = intent.getStringExtra("to_do_id");
        }

        if (intent.getStringExtra("user_id") != null) {
            user_id = intent.getStringExtra("user_id");
        }
        if (!SubTodo_id.equals("")) {
            Intent intent1 = new Intent(context, ExpandToDoSubTaskActivity.class);
            intent1.putExtra("sub_task_id", SubTodo_id);
            startActivity(intent1);
        }
        toDoPosition = intent.getIntExtra("position", 0);
        initUI();
        initNotifiedModule();
        loadDetails();
        getCommentList(page);
        applyPagination();
    }

    private void loadDetails() {
        progressBarPagination.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("todo_list_id", record_id);
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequest(BASE_URL + FETCH_SINGLE_TODO, FETCH_SINGLE_TODO, paramsReq, Request.Method.POST);
    }

    private void getCommentList(int cPage) {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "todo_list");
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("todo_id", record_id);
        paramsReq.put("page", String.valueOf(cPage));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);

    }


    private void initUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(getResources().getString(R.string.to_dos));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();
        madd_todo_ll = findViewById(R.id.add_todo_ll);

        user_image = findViewById(R.id.user_image);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        tv_selectOption = findViewById(R.id.tv_selectOption);

        mto_do_description = findViewById(R.id.to_do_description);
        sub_task_rv = findViewById(R.id.sub_task_rv);
        tv_view = findViewById(R.id.tv_view);
        sub_task_rv.setHasFixedSize(true);
        sub_task_rv.setLayoutManager(new LinearLayoutManager(context));
        completed_subtask_rv = findViewById(R.id.completed_subtask_rv);
        completed_subtask_rv.setLayoutManager(new LinearLayoutManager(context));
        completed_subtask_rv.setHasFixedSize(true);
        mcomments_rv = findViewById(R.id.comments_rv);
        layoutManagerComment = new LinearLayoutManager(context);
        layoutManagerComment.setOrientation(RecyclerView.VERTICAL);
        mcomments_rv.setLayoutManager(layoutManagerComment);
        mcomments_rv.setHasFixedSize(true);
        madd_comment_tv = findViewById(R.id.add_comment_tv);
        message_count_tv = findViewById(R.id.message_count_tv);
        rel_comment = findViewById(R.id.rel_comment);
        progressBarPagination = findViewById(R.id.progress_bar_pagination);

        madd_todo_ll.setOnClickListener(this);
        madd_comment_tv.setOnClickListener(this);

        tv_selectOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectOption(view);
            }
        });
    }

    private void SelectOption(View v) {
        popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.message_board_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit_menu:
                        Intent intent = new Intent(context, AddToDoActivity.class);
                        intent.putExtra("id", record_id);
                        intent.putExtra("title", Utils.getUnicodeString(Utils.escapeUnicodeText(title)));
                        intent.putExtra("description", Utils.getUnicodeString(Utils.escapeUnicodeText(to_do_desc)));
                        intent.putExtra("users", all_users_id);
                        intent.putExtra("classType", AddToDoActivity.TYPE_EDIT);
                        startActivityForResult(intent, REQUEST_CODE_EDIT);
                        return true;
                    case R.id.menu_trash:
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                        paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("record_type", "Todo");
                        paramsReq.put("record_id", record_id);
                        mApiRequest.postRequest(BASE_URL + DELETE_TODO, DELETE_TODO, paramsReq, Request.Method.POST);
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public void initNotifiedModule() {
        tv_add_remove = findViewById(R.id.tv_add_remove);
        tv_people = findViewById(R.id.tv_people);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_unsubscribe = findViewById(R.id.btn_unsubscribe);
        imgApplause = findViewById(R.id.imgApplause);
        employeee_icon_rv = findViewById(R.id.employeee_icon_rv);
        mEmployeListanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_icon_rv.setLayoutManager(mEmployeListanager);

        employeee_applause_rv = findViewById(R.id.employeee_applause_rv);
        mApplauseManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_applause_rv.setLayoutManager(mApplauseManager);

        tv_add_remove.setOnClickListener(this);
        imgApplause.setOnClickListener(this);
        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.VISIBLE);
                btn_subscribe.setVisibility(View.GONE);
                btn_unsubscribe.setVisibility(View.VISIBLE);
                actionSubscribe("1");
            }
        });

        btn_unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.GONE);
                btn_subscribe.setVisibility(View.VISIBLE);
                btn_unsubscribe.setVisibility(View.GONE);
                actionSubscribe("0");
            }
        });
    }

    private void actionSubscribe(String isSubscribe) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", record_id);
        paramsReq.put("company_id", ShardPreferences.get(ExpandToDoActivity.this, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(ExpandToDoActivity.this, share_UserId_key));
        paramsReq.put("loggedIn_user_id", commaSeperatedID);
        paramsReq.put("project_team_id", ShardPreferences.get(ExpandToDoActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "todo_list");
        paramsReq.put("isSubscribe", isSubscribe);
        mApiRequest.postRequestBackground(BASE_URL + SUBSCRIBE, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);

        Intent thisIntent = getIntent();
        startActivity(thisIntent);
        finish();
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("position", -1);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_todo_ll:
                Intent intent = new Intent(this, ActivityAddEditToDoTask.class);
                intent.putExtra("to_do_id", record_id);
                intent.putExtra("to_do_title", title);
                intent.putExtra("class_type", ActivityAddEditToDoTask.TYPE_ADD);
                startActivityForResult(intent, REQUEST_SUB_TO_DO_ADD);
                break;
            case R.id.tv_add_remove:
                addRemovePeopleDialog();
                break;
            case R.id.imgApplause:
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause();
                break;
            case R.id.add_comment_tv:
                Intent intent1 = new Intent(ExpandToDoActivity.this, AddCommentActivity.class);
                intent1.putExtra("call_type", "todo_list");
                intent1.putExtra("message_title", title);
                intent1.putExtra("message_email", email);
                intent1.putExtra("name", name);
                intent1.putExtra("record_id", record_id);
                intent1.putExtra("all_users_id", all_users_id);
                startActivityForResult(intent1, REQUEST_ADD_COMMENT);
                break;
        }
    }

    public void addApplause() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", record_id);
        paramsReq.put("company_id", ShardPreferences.get(ExpandToDoActivity.this, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(ExpandToDoActivity.this, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(ExpandToDoActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "todo_list");
        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }
        mApiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);
    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        progressBarPagination.setVisibility(View.GONE);
        if (tag_json_obj.equals(FETCH_SINGLE_TODO)) {

            Log.d("SINGLE_TODO_API", response);
            try {
                JSONObject jsonObject = new JSONObject(response);

                int status = jsonObject.getInt("status");
                if (status == 1) {
                    isApplause = jsonObject.getBoolean("isApplause");

                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                        tv_view.setVisibility(View.VISIBLE);
                        btn_subscribe.setVisibility(View.GONE);
                    } else {
                        tv_view.setVisibility(View.GONE);
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    JSONObject jsonObject1 = jsonObject.getJSONObject("todo_details");
                    name = jsonObject1.getString("employee_name");
                    title = jsonObject1.getString("todo_list_title");
                    tv_messageTitle.setText(jsonObject1.getString("todo_list_title"));
                    to_do_desc = jsonObject1.getString("todo_list_desc");

                    if (!jsonObject1.getString("user_image").equals("")) {
                        Picasso.with(this).load(ShardPreferences.get(context, ShardPreferences.share_user_image_key)).into(user_image);

                    } else {
                        user_image.setImageDrawable(imageLatter(Utils.word(name), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
                    }
                    JSONArray jsonArray = jsonObject1.getJSONArray("todoSubtask");
                    tv_messageTitle.setText(Utils.checkStringWithEmpty(Utils.unescapeJava(jsonObject1.getString("todo_list_title"))));
                    title = Utils.checkStringWithEmpty(Utils.unescapeJava(jsonObject1.getString("todo_list_title")));

                    if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
                        mto_do_description.loadData("<body dir=\"rtl\"><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + Utils.unescapeJava(jsonObject1.getString("todo_list_desc")), "text/html; charset=utf-8", null);
                    } else {
                        mto_do_description.loadData("<body dir=\"ltr\" ><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + Utils.unescapeJava(jsonObject1.getString("todo_list_desc")), "text/html; charset=utf-8", null);
                    }

                    Log.d("SINGLE_TODO_API", jsonArray.toString());
                    if (jsonArray.length() > 0) {

                        mToDoSubTaskPOJOS = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<ToDoSubTaskPOJO>>() {
                        }.getType());

                        for (ToDoSubTaskPOJO list : mToDoSubTaskPOJOS) {
                            if (list.getCompletedOn() != null) {
                                mCompletedSubTask.add(list);
                            } else {
                                mUnCompleteSubTask.add(list);
                            }
                        }
                        if (mUnCompleteSubTask != null && mUnCompleteSubTask.size() > 0) {
                            mToDoSubTaskAdapter = new ToDoSubTaskAdapter(mUnCompleteSubTask, context);
                            sub_task_rv.setAdapter(mToDoSubTaskAdapter);
                        }
                        if (mCompletedSubTask != null && mCompletedSubTask.size() > 0) {
                            mToDoComSubTaskAdapter = new ToDoComSubTaskAdapter(mCompletedSubTask, context);
                            completed_subtask_rv.setAdapter(mToDoComSubTaskAdapter);
                        }
                    }
                    JSONObject jsonObject2;
                    JSONArray mUserArray = jsonObject1.getJSONArray("users");
                    if (mUserArray != null) {
                        for (int i = 0; i < mUserArray.length(); i++) {
                            jsonObject1 = mUserArray.getJSONObject(i);
                            if (i == 0) {
                                all_users_id = jsonObject1.getString("user_id");
                            } else {
                                all_users_id = all_users_id + "," + jsonObject1.getString("user_id");
                            }
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));

                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }

                    tv_people.setText("" + mUserArray.length() + " ");
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");

                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject3 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject3.getString("user_id"));
                            mModel.setEmployeeName(jsonObject3.getString("employee_name"));
                            mModel.setUserImage(jsonObject3.getString("user_image"));
                            mModel.setFontColor(jsonObject3.getString("fontColor"));
                            mModel.setBgColor(jsonObject3.getString("bgColor"));

                            mApplauseUserList.add(mModel);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                        employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(DELETE_TODO)) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.getString("status").equals("1")) {
                    Intent intent = getIntent();
                    intent.putExtra("position", toDoPosition);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }
        if (tag_json_obj.equals(FETCH_COMMENT_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    rel_comment.setVisibility(View.VISIBLE);
                    JSONArray mArray = jsonObject.getJSONArray("commentList");
                    if (mArray != null && mArray.length() > 0) {
                        if (page == 0 && messageCommentPOJOS != null) {
                            messageCommentPOJOS.clear();
                        }
                        messageCommentPOJOS = new Gson().fromJson(mArray.toString(), new TypeToken<List<MessageCommentPOJO>>() {
                        }.getType());

                        message_count_tv.setText(messageCommentPOJOS.size() + "");

                        if (messageCommentPOJOS != null && messageCommentPOJOS.size() > 0) {
                            if (page == 0) {
                                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, context, title, record_id, this,0);
                                mcomments_rv.setAdapter(mcomments_rv_adapter);
                            } else {
                                mcomments_rv.post(new Runnable() {
                                    public void run() {
                                        mcomments_rv_adapter.setNotifyData(messageCommentPOJOS);
                                    }
                                });
                            }
                        }

                        page = page + 1;
                        loading = true;
                    }
                } else {
                    if (page == 0) {
                        rel_comment.setVisibility(View.GONE);
                    }
                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new AddRemoveUserAdapter(all_users_id, mEmployeeDetailList, this);
                        recyclerView.setAdapter(mAddRemoveUserAdapter);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_subscribe.setVisibility(View.GONE);
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    tv_people.setText("" + jsonObject.getString("userCount") + " ");
                    JSONArray mUserArray = jsonObject.getJSONArray("update_user_list");
                    if (mUserArray != null) {
                        if (mEmployeeDetalPOJO != null) {
                            mEmployeeDetalPOJO.clear();
                        }
                        for (int i = 0; i < mUserArray.length(); i++) {
                            JSONObject jsonObject1 = mUserArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));

                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mApplauseUserList != null) {
                        mApplauseUserList.clear();
                    }
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));

                            mApplauseUserList.add(mModel);
                        }
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public String getAll_users_id() {
        return all_users_id;
    }

    public String getEmail() {
        return email;
    }

    public String getTo_do_title() {
        return title;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SUB_TO_DO_CHANGES) {
                recreate();
            }
            if (requestCode == REQUEST_ADD_COMMENT) {
                if (messageCommentPOJOS == null) {
                    messageCommentPOJOS = new ArrayList<>();
                }

                if (mcomments_rv_adapter == null) {
                    messageCommentPOJOS.add((MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, title, record_id, this,0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                } else {
                    messageCommentPOJOS.add(0, (MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, title, record_id, this,0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                }
            }
            if (requestCode == 22) {
                finish();
                startActivity(getIntent());
            }
            if (requestCode == REQUEST_CODE_EDIT) {
                ToDoList toDoList;
                toDoList = (ToDoList) data.getSerializableExtra("todo");
                if (toDoList != null) {
                    title = toDoList.getTodoListTitle();
                    to_do_desc = toDoList.getTodoListDesc();
                    tv_messageTitle.setText(Utils.isStringValid(Utils.unescapeJava(toDoList.getTodoListTitle())) ? Utils.unescapeJava(toDoList.getTodoListTitle()) : getResources().getString(R.string.to_dos));
                    title = Utils.isStringValid(Utils.unescapeJava(toDoList.getTodoListTitle())) ? Utils.unescapeJava(toDoList.getTodoListTitle()) : getResources().getString(R.string.to_dos);
                    mto_do_description.loadData("<style>img{display: inline;height: auto;max-width: 100%;}</style>" + Utils.unescapeJava(toDoList.getTodoListDesc()), "text/html; charset=utf-8", null);
                    record_id = toDoList.getTodoListId();
                }
            }
            if (requestCode == 1) {
                ((MessageCommentsAdapter) mcomments_rv_adapter).updateData(data.getIntExtra("position", -1), data.getStringExtra("comment"));
            }
            if (requestCode == REQUEST_SUB_TO_DO_ADD) {
                recreate();
            }
            if (requestCode == 5) {

                String position = data.getStringExtra("position");
                String comment = Utils.unescapeJava(data.getStringExtra("comment"));
                messageCommentPOJOS.get(Integer.parseInt(position)).setCommentDescription(comment);

                mcomments_rv_adapter.notifyDataSetChanged();
            }
        }
    }

    private void applyPagination() {
        // Add Pagination on recycler view android
        mcomments_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManagerComment.getChildCount();
                    totalItemCount = layoutManagerComment.getItemCount();
                    pastVisiblesItems = layoutManagerComment.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            getCommentList(page);

                        }
                    }
                }
            }
        });
    }

    public void refreshCompleteTask(ToDoSubTaskPOJO taskPOJO) {
        if (mToDoComSubTaskAdapter != null) {
            mToDoComSubTaskAdapter.addItem(taskPOJO);
            completed_subtask_rv.smoothScrollToPosition(0);
        } else {
            mCompletedSubTask.add(taskPOJO);
            mToDoComSubTaskAdapter = new ToDoComSubTaskAdapter(mCompletedSubTask, context);
            completed_subtask_rv.setAdapter(mToDoComSubTaskAdapter);
            completed_subtask_rv.smoothScrollToPosition(0);
        }

    }

    public void refreshUnCompleteTask(ToDoSubTaskPOJO taskPOJO) {
        if (mToDoSubTaskAdapter != null) {
            mToDoSubTaskAdapter.addItem(taskPOJO);
            sub_task_rv.smoothScrollToPosition(0);
        } else {
            mUnCompleteSubTask.add(taskPOJO);
            mToDoSubTaskAdapter = new ToDoSubTaskAdapter(mUnCompleteSubTask, context);
            sub_task_rv.setAdapter(mToDoSubTaskAdapter);
            sub_task_rv.smoothScrollToPosition(0);
        }
    }


    @Override
    public void setCommentData(int commentSize) {
        message_count_tv.setText(commentSize + "");
        if (commentSize > 0) {
        } else {
        }
    }


    ProgressBar pb_process;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    AddRemoveUserAdapter mAddRemoveUserAdapter;
    RecyclerView recyclerView;

    public void addRemovePeopleDialog() {
        final Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_remove_user, null);
        dialogBuilder.setContentView(dialogView);
        TextView selectEveryOne = dialogView.findViewById(R.id.selectEveryOne);
        TextView selectNoOne = dialogView.findViewById(R.id.selectNoOne);
        TextView tv_cancel = dialogView.findViewById(R.id.tv_cancel);
        recyclerView = dialogView.findViewById(R.id.rv_dialogAddRemove);
        pb_process = dialogView.findViewById(R.id.pb_process);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);

        selectEveryOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.selectAllSelections();
                    }
                }
            }
        });

        selectNoOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.clearAllSelections();
                    }
                }
            }
        });

        Button btnSave = dialogView.findViewById(R.id.btnSave);
        fetchEmployee();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commaSeperatedID = getAssignedUserString();
                /*if (commaSeperatedID.equals("")) {
                    Toast.makeText(ExpandToDoActivity.this, getResources().getString(R.string.please_select_atleast_one), Toast.LENGTH_SHORT).show();
                } else {*/
                pb_process.setVisibility(View.VISIBLE);
                all_users_id = commaSeperatedID;
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("record_id", record_id);
                paramsReq.put("loggedIn_user_id", ShardPreferences.get(ExpandToDoActivity.this, share_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(ExpandToDoActivity.this, share_Company_Id_key));
                paramsReq.put("user_id", commaSeperatedID);
                paramsReq.put("project_team_id", ShardPreferences.get(ExpandToDoActivity.this, share_current_project_team_id));
                paramsReq.put("notification_type", "todo");
                mApiRequest.postRequestBackground(BASE_URL + ADD_REMOVE_NOTIFIED, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
                dialogBuilder.dismiss();
                //}
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });
        dialogBuilder.show();
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private String getAssignedUserString() {
        if (mAddRemoveUserAdapter != null) {
            return mAddRemoveUserAdapter.getAssignedUserString();
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
