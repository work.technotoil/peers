package com.peerbuckets.peerbucket.Activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.message.AddMessageBoardActivity;
import com.peerbuckets.peerbucket.Adapter.MessageTypeAdapter;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageTypePOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.EDIT_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_TODO;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;


public class AddEditMessageBoardActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, EditorHtmlInterface {

    WebView editor;
    EditText mmessage_board_title;
    Button mpost_message;
    ApiRequest mApiRequest;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "";
    String groupMembersUserId = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    String classType = "", message_id = "", message_title = "", message_body = "";
    Intent intent;
    ArrayList<MessageTypePOJO> messageTypePOJOArrayList;
    MessageTypeAdapter mmessage_type_rv_adapter;
    RecyclerView mmessage_type_rv;
    RecyclerView.LayoutManager mmessage_type_rv_layout_manager;
    AlertDialog alertDialog;
    LinearLayout mmessage_sort_LL;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    LinearLayout ll_linearMaster;
    private ShimmerFrameLayout mShimmerViewContainer;
    private String editorData = "", messageTitle = "";
    private Toolbar toolbar;
    ProgressBar pb_progressBar, progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_addmessageboard);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        intent = getIntent();
        if (getIntent().getStringExtra("class_type") != null) {
            classType = intent.getStringExtra("class_type");
        }
        if (getIntent().getStringExtra("message_body") != null) {
            message_body = intent.getStringExtra("message_body");
        }
        if (getIntent().getStringExtra("message_id") != null) {
            message_id = intent.getStringExtra("message_id");
        }
        if (getIntent().getStringExtra("message_title") != null) {
            message_title = intent.getStringExtra("message_title");
        }


        initUI();
        getPrefrences();

        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });


    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                mpost_message.setVisibility(View.GONE);
                pb_progressBar.setVisibility(View.VISIBLE);
                if (classType.equals("edit") || classType.equals("add")) {
                    editor.loadUrl("javascript:copyContent()");
                    editor.loadUrl("javascript:hello()");
                }
                break;
            case R.id.message_sort_LL:
                showMessageTypeDialog();
                break;

        }
    }

    @SuppressLint("ResourceType")
    private void initUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.edit_a_message));
        back();

        pb_progressBar = findViewById(R.id.pb_progressBar);
        mmessage_board_title = findViewById(R.id.message_board_title);
        mpost_message = findViewById(R.id.btn_add_to_do);
        editor = findViewById(R.id.editor_add_to_do);
        editor.getSettings().setJavaScriptEnabled(true);
        mmessage_sort_LL = findViewById(R.id.message_sort_LL);

        progress = findViewById(R.id.progress);
        ll_linearMaster = findViewById(R.id.ll_linearMaster);
        editor.setWebChromeClient(new WebChromeClient() {

            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });

        editor.addJavascriptInterface(new WebAppInterfaceEdit(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);

        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
        String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
        String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
        String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
        String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
        String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
        String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
        String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
        String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
        String messageBody = intent.getStringExtra("message_body");
        messageBody = messageBody.replace("\"", "'");

        String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "/><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                "<script src=" + mentionJsPath + "></script>" +
                "</head><body>";
        String editorHtml = "<form>" +
                "  <input id=\'x\' value=\"" + messageBody + "\" type=\'hidden\' name=\'content\'>\n" +
                "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                "</form>";
        String pas = "<script>" +
                "function hello()" +
                "{" +
                "   var _id = document.getElementById('x').value;" +
                "   var mention_uid = document.getElementById('mention_uid').value;" +
                "    Android.nextScreen(_id,mention_uid);" +
                "}" +
                "</script></body></html>";
        String myHtmlString = pish + editorHtml + pas;
        editor.loadData(myHtmlString, "text/html; charset=utf-8", null);


        if (classType.equals("edit")) {
            mmessage_board_title.setText(intent.getStringExtra("message_title"));
            mpost_message.setText(R.string.update);
        }
        if (classType.equals("to_do")) {
            mmessage_board_title.setText(intent.getStringExtra("to_do_title"));
            mpost_message.setText(R.string.save_button);
        }
        mpost_message.setOnClickListener(this);
        mmessage_sort_LL.setOnClickListener(this);
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", mcurrent_type);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);

    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(AddEditMessageBoardActivity.this, share_Company_Id_key);
        mcurrentProjectTeamId = ShardPreferences.get(AddEditMessageBoardActivity.this, share_current_project_team_id);
        mcurrent_type = ShardPreferences.get(AddEditMessageBoardActivity.this, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(AddEditMessageBoardActivity.this, share_current_UserId_key);
    }

    private void setMessageTypeRecyclerView() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_message_type, null);
        dialogBuilder.setView(dialogView);

        mmessage_type_rv = dialogView.findViewById(R.id.message_type_rv);
        mmessage_type_rv.setHasFixedSize(true);

        mmessage_type_rv_layout_manager = new LinearLayoutManager(this);
        mmessage_type_rv.setLayoutManager(mmessage_type_rv_layout_manager);

        alertDialog = dialogBuilder.create();
        mmessage_type_rv_adapter = new MessageTypeAdapter(messageTypePOJOArrayList, this, "AddEdit");
        mmessage_type_rv.setAdapter(mmessage_type_rv_adapter);
    }

    public void showMessageTypeDialog() {
        if (alertDialog != null)
            alertDialog.show();
    }

    public void hideMessageTypeDialog() {
        if (alertDialog != null)
            alertDialog.hide();

    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            ll_linearMaster.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");
                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (tag_json_obj.equals(ADD_MESSAGE_BOARD)) {
            Log.d("ADD_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    finish();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.getMessage());
            }
        }

        if (tag_json_obj.equals(UPDATE_TODO)) {
            Log.d("ADD_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    finish();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.getMessage());
            }
        }
        if (tag_json_obj.equals(EDIT_MESSAGE_BOARD)) {
            Log.d("EDIT_MB", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                pb_progressBar.setVisibility(View.GONE);
                mpost_message.setVisibility(View.VISIBLE);
                if (status == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("desc", editorData);
                    intent.putExtra("title", messageTitle);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } catch (Exception e) {
                pb_progressBar.setVisibility(View.GONE);
                mpost_message.setVisibility(View.VISIBLE);
                Log.d("EXCPTN", e.getMessage());
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void setData(String htmlData, String mention_uid) {
        int temp = 0;
        editorData = htmlData;
        messageTitle = mmessage_board_title.getText().toString();

        if (Utils.isStringValid(messageTitle)) {
            Map<String, String> paramsReq = new HashMap<>();
            paramsReq.put("mention_uid", mention_uid);
            paramsReq.put("user_id", mCurrentuserId);
            paramsReq.put("project_team_id", mcurrentProjectTeamId);
            paramsReq.put("company_id", mcompanyId);
            String MessTitle = Utils.getUnicodeString(Utils.escapeUnicodeText(mmessage_board_title.getText().toString()));
            paramsReq.put("message_title", MessTitle);
            String description = Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData));
            paramsReq.put("message_description", description);
            paramsReq.put("message_type_id", String.valueOf(temp));
            paramsReq.put("users", groupMembersUserId);
            paramsReq.put("platform", "mobile");
            Log.d("GROUP_USER_ID", groupMembersUserId.toString());
            paramsReq.put("message_id", intent.getStringExtra("message_id"));
            paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
            mApiRequest.postRequest(BASE_URL + EDIT_MESSAGE_BOARD, EDIT_MESSAGE_BOARD, paramsReq, Request.Method.POST);

        } else {
            Toast.makeText(this, getResources().getString(R.string.please_add_message_title), Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
// Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
// Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = intent == null || resultCode != AddMessageBoardActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
    }
}

class WebAppInterfaceEdit {
    Context mContext;
    EditorHtmlInterface mEditorHtmlInterface;

    /**
     * Instantiate the interface and set the context
     */
    WebAppInterfaceEdit(Context c, EditorHtmlInterface mEditorHtmlInterface) {
        mContext = c;
        this.mEditorHtmlInterface = mEditorHtmlInterface;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void nextScreen(String htmlData, String mention_uid) {
        mEditorHtmlInterface.setData(htmlData, mention_uid);

    }
}
