package com.peerbuckets.peerbucket.Activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_COMMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;


public class AddCommentActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse, EditorHtmlInterface {

    String name, email, title = "", all_users_id = "", call_type = "", record_id = "";
    TextView mmessage_title, memail;
    Button madd_comment_tv;
    WebView editor;
    CircleImageView user_image;
    ApiRequest mApiRequest;
    private ShimmerFrameLayout mShimmerViewContainer;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    TextView tv_cancel;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    ProgressBar pb_progress, progress;
    Context context = this;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_comment_layout);
        mApiRequest = new ApiRequest(this, this);
        Intent intent = getIntent();
        if (getIntent().getStringExtra("call_type") != null) {
            call_type = intent.getStringExtra("call_type");
        }
        if (getIntent().getStringExtra("name") != null) {
            name = intent.getStringExtra("name");

        }
        if (getIntent().getStringExtra("message_title") != null) {
            title = intent.getStringExtra("message_title");
        }
        if (getIntent().getStringExtra("message_email") != null) {
            email = intent.getStringExtra("message_email");
        }
        if (getIntent().getStringExtra("record_id") != null) {
            record_id = intent.getStringExtra("record_id");
        }

        initUI();

        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });
    }

    private void initUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.add_comment));

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        back();

        pb_progress = findViewById(R.id.pb_progressBar);
        mmessage_title = findViewById(R.id.message_title);
        memail = findViewById(R.id.email);
        user_image = findViewById(R.id.user_image);
        editor = findViewById(R.id.add_comment);
        editor.getSettings().setJavaScriptEnabled(true);
        editor.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        progress = findViewById(R.id.progress);

        editor.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });


        editor.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);
        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]

        String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

        String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                "<link rel='stylesheet' type='text/css' href='file:///android_asset/trix.css'><link rel='stylesheet' type='text/css' href='file:///android_asset/tribute.css'><script src='file:///android_asset/jquery.js'>" +
                "</script><script src='file:///android_asset/attachments.js'>" +
                "</script><script src='file:///android_asset/trix.js'></script><script src='file:///android_asset/tribute.js'></script>" +
                "<script src='file:///android_asset/mentionmobile.js'></script>" +
                "</script>\n</head><body>";
        String editorHtml = "<form>" +
                "  <input id=\"x\"  type=\"hidden\" name=\"content\">\n" +
                "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                "</form>";
        String pas = "<script>" +
                "function hello()" +
                "{" +
                "   var _id = document.getElementById('x').value;" +
                "   var mention_uid = document.getElementById('mention_uid').value;" +
                "    Android.nextScreen(_id,mention_uid);" +
                "}</script></body></html>";
        String myHtmlString = pish + editorHtml + pas;
        editor.loadData(myHtmlString, "text/html; charset=utf-8", null);
        editor.loadUrl("file:///android_asset/index1.html");
        madd_comment_tv = findViewById(R.id.btn_add_to_do);
        mmessage_title.setText(Utils.unescapeJava(title));
        memail.setText(Utils.unescapeJava(name));
        madd_comment_tv.setOnClickListener(this);

        if (!ShardPreferences.get(context, ShardPreferences.share_user_image_key).equals("")) {
            Picasso.with(context).load(ShardPreferences.get(context, ShardPreferences.share_user_image_key)).into(user_image);
        } else {
            user_image.setImageDrawable(imageLatter(Utils.word(name), ShardPreferences.get(context, ShardPreferences.key_backColor), ShardPreferences.get(context, ShardPreferences.key_fontColor)));
        }
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(60)  // width in px
                .height(60)
                .fontSize(30)
                .bold()// height in px
                .textColor(Color.parseColor("#" + fontColor))
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));
        return drawable;
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                madd_comment_tv.setVisibility(View.GONE);
                pb_progress.setVisibility(View.VISIBLE);
                editor.loadUrl("javascript:copyContent()");
                editor.loadUrl("javascript:hello()");
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(ADD_COMMENT)) {
            Log.d("ADD_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    pb_progress.setVisibility(View.GONE);
                    madd_comment_tv.setVisibility(View.VISIBLE);
                    MessageCommentPOJO commentPOJO = new MessageCommentPOJO();
                    String comment = Utils.unescapeJava(jsonObject.getString("comment"));
                    commentPOJO.setCommentDescription(comment);
                    commentPOJO.setCommentId(jsonObject.getString("comment_id"));
                    commentPOJO.setCreatedAt(jsonObject.getString("date"));
                    commentPOJO.setCommentAt(jsonObject.getString("date"));

                    commentPOJO.setBgColor(ShardPreferences.get(context, ShardPreferences.key_backColor));
                    commentPOJO.setFontColor(ShardPreferences.get(context, ShardPreferences.key_fontColor));
                    commentPOJO.setEmployeeName(ShardPreferences.get(context, ShardPreferences.share_user_name_key));
                    commentPOJO.setUser_image(ShardPreferences.get(context, ShardPreferences.share_current_profile_url));
                    commentPOJO.setUserId(ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                    Intent intent = getIntent();
                    intent.putExtra("commentPOJO", commentPOJO);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    pb_progress.setVisibility(View.GONE);
                    madd_comment_tv.setVisibility(View.VISIBLE);
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                pb_progress.setVisibility(View.GONE);
                madd_comment_tv.setVisibility(View.VISIBLE);
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            editor.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");

                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                all_users_id = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                all_users_id = all_users_id + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            finish();
            startActivity(getIntent());
        }
        if (requestCode == 22) {
            finish();
            startActivity(getIntent());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
// Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
// Use RESULT_OK only if you're implementing WebView inside an Activity
            Uri result = data == null || resultCode != AddCommentActivity.RESULT_OK ? null : data.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void setData(String htmlData, String mention_uid) {

        /*if (htmlData == null || htmlData.equals("")) {
            Toast.makeText(this, getResources().getString(R.string.please_enter_description), Toast.LENGTH_SHORT).show();
        } else {*/
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("record_id", record_id);
        paramsReq.put("record_type", call_type);
        String unicodeString = Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData));
        paramsReq.put("mention_uid_commentt", mention_uid);
        paramsReq.put("comment", unicodeString);
        paramsReq.put("users", all_users_id);
        mApiRequest.postRequest(BASE_URL + ADD_COMMENT, ADD_COMMENT, paramsReq, Request.Method.POST);
        //}
    }
}