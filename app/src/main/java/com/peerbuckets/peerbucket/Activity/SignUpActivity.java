package com.peerbuckets.peerbucket.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.USER_PASSWORD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.USER_REGISTER;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_ISFLOW;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;

/**
 * Created by rishi on 9/20/2018.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    private LinearLayout forgotPasswordLayout, mainLayout;
    private ImageView navigateBack;
    private EditText email;
    private Button navigateNext;
    private TextView mContactSupport, tv_googleAccount;
    private ApiRequest mApiRequest;
    int RC_SiGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    Context context = this;
    FrameLayout fl_google_signUp;
    ProgressBar pb_progress, pb_google;
    RelativeLayout rl_signUpGoogle;
    String language = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        language = Locale.getDefault().getDisplayLanguage();
        rl_signUpGoogle = findViewById(R.id.rl_signUpGoogle);
        forgotPasswordLayout = findViewById(R.id.forgot_password_layout);
        fl_google_signUp = findViewById(R.id.fl_google_signUp);
        mainLayout = findViewById(R.id.main_layout);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        pb_google = findViewById(R.id.pb_google);
        pb_progress = findViewById(R.id.pb_progress);
        navigateBack = findViewById(R.id.navigate_back);
        tv_googleAccount = findViewById(R.id.tv_googleAccount);

        email = findViewById(R.id.email);
        navigateNext = findViewById(R.id.navigate_next);
        if (language.equals("???????")){
            navigateBack.setImageResource(R.drawable.ic_arrow_forward);
        }
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        fl_google_signUp.setOnClickListener(this);
        navigateNext.setOnClickListener(this);
        navigateBack.setOnClickListener(this);
        tv_googleAccount.setOnClickListener(this);
        mContactSupport = findViewById(R.id.contact_support_tv);
        mContactSupport.setOnClickListener(this);
    }

    private static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.navigate_back:
                finish();
                break;
            case R.id.fl_google_signUp:
                pb_google.setVisibility(View.VISIBLE);
                rl_signUpGoogle.setVisibility(View.GONE);
                Toast.makeText(context, getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                break;

            case R.id.navigate_next:
                if (isEmailValid(email.getText().toString())) {
                    pb_progress.setVisibility(View.VISIBLE);
                    navigateNext.setVisibility(View.GONE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("email", email.getText().toString());
                    ShardPreferences.save(context, ShardPreferences.share_user_email_key, email.getText().toString());
                    paramsReq.put("userType", "owner");
                    mApiRequest.postRequest(BASE_URL + USER_REGISTER, USER_REGISTER, paramsReq, Request.Method.POST);

                } else {
                    Toast.makeText(this, getResources().getString(R.string.email_is_not_valid), Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.contact_support_tv:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_support_team, null);
                dialogBuilder.setView(dialogView);

                TextView mDialogCancel = (TextView) dialogView.findViewById(R.id.contact_support_cancel_tv);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;
            case R.id.tv_googleAccount:
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SiGN_IN);
                GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(context);
                if (acct != null) {
                    String personEmail = acct.getEmail();
                    email.setText(getResources().getString(R.string.EMAIL_dot) + personEmail);
                }
                break;
        }
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(USER_REGISTER)) {

            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("code").equals("202")) {
                    pb_progress.setVisibility(View.GONE);
                    navigateNext.setVisibility(View.VISIBLE);
                    Toast.makeText(this, getResources().getString(R.string.User_already_exist_please_do_login), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (jsonObject.getString("code").equals("200")) {
                    pb_progress.setVisibility(View.GONE);
                    navigateNext.setVisibility(View.VISIBLE);
                    ShardPreferences.save(this, share_Company_Id_key, jsonObject.getString("companyId"));
                    ShardPreferences.save(this, share_employee_Id_key, jsonObject.getString("employeeId"));
                    ShardPreferences.save(this, share_UserId_key, jsonObject.getString("userId"));
                    ShardPreferences.save(this, share_current_UserId_key, jsonObject.getString("userId"));
                    ShardPreferences.save(this, share_ISFLOW, jsonObject.getString("isFlow"));
                    Intent intent = new Intent(context, SignUpPasswordActivity.class);

                    startActivity(intent);
                }
            } catch (JSONException e) {
                pb_progress.setVisibility(View.GONE);
                navigateNext.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SiGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            Map<String, String> map = new HashMap<>();
            map.put("email", email.getText().toString());
            map.put("userType", "owner");
            mApiRequest.postRequest(BASE_URL + USER_REGISTER, USER_REGISTER, map, Request.Method.POST);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (ApiException e) {
            Toast.makeText(context, getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            Map<String, String> map = new HashMap<>();
            map.put("email", email.getText().toString());
            map.put("userType", "owner");
            mApiRequest.postRequest(BASE_URL + USER_REGISTER, USER_REGISTER, map, Request.Method.POST);
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        super.onStart();
    }

}
