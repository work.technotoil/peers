package com.peerbuckets.peerbucket.Activity.message;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.Utils.WebAppInterface;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.get;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;


public class AddMessageBoardActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, EditorHtmlInterface {

    EditText mmessage_board_title;
    Button mpost_message;
    TextView tv_messageTitle;
    ApiRequest mApiRequest;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "", message_desc = "";
    String groupMembersUserId = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    WebView editor;
    RecyclerView recyclerEmployee;
    RecyclerView.LayoutManager mLayoutManager;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    LinearLayout ll_linearMaster;
    private ShimmerFrameLayout mShimmerViewContainer;
    private Toolbar toolbar;
    ProgressBar pb_progressBar, progress;
    Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_addmessageboard);
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        initUI();
        getPrefrences();

        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                if (mmessage_board_title.getText().toString().trim().equals("")) {
                    Toast.makeText(this, getString(R.string.please_enter_title), Toast.LENGTH_SHORT).show();
                } else {
                    pb_progressBar.setVisibility(View.VISIBLE);
                    mpost_message.setVisibility(View.GONE);
                    editor.loadUrl("javascript:copyContent()");
                    editor.loadUrl("javascript:hello()");
                }
                break;
        }
    }

    private void initUI() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.post_a_message);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        back();

        recyclerEmployee = findViewById(R.id.recyclerEmployee);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerEmployee.setLayoutManager(mLayoutManager);
        mmessage_board_title = findViewById(R.id.message_board_title);
        mpost_message = findViewById(R.id.btn_add_to_do);
        tv_messageTitle.setText(ShardPreferences.get(this, share_current_module));
        progress = findViewById(R.id.progress);
        pb_progressBar = findViewById(R.id.pb_progressBar);
        ll_linearMaster = findViewById(R.id.ll_linearMaster);

        editor = findViewById(R.id.editor_add_to_do);
        editor.getSettings().setJavaScriptEnabled(true);
        editor.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        editor.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });
        editor.addJavascriptInterface(new WebAppInterface(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);
        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]

        editor.loadUrl("file:///android_asset/index1.html");
        mpost_message.setOnClickListener(this);
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", mcurrent_type);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(AddMessageBoardActivity.this, share_Company_Id_key);
        mcurrentProjectTeamId = ShardPreferences.get(AddMessageBoardActivity.this, share_current_project_team_id);
        mcurrent_type = ShardPreferences.get(AddMessageBoardActivity.this, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(AddMessageBoardActivity.this, share_current_UserId_key);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            ll_linearMaster.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }

                        //recyclerEmployee

                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_MESSAGE_BOARD)) {

            Log.d("ADD_MESSAGE_BOARD", response);

            try {

                JSONObject jsonObjec = new JSONObject(response);
                String status = jsonObjec.getString("status");
                if (status.equals("1")) {
                    pb_progressBar.setVisibility(View.GONE);
                    mpost_message.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(AddMessageBoardActivity.this, ExpandMessageBoardActivity.class);
                    intent.putExtra("message_id", jsonObjec.getString("message_id"));
                    startActivityForResult(intent, 12);

                } else {
                    pb_progressBar.setVisibility(View.GONE);
                    mpost_message.setVisibility(View.VISIBLE);
                    Toast.makeText(this, jsonObjec.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                pb_progressBar.setVisibility(View.GONE);
                mpost_message.setVisibility(View.VISIBLE);
                Toast.makeText(this, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


    @Override
    public void setData(String htmlData, String mention_uid) {
       /* if (htmlData.equals("")) {
            Toast.makeText(this, getString(R.string.please_enter_description), Toast.LENGTH_SHORT).show();
        } else {*/
        message_desc = htmlData;
        int temp = 0;
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", mCurrentuserId);
        paramsReq.put("company_id", mcompanyId);
        paramsReq.put("team_id", mcurrentProjectTeamId);
        String unicodeTitle = Utils.getUnicodeString(Utils.escapeUnicodeText(mmessage_board_title.getText().toString()));
        paramsReq.put("message_title", unicodeTitle);
        String unicodeString = Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData));
        paramsReq.put("message_description", unicodeString);
        paramsReq.put("type_id", String.valueOf(temp));
        paramsReq.put("users", groupMembersUserId);
        paramsReq.put("mention_uid", mention_uid);
        paramsReq.put("platform", "mobile");
        Log.d("GROUP_USER_ID", groupMembersUserId.toString());

        mApiRequest.postRequest(BASE_URL + ADD_MESSAGE_BOARD, ADD_MESSAGE_BOARD, paramsReq, Request.Method.POST);
        //}
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (requestCode == REQUEST_SELECT_FILE) {
                    if (uploadMessage == null)
                        return;
                    uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                    uploadMessage = null;
                }
            } else if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == mUploadMessage)
                    return;
// Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
// Use RESULT_OK only if you're implementing WebView inside an Activity
                Uri result = intent == null || resultCode != AddMessageBoardActivity.RESULT_OK ? null : intent.getData();
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
            }
            if (requestCode == 12) {
                Intent intent1 = new Intent(context, Activity_MessageBoard.class);
                startActivity(intent1);
                finish();
            }
        }
    }
}