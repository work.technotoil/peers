package com.peerbuckets.peerbucket.Activity.docs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.applozic.mobicommons.file.FileUtils;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.GoogleDriveActivity;
import com.peerbuckets.peerbucket.Activity.UploadFileActivity;
import com.peerbuckets.peerbucket.Adapter.DocumentListAdapter;
import com.peerbuckets.peerbucket.Adapter.ScheduleListAdapter;
import com.peerbuckets.peerbucket.POJO.AlldocumentPOJO;
import com.peerbuckets.peerbucket.POJO.ScheduleListModel;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.peerbuckets.peerbucket.volley.MultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.CREATE_FOLDER;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DOCUMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SCHEDULE_LIST_BY_DATE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SCHEDULE_LIST_MONTH;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPLOAD_DOCUMENT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class DocsAndFilesActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    FloatingActionButton madd_docs_fab;
    TextView mbs_docs_start, mbs_docs_folder, mbs_docs_upload, mbs_docs_gd, mbs_docs_dropbox, mbs_docs_box,
            mbs_docs_onedrive;
    Typeface typeface;
    CardView ll_no_data, ll_no_data_folder;
    RecyclerView doc_recyclerView;
    ApiRequest mApiRequest;
    Context mContext;
    ArrayList<AlldocumentPOJO> mAlldocumentPOJO = new ArrayList<>();
    DocumentListAdapter mAdapter;
    LinearLayout llGoogleDrive, llMakeFolder, llUploadFile;
    public static int STORAGE_PERMISSION_CODE = 144;
    public static final int REQUEST_NEW_ADDED_DATA = 154;
    public static final int REQUEST_CODE_DRIVE = 515;
    String parent_id = "0";
    BottomSheetDialog dialog;
    public static final int REQUST_CODE = 342;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs_and_files);
        mContext = this;
        toolbar = findViewById(R.id.toolbar);
        int page = 0;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.docs_and_files));
        if (ShardPreferences.get(mContext, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        back();

        typeface = Typeface.createFromAsset(getAssets(), "font/fawsmsolid.ttf");
        doc_recyclerView = findViewById(R.id.doc_recyclerView);

        if (getIntent().getStringExtra("parent_id") != null && !getIntent().getStringExtra("parent_id").equals("")) {
            parent_id = getIntent().getStringExtra("parent_id");
        }

        requestStoragePermission();

        ll_no_data_folder = findViewById(R.id.ll_no_data_folder);
        ll_no_data = findViewById(R.id.ll_no_data);
        madd_docs_fab = (FloatingActionButton) findViewById(R.id.add_docs_fab);
        madd_docs_fab.setOnClickListener(this);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        doc_recyclerView.setLayoutManager(linearLayoutManager);

        mApiRequest = new ApiRequest(this, (IApiResponse) this);

        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(mContext, share_current_project_team_id));
        paramsReq.put("company_id", ShardPreferences.get(mContext, share_Company_Id_key));
        paramsReq.put("parent_id", parent_id);
        paramsReq.put("page", page + "");
        mApiRequest.postRequest(BASE_URL + DOCUMENT_LIST, DOCUMENT_LIST, paramsReq, Request.Method.POST);

    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_docs_fab:
                View view = getLayoutInflater().inflate(R.layout.bottom_sheet_add_doc_and_files, null);

                dialog = new BottomSheetDialog(this);
                dialog.setContentView(view);
                dialog.show();

                llGoogleDrive = view.findViewById(R.id.llGoogleDrive);
                llMakeFolder = view.findViewById(R.id.llMakeFolder);
                llUploadFile = view.findViewById(R.id.llUploadFile);

                mbs_docs_start = (TextView) view.findViewById(R.id.bs_docs_start);
                mbs_docs_folder = (TextView) view.findViewById(R.id.bs_docs_folder);
                mbs_docs_upload = (TextView) view.findViewById(R.id.bs_docs_upload);
                mbs_docs_gd = (TextView) view.findViewById(R.id.bs_docs_gd);
                mbs_docs_dropbox = (TextView) view.findViewById(R.id.bs_docs_dropbox);
                mbs_docs_box = (TextView) view.findViewById(R.id.bs_docs_box);
                mbs_docs_onedrive = (TextView) view.findViewById(R.id.bs_docs_onedrive);

                mbs_docs_start.setTypeface(typeface);
                mbs_docs_folder.setTypeface(typeface);
                mbs_docs_upload.setTypeface(typeface);
                mbs_docs_gd.setTypeface(typeface);
                mbs_docs_dropbox.setTypeface(typeface);
                mbs_docs_box.setTypeface(typeface);
                mbs_docs_onedrive.setTypeface(typeface);

                mbs_docs_start.setText("\uF055");
                mbs_docs_folder.setText("\uF055");
                mbs_docs_upload.setText("\uF055");
                mbs_docs_gd.setText("\uF055");
                mbs_docs_dropbox.setText("\uF055");
                mbs_docs_box.setText("\uF055");
                mbs_docs_onedrive.setText("\uF055");

                llUploadFile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                        intent.setType("*/*");
                        startActivityForResult(intent, 7);
                    }
                });

                llMakeFolder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        openUploadDialog();
                    }
                });

                llGoogleDrive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        Intent i = new Intent(mContext, GoogleDriveActivity.class);
                        i.putExtra("parent_id", parent_id);
                        startActivityForResult(i, REQUEST_CODE_DRIVE);
                    }
                });
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals(DOCUMENT_LIST)) {
            Log.d("GET_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray all_documents = jsonObject.getJSONArray("alldocuments");
                    mAlldocumentPOJO = new Gson().fromJson(all_documents.toString(), new TypeToken<List<AlldocumentPOJO>>() {
                    }.getType());
                    if (mAlldocumentPOJO != null && mAlldocumentPOJO.size() > 0) {
                        ll_no_data.setVisibility(View.GONE);
                        ll_no_data_folder.setVisibility(View.GONE);

                        mAdapter = new DocumentListAdapter(parent_id, mAlldocumentPOJO, this);
                        doc_recyclerView.setAdapter(mAdapter);
                    } else {
                        if (parent_id != null && parent_id != "") {
                            ll_no_data_folder.setVisibility(View.VISIBLE);
                            ll_no_data.setVisibility(View.GONE);
                        } else {
                            ll_no_data_folder.setVisibility(View.GONE);
                            ll_no_data.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(mContext, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
            }
        }

        if (tag_json_obj.equals(CREATE_FOLDER)) {
            Log.d("GET_MESSAGE_BOARD", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {

                    JSONArray mArray = jsonObject.getJSONArray("documents");
                    if (mArray.length() > 0) {
                        ArrayList<AlldocumentPOJO> mUpdatedDOcument = new ArrayList<>();
                        mUpdatedDOcument = new Gson().fromJson(mArray.toString(), new TypeToken<List<AlldocumentPOJO>>() {
                        }.getType());

                        if (mAdapter != null) {
                            mAdapter.addUpdatedList(mUpdatedDOcument);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mAlldocumentPOJO.add(0, mUpdatedDOcument.get(0));
                            ll_no_data.setVisibility(View.GONE);
                            mAdapter = new DocumentListAdapter(parent_id, mAlldocumentPOJO, this);
                            doc_recyclerView.setAdapter(mAdapter);
                        }
                    }
                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(mContext, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        switch (requestCode) {
            case 7:
                if (resultCode == RESULT_OK) {
                    Uri PathHolder = data.getData();
                    ContentResolver cr = this.getContentResolver();
                    //String mime = cr.getType(PathHolder);

                    File mfile = FileUtils.getFile(mContext, PathHolder);
                    Intent i = new Intent(mContext, UploadFileActivity.class);

                    i.putExtra("PathHolder", mfile.getAbsolutePath());
                    i.putExtra("record_id", parent_id);

                    i.putExtra("class_type", "add");
                    startActivityForResult(i, REQUEST_NEW_ADDED_DATA);

                } else {
                    Toast.makeText(mContext, getString(R.string.choose_other_file), Toast.LENGTH_SHORT).show();
                }
                break;

            case REQUST_CODE:
//                if (data != null) {
//                    mAdapter.removeMessage(data.getStringExtra("id"),data.getStringExtra("message_title"));
//                }
                break;

            case REQUEST_NEW_ADDED_DATA:
                if (data != null) {
                    String getDOcumentData = data.getStringExtra("documentData");
                    ArrayList<AlldocumentPOJO> mUpdatedDOcument = new ArrayList<>();
                    mUpdatedDOcument = new Gson().fromJson(getDOcumentData.toString(), new TypeToken<List<AlldocumentPOJO>>() {
                    }.getType());

                    if (mAdapter != null) {
                        mAdapter.addUpdatedList(mUpdatedDOcument);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mAlldocumentPOJO.add(0, mUpdatedDOcument.get(0));
                        ll_no_data.setVisibility(View.GONE);
                        mAdapter = new DocumentListAdapter(parent_id, mAlldocumentPOJO, this);
                        doc_recyclerView.setAdapter(mAdapter);
                    }
                }

                break;

            case REQUEST_CODE_DRIVE:
                if (data != null) {
                    String getDOcumentDaxta = data.getStringExtra("documentData");
                    ArrayList<AlldocumentPOJO> mUpdatedDOcumentDrive = new ArrayList<>();
                    mUpdatedDOcumentDrive = new Gson().fromJson(getDOcumentDaxta.toString(), new TypeToken<List<AlldocumentPOJO>>() {
                    }.getType());

                    if (mAdapter != null) {
                        if (mUpdatedDOcumentDrive.size() == 1) {
                            ll_no_data.setVisibility(View.GONE);
                            ll_no_data_folder.setVisibility(View.GONE);
                        }
                        if (mUpdatedDOcumentDrive.size() > 0) {
                            mAdapter.addUpdatedList(mUpdatedDOcumentDrive);
                            mAdapter.notifyDataSetChanged();
                        }

                    } else {
                        if (mUpdatedDOcumentDrive.size() > 0) {
                            mAlldocumentPOJO.add(0, mUpdatedDOcumentDrive.get(0));
                            ll_no_data.setVisibility(View.GONE);
                            ll_no_data_folder.setVisibility(View.GONE);
                            mAdapter = new DocumentListAdapter(parent_id, mAlldocumentPOJO, this);
                            doc_recyclerView.setAdapter(mAdapter);
                        }
                    }
                }
                break;
        }
    }


    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, getResources().getString(R.string.Oops_you_just_denied_the_permission), Toast.LENGTH_LONG).show();
            }
        }
    }


    private void openUploadDialog() {
        final AlertDialog.Builder videoDialog = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        final View convertView = inflater.inflate(R.layout.layout_dialog_folder, null);
        videoDialog.setView(convertView);
        final EditText et_folder_name = convertView.findViewById(R.id.et_folder_name);

        videoDialog.setTitle(getResources().getString(R.string.Add_Folder));
        videoDialog.setIcon(R.drawable.folder_img);
        videoDialog.setPositiveButton(getResources().getString(R.string.okS), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (TextUtils.isEmpty(et_folder_name.getText())) {
                    Toast.makeText(mContext, getString(R.string.please_enter_folder_name), Toast.LENGTH_SHORT).show();
                } else {

                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
                    paramsReq.put("project_team_id", ShardPreferences.get(mContext, share_current_project_team_id));
                    paramsReq.put("company_id", ShardPreferences.get(mContext, share_Company_Id_key));
                    paramsReq.put("parent_id", parent_id);
                    paramsReq.put("title", et_folder_name.getText().toString().trim());
                    mApiRequest.postRequest(BASE_URL + CREATE_FOLDER, CREATE_FOLDER, paramsReq, Request.Method.POST);

                }
            }
        });

        videoDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        videoDialog.show();
    }
}
