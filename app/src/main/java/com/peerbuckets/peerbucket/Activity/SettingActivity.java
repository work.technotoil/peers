package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.peerbuckets.peerbucket.Adapter.SettingAdapter;
import com.peerbuckets.peerbucket.Hpojo.HomePojo;
import com.peerbuckets.peerbucket.Hpojo.ModuleList;
import com.peerbuckets.peerbucket.POJO.SettingListResponce.ModuleListing;
import com.peerbuckets.peerbucket.POJO.SettingListResponce.SettingListResponse;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;


public class SettingActivity extends AppCompatActivity {
    RecyclerView rv_setting;
    Context ctx = this;
    SettingAdapter settingAdapter;
    RecyclerView.LayoutManager rv_settingLayoutManager;
    Toolbar toolbar;
    ProgressBar pb_progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        pb_progress = findViewById(R.id.pb_progress);
        rv_setting = findViewById(R.id.rv_setting);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.setting));
        if (ShardPreferences.get(ctx,share_language).equals("2")){
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        rv_setting.setHasFixedSize(true);
        rv_settingLayoutManager = new LinearLayoutManager(ctx);
        rv_setting.setLayoutManager(rv_settingLayoutManager);
        getSettingData();
    }

    private void getSettingData() {
        pb_progress.setVisibility(View.VISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(ctx, ShardPreferences.share_current_UserId_key));
        map.put("company_id", ShardPreferences.get(ctx, ShardPreferences.key_company_Id));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SettingListResponse> resultCall = apiInterface.callSettingList(map);
        resultCall.enqueue(new Callback<SettingListResponse>() {

            @Override
            public void onResponse(Call<SettingListResponse> call, Response<SettingListResponse> response) {
                pb_progress.setVisibility(View.GONE);
                if (response.body().getStatus()) {
                    pb_progress.setVisibility(View.GONE);
                    module(response.body().getModuleListing());
                } else {
                    pb_progress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<SettingListResponse> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
            }
        });
    }

    private void module(List<ModuleListing> list) {
        if (list != null && list.size() > 0) {
            settingAdapter = new SettingAdapter(ctx, (ArrayList<ModuleListing>) list);
            rv_setting.setAdapter(settingAdapter);
        } else {
            list = new ArrayList<>();
            settingAdapter = new SettingAdapter(ctx, (ArrayList<ModuleListing>) list);
            rv_setting.setAdapter(settingAdapter);
        }
    }
}
