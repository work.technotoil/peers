package com.peerbuckets.peerbucket.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.firebase.database.core.Context;
import com.peerbuckets.peerbucket.Adapter.HomeAdapter;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.interfaces.RefreshHomeScreenInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.INSERT_SINGLE_TEAM;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;

/**
 * Created by rishi on 10/3/2018.F
 */

public class CreateTeamActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    TextView mclose_tcp_dialog_tv, mheader_tcp_dialog_tv, optional_desc_tv;
    EditText mtcp_name_et, moptional_desc_et;
    Button msave_team_button;
    private ApiRequest mApiRequest;
    HomeAdapter home_adapter;
    RefreshHomeScreenInterface refreshHomeScreenInterface;
    Typeface typeface;
    String mcompanyId = "", muserId = "", id = "", projectTeamId = "", type = "";
    ProgressBar mProgressBar;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tcp);

        typeface = Typeface.createFromAsset(getAssets(), "font/fawsmsolid.ttf");
        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        mclose_tcp_dialog_tv = findViewById(R.id.close_tcp_dialog_tv);
        mheader_tcp_dialog_tv = findViewById(R.id.header_tcp_dialog_tv);
        mtcp_name_et = findViewById(R.id.tcp_name_et);
        optional_desc_tv = findViewById(R.id.optional_desc_tv);
        moptional_desc_et = findViewById(R.id.optional_desc_et);
        msave_team_button = findViewById(R.id.save_team_button);
        mProgressBar = findViewById(R.id.pb_subModule);
        mcompanyId = ShardPreferences.get(CreateTeamActivity.this, share_Company_Id_key);
        muserId = ShardPreferences.get(CreateTeamActivity.this, share_UserId_key);
        msave_team_button.setOnClickListener(this);
        mclose_tcp_dialog_tv.setOnClickListener(this);

        if (getIntent().getStringExtra("name") != null) {
            mtcp_name_et.setText(Utils.unescapeJava(getIntent().getStringExtra("name")));
            mheader_tcp_dialog_tv.setText(getResources().getString(R.string.update) /*+ " " + Utils.unescapeJava(getIntent().getStringExtra("name"))*/);
        }
        if (getIntent().getStringExtra("description") != null) {
            optional_desc_tv.setText(getResources().getString(R.string.optional_desc_update));
            moptional_desc_et.setText(Utils.unescapeJava(getIntent().getStringExtra("description")));
        }
        if (getIntent().getStringExtra("projectTeamId") != null) {
            projectTeamId = getIntent().getStringExtra("projectTeamId");
        }
        if (getIntent().getStringExtra("type") != null) {
            type = getIntent().getStringExtra("type");
        }
        if (getIntent().getStringExtra("id") != null) {
            id = getIntent().getStringExtra("id");
        }
        if (getIntent().getStringExtra("moduleName") != null) {
            mheader_tcp_dialog_tv.setText(getResources().getString(R.string.New) /*+ " " + Utils.unescapeJava(getIntent().getStringExtra("moduleName"))*/);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_team_button:
                if (mtcp_name_et.getText().toString().equals("")) {
                    Toast.makeText(this, getResources().getString(R.string.please_enter_name), Toast.LENGTH_SHORT).show();
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    msave_team_button.setVisibility(View.GONE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("projectTeamName", Utils.getUnicodeString(Utils.escapeUnicodeText(mtcp_name_et.getText().toString())));
                    paramsReq.put("projectTeamDescription", Utils.getUnicodeString(Utils.escapeUnicodeText(moptional_desc_et.getText().toString())));
                    paramsReq.put("companyId", mcompanyId);
                    paramsReq.put("userId", muserId);
                    paramsReq.put("moduleId", id);
                    paramsReq.put("projectTeamId", projectTeamId);
                    mApiRequest.postRequest(BASE_URL + INSERT_SINGLE_TEAM, INSERT_SINGLE_TEAM, paramsReq, Request.Method.POST);
                }
                break;
            case R.id.close_tcp_dialog_tv:
                finish();
                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(INSERT_SINGLE_TEAM)) {

            Log.d("ADD_NEW_MODULE", response);
            try {
                mProgressBar.setVisibility(View.GONE);
                msave_team_button.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {

                    if (type.equals("1")) {
                        Intent intent = getIntent();
                        intent.putExtra("name", mtcp_name_et.getText().toString());
                        intent.putExtra("description", moptional_desc_et.getText().toString());
                        intent.putExtra("projectId", projectTeamId);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        Intent intentnewT = new Intent(CreateTeamActivity.this, HomeActivity.class);
                        startActivity(intentnewT);
                        finish();
                    }

                } else {

                }
            } catch (JSONException e) {
                mProgressBar.setVisibility(View.GONE);
                msave_team_button.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

