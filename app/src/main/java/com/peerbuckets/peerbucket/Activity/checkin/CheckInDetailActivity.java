package com.peerbuckets.peerbucket.Activity.checkin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddEditMessageBoardActivity;
import com.peerbuckets.peerbucket.Activity.message.ExpandMessageBoardActivity;
import com.peerbuckets.peerbucket.Adapter.AddRemoveUserAdapter;
import com.peerbuckets.peerbucket.Adapter.EmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.Adapter.ReplyAdapter;
import com.peerbuckets.peerbucket.POJO.CheckinReply;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_REMOVE_NOTIFIED;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_CHECK_IN;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHECK_IN_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_CHECK_IN_REPLY_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SUBSCRIBE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_image_key;

public class CheckInDetailActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, RefreshCommentInterface {
    Context context = this;
    static int REQUEST_CODE_MESSAGE_EDIT = 3;
    static int REQUEST_CODE_MESSAGE_ADD = 2;
    TextView tvQuestion, tvUserName, tvAssignedTime, tv_selectOption;
    CircleImageView imgUserImage, user_image;
    WebView webViewAssignedUser;
    List<User> assignedUsersList = new ArrayList<>();
    String userName;
    ApiRequest apiRequest;
    String checkInId = "", user_id = "", checkin_title = "", firstName = "", lastName = "";
    private Toolbar toolbar;
    int page = 0;
    public static int REQUEST_CODE_EDIT = 100, REQUEST_CODE_ADD_REPLY = 101;
    private TextView btnAddReply;
    private List<CheckinReply> replyList = new ArrayList<>();
    RecyclerView recyclerReply;
    Boolean loading = true;
    ReplyAdapter replyAdapter;
    private TextView tvReplyCount;
    RelativeLayout rel_comment;
    ProgressBar mProgressBar;
    String questionStr = "", all_users_id = "";
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    String commaSeperatedID = "";
    LinearLayoutManager layoutManagerComment;

    RecyclerView employeee_icon_rv, employeee_applause_rv;
    RecyclerView.LayoutManager mEmployeListanager, mApplauseManager;
    EmployeeIconAdapter mNotifiedUserAdapter;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetalPOJO = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mApplauseUserList = new ArrayList<>();
    ImageView imgApplause;
    Boolean isApplause = false;
    Button btn_subscribe, btn_unsubscribe;
    TextView tv_add_remove, tv_people;
    PopupMenu popup;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_detail);

        if (getIntent().getStringExtra("checkin_title") != null) {
            checkin_title = getIntent().getStringExtra("checkin_title");
        }

        if (getIntent().getStringExtra("user_id") != null) {
            user_id = getIntent().getStringExtra("user_id");
        }

        if (getIntent().getStringExtra("checkInId") != null) {
            checkInId = getIntent().getStringExtra("checkInId");
        }

        inIt();
        initNotifiedModule();
        getCheckInDetail();
        getReplyList(page);
    }

    private void getCheckInDetail() {
        mProgressBar.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("checkin_id", checkInId);
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(context, ShardPreferences.share_addd_remove_current_type));
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        apiRequest.postRequest(BASE_URL + GET_CHECK_IN_DETAIL, GET_CHECK_IN_DETAIL, paramsReq, Request.Method.POST);
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.Automatic_check_ins));

        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        apiRequest = new ApiRequest(context, this);
        tv_selectOption = findViewById(R.id.tv_selectOption);
        tvQuestion = findViewById(R.id.tv_question_check_in_detail);
        tvUserName = findViewById(R.id.tv_user_name_check_in_detail);
        tvAssignedTime = findViewById(R.id.tv_assigned_check_in_detail);
        imgUserImage = findViewById(R.id.img_user_check_in_detail);
        user_image = findViewById(R.id.user_image);
        mProgressBar = findViewById(R.id.pb_layoutProgress);
        webViewAssignedUser = findViewById(R.id.web_view_check_in_detail);
        webViewAssignedUser.getSettings().setJavaScriptEnabled(true);
        webViewAssignedUser.setLayerType(WebView.LAYER_TYPE_NONE, null);
        btnAddReply = findViewById(R.id.btn_add_reply_check_in_detail);
        btnAddReply.setOnClickListener(this);
        recyclerReply = findViewById(R.id.recycler_reply_check_in_detail);
        recyclerReply.setLayoutManager(new LinearLayoutManager(context));
        recyclerReply.setNestedScrollingEnabled(false);
        tvReplyCount = findViewById(R.id.message_count_tv);
        rel_comment = findViewById(R.id.rel_comment);

        tv_selectOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectOption(view);
            }
        });
    }

    private void SelectOption(View v) {
        popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.message_board_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit_menu:
                        Intent intent = new Intent(context, AddCheckInActivity.class);
                        intent.putExtra("type", AddCheckInActivity.TYPE_EDIT);
                        intent.putExtra("checkInId", checkInId);
                        intent.putExtra("Added", "0");
                        startActivityForResult(intent, REQUEST_CODE_EDIT);
                        return true;
                    case R.id.menu_trash:
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("record_type", "Checkin");
                        paramsReq.put("record_id", checkInId);
                        apiRequest.postRequest(BASE_URL + DELETE_CHECK_IN, DELETE_CHECK_IN, paramsReq, Request.Method.POST);
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public void initNotifiedModule() {
        tv_add_remove = findViewById(R.id.tv_add_remove);
        tv_people = findViewById(R.id.tv_people);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_unsubscribe = findViewById(R.id.btn_unsubscribe);
        imgApplause = findViewById(R.id.imgApplause);
        employeee_icon_rv = findViewById(R.id.employeee_icon_rv);
        mEmployeListanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_icon_rv.setLayoutManager(mEmployeListanager);
        employeee_applause_rv = findViewById(R.id.employeee_applause_rv);
        mApplauseManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_applause_rv.setLayoutManager(mApplauseManager);
        tv_add_remove.setOnClickListener(this);
        imgApplause.setOnClickListener(this);
        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_subscribe.setVisibility(View.GONE);
                btn_unsubscribe.setVisibility(View.VISIBLE);
                actionSubscribe("1");
            }
        });

        btn_unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_subscribe.setVisibility(View.VISIBLE);
                btn_unsubscribe.setVisibility(View.GONE);
                actionSubscribe("0");
            }
        });
    }

    private void actionSubscribe(String isSubscribe) {
        mProgressBar.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", checkInId);
        paramsReq.put("company_id", ShardPreferences.get(CheckInDetailActivity.this, share_Company_Id_key));
        paramsReq.put("loggedIn_user_id", ShardPreferences.get(CheckInDetailActivity.this, share_UserId_key));
        paramsReq.put("user_id", commaSeperatedID);
        paramsReq.put("project_team_id", ShardPreferences.get(CheckInDetailActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "checkin");
        paramsReq.put("isSubscribe", isSubscribe);
        apiRequest.postRequestBackground(BASE_URL + SUBSCRIBE, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
        Intent thisintent = getIntent();
        startActivity(thisintent);
        finish();
    }

    private TextDrawable imageLatter(String name, String backColor, String fountColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fountColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_CHECK_IN_DETAIL)) {
            mProgressBar.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(response);
                mProgressBar.setVisibility(View.GONE);
                if (jsonObject.getString("status").equals("1")) {
                    mProgressBar.setVisibility(View.GONE);
                    isApplause = jsonObject.getBoolean("isApplause");

                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                        btn_subscribe.setVisibility(View.GONE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }

                    JSONObject checkInObject = jsonObject.getJSONObject("checkins");
                    updateUI(checkInObject);

                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (!checkInObject.getString("user_image").equals("")) {
                        Picasso.with(context).load(DEVELOPMENT_URL + checkInObject.getString("user_image")).into(imgUserImage);
                    } else {
                        imgUserImage.setImageDrawable(imageLatter(Utils.word(checkInObject.getString("employee_name")), jsonObject.getString("bgColor"), jsonObject.getString("fontColor")));
                    }
                    if (!ShardPreferences.get(context, share_user_image_key).equals("")) {
                        Picasso.with(context).load(ShardPreferences.get(context, share_user_image_key)).into(user_image);
                    } else {
                        user_image.setImageDrawable(imageLatter(Utils.word(checkInObject.getString("employee_name")), checkInObject.getString("bgColor"), checkInObject.getString("fontColor")));
                    }

                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        mProgressBar.setVisibility(View.GONE);
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mApplauseUserList.add(mModel);
                        }
                        mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                        employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                    }
                } else {
                    mProgressBar.setVisibility(View.GONE);
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (JSONException e) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(context, e + "", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(GET_CHECK_IN_REPLY_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    rel_comment.setVisibility(View.GONE);
                    JSONArray jsonArray = jsonObject.getJSONArray("checkin_reply");
                    if (jsonArray.length() > 0) {
                        if (page == 0 && replyList != null) {
                            replyList.clear();
                        }
                        replyList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<CheckinReply>>() {
                        }.getType());
                        tvReplyCount.setText(replyList.size() + "");

                        if (replyList != null && replyList.size() > 0) {
                            replyAdapter = new ReplyAdapter(replyList, context, questionStr, checkInId);
                            recyclerReply.setAdapter(replyAdapter);
                            replyAdapter.notifyDataSetChanged();
                        } else {
                            recyclerReply.post(new Runnable() {
                                public void run() {
                                }
                            });
                        }
                        page = page + 1;
                        loading = true;
                    } else {
                        tvReplyCount.setText("0");
                    }
                } else {
                    tvReplyCount.setText("0");
                    rel_comment.setVisibility(View.GONE);
                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                mProgressBar.setVisibility(View.GONE);
                if (status == 1) {
                    mProgressBar.setVisibility(View.GONE);
                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_subscribe.setVisibility(View.GONE);
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    tv_people.setText("" + jsonObject.getString("userCount") + " ");
                    JSONArray mUserArray = jsonObject.getJSONArray("update_user_list");
                    if (mUserArray != null) {
                        if (mEmployeeDetalPOJO != null) {
                            mEmployeeDetalPOJO.clear();
                        }
                        for (int i = 0; i < mUserArray.length(); i++) {
                            JSONObject jsonObject1 = mUserArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            if (jsonObject1.getString("user_image").equals("")) {
                                String s = jsonObject1.getString("employee_name").substring(0, 1);
                                String userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                                mModel.setUserImage(DEVELOPMENT_URL + "" + userImage);
                            } else {
                                mModel.setUserImage(DEVELOPMENT_URL + "" + jsonObject1.getString("user_image"));
                            }
                            mEmployeeDetalPOJO.add(mModel);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                mProgressBar.setVisibility(View.GONE);
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                mProgressBar.setVisibility(View.GONE);
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    mProgressBar.setVisibility(View.GONE);
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mApplauseUserList != null) {
                        mApplauseUserList.clear();
                    }
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mApplauseUserList.add(mModel);
                        }
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }
            } catch (Exception e) {
                mProgressBar.setVisibility(View.GONE);
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(DELETE_CHECK_IN)) {
            try {
                mProgressBar.setVisibility(View.GONE);
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    mProgressBar.setVisibility(View.GONE);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                }
            } catch (JSONException e) {
                mProgressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            mShimmerDialog.stopShimmerAnimation();
            mShimmerDialog.setVisibility(View.GONE);
            try {
                mProgressBar.setVisibility(View.GONE);
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    mProgressBar.setVisibility(View.GONE);
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new AddRemoveUserAdapter(all_users_id, mEmployeeDetailList, this);
                        recyclerView.setAdapter(mAddRemoveUserAdapter);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                mProgressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void updateUI(JSONObject object) {
        try {
            userName = object.getString("employee_name");
            String userImage = object.getString("user_image");
            String assignedUser = "";

            // getSupportActionBar().setTitle(Utils.checkStringWithHash(object.getString("question")));
            questionStr = object.getString("question");
            tvQuestion.setText(Utils.unescapeJava(Utils.checkStringWithHash(object.getString("question"))));

            if (ShardPreferences.get(context, ShardPreferences.share_language).equals("") || ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
                tvQuestion.setGravity(Gravity.LEFT);

            } else {

            }

            tvAssignedTime.setText(Utils.checkStringWithHash(object.getString("asking_people")));

            tvUserName.setText(Utils.unescapeJava(Utils.checkStringWithHash(object.getString("employee_name"))));

            if (!userImage.equals("") && userImage != null) {
                Picasso.with(context).load(BASE_URL + userImage).into(imgUserImage);
            } else {
                imgUserImage.setImageDrawable(imageLatter(Utils.word(object.getString("employee_name")), object.getString("bgColor"), object.getString("fontColor")));
            }

            if (!Utils.isStringValid(userImage)) {
                if (Utils.isStringValid(userName)) {
                    String s = userName.substring(0, 1);
                    userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                    //imgUserImage.setImageDrawable(imageLatter(Utils.word(userName), .getBgColor(), list.get(position).getFontColor()));
                }
            }

            JSONArray userArray = object.getJSONArray("users");
            if (userArray.length() > 0) {
                assignedUsersList = new Gson().fromJson(userArray.toString(), new TypeToken<List<User>>() {
                }.getType());
                if (assignedUsersList != null && assignedUsersList.size() > 0) {
                    for (User selectedUser : assignedUsersList) {
                        assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                        System.getProperty("\n");
                        webViewAssignedUser.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUser, "text/html", "utf-8", null);

                    }
                } else {
                    System.getProperty("\n");
                    webViewAssignedUser.loadData(assignedUser, "text/html", "utf-8");
                }
            }

            if (userArray != null) {
                JSONObject jsonObject1 = new JSONObject();
                for (int i = 0; i < userArray.length(); i++) {
                    jsonObject1 = userArray.getJSONObject(i);
                    if (i == 0) {
                        all_users_id = jsonObject1.getString("user_id");
                    } else {
                        all_users_id = all_users_id + "," + jsonObject1.getString("user_id");
                    }
                    EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                    mModel.setUserId(jsonObject1.getString("user_id"));
                    mModel.setEmployeeName(jsonObject1.getString("employee_name"));

                    if (jsonObject1.getString("user_image").equals("")) {
                        String s = jsonObject1.getString("employee_name").substring(0, 1);
                        String userImageUpdated = "image/PreviewImages/" + s.toLowerCase() + ".png";
                        mModel.setUserImage(DEVELOPMENT_URL + "" + userImageUpdated);
                    } else {
                        mModel.setUserImage(DEVELOPMENT_URL + "" + jsonObject1.getString("user_image"));
                    }
                    mEmployeeDetalPOJO.add(mModel);
                    Log.d("ALL_USERS", all_users_id);
                }
                mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getReplyList(int cPage) {
        mProgressBar.setVisibility(View.VISIBLE);
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("checkin_id", checkInId);
        paramsReq.put("type", "checkin");
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("page", String.valueOf(cPage));
        apiRequest.postRequestBackground(BASE_URL + GET_CHECK_IN_REPLY_LIST, GET_CHECK_IN_REPLY_LIST, paramsReq, Request.Method.POST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_ADD_REPLY) {
                recreate();
            } else if (requestCode == REQUEST_CODE_EDIT) {
                replyList.add((CheckinReply) data.getSerializableExtra("reply_list"));
                replyAdapter = new ReplyAdapter(replyList, context, questionStr, checkInId);
                recyclerReply.setAdapter(replyAdapter);
                replyAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_reply_check_in_detail:
                Intent intent = new Intent(context, AddEditReplyActivity.class);
                intent.putExtra("check_in_id", checkInId);
                intent.putExtra("type", AddEditReplyActivity.TYPE_ADD);
                intent.putExtra("message_title", tvQuestion.getText().toString());
                intent.putExtra("userName", userName);
                startActivityForResult(intent, REQUEST_CODE_ADD_REPLY);
                break;
            case R.id.tv_add_remove:
                addRemovePeopleDialog();
                break;
            case R.id.imgApplause:
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause();
                break;
        }
    }

    public void addApplause() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", checkInId);
        paramsReq.put("company_id", ShardPreferences.get(CheckInDetailActivity.this, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(CheckInDetailActivity.this, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(CheckInDetailActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "checkin_list");
        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }
        apiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);
    }

    @Override
    public void setCommentData(int commentSize) {
    }

    private void getCommentList(int cPage) {

        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "todo_list");
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("todo_id", checkInId);
        paramsReq.put("page", String.valueOf(cPage));
        apiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);
    }

    RelativeLayout relMaster;
    ShimmerFrameLayout mShimmerDialog;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    AddRemoveUserAdapter mAddRemoveUserAdapter;
    RecyclerView recyclerView;

    public void addRemovePeopleDialog() {
        final Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_remove_user, null);
        dialogBuilder.setContentView(dialogView);
        TextView selectEveryOne = dialogView.findViewById(R.id.selectEveryOne);
        TextView selectNoOne = dialogView.findViewById(R.id.selectNoOne);
        TextView tv_cancel = dialogView.findViewById(R.id.tv_cancel);
        recyclerView = dialogView.findViewById(R.id.rv_dialogAddRemove);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        ;

        selectEveryOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.selectAllSelections();
                    }
                }
            }
        });

        selectNoOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.clearAllSelections();
                    }
                }
            }
        });

        Button btnSave = dialogView.findViewById(R.id.btnSave);
        mShimmerDialog = dialogView.findViewById(R.id.shimmer_view_container);
        fetchEmployee();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commaSeperatedID = getAssignedUserString();
                if (commaSeperatedID.equals("")) {
                    Toast.makeText(CheckInDetailActivity.this, getResources().getString(R.string.please_select_atleast_one), Toast.LENGTH_SHORT).show();
                } else {
                    all_users_id = commaSeperatedID;
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("record_id", checkInId);
                    paramsReq.put("loggedIn_user_id", ShardPreferences.get(CheckInDetailActivity.this, share_UserId_key));
                    paramsReq.put("company_id", ShardPreferences.get(CheckInDetailActivity.this, share_Company_Id_key));
                    paramsReq.put("user_id", commaSeperatedID);
                    paramsReq.put("project_team_id", ShardPreferences.get(CheckInDetailActivity.this, share_current_project_team_id));
                    paramsReq.put("notification_type", "checkin");
                    apiRequest.postRequestBackground(BASE_URL + ADD_REMOVE_NOTIFIED, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
                    dialogBuilder.dismiss();
                }
            }
        });

        recyclerReply.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManagerComment.getChildCount();
                    totalItemCount = layoutManagerComment.getItemCount();
                    pastVisiblesItems = layoutManagerComment.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data
                            getReplyList(page);
                        }
                    }
                }
            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.show();
    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        apiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private String getAssignedUserString() {
        if (mAddRemoveUserAdapter != null) {
            return mAddRemoveUserAdapter.getAssignedUserString();
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("checkin_id", checkInId);
        paramsReq.put("type", "checkin");
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("page", String.valueOf(0));
        apiRequest.postRequestBackground(BASE_URL + GET_CHECK_IN_REPLY_LIST, GET_CHECK_IN_REPLY_LIST, paramsReq, Request.Method.POST);

    }
}