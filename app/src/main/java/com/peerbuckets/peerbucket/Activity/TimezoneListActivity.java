package com.peerbuckets.peerbucket.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Adapter.MessageListAdapter;
import com.peerbuckets.peerbucket.Adapter.TimezoneAdapter;
import com.peerbuckets.peerbucket.POJO.MessageTypePOJO;
import com.peerbuckets.peerbucket.POJO.TimezoneListModel;
import com.peerbuckets.peerbucket.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TimezoneListActivity extends AppCompatActivity {

    RecyclerView recyclerTimezone;
    TimezoneAdapter mAdapter;
    ArrayList<TimezoneListModel> mTImezoneList = new ArrayList<>();
    LinearLayoutManager mmessage_type_rv_layout_manager;
    ImageView ic_close_lg;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timezone);

        ic_close_lg = findViewById(R.id.close_tcp_dialog_iv);
        ic_close_lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerTimezone = findViewById(R.id.recyclerTimezone);
        recyclerTimezone.setHasFixedSize(true);
        mmessage_type_rv_layout_manager = new LinearLayoutManager(this);
        mmessage_type_rv_layout_manager.setOrientation(RecyclerView.VERTICAL);
        recyclerTimezone.setLayoutManager(mmessage_type_rv_layout_manager);

        setTimezoneList();
        
    }

    public void setTimezoneList(){
        mTImezoneList = new Gson().fromJson(loadJSONFromAsset().toString(), new TypeToken<List<TimezoneListModel>>() {
        }.getType());

        mAdapter = new TimezoneAdapter(mTImezoneList, this);
        recyclerTimezone.setAdapter(mAdapter);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("timezone.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
