package com.peerbuckets.peerbucket.Activity.schedule;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddCommentActivity;
import com.peerbuckets.peerbucket.Adapter.AddRemoveUserAdapter;
import com.peerbuckets.peerbucket.Adapter.EmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_REMOVE_NOTIFIED;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_SCHEDULE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.EDIT_MESSAGE_BOARD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_SCHEDULE_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SUBSCRIBE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_image_key;

public class ScheduleDetailActivity extends AppCompatActivity implements IApiResponse, View.OnClickListener, RefreshCommentInterface {
    Context context = this;
    private Toolbar toolbar;
    Intent intent;
    WebView webViewDesc, webViewUser;
    TextView tvStartDateMonth, tvStartDate, tvEndDateMonth, tvEndDate, tvTitle, tvWhen, tv_view, tv_messageTitle;
    String scheduleId = "", assignedUser = "", all_users_id = "", description = "",
            user_id = "", schedule_title = "", commaSeperatedID = "", firstName = "", lastName = "";
    ApiRequest apiRequest;

    //Init Comments View
    private LinearLayout mcomments_LL;
    //    private LinearLayout mno_comments_LL;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 0;
    private Boolean loading = true;
    LinearLayoutManager layoutManagerComment;
    RecyclerView mcomments_rv;
    ArrayList<MessageCommentPOJO> messageCommentPOJOS;
    TextView message_count_tv;
    MessageCommentsAdapter mcomments_rv_adapter;
    TextView madd_comment_tv, tv_posted_by;
    public static int REQUEST_ADD_COMMENT = 2, REQUEST_CODE_EDIT = 3;
    List<User> usersList = new ArrayList<>();
    RelativeLayout rel_comment;
    TextView tv_people, tv_selectOption;

    PopupMenu popup;

    RecyclerView employeee_icon_rv, employeee_applause_rv;
    RecyclerView.LayoutManager mEmployeListanager, mApplauseManager;
    EmployeeIconAdapter mNotifiedUserAdapter;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetalPOJO = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mApplauseUserList = new ArrayList<>();
    ImageView imgApplause, user_image12;
    Boolean isApplause = false;
    Button btn_subscribe, btn_unsubscribe;
    TextView tv_add_remove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_detail);
        if (getIntent().getStringExtra("schedule_id") != null) {
            scheduleId = getIntent().getStringExtra("schedule_id");
        }
        if (getIntent().getStringExtra("user_id") != null) {
            user_id = getIntent().getStringExtra("user_id");
        }
        if (getIntent().getStringExtra("schedule_title") != null) {
            schedule_title = getIntent().getStringExtra("schedule_title");
        }
        intent = getIntent();


        inIt();
        initNotifiedModule();
        getScheduleDetail();
        initCommentUI();
        getCommentList(page);

        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
    }

    private void inIt() {
        apiRequest = new ApiRequest(context, this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.Schedule_Details));

        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                intent.putExtra("id", scheduleId);
                intent.putExtra("message_title", tv_messageTitle.getText().toString());
                intent.putExtra("isDelete", false);
                if (messageCommentPOJOS != null) {
                    intent.putExtra("commentCount", messageCommentPOJOS.size() + "");
                } else {
                    intent.putExtra("commentCount", "0");
                }
                onBackPressed();
            }
        });

        tv_view = findViewById(R.id.tv_view);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        tv_selectOption = findViewById(R.id.tv_selectOption);
        tv_people = findViewById(R.id.tv_people);
        user_image12 = findViewById(R.id.user_image);
        rel_comment = findViewById(R.id.rel_comment);
        tv_posted_by = findViewById(R.id.tv_posted_by);
        webViewDesc = findViewById(R.id.web_view_desc_schedule_detail);
        tvStartDateMonth = findViewById(R.id.tv_start_month_schedule_detail);
        tvStartDate = findViewById(R.id.tv_start_date_schedule_detail);
        tvEndDateMonth = findViewById(R.id.tv_end_month_schedule_detail);
        tvEndDate = findViewById(R.id.tv_end_date_schedule_detail);
        tvTitle = findViewById(R.id.tv_title_schedule_detail);
        webViewUser = findViewById(R.id.web_view_users_schedule_detail);
        tvWhen = findViewById(R.id.tv_when_schedule_detail);
        webViewDesc.getSettings().setJavaScriptEnabled(true);
        webViewDesc.setLayerType(WebView.LAYER_TYPE_NONE, null);

        tv_selectOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectOption(view);
            }
        });
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, intent);
                onBackPressed();
            }
        });
    }

    public void initNotifiedModule() {
        tv_add_remove = findViewById(R.id.tv_add_remove);
        tv_people = findViewById(R.id.tv_people);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_unsubscribe = findViewById(R.id.btn_unsubscribe);
        imgApplause = findViewById(R.id.imgApplause);
        employeee_icon_rv = findViewById(R.id.employeee_icon_rv);
        mEmployeListanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_icon_rv.setLayoutManager(mEmployeListanager);

        employeee_applause_rv = findViewById(R.id.employeee_applause_rv);
        mApplauseManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_applause_rv.setLayoutManager(mApplauseManager);

        tv_add_remove.setOnClickListener(this);
        imgApplause.setOnClickListener(this);

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.VISIBLE);
                btn_subscribe.setVisibility(View.GONE);
                btn_unsubscribe.setVisibility(View.VISIBLE);
                actionSubscribe("1");
            }
        });

        btn_unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.GONE);
                btn_subscribe.setVisibility(View.VISIBLE);
                btn_unsubscribe.setVisibility(View.GONE);
                actionSubscribe("0");
            }
        });
    }

    private void actionSubscribe(String isSubscribe) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", scheduleId);
        paramsReq.put("company_id", ShardPreferences.get(ScheduleDetailActivity.this, share_Company_Id_key));
        paramsReq.put("loggedIn_user_id", ShardPreferences.get(ScheduleDetailActivity.this, share_UserId_key));
        paramsReq.put("user_id", commaSeperatedID);
        paramsReq.put("project_team_id", ShardPreferences.get(ScheduleDetailActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "schedule");
        paramsReq.put("isSubscribe", isSubscribe);
        apiRequest.postRequestBackground(BASE_URL + SUBSCRIBE, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);

        Intent thisintent = getIntent();
        startActivity(thisintent);
    }

    private void getScheduleDetail() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, ShardPreferences.share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(context, share_addd_remove_current_type));
        paramsReq.put("schedule_id", scheduleId);
        apiRequest.postRequest(BASE_URL + GET_SCHEDULE_DETAIL, GET_SCHEDULE_DETAIL, paramsReq, Request.Method.POST);
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_SCHEDULE_DETAIL)) {
            try {

                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.getString("status").equals("1")) {

                    isApplause = jsonObject.getBoolean("isApplause");

                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                        btn_subscribe.setVisibility(View.GONE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }

                    JSONObject detailObject = jsonObject.getJSONObject("schedule_detail");
                    updateUI(detailObject);

                    JSONObject mMessageDetail = jsonObject.getJSONObject("message_details");
//change
                    JSONArray mUserArray = mMessageDetail.getJSONArray("users");

                    if (!ShardPreferences.get(context, share_user_image_key).equals("")) {
                        Picasso.with(context).load(ShardPreferences.get(context, share_user_image_key)).into(user_image12);
                    } else {
                        user_image12.setImageDrawable(imageLatter(Utils.word(detailObject.getString("employee_name")), detailObject.getString("bgColor"), detailObject.getString("fontColor")));
                    }

                    JSONObject jsonObject1 = new JSONObject();
                    if (mUserArray != null) {
                        for (int i = 0; i < mUserArray.length(); i++) {
                            jsonObject1 = mUserArray.getJSONObject(i);
                            if (i == 0) {
                                all_users_id = jsonObject1.getString("user_id");
                            } else {
                                all_users_id = all_users_id + "," + jsonObject1.getString("user_id");
                            }

                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));
                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));

                            mApplauseUserList.add(mModel);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                        employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                    }
                } else {
                    Toast.makeText(context, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(EDIT_MESSAGE_BOARD)) {
            Log.d("EDIT_MB", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("title", tv_messageTitle.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            } catch (Exception e) {

                Log.d("EXCPTN", e.getMessage());
            }
        }

        if (tag_json_obj.equals(FETCH_COMMENT_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    rel_comment.setVisibility(View.GONE);
                    JSONArray mArray = jsonObject.getJSONArray("commentList");
                    if (mArray != null && mArray.length() > 0) {
                        if (page == 0 && messageCommentPOJOS != null) {
                            messageCommentPOJOS.clear();
                        }

                        messageCommentPOJOS = new Gson().fromJson(mArray.toString(), new TypeToken<List<MessageCommentPOJO>>() {
                        }.getType());

                        message_count_tv.setText(messageCommentPOJOS.size() + "");

                        if (messageCommentPOJOS != null && messageCommentPOJOS.size() > 0) {
                            if (page == 0) {
                                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, context, schedule_title, scheduleId, this, 1);
                                mcomments_rv.setAdapter(mcomments_rv_adapter);
                            } else {
                                mcomments_rv.post(new Runnable() {
                                    public void run() {
                                        mcomments_rv_adapter.setNotifyData(messageCommentPOJOS);
                                    }
                                });
                            }
                        }

                        page = page + 1;
                        loading = true;
                    }
                } else {
                    if (page == 0) {
                        rel_comment.setVisibility(View.GONE);
                    }
                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(DELETE_SCHEDULE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("id", scheduleId);
                    intent.putExtra("isDelete", true);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(context, getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            mShimmerDialog.stopShimmerAnimation();
            mShimmerDialog.setVisibility(View.GONE);
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new AddRemoveUserAdapter(all_users_id, mEmployeeDetailList, this);
                        recyclerView.setAdapter(mAddRemoveUserAdapter);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_subscribe.setVisibility(View.GONE);
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    tv_people.setText("" + jsonObject.getString("userCount") + " ");
                    JSONArray mUserArray = jsonObject.getJSONArray("update_user_list");
                    if (mUserArray != null) {
                        if (mEmployeeDetalPOJO != null) {
                            mEmployeeDetalPOJO.clear();
                        }
                        for (int i = 0; i < mUserArray.length(); i++) {
                            JSONObject jsonObject1 = mUserArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));


                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mApplauseUserList != null) {
                        mApplauseUserList.clear();
                    }
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));

                            mApplauseUserList.add(mModel);
                        }
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void updateUI(JSONObject detailObject) {
        try {

            String userImage = detailObject.getString("user_image");
            String postedby = getResources().getString(R.string.Posted_by) + detailObject.getString("name");
            tv_posted_by.setText(Utils.unescapeJava(postedby));

            if (!ShardPreferences.get(context, share_user_image_key).equals("")) {
                Picasso.with(context).load(ShardPreferences.get(context, share_user_image_key)).into(user_image12);
            } else {
                user_image12.setImageDrawable(imageLatter(Utils.word(detailObject.getString("employee_name")), detailObject.getString("bgColor"), detailObject.getString("fontColor")));
            }
            tvTitle.setText(Utils.checkStringWithHash(Utils.unescapeJava(detailObject.getString("title"))));
            tvWhen.setText(detailObject.getString("start_date") + " - " + detailObject.getString("end_date") + " (" + detailObject.getString("days_difference") + " days)");

            if (Utils.isStringValid(detailObject.getString("start_date"))) {
                tvStartDateMonth.setText(Utils.parseDateWithFormat(detailObject.getString("start_date"), "dd MMM,yy", "MMM"));
                tvStartDate.setText(Utils.parseDateWithFormat(detailObject.getString("start_date"), "dd MMM,yy", "dd"));
            }

            if (Utils.isStringValid(detailObject.getString("end_date"))) {
                tvEndDateMonth.setText(Utils.parseDateWithFormat(detailObject.getString("end_date"), "dd MMM,yy", "MMM"));
                tvEndDate.setText(Utils.parseDateWithFormat(detailObject.getString("end_date"), "dd MMM,yy", "dd"));
            }

            JSONArray usersArray = detailObject.getJSONArray("users");
            JSONObject jsonObject1;
            if (usersArray.length() > 0) {

                for (int i = 0; i < usersArray.length(); i++) {
                    jsonObject1 = usersArray.getJSONObject(i);

                    if (i == 0) {
                        all_users_id = jsonObject1.getString("user_id");
                    } else {
                        all_users_id = usersArray + "," + jsonObject1.getString("user_id");
                    }

                    EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                    mModel.setUserId(jsonObject1.getString("user_id"));
                    mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                    mModel.setUserImage(jsonObject1.getString("user_image"));
                    mModel.setFontColor(jsonObject1.getString("fontColor"));
                    mModel.setBgColor(jsonObject1.getString("bgColor"));

                    mEmployeeDetalPOJO.add(mModel);
                    Log.d("ALL_USERS", all_users_id);
                }

                mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                employeee_icon_rv.setAdapter(mNotifiedUserAdapter);

                tv_people.setText("" + usersArray.length() + " ");

                usersList = new Gson().fromJson(usersArray.toString(), new TypeToken<List<User>>() {
                }.getType());
                if (usersList != null && usersList.size() > 0) {
                    for (User selectedUser : usersList) {
                        assignedUser = assignedUser + Utils.createUserHtml(selectedUser.getEmployeeName(), selectedUser.getUserImage(), selectedUser.getBgColor(), selectedUser.getFontColor());
                    }
                    webViewUser.loadDataWithBaseURL(DEVELOPMENT_URL, assignedUser, "text/html", "utf-8", null);
                } else {
                    webViewUser.loadData("", "text/html", "utf-8");
                }
            }

            Utils.setDescription(Utils.unescapeJava("<style>img{display: inline;height: auto;max-width: 100%;}</style>" + detailObject.getString("description")), webViewDesc, context);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getCommentList(int cPage) {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "schedule");
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("todo_id", scheduleId);
        paramsReq.put("page", String.valueOf(cPage));
        apiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);

    }

    private void initCommentUI() {
        madd_comment_tv = findViewById(R.id.add_comment_tv);
        madd_comment_tv.setOnClickListener(this);
        mcomments_rv = findViewById(R.id.comments_rv);
        mcomments_LL = findViewById(R.id.comments_LL);
        mcomments_LL.setVisibility(View.VISIBLE);

        layoutManagerComment = new LinearLayoutManager(context);
        layoutManagerComment.setOrientation(RecyclerView.VERTICAL);
        mcomments_rv.setLayoutManager(layoutManagerComment);
        message_count_tv = findViewById(R.id.message_count_tv);

        mcomments_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManagerComment.getChildCount();
                    totalItemCount = layoutManagerComment.getItemCount();
                    pastVisiblesItems = layoutManagerComment.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data

                            getCommentList(page);

                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_comment_tv:
                Intent intent1 = new Intent(this, AddCommentActivity.class);
                intent1.putExtra("call_type", "schedule");
                intent1.putExtra("message_title", getResources().getString(R.string.add_comment));
                intent1.putExtra("message_email", "");
                intent1.putExtra("name", "");
                intent1.putExtra("record_id", scheduleId);
                intent1.putExtra("all_users_id", all_users_id);
                startActivityForResult(intent1, REQUEST_ADD_COMMENT);
                break;
            case R.id.tv_add_remove:
                addRemovePeopleDialog();
                break;
            case R.id.imgApplause:
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause();
                break;
        }
    }

    public void addApplause() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", scheduleId);
        paramsReq.put("company_id", ShardPreferences.get(ScheduleDetailActivity.this, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(ScheduleDetailActivity.this, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(ScheduleDetailActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "schedule");
        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }

        apiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);
    }

    @Override
    public void setCommentData(int commentSize) {
        message_count_tv.setText(commentSize + "");
        if (commentSize > 0) {
            mcomments_LL.setVisibility(View.VISIBLE);
        } else {
            mcomments_LL.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ADD_COMMENT) {
                if (messageCommentPOJOS == null) {
                    messageCommentPOJOS = new ArrayList<>();
                }

                if (mcomments_rv_adapter == null) {
                    messageCommentPOJOS.add((MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, schedule_title, scheduleId, this, 1);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                } else {
                    messageCommentPOJOS.add(0, (MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, schedule_title, scheduleId, this, 1);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                }
                ((MessageCommentsAdapter) mcomments_rv_adapter).addCommentSize();

            } else if (requestCode == REQUEST_CODE_EDIT) {
                recreate();
            } else if (requestCode == 1) {
                ((MessageCommentsAdapter) mcomments_rv_adapter).updateData(data.getIntExtra("position", -1), data.getStringExtra("comment"));
            }
            if (requestCode == 5) {

                String position = data.getStringExtra("position");
                String comment = Utils.unescapeJava(data.getStringExtra("comment"));
                messageCommentPOJOS.get(Integer.parseInt(position)).setCommentDescription(comment);

                mcomments_rv_adapter.notifyDataSetChanged();
            }
        }
    }

    private void SelectOption(View v) {
        popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.message_board_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit_menu:
                        Intent intent = new Intent(context, AddScheduleActivity.class);
                        intent.putExtra("type", AddScheduleActivity.TYPE_EDIT);
                        intent.putExtra("schedule_id", scheduleId);
                        intent.putExtra("description", description);
                        startActivityForResult(intent, REQUEST_CODE_EDIT);
                        return true;
                    case R.id.menu_trash:
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("record_id", scheduleId);
                        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                        paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("record_type", "Schedule");
                        apiRequest.postRequest(BASE_URL + DELETE_SCHEDULE, DELETE_SCHEDULE, paramsReq, Request.Method.POST);
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    RelativeLayout relMaster;
    ShimmerFrameLayout mShimmerDialog;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    AddRemoveUserAdapter mAddRemoveUserAdapter;
    RecyclerView recyclerView;

    public void addRemovePeopleDialog() {
        final Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_remove_user, null);
        dialogBuilder.setContentView(dialogView);
        TextView selectEveryOne = dialogView.findViewById(R.id.selectEveryOne);
        TextView selectNoOne = dialogView.findViewById(R.id.selectNoOne);
        TextView tv_cancel = dialogView.findViewById(R.id.tv_cancel);
        recyclerView = dialogView.findViewById(R.id.rv_dialogAddRemove);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        ;

        selectEveryOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.selectAllSelections();
                    }
                }
            }
        });

        selectNoOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.clearAllSelections();
                    }
                }
            }
        });

        Button btnSave = dialogView.findViewById(R.id.btnSave);
        mShimmerDialog = dialogView.findViewById(R.id.shimmer_view_container);
        fetchEmployee();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commaSeperatedID = getAssignedUserString();
                if (commaSeperatedID.equals("")) {
                    Toast.makeText(ScheduleDetailActivity.this, getResources().getString(R.string.please_select_atleast_one), Toast.LENGTH_SHORT).show();
                } else {
                    all_users_id = commaSeperatedID;
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("record_id", scheduleId);
                    paramsReq.put("loggedIn_user_id", ShardPreferences.get(ScheduleDetailActivity.this, share_UserId_key));
                    paramsReq.put("company_id", ShardPreferences.get(ScheduleDetailActivity.this, share_Company_Id_key));
                    paramsReq.put("user_id", commaSeperatedID);
                    paramsReq.put("project_team_id", ShardPreferences.get(ScheduleDetailActivity.this, share_current_project_team_id));
                    paramsReq.put("notification_type", "schedule");
                    apiRequest.postRequestBackground(BASE_URL + ADD_REMOVE_NOTIFIED, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
                    dialogBuilder.dismiss();
                }
            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.show();

    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        apiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);

    }

    private String getAssignedUserString() {
        if (mAddRemoveUserAdapter != null) {
            return mAddRemoveUserAdapter.getAssignedUserString();
        }
        return "";
    }
}