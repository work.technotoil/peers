package com.peerbuckets.peerbucket.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.USER_PASSWORD;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_ISFLOW;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_email_key;


public class SignUpPasswordActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse {

    ProgressBar pb_progress;
    EditText password;
    ApiRequest mApiRequest;
    Context context = this;
    Button navigateNextPassword;
    TextView contact_support_tv;
    ImageView navigate_back;
    String language = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_password);

        mApiRequest = new ApiRequest(this, (IApiResponse) this);
        language = Locale.getDefault().getDisplayLanguage();
        navigate_back = findViewById(R.id.navigate_back);
        pb_progress = findViewById(R.id.pb_progress);
        password = findViewById(R.id.password_text);
        navigateNextPassword = findViewById(R.id.next_password);
        contact_support_tv = findViewById(R.id.contact_support_tv);

        if (language.equals("???????")) {
            ShardPreferences.save(context, share_language, "2");
            navigate_back.setImageResource(R.drawable.ic_arrow_forward);
        } else {
            ShardPreferences.save(context, share_language, "1");
        }

        navigateNextPassword.setOnClickListener(this);
        contact_support_tv.setOnClickListener(this);
        navigate_back.setOnClickListener(this);

    }

    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.next_password:
                if (password.getText().toString().length() > 7) {
                    navigateNextPassword.setVisibility(View.GONE);
                    pb_progress.setVisibility(View.VISIBLE);
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("password", password.getText().toString());
                    paramsReq.put("userId", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
                    mApiRequest.postRequest(BASE_URL + USER_PASSWORD, USER_PASSWORD, paramsReq, Request.Method.POST);
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } else {
                    Toast.makeText(context, getResources().getString(R.string.Please_Enter_your_Password), Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.contact_support_tv:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_support_team, null);
                dialogBuilder.setView(dialogView);

                TextView mDialogCancel = (TextView) dialogView.findViewById(R.id.contact_support_cancel_tv);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                mDialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                break;

            case R.id.navigate_back:

                finish();

                break;
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(USER_PASSWORD)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                navigateNextPassword.setVisibility(View.VISIBLE);
                pb_progress.setVisibility(View.GONE);
                if (jsonObject.getString("code").equals("200")) {
                    ShardPreferences.save(this, share_ISFLOW, jsonObject.getString("isFlow"));
                    Intent intent = new Intent(this, SetupAccountActivity.class);
                    startActivity(intent);
                    finish();
                }
            } catch (JSONException e) {
                navigateNextPassword.setVisibility(View.VISIBLE);
                pb_progress.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
