package com.peerbuckets.peerbucket.Activity;

import com.teamwork.autocomplete.MultiAutoComplete;

class TestDelayer implements MultiAutoComplete.Delayer {
    @Override
    public long getPostingDelay(CharSequence constraint) {
        return 10;
    }
}
