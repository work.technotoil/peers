package com.peerbuckets.peerbucket.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Adapter.IteamShowAdapter;
import com.peerbuckets.peerbucket.Adapter.RecentActivityAdapter;
import com.peerbuckets.peerbucket.Hpojo.HomeModule;
import com.peerbuckets.peerbucket.POJO.MeActivitiListResponse.Activity;
import com.peerbuckets.peerbucket.POJO.MeActivitiListResponse.RecentLatestActivityResponse;
import com.peerbuckets.peerbucket.POJO.deleteTrash.TrashDeleteResponse;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;

public class RecentActivity extends AppCompatActivity {
    ImageView imgBack;
    RecyclerView rv_latestActivityMe;
    RecyclerView.LayoutManager rv_latestActivityMeLayoutManager;
    RecentActivityAdapter recentActivityAdapter;
    Context ctx = this;
    TextView tv_tool;
    ProgressBar recent_progress;
    int page = 0, currentItemSize = 0;
    boolean responseWait = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent);
        imgBack = findViewById(R.id.imgBack);
        recent_progress = findViewById(R.id.recent_progress);
        tv_tool = findViewById(R.id.tv_tool);
        rv_latestActivityMe = findViewById(R.id.rv_latestActivityMe);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (ShardPreferences.get(context,share_language).equals("2")){
            imgBack.setImageResource(R.drawable.ic_arrow_forward);
        }

        rv_latestActivityMe.setHasFixedSize(true);
        rv_latestActivityMeLayoutManager = new LinearLayoutManager(ctx);
        rv_latestActivityMe.setLayoutManager(rv_latestActivityMeLayoutManager);

        tv_tool.setText(Utils.unescapeJava(ShardPreferences.get(ctx, ShardPreferences.share_user_name_key)) + " " + "Activity");
        seeRecentActivity(page);
        applyPagination();
    }

    private void applyPagination() {
        rv_latestActivityMe.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) rv_latestActivityMe.getChildAt(rv_latestActivityMe.getChildCount() - 1);

                int diff = (view.getBottom() - (rv_latestActivityMe.getHeight() + rv_latestActivityMe
                        .getScrollY()));

                if (diff == 0) {
                    if (responseWait) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            recent_progress.setVisibility(View.VISIBLE);
                            Log.v("...", "Last Item Wow !" + page);
                            responseWait = false;
                            //Do pagination.. i.e. fetch new data

                            seeRecentActivity(0);

                        }
                    }
                }
            }
        });

    }

    private void seeRecentActivity(final int i) {
        Map<String, String> map = new HashMap<>();
        map.put("userId", ShardPreferences.get(ctx, share_current_UserId_key));
        map.put("companyId", ShardPreferences.get(ctx, ShardPreferences.key_company_Id));
        map.put("page", i + "");

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RecentLatestActivityResponse> resultCall = apiInterface.callRecentLatestActivity(map);
        resultCall.enqueue(new Callback<RecentLatestActivityResponse>() {

            @Override
            public void onResponse(Call<RecentLatestActivityResponse> call, Response<RecentLatestActivityResponse> response) {
                recent_progress.setVisibility(View.GONE);
                if (response.body().getStatus()) {
                    recentActivity(response.body().getActivities());
                    if (i == 0) {
                        page = page + 1;
                        responseWait = true;
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<RecentLatestActivityResponse> call, Throwable t) {
                recent_progress.setVisibility(View.GONE);
            }
        });
    }

    private void recentActivity(List<Activity> list) {
        if (list != null && list.size() > 0) {
            currentItemSize = list.size();
            recentActivityAdapter = new RecentActivityAdapter(ctx, (ArrayList<Activity>) list);
            rv_latestActivityMe.setAdapter(recentActivityAdapter);
        } else {
            list = new ArrayList<>();
            recentActivityAdapter = new RecentActivityAdapter(ctx, (ArrayList<Activity>) list);
            rv_latestActivityMe.setAdapter(recentActivityAdapter);
        }
    }
}
