package com.peerbuckets.peerbucket.Activity.todo;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.message.AddMessageBoardActivity;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.ToDoList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_TODO;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.UPDATE_TODO;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class AddToDoActivity extends AppCompatActivity implements IApiResponse, EditorHtmlInterface, View.OnClickListener {

    Context context = AddToDoActivity.this;
    EditText etTitle;
    WebView editor;
    ApiRequest mApiRequest;
    String mcompanyId = "", mcurrentProjectTeamId = "", mcurrent_type = "", mCurrentuserId = "", groupMembersUserId = "";
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    TextView tv_messageTitle;
    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;
    private String editorData = "", messageTitle = "", classType = "", todoId = "";
    public static final String TYPE_ADD = "add", TYPE_EDIT = "edit";
    Button btnAddToDo;
    private Toolbar toolbar;
    ProgressBar pb_progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_do);

        if (getIntent().getStringExtra("classType") != null) {
            classType = getIntent().getStringExtra("classType");
        }
        if (getIntent().getStringExtra("title") != null) {
            messageTitle = getIntent().getStringExtra("title");
        }
        if (getIntent().getStringExtra("description") != null) {
            editorData = getIntent().getStringExtra("description");
        }
        if (getIntent().getStringExtra("id") != null) {
            todoId = getIntent().getStringExtra("id");
        }
        inIt();
        getPrefrences();
        editor.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                fetchEmployee(); // Mentioned User
            }
        });
    }

    private void back() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void inIt() {
        toolbar = findViewById(R.id.toolbar);
        pb_progressBar = findViewById(R.id.pb_progressBar);
        tv_messageTitle = findViewById(R.id.tv_messageTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle("");
        back();
        tv_messageTitle.setText(ShardPreferences.get(context, share_current_module));
        mApiRequest = new ApiRequest(context, this);
        etTitle = findViewById(R.id.et_title_add_to_do);
        editor = findViewById(R.id.editor_add_to_do);
        editor.getSettings().setJavaScriptEnabled(true);
        btnAddToDo = findViewById(R.id.btn_add_to_do);
        btnAddToDo.setOnClickListener(this);
        editor.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        editor.setWebChromeClient(new WebChromeClient() {
            // For Lollipop 5.0+ Devices
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }

                uploadMessage = filePathCallback;

                Intent intent = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    intent = fileChooserParams.createIntent();
                }
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                } catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cannot_Open_File_Chooser), Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }

            //For Android 4.1 only
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.File_Browser)), FILECHOOSER_RESULTCODE);
                finish();
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.File_Chooser)), FILECHOOSER_RESULTCODE);
            }
        });
        editor.addJavascriptInterface(new WebAppInterfaceEdit(this, this), "Android");
        editor.setWebContentsDebuggingEnabled(true);
        editor.getSettings().setDomStorageEnabled(true);

        if (classType.equals(TYPE_EDIT)) {
            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String trixCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.css";
            String attachemtPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/attachments.js?" + random;
            String trixJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/trix.js?" + random;
            String tributeJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.js?" + random;
            String mentionJsPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/mentionmobile.js?" + random;
            String jQueryPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/jQuery.js?" + random;
            String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;
            String messageBody = Utils.unescapeJava(editorData);
            messageBody = messageBody.replace("\"", "'");
            String pish = "<html><head><link rel='stylesheet' media='all' href=" + basecampCSS + " data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href=" + tributeCssPath + "><script src=" + jQueryPath + "></script><script src=" + attachemtPath + ">" +
                    "</script><script src=" + trixJsPath + "></script><script src=" + tributeJsPath + "></script>" +
                    "<script src=" + mentionJsPath + "></script>" +
                    "</script>\n</head><body>";
            String editorHtml = "<form>" +
                    "  <input id=\"x\" value='" + messageBody + "'  type=\"hidden\" name=\"content\">\n" +
                    "  <input id=\"mention_uid\"  type=\"hidden\" name=\"content\">\n" +
                    "  <trix-editor id='zss_editor_content' input=\"x\"></trix-editor>\n" +
                    "</form>";
            String pas = "<script>" +
                    "function hello()" +
                    "{" +
                    "   var _id = document.getElementById('x').value;" +
                    "   var mention_uid = document.getElementById('mention_uid').value;" +
                    "    Android.nextScreen(_id,mention_uid);" +
                    "}" +
                    "</script></body></html>";
            String myHtmlString = pish + editorHtml + pas;
            editor.loadData("<style>img{display: inline;height: auto;max-width: 100%;}</style>" + myHtmlString, "text/html; charset=utf-8", null);

        } else {
            editor.loadUrl("file:///android_asset/index1.html");
        }

        if (classType.equals(TYPE_EDIT)) {
            etTitle.setText(Utils.checkStringWithEmpty(messageTitle));
            btnAddToDo.setText(getResources().getString(R.string.update));
            getSupportActionBar().setTitle(getResources().getString(R.string.update_todo));
        } else {
            btnAddToDo.setText(getResources().getString(R.string.add));
            getSupportActionBar().setTitle(getResources().getString(R.string.add_new_todo));
        }

    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", mcompanyId);
        paramsReq.put("type", mcurrent_type);
        paramsReq.put("typeId", mcurrentProjectTeamId);
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);
    }

    private void getPrefrences() {
        mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
        mcurrentProjectTeamId = ShardPreferences.get(context, share_current_project_team_id);
        mcurrent_type = ShardPreferences.get(context, share_addd_remove_current_type);
        mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {

            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        editor.loadUrl("javascript:setMentioningData(" + mEmplyeeListArray.toString() + ")");
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        for (int i = 0; i < mEmployeeDetailList.size(); i++) {
                            if (i == 0) {
                                groupMembersUserId = mEmployeeDetailList.get(i).getUserId();
                            } else {
                                groupMembersUserId = groupMembersUserId + "," + mEmployeeDetailList.get(i).getUserId();
                            }
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(ADD_TODO)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    btnAddToDo.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    ToDoList toDoList = new ToDoList();
                    toDoList.setCommentCount(0);
                    toDoList.setCompletedTask("0");
                    toDoList.setTotalTask("0");
                    toDoList.setTodoListDesc(Utils.getUnicodeString(Utils.escapeUnicodeText(editorData)));
                    toDoList.setTodoListTitle(Utils.getUnicodeString(Utils.escapeUnicodeText(messageTitle)));
                    toDoList.setUser_id(mCurrentuserId);
                    toDoList.setTodoListId(jsonObject.getString("todo_id"));
                    Intent intent = new Intent(context, ExpandToDoActivity.class);
                    intent.putExtra("to_do_id", jsonObject.getString("todo_id"));
                    intent.putExtra("user_id", ShardPreferences.get(context, share_current_UserId_key));
                    startActivityForResult(intent, 12);

                } else {
                    btnAddToDo.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                btnAddToDo.setVisibility(View.VISIBLE);
                pb_progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }

        }

        if (tag_json_obj.equals(UPDATE_TODO)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    btnAddToDo.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    ToDoList toDoList = new ToDoList();
                    toDoList.setTodoListDesc(Utils.getUnicodeString(Utils.escapeUnicodeText(editorData)));
                    toDoList.setTodoListTitle(Utils.getUnicodeString(Utils.escapeUnicodeText(messageTitle)));
                    toDoList.setUser_id(mCurrentuserId);
                    toDoList.setTodoListId(jsonObject.getString("todo_id"));
                    Intent intent = getIntent();
                    intent.putExtra("todo", toDoList);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    btnAddToDo.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                btnAddToDo.setVisibility(View.VISIBLE);
                pb_progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void setData(String htmlData, String mention_uid) {
        editorData = htmlData.trim();
        messageTitle = etTitle.getText().toString();
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", mCurrentuserId);
        paramsReq.put("project_id", mcurrentProjectTeamId);
        paramsReq.put("mention_user_id", mention_uid);
        paramsReq.put("company_id", mcompanyId);
        paramsReq.put("todo_list_title", Utils.getUnicodeString(Utils.escapeUnicodeText(messageTitle)));
        paramsReq.put("new_todo_desc", Utils.getUnicodeString(Utils.escapeUnicodeText(htmlData)));
        paramsReq.put("users", groupMembersUserId);
        paramsReq.put("platform", "mobile");
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));

        if (classType.equals(TYPE_EDIT)) {
            paramsReq.put("todo_id", todoId);
            paramsReq.put("todo_title", Utils.getUnicodeString(Utils.escapeUnicodeText(messageTitle)));
            mApiRequest.postRequest(BASE_URL + UPDATE_TODO, UPDATE_TODO, paramsReq, Request.Method.POST);
        } else if (classType.equals(TYPE_ADD)) {
            mApiRequest.postRequest(BASE_URL + ADD_TODO, ADD_TODO, paramsReq, Request.Method.POST);
        }

    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null)
                    return;
                uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                uploadMessage = null;
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;
            Uri result = intent == null || resultCode != AddMessageBoardActivity.RESULT_OK ? null : intent.getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed_to_Upload_Image), Toast.LENGTH_LONG).show();
        }
        if (requestCode == 12) {
            Intent intent1 = new Intent(context, ToDoActivity.class);
            startActivity(intent1);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_to_do:
                btnAddToDo.setVisibility(View.GONE);
                pb_progressBar.setVisibility(View.VISIBLE);
                if (etTitle.getText().toString().equals("")) {
                    btnAddToDo.setVisibility(View.VISIBLE);
                    pb_progressBar.setVisibility(View.GONE);
                    etTitle.setFocusable(true);
                    etTitle.setError(getResources().getString(R.string.please_enter_title));
                } else {
                    editor.loadUrl("javascript:copyContent()");
                    editor.loadUrl("javascript:hello()");
                }


                break;
        }
    }

    class WebAppInterfaceEdit {
        Context mContext;
        EditorHtmlInterface mEditorHtmlInterface;

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterfaceEdit(Context c, EditorHtmlInterface mEditorHtmlInterface) {
            mContext = c;
            this.mEditorHtmlInterface = mEditorHtmlInterface;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void nextScreen(String htmlData, String mention_uid) {
            mEditorHtmlInterface.setData(htmlData, mention_uid);

        }
    }
}
