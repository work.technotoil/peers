package com.peerbuckets.peerbucket.Activity.docs;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.Activity.AddCommentActivity;
import com.peerbuckets.peerbucket.Activity.GoogleDriveActivity;
import com.peerbuckets.peerbucket.Activity.UploadFileActivity;
import com.peerbuckets.peerbucket.Adapter.AddRemoveUserAdapter;
import com.peerbuckets.peerbucket.Adapter.EmployeeIconAdapter;
import com.peerbuckets.peerbucket.Adapter.MessageCommentsAdapter;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.ToDoList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.Activity.todo.ExpandToDoActivity.REQUEST_SUB_TO_DO_ADD;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_REMOVE_NOTIFIED;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_DOCUMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_COMMENT_LIST;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.FETCH_EMPLOYEE_DETAILS;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.GET_DOCUMENT_DETAIL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SUBSCRIBE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_language;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_image_key;

public class DocumentDetailActivity extends AppCompatActivity implements View.OnClickListener, IApiResponse, RefreshCommentInterface {

    ApiRequest mApiRequest;

    Toolbar toolbar;

    WebView m_description;

    TextView tv_download, tv_view, tv_user_name_upload_details, tv_user_upload_details,
            tv_add_remove, tv_people, message_count_tv, madd_comment_tv, tv_selectOption;

    ImageView img_file, user_image, img_user_upload_details, imgApplause;

    Context context = this;

    String name = "", all_users_id = "", fileName = "", record_id = "",
            title = "", email = "", description = "", user_id = "", parent_id = "",
            lastName = "", firstName = "", document_type = "", commaSeperatedID = "";

    PopupMenu popup;

    private LinearLayout mno_comments_LL, mcomments_LL;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 0;
    private Boolean loading = true;
    public static int REQ_CODE_ADD = 2;
    static int REQUEST_CODE_MESSAGE_EDIT = 3;
    private int REQUEST_ADD_COMMENT = 552;

    RecyclerView employeee_icon_rv, employeee_applause_rv, mcomments_rv;

    LinearLayoutManager layoutManagerComment;
    RecyclerView.LayoutManager mEmployeListanager, mApplauseManager;

    MessageCommentsAdapter mcomments_rv_adapter;
    EmployeeIconAdapter mNotifiedUserAdapter;

    ArrayList<MessageCommentPOJO> messageCommentPOJOS;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetalPOJO = new ArrayList<>();
    ArrayList<EmployeeDetalPOJO> mApplauseUserList = new ArrayList<>();

    Boolean isApplause = false;

    Button btn_subscribe, btn_unsubscribe;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_detail);
        mApiRequest = new ApiRequest(this, this);
        record_id = getIntent().getStringExtra("document_id");
        parent_id = getIntent().getStringExtra("parent_id");
        user_id = getIntent().getStringExtra("user_id");
        checkFolderExist();
        initUI();
        initNotifiedModule();
        initCommentUI();

        loadDetails();
        getCommentList(page);
    }


    public void initNotifiedModule() {

        tv_view = findViewById(R.id.tv_view);
        tv_user_upload_details = findViewById(R.id.tv_user_upload_details);
        tv_user_name_upload_details = findViewById(R.id.tv_user_name_upload_details);
        img_user_upload_details = findViewById(R.id.img_user_upload_details);
        tv_add_remove = findViewById(R.id.tv_add_remove);
        tv_people = findViewById(R.id.tv_people);
        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_unsubscribe = findViewById(R.id.btn_unsubscribe);
        imgApplause = findViewById(R.id.imgApplause);
        employeee_icon_rv = findViewById(R.id.employeee_icon_rv);
        mEmployeListanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_icon_rv.setLayoutManager(mEmployeListanager);

        employeee_applause_rv = findViewById(R.id.employeee_applause_rv);
        mApplauseManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        employeee_applause_rv.setLayoutManager(mApplauseManager);

        tv_add_remove.setOnClickListener(this);
        imgApplause.setOnClickListener(this);
        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_subscribe.setVisibility(View.GONE);
                btn_unsubscribe.setVisibility(View.VISIBLE);
                actionSubscribe("1");
                tv_view.setVisibility(View.VISIBLE);
            }
        });

        btn_unsubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_view.setVisibility(View.GONE);
                btn_subscribe.setVisibility(View.VISIBLE);
                btn_unsubscribe.setVisibility(View.GONE);
                actionSubscribe("0");
            }
        });
    }

    private void SelectOption(View v) {
        popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.message_board_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit_menu:
                        if (document_type.equals("drive")) {
                            Intent intent = new Intent(context, GoogleDriveActivity.class);
                            intent.putExtra("record_id", record_id);
                            intent.putExtra("parent_id", parent_id);
                            intent.putExtra("class_type", "edit");
                            startActivityForResult(intent, REQUEST_CODE_MESSAGE_EDIT);
                        } else {
                            Intent intent = new Intent(context, UploadFileActivity.class);
                            intent.putExtra("record_id", record_id);
                            intent.putExtra("class_type", "edit");
                            startActivityForResult(intent, REQUEST_CODE_MESSAGE_EDIT);
                        }
                        return true;
                    case R.id.menu_trash:
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("record_id", record_id);
                        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                        paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("record_type", "File");
                        mApiRequest.postRequest(BASE_URL + DELETE_DOCUMENT, DELETE_DOCUMENT, paramsReq, Request.Method.POST);
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    private void actionSubscribe(String isSubscribe) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", record_id);
        paramsReq.put("company_id", ShardPreferences.get(DocumentDetailActivity.this, share_Company_Id_key));
        paramsReq.put("loggedIn_user_id", commaSeperatedID);
        paramsReq.put("user_id", ShardPreferences.get(DocumentDetailActivity.this, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(DocumentDetailActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "doc");
        paramsReq.put("isSubscribe", isSubscribe);
        mApiRequest.postRequestBackground(BASE_URL + SUBSCRIBE, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);

    }

    private void loadDetails() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("file_id", getIntent().getStringExtra("document_id"));
        paramsReq.put("project_team_id", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequest(BASE_URL + GET_DOCUMENT_DETAIL, GET_DOCUMENT_DETAIL, paramsReq, Request.Method.POST);
    }

    private void getCommentList(int cPage) {
        Map<String, String> paramsReq = new HashMap<String, String>();
        paramsReq.put("user_id", ShardPreferences.get(context, ShardPreferences.share_UserId_key));
        paramsReq.put("company_id", ShardPreferences.get(context, ShardPreferences.share_Company_Id_key));
        paramsReq.put("type", "document");
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("todo_id", record_id);
        paramsReq.put("page", String.valueOf(cPage));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_COMMENT_LIST, FETCH_COMMENT_LIST, paramsReq, Request.Method.POST);

    }

    private void initCommentUI() {
        madd_comment_tv = findViewById(R.id.add_comment_tv);
        madd_comment_tv.setOnClickListener(this);
        mcomments_rv = findViewById(R.id.comments_rv);
        mno_comments_LL = findViewById(R.id.no_comments_LL);
        mcomments_LL = findViewById(R.id.comments_LL);
        mno_comments_LL.setVisibility(View.GONE);
        mcomments_LL.setVisibility(View.GONE);

        layoutManagerComment = new LinearLayoutManager(context);
        layoutManagerComment.setOrientation(RecyclerView.VERTICAL);
        mcomments_rv.setLayoutManager(layoutManagerComment);
        message_count_tv = findViewById(R.id.message_count_tv);

        mcomments_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManagerComment.getChildCount();
                    totalItemCount = layoutManagerComment.getItemCount();
                    pastVisiblesItems = layoutManagerComment.findFirstVisibleItemPosition();
                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Log.v("...", "Last Item Wow !" + page);
                            loading = false;
                            //Do pagination.. i.e. fetch new data

                            getCommentList(page);

                        }
                    }
                }
            }
        });
    }


    private void initUI() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        img_file = findViewById(R.id.img_file);
        user_image = findViewById(R.id.user_image);
        tv_download = findViewById(R.id.tv_download);
        m_description = findViewById(R.id.m_description);
        tv_selectOption = findViewById(R.id.tv_selectOption);
        toolbar.setTitle(getResources().getString(R.string.docs_files));
        if (ShardPreferences.get(context, share_language).equals("2")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_forward);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                intent.putExtra("id", record_id);
                intent.putExtra("message_title", title);
                intent.putExtra("isDelete", false);
                onBackPressed();
            }
        });

        tv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFileFromURL().execute(DEVELOPMENT_URL + "uploads/" + fileName, fileName);
            }
        });
        tv_selectOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectOption(view);
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_comment_tv:
                Intent intent1 = new Intent(DocumentDetailActivity.this, AddCommentActivity.class);
                intent1.putExtra("call_type", "document");
                intent1.putExtra("message_title", title);
                intent1.putExtra("message_email", email);
                intent1.putExtra("name", name);
                intent1.putExtra("record_id", record_id);
                intent1.putExtra("all_users_id", all_users_id);
                startActivityForResult(intent1, REQUEST_ADD_COMMENT);
                break;
            case R.id.tv_add_remove:
                addRemovePeopleDialog();
                break;
            case R.id.imgApplause:
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause();
                break;
        }

    }

    public void addApplause() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", record_id);
        paramsReq.put("company_id", ShardPreferences.get(DocumentDetailActivity.this, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(DocumentDetailActivity.this, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(DocumentDetailActivity.this, share_current_project_team_id));
        paramsReq.put("notification_type", "doc");
        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }

        mApiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(40)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(GET_DOCUMENT_DETAIL)) {
            Log.d("SINGLE_TODO_API", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject jsonObject1 = jsonObject.getJSONObject("document");
                int status = jsonObject.getInt("status");
                if (status == 1) {

                    isApplause = jsonObject.getBoolean("isApplause");

                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                        btn_subscribe.setVisibility(View.GONE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }


                    if (!jsonObject1.getString("user_image").equals("")) {
                        Picasso.with(context).load(DEVELOPMENT_URL + jsonObject1.getString("user_image")).into(img_user_upload_details);
                    } else {
                        img_user_upload_details.setImageDrawable(imageLatter(Utils.word(jsonObject1.getString("employee_name")), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
                    }

                    if (!ShardPreferences.get(context, share_user_image_key).equals("")) {
                        Picasso.with(this).load(ShardPreferences.get(context, ShardPreferences.share_user_image_key)).into(user_image);
                    } else {
                        user_image.setImageDrawable(imageLatter(Utils.word(jsonObject1.getString("employee_name")), jsonObject1.getString("bgColor"), jsonObject1.getString("fontColor")));
                    }

                    description = jsonObject1.getString("document_description");
                    name = jsonObject1.getString("employee_name");
                    fileName = jsonObject1.getString("name_on_disk");
                    title = jsonObject1.getString("document_title");
                    email = jsonObject1.getString("email");
                    document_type = jsonObject1.getString("type");
                    JSONArray jsonArray_Users = jsonObject1.getJSONArray("users");
                    toolbar.setTitle(Utils.checkStringWithEmpty(Utils.unescapeJava(jsonObject1.getString("document_title"))));
                    Utils.setDescription(jsonObject1.getString("document_description"), m_description, context);
                    tv_user_name_upload_details.setText(Utils.unescapeJava(jsonObject1.getString("employee_name")));
                    Picasso.with(this).load(jsonObject1.getString("folder_image")).into(img_file);

                    tv_user_upload_details.setText(getResources().getString(R.string.Uploaded_on) + jsonObject1.getString("updated_at"));
                    JSONObject jsonObject2;
                    if (jsonArray_Users != null) {

                        for (int i = 0; i < jsonArray_Users.length(); i++) {
                            jsonObject2 = jsonArray_Users.getJSONObject(i);
                            if (i == 0) {
                                all_users_id = jsonObject2.getString("user_id");
                            } else {
                                all_users_id = all_users_id + "," + jsonObject2.getString("user_id");
                            }
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));


                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }
                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                    tv_people.setText("" + jsonArray_Users.length() + " ");

                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");

                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject3 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject3.getString("user_id"));
                            mModel.setEmployeeName(jsonObject3.getString("employee_name"));
                            mModel.setBgColor(jsonObject3.getString("bgColor"));
                            mModel.setFontColor(jsonObject3.getString("fontColor"));
                            mModel.setUserImage(jsonObject3.getString("user_image"));

                            mApplauseUserList.add(mModel);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                        employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("EXCPTN", e.toString());
            }
        }

        if (tag_json_obj.equals(FETCH_COMMENT_LIST)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("1")) {
                    JSONArray mArray = jsonObject.getJSONArray("commentList");
                    if (mArray != null && mArray.length() > 0) {
                        if (page == 0 && messageCommentPOJOS != null) {
                            messageCommentPOJOS.clear();
                        }

                        messageCommentPOJOS = new Gson().fromJson(mArray.toString(), new TypeToken<List<MessageCommentPOJO>>() {
                        }.getType());

                        message_count_tv.setText(messageCommentPOJOS.size() + "");

                        if (messageCommentPOJOS != null && messageCommentPOJOS.size() > 0) {
                            if (page == 0) {
                                mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, context, title, record_id, this, 0);
                                mcomments_rv.setAdapter(mcomments_rv_adapter);
                            } else {

                                mcomments_rv.post(new Runnable() {
                                    public void run() {
                                        mcomments_rv_adapter.setNotifyData(messageCommentPOJOS);
                                    }
                                });
                            }
                        }

                        page = page + 1;
                        loading = true;
                    } else {
                        message_count_tv.setText("0");
                    }

                } else {
                    message_count_tv.setText("0");
                    loading = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(DELETE_DOCUMENT)) {
            Log.d("DELETE_MB", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("id", record_id);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d("EXCPTN", e.toString());
            }
        }


        if (tag_json_obj.equals(ADD_REMOVE_NOTIFIED)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    if (jsonObject.getBoolean("isSubscribe")) {
                        btn_subscribe.setVisibility(View.GONE);
                        btn_unsubscribe.setVisibility(View.VISIBLE);
                    } else {
                        btn_subscribe.setVisibility(View.VISIBLE);
                        btn_unsubscribe.setVisibility(View.GONE);
                    }
                    tv_people.setText("" + jsonObject.getString("userCount") + " ");
                    JSONArray mUserArray = jsonObject.getJSONArray("update_user_list");
                    if (mUserArray != null) {
                        if (mEmployeeDetalPOJO != null) {
                            mEmployeeDetalPOJO.clear();
                        }
                        for (int i = 0; i < mUserArray.length(); i++) {
                            JSONObject jsonObject1 = mUserArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject1.getString("user_id"));
                            mModel.setEmployeeName(jsonObject1.getString("employee_name"));
                            mModel.setBgColor(jsonObject1.getString("bgColor"));
                            mModel.setFontColor(jsonObject1.getString("fontColor"));
                            mModel.setUserImage(jsonObject1.getString("user_image"));


                            mEmployeeDetalPOJO.add(mModel);
                            Log.d("ALL_USERS", all_users_id);
                        }

                        mNotifiedUserAdapter = new EmployeeIconAdapter(mEmployeeDetalPOJO, this);
                        employeee_icon_rv.setAdapter(mNotifiedUserAdapter);
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");
                    if (mApplauseUserList != null) {
                        mApplauseUserList.clear();
                    }
                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {
                        for (int i = 0; i < mGetApplauseArray.length(); i++) {
                            JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setUserId(jsonObject2.getString("user_id"));
                            mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                            mModel.setUserImage(jsonObject2.getString("user_image"));
                            mModel.setFontColor(jsonObject2.getString("fontColor"));
                            mModel.setBgColor(jsonObject2.getString("bgColor"));

                            mApplauseUserList.add(mModel);
                        }
                    }
                    mNotifiedUserAdapter = new EmployeeIconAdapter(mApplauseUserList, this);
                    employeee_applause_rv.setAdapter(mNotifiedUserAdapter);
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }

        if (tag_json_obj.equals(FETCH_EMPLOYEE_DETAILS)) {
            mShimmerDialog.stopShimmerAnimation();
            mShimmerDialog.setVisibility(View.GONE);
            try {
                JSONObject jsonObjec = new JSONObject(response);
                String code = jsonObjec.getString("code");
                if (code.equals("200")) {
                    JSONArray mEmplyeeListArray = jsonObjec.getJSONArray("employeeList");

                    if (mEmplyeeListArray.length() > 0) {
                        mEmployeeDetailList = new Gson().fromJson(mEmplyeeListArray.toString(), new TypeToken<List<EmployeeDetalPOJO>>() {
                        }.getType());
                        mAddRemoveUserAdapter = new AddRemoveUserAdapter(all_users_id, mEmployeeDetailList, this);
                        recyclerView.setAdapter(mAddRemoveUserAdapter);
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.no_employee_found), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.server_issue) + code, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void setCommentData(int commentSize) {
        message_count_tv.setText(commentSize + "");
        if (commentSize > 0) {
            mcomments_LL.setVisibility(View.GONE);
            mno_comments_LL.setVisibility(View.GONE);
        } else {
            mcomments_LL.setVisibility(View.GONE);
            mno_comments_LL.setVisibility(View.GONE);
        }
    }


    class DownloadFileFromURL extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog = new ProgressDialog(context);
        String filename = "";

        @Override
        protected String doInBackground(String... strings) {
            int count;
            try {

                filename = strings[1];

                URL url = new URL(strings[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(
                        url.openStream(), 8192);

                File mFile = Utils.getOutputMediaFile(filename, context);
                OutputStream output = new FileOutputStream(mFile);

                byte data[] = new byte[2048];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress((int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage(getResources().getString(R.string.Downloading_Please_wait));
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            if (pDialog != null && !pDialog.isShowing()) {
                pDialog.show();
            }
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
            pDialog.setProgress(progress[0]);
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {
                String downloaded = Environment.getExternalStorageDirectory()
                        .toString() + "/" + context.getResources().getString(R.string.app_name) + "/" + filename;

                Toast.makeText(context, getResources().getString(R.string.Downloading_completed), Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private boolean checkFolderExist() {
        String[] PERMISSIONS = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Utils.hasPermissions(context, PERMISSIONS)) {
            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + context.getResources().getString(R.string.app_name));

            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdirs();
            }
            if (success) {
                return true;

                // Do something on success
            } else {
                return false;
                // Do something else on failure
            }
        } else {
            Toast.makeText(context, context.getResources().getString(R.string.permission_grant_message), Toast.LENGTH_SHORT).show();
            return false;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_CODE_ADD) {
                recreate();
            }
            if (requestCode == REQUEST_ADD_COMMENT) {
                if (messageCommentPOJOS == null) {
                    messageCommentPOJOS = new ArrayList<>();
                }

                if (mcomments_rv_adapter == null) {
                    messageCommentPOJOS.add((MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, title, record_id, this, 0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                } else {
                    messageCommentPOJOS.add(0, (MessageCommentPOJO) data.getSerializableExtra("commentPOJO"));
                    mcomments_rv_adapter = new MessageCommentsAdapter(messageCommentPOJOS, this, title, record_id, this, 0);
                    mcomments_rv.setAdapter(mcomments_rv_adapter);
                    mcomments_rv_adapter.notifyDataSetChanged();
                }
            } else if (requestCode == REQUEST_CODE_MESSAGE_EDIT) {
                toolbar.setTitle(Html.fromHtml(data.getStringExtra("title")));
                Utils.setDescription(data.getStringExtra("desc"), m_description, context);
            }
        }
        if (requestCode == 1) {
            ((MessageCommentsAdapter) mcomments_rv_adapter).updateData(data.getIntExtra("position", -1), data.getStringExtra("comment"));
        }
        if (requestCode == REQUEST_SUB_TO_DO_ADD) {
            recreate();
        }
        if (requestCode == 5) {

            String position = data.getStringExtra("position");
            String comment = Utils.unescapeJava(data.getStringExtra("comment"));
            messageCommentPOJOS.get(Integer.parseInt(position)).setCommentDescription(comment);

            mcomments_rv_adapter.notifyDataSetChanged();
        }
    }


    RelativeLayout relMaster;
    ShimmerFrameLayout mShimmerDialog;
    ArrayList<EmployeeDetalPOJO> mEmployeeDetailList;
    AddRemoveUserAdapter mAddRemoveUserAdapter;
    RecyclerView recyclerView;

    public void addRemovePeopleDialog() {
        final Context mContext = this;
        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_remove_user, null);
        dialogBuilder.setContentView(dialogView);
        TextView selectEveryOne = dialogView.findViewById(R.id.selectEveryOne);
        TextView selectNoOne = dialogView.findViewById(R.id.selectNoOne);
        TextView tv_cancel = dialogView.findViewById(R.id.tv_cancel);
        recyclerView = dialogView.findViewById(R.id.rv_dialogAddRemove);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        ;

        selectEveryOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.selectAllSelections();
                    }
                }
            }
        });

        selectNoOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmployeeDetailList != null && mEmployeeDetailList.size() > 0) {
                    if (mAddRemoveUserAdapter != null) {
                        mAddRemoveUserAdapter.clearAllSelections();
                    }
                }
            }
        });

        Button btnSave = dialogView.findViewById(R.id.btnSave);
        mShimmerDialog = dialogView.findViewById(R.id.shimmer_view_container);
        fetchEmployee();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commaSeperatedID = getAssignedUserString();
                if (commaSeperatedID.equals("")) {
                    Toast.makeText(DocumentDetailActivity.this, getResources().getString(R.string.please_select_atleast_one), Toast.LENGTH_SHORT).show();
                } else {
                    all_users_id = commaSeperatedID;
                    Map<String, String> paramsReq = new HashMap<>();
                    paramsReq.put("record_id", record_id);
                    paramsReq.put("loggedIn_user_id", ShardPreferences.get(DocumentDetailActivity.this, share_UserId_key));
                    paramsReq.put("company_id", ShardPreferences.get(DocumentDetailActivity.this, share_Company_Id_key));
                    paramsReq.put("user_id", commaSeperatedID);
                    paramsReq.put("project_team_id", ShardPreferences.get(DocumentDetailActivity.this, share_current_project_team_id));
                    paramsReq.put("notification_type", "doc");
                    mApiRequest.postRequestBackground(BASE_URL + ADD_REMOVE_NOTIFIED, ADD_REMOVE_NOTIFIED, paramsReq, Request.Method.POST);
                    dialogBuilder.dismiss();
                }
            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.show();

    }

    private void fetchEmployee() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("user_id", ShardPreferences.get(this, share_current_UserId_key));
        paramsReq.put("companyId", ShardPreferences.get(this, share_Company_Id_key));
        paramsReq.put("typeId", ShardPreferences.get(this, share_current_project_team_id));
        paramsReq.put("type", ShardPreferences.get(this, share_addd_remove_current_type));
        mApiRequest.postRequestBackground(BASE_URL + FETCH_EMPLOYEE_DETAILS, FETCH_EMPLOYEE_DETAILS, paramsReq, Request.Method.POST);

    }

    private String getAssignedUserString() {
        if (mAddRemoveUserAdapter != null) {
            return mAddRemoveUserAdapter.getAssignedUserString();
        }
        return "";
    }


}
