package com.peerbuckets.peerbucket.Hpojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.peerbuckets.peerbucket.Hpojo.UserListing;

public class CompanyList {

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("project_team_description")
    @Expose
    private String projectTeamDescription;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("company_logo")
    @Expose
    private String companyLogo;
    @SerializedName("userListing")
    @Expose
    private List<UserListing> userListing = null;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProjectTeamDescription() {
        return projectTeamDescription;
    }

    public void setProjectTeamDescription(String projectTeamDescription) {
        this.projectTeamDescription = projectTeamDescription;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public List<UserListing> getUserListing() {
        return userListing;
    }

    public void setUserListing(List<UserListing> userListing) {
        this.userListing = userListing;
    }

}
