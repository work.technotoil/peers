package com.peerbuckets.peerbucket.Hpojo;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ModuleList {

    @SerializedName("module_name")
    @Expose
    private String moduleName;
    @SerializedName("module_id_PK")
    @Expose
    private String moduleIdPK;
    @SerializedName("moduleList")
    @Expose
    private List<HomeModule> moduleList = null;

    @SerializedName("isSelected")
    @Expose
    private Boolean isSelected = false;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleIdPK() {
        return moduleIdPK;
    }

    public void setModuleIdPK(String moduleIdPK) {
        this.moduleIdPK = moduleIdPK;
    }

    public List<HomeModule> getModuleList() {
        return moduleList;
    }

    public void setModuleList(List<HomeModule> moduleList) {
        this.moduleList = moduleList;
    }

}
