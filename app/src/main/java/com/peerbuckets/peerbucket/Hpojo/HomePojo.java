package com.peerbuckets.peerbucket.Hpojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.peerbuckets.peerbucket.Hpojo.CompanyList;
import com.peerbuckets.peerbucket.Hpojo.ModuleList;

public class HomePojo {

    @SerializedName("companyList")
    @Expose
    private List<CompanyList> companyList = null;
    @SerializedName("moduleList")
    @Expose
    private List<ModuleList> moduleList = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("employeeID")
    @Expose
    private String employeeID;
    @SerializedName("url")
    @Expose
    private String url;

    public List<CompanyList> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<CompanyList> companyList) {
        this.companyList = companyList;
    }

    public List<ModuleList> getModuleList() {
        return moduleList;
    }

    public void setModuleList(List<ModuleList> moduleList) {
        this.moduleList = moduleList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
