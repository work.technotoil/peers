package com.peerbuckets.peerbucket.Hpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserListing {

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("bgColor")
    @Expose
    private String bgColor;

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    @SerializedName("fontColor")
    @Expose
    private String fontColor;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

}
