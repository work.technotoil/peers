package com.peerbuckets.peerbucket.Hpojo;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeModule {

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("project_team_name")
    @Expose
    private String projectTeamName;
    @SerializedName("project_team_description")
    @Expose
    private String projectTeamDescription;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("userListing")
    @Expose
    private List<UserListing> userListing = null;

    @SerializedName("isSelected")
    @Expose
    private Boolean isSelected = false;

    public Boolean getSelected() {
        return isSelected;
    }
    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getProjectTeamName() {
        return projectTeamName;
    }

    public void setProjectTeamName(String projectTeamName) {
        this.projectTeamName = projectTeamName;
    }

    public String getProjectTeamDescription() {
        return projectTeamDescription;
    }

    public void setProjectTeamDescription(String projectTeamDescription) {
        this.projectTeamDescription = projectTeamDescription;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public List<UserListing> getUserListing() {
        return userListing;
    }

    public void setUserListing(List<UserListing> userListing) {
        this.userListing = userListing;
    }

}
