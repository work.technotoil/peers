package com.peerbuckets.peerbucket.volley;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.net.ConnectivityManager.*;

public class CheckConnection {
    private static NetworkInfo WifiInformation, mobileInformation;
    static ConnectivityManager connection_manager;
    private static boolean isNetworkAvaliable = false;

    public static boolean isNetworkAvailable(Context cxt) {

        connection_manager = (ConnectivityManager) cxt.getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiInformation = connection_manager.getNetworkInfo(TYPE_WIFI);
        mobileInformation = connection_manager.getNetworkInfo(TYPE_MOBILE);

        isNetworkAvaliable = WifiInformation.isConnected() || mobileInformation.isConnected();
        return isNetworkAvaliable;
    }
}

