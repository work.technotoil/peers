package com.peerbuckets.peerbucket.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.widget.EditText;

import com.peerbuckets.peerbucket.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.teamwork.autocomplete.view.MultiAutoCompleteEditText;

public class PicassoImageGetter implements Html.ImageGetter {

    private MultiAutoCompleteEditText textView = null;
    private Context mcontext = null;

   /* public PicassoImageGetter() {

    }*/

    public PicassoImageGetter(MultiAutoCompleteEditText target, Context context) {

        textView = target;
        mcontext =  context;
    }

    @Override
    public Drawable getDrawable(String source) {
        BitmapDrawablePlaceHolder drawable = new BitmapDrawablePlaceHolder();

        /*Picasso.with(mcontext)
                .load(source)
                .placeholder(R.drawable.ic_launcher)
                .into(drawable);*/

        return drawable;
    }

 class BitmapDrawablePlaceHolder extends BitmapDrawable implements Target {

        protected Drawable drawable;

        @Override
        public void draw(final Canvas canvas) {
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            drawable.setBounds(0, 0, width, height);
            setBounds(0, 0, width, height);
            if (textView != null) {
                textView.setText(textView.getText());
            }


        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            setDrawable(new BitmapDrawable(mcontext.getResources(), bitmap));
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

    }
}