package com.peerbuckets.peerbucket.Utils;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.peerbuckets.peerbucket.interfaces.EditorHtmlInterface;

public class WebAppInterface {
    Context mContext;
    EditorHtmlInterface mEditorHtmlInterface;

    /**
     * Instantiate the interface and set the context
     */
    public WebAppInterface(Context c, EditorHtmlInterface mEditorHtmlInterface) {
        mContext = c;
        this.mEditorHtmlInterface = mEditorHtmlInterface;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void nextScreen(String htmlData, String mention_uid) {

        mEditorHtmlInterface.setData(htmlData, mention_uid);

    }
}