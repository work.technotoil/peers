package com.peerbuckets.peerbucket.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;

import androidx.core.app.ActivityCompat;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ImageConfig.ImagePath_Marshmallow.getDataColumn;
import static com.peerbuckets.peerbucket.ImageConfig.ImagePath_Marshmallow.isDownloadsDocument;
import static com.peerbuckets.peerbucket.ImageConfig.ImagePath_Marshmallow.isExternalStorageDocument;
import static com.peerbuckets.peerbucket.ImageConfig.ImagePath_Marshmallow.isMediaDocument;

public class Utils {

    public static RequestBody getRequestBodyParameter(String data) {

        return RequestBody.create(MediaType.parse("text/plain"), data);
    }

    public static String convertTimestampToDate(String data, String format) {
        //dd-MM-yyyy HH:mm:ss
        if (data != null && !data.equals("")) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.format(Long.parseLong(data));

        } else {
            return "";
        }

    }
    public static String getPath(final Context context, final Uri uri)
    {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    public static boolean isStringValid(String data) {
        if (data != null && !data.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public static void setDescription(String description, WebView mWebview, Context context) {
        Context ctx = context;
        if (Utils.isStringValid(description)) {
            String newString = description;


//            Spanned spanned = Html.fromHtml(newString);
//            char[] chars = new char[spanned.length()];
//            TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
//            String plainText = new String(chars);
            newString = newString.replace("\"", "'");

            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String tributeCssPath = DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

            String pish = "<html><head><link rel='stylesheet' media='all' href='" + basecampCSS + "' data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href='" + tributeCssPath + "'>" +
                    "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                    "<style type='text/css'> .boady { font-size: 15.0px; color:#000000;    background-color: #ffffff;} </style>" +
                    "</head><body class='boady'>";
            String pas = "</body></html>";
            String myHtmlString = pish + "<b>" + newString + "</b>" + pas;
            if (ShardPreferences.get(ctx, ShardPreferences.share_language).equals("2")) {
                mWebview.loadData("<body dir=\"rtl\"><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + description, "text/html; charset=utf-8", null);
            } else {
                mWebview.loadData("<body dir=\"ltr\" ><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + description, "text/html; charset=utf-8", null);
            }
        }

    }


    public static void setDescriptionHTML(String description, WebView mWebview) {
        if (Utils.isStringValid(description)) {
            String newString = description;


//            Spanned spanned = Html.fromHtml(newString);
//            char[] chars = new char[spanned.length()];
//            TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
//            String plainText = new String(chars);
            newString = newString.replace("\"", "'");

            final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
            String tributeCssPath = DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
            String basecampCSS = DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

            String pish = "<html><head><link rel='stylesheet' media='all' href='" + basecampCSS + "' data-turbolinks-track='reload'/>" +
                    "<link rel='stylesheet' type='text/css' href='" + tributeCssPath + "'>" +
                    "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                    "<style type='text/css'> .boady { font-size: 15.0px; color:#000000;    background-color: #ffffff;} </style>" +
                    "</head><body class='boady'>";
            String pas = "</body></html>";
            String myHtmlString = pish + "<b>" + newString + "</b>" + pas;

            mWebview.loadData(myHtmlString, "text/html; charset=utf-8", null);
        }

    }


    public static void setDescriptionWithDate(String description, String date, String assignedMembers, WebView mWebview, Context context) {
        String newString = description;
        newString = newString.replace("\"", "'");
        String mMembers = assignedMembers;
        mMembers = mMembers.replace("\"", "'");
        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
        String tributeCssPath = DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
        String basecampCSS = DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

        String pish = "<html><head><link rel='stylesheet' media='all' href='" + basecampCSS + "' data-turbolinks-track='reload'/>" +
                "<link rel='stylesheet' type='text/css' href='" + tributeCssPath + "'>" +
                "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                "<style type='text/css'> .boady { font-size: 15.0px; color:#000000;    background-color: #ffffff;} </style>" +
                "</head><body class='boady'>";
        String mDate = "<div style=\"display:inline-flex\"><img style=\"width:20px ; height:20px; border-radius:50%\"src='" + DEVELOPMENT_URL + "image/calender.png" + "' /><p style='margin:2px'>" + date + "</p></div> ";
        String pas = "</body></html>";
        String myHtmlString = pish + newString + "          " + mDate + "          " + mMembers + pas;
        if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
            mWebview.loadData("<body dir=\"rtl\"><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + newString, "text/html; charset=utf-8", null);
        } else {
            mWebview.loadData("<body dir=\"ltr\" ><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + newString, "text/html; charset=utf-8", null);
        }

        //  mWebview.loadData("<body dir=\"rtl\"><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" +newString, "text/html; charset=utf-8", null);
    }

    public static void setDescriptionWithDateHtml(String description, String date, String assignedMembers, WebView mWebview) {
        String newString = description;
        newString = newString.replace("\"", "'");
        String mMembers = assignedMembers;
        mMembers = mMembers.replace("\"", "'");
        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
        String tributeCssPath = DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
        String basecampCSS = DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

        String pish = "<html><head><link rel='stylesheet' media='all' href='" + basecampCSS + "' data-turbolinks-track='reload'/>" +
                "<link rel='stylesheet' type='text/css' href='" + tributeCssPath + "'>" +
                "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                "<style type='text/css'> .boady { font-size: 15.0px; color:#000000;    background-color: #ffffff;} </style>" +
                "</head><body class='boady'>";
        String mDate = "<div style=\"display:inline-flex\"><img style=\"width:20px ; height:20px; border-radius:50%\"src='" + DEVELOPMENT_URL + "image/calender.png" + "' /><p style='margin:2px'>" + date + "</p></div> ";
        String pas = "</body></html>";
        String myHtmlString = pish + newString + "          " + mDate + "          " + mMembers + pas;


        mWebview.loadData(myHtmlString, "text/html; charset=utf-8", null);
    }

    public static String parseDateChange(String time) {

        String outputPattern = "";
        String inputPattern = "";
        inputPattern = "yyyy-MM-dd HH:mm:ss";
        outputPattern = "dd MMM HH:mm";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = "";

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateWithFormat(String time, String inputPattern, String outputPattern) {
        if (!isStringValid(time)) {
            return "";
        }

//        inputPattern = "yyyy-MM-dd HH:mm:ss";
//        outputPattern = "dd MMM HH:mm";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = "";

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /*public static Boolean setCommentList(Context context, RecyclerView recyclerView, List<MessageCommentPOJO> list, MessageCommentsAdapter adapter, int type) {
        if (list != null && list.size() > 0) {
            adapter = new MessageCommentsAdapter(list, context, "", "");
            recyclerView.setAdapter(adapter);
        } else {
            return false;
        }
        return false;
    }*/

    public static String checkStringWithHash(String data) {
        if (data != null && !data.equals("")) {
            return data;
        } else {
            return "-";
        }
    }

    public static String checkStringWithEmpty(String data) {
        if (data != null && !data.equals("")) {
            return data;
        } else {
            return "";
        }
    }

    public static String word(String userName1) {

        String firstName = "", lastName = "", userName = "";
        if(userName.equals("")){
            userName = unescapeJava(userName1);
        }
        if (userName.trim().contains(" ")) {
            String[] nameParts = userName.split(" ");
            if (String.valueOf(nameParts[1].charAt(0)) != null) {
                firstName = String.valueOf(nameParts[0].charAt(0)).toUpperCase();
                lastName = String.valueOf(nameParts[1].charAt(0)).toUpperCase();
                return firstName + lastName;
            } else {
                firstName = String.valueOf(nameParts[0].charAt(0)).toUpperCase();
                return firstName;
            }

        } else {
            if (!userName.equals("")) {
                String[] nameParts = userName.split(" ");
                firstName = String.valueOf(nameParts[0].charAt(0)).toUpperCase();
            }

            //     Toast.makeText(mContext, firstName, Toast.LENGTH_SHORT).show();
        }
        return firstName;
    }

    public static void showNameDialog(Context context, String image, String name, String email, String bgColor, String fontColor) {
        final Dialog dialogBuilder = new Dialog(context);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_user_name_dialog, null);
        dialogBuilder.setContentView(dialogView);
        CircleImageView imageView = dialogView.findViewById(R.id.img_user_image_name_dialog);
        ImageView imgClose = dialogView.findViewById(R.id.img_close_name_dialog);
        TextView tvName = dialogView.findViewById(R.id.tv_user_name_dialog);
        TextView tvEmail = dialogView.findViewById(R.id.tv_user_email_name_dialog);
        try {
            if (isStringValid(image)) {
                Picasso.with(context).load(image).into(imageView);
            } else {
                imageView.setImageDrawable(imageLatter(Utils.word(name), bgColor, fontColor));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvName.setText(Utils.checkStringWithHash(name));
        tvEmail.setText(Utils.checkStringWithHash(email));
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });
        dialogBuilder.show();
    }

    public static String createUserHtml(String userName, String userImage, String backColor, String fontColor) {
        String data;
        if (isStringValid(userName)) {
           /* if (!isStringValid(userImage)) {
                String s = userName.substring(0, 1);
                userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
            }*/
            if (!userImage.equals("")) {
                data = "<div style=\"-webkit-border-radius: 50%;-webkit-background-clip: padding-box;-moz-border-radius: 50%;-moz-background-clip: padding;border-radius: 50%;background-clip: padding-box;line-height: 30px;display:inline-block\"><img style=\" width:30px ; height:30px;  border-radius:50%\"src='" + DEVELOPMENT_URL + userImage + "' /><p style='margin:2px'>" + unescapeJava(userName) + "</p></div> ";
            } else {
                userImage = imageLatter(word(unescapeJava(userName)), backColor, fontColor) + "";
                //data = "<span style = \"color: #\" " + fontColor + ";background:#\" " + backColor + " \";-webkit-border-radius: 50%;-webkit-background-clip: padding-box;-moz-border-radius: 50%;-moz-background-clip: padding;border-radius: 50%;background-clip: padding-box;height: 30px;width: 30px;line-height: 30px;text-align: center;font-size: 13px;font-weight: 500;display: inline-block;\" > " + word(userName) + " </ span ><p style = \" margin:2 px;display:inline - block; \" > " + userName + " </p >";
                // data = "<span style =color: # " + fontColor + ";background: # " + backColor + ";-webkit-border-radius: 50%;-webkit-background-clip: padding-box;-moz-border-radius: 50%;-moz-background-clip: padding;border-radius: 50%;background-clip: padding-box;height: 30px;width: 30px;line-height: 30px;text-align: center;font-size: 16px;font-weight: 400;display: block;\" > " + word(userName) + " </span ><span style =\"margin:2px; display:inline - block;\">" + userName + " </span >";
                data = "<span style = 'color: #" + fontColor + ";background: #" + backColor + ";-webkit-border-radius: 50%;-webkit-background-clip: padding-box;-moz-border-radius: 50%;-moz-background-clip: padding;border-radius: 50%;background-clip: padding-box;height: 30px;width: 30px;line-height: 30px;text-align: center;font-size: 16px;font-weight: 400;display: inline-block;'>" + word(unescapeJava(userName)) + "</span> <span style = 'margin:2px; display:inline - block; color:#000000;' >" + unescapeJava(userName) + "</span>";
            }
            return data;
        } else {
            return "";
        }
    }

    public static TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(300)  // width in px
                .height(300)
                .fontSize(150)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    public static String escapeUnicodeText(String input) {

        StringBuilder b = new StringBuilder(input.length());

        java.util.Formatter f = new java.util.Formatter(b);

        for (char c : input.toCharArray()) {
            if (c < 128) {
                b.append(c);
            } else {
                f.format("\\u%04x", (int) c);
            }
        }

        return b.toString();
    }

    public static String getUnicodeString(String myString) {
        String text = "";
        try {

            byte[] utf8Bytes = myString.getBytes("UTF8");
            text = new String(utf8Bytes, "UTF8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text;
    }


    public static String unescapeJava(String escaped) {

        if (escaped.indexOf("\\u") == -1)
            return escaped;

        String processed = "";

        int position = escaped.indexOf("\\u");
        while (position != -1) {
            if (position != 0)
                processed += escaped.substring(0, position);
            String token = escaped.substring(position + 2, position + 6);
            escaped = escaped.substring(position + 6);
            processed += (char) Integer.parseInt(token, 16);
            position = escaped.indexOf("\\u");
        }
        processed += escaped;

        return processed;
    }

    public static String ReceivingBase64(String escaped) {

        byte[] data = Base64.decode(escaped, Base64.DEFAULT);
        String text = new String(data, StandardCharsets.UTF_8);

        return text;
    }

    public static String SendingBase64(String escaped) {

        byte[] data = escaped.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

        return base64;
    }

    public static String getUserImage(String userName, String userImage) {
        if (!isStringValid(userImage)) {
            if (!isStringValid(userName)) {
                String s = userName.substring(0, 1);
                userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                return DEVELOPMENT_URL + userImage;
            } else {
                return "";
            }
        } else {
            return DEVELOPMENT_URL + userImage;
        }
    }

    public static String bindTwoString(String data1, String data2, String replaceString) {
        if (isStringValid(data1)) {
            if (isStringValid(data2)) {
                return data1 + " " + data2;
            } else {
                return data1;
            }

        } else {
            return replaceString;
        }

    }

    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String bindTwoStringWithData(String data1, String data2, String bindData, String replaceString) {
        if (isStringValid(data1)) {
            if (isStringValid(data2)) {
                return data1 + "  " + bindData + "  " + data2;
            } else {
                return data1;
            }

        } else {
            return replaceString;
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static File getOutputMediaFile(String filename, Context context) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.app_name));

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + filename);
        return mediaFile;
    }

    public static Uri getLocalBitmapUri(Bitmap bmp) {
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".jpeg");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
}
