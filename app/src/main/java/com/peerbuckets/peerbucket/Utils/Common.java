package com.peerbuckets.peerbucket.Utils;

public class Common {
    public  static final String TYPE_TEXT = "1";
    public  static final String TYPE_IMAGE = "2";
    public  static final String TYPE_VIDEO = "3";
    public  static final String TYPE_GIPHY = "4";
    public  static final String TYPE_FILE = "5";
}
