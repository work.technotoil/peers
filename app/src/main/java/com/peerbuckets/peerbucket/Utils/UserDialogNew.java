package com.peerbuckets.peerbucket.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.peerbuckets.peerbucket.Activity.EditProfileActivity;
import com.peerbuckets.peerbucket.Activity.PingActivity;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserDialogNew {

    public static void show(final Context mContext, String user_id, String username, String userImage, String timezone) {


        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_other_user_profile, null);
        dialogBuilder.setContentView(dialogView);
        CircleImageView user_image = dialogView.findViewById(R.id.user_image);
        TextView tv_username = dialogView.findViewById(R.id.tv_username);
        TextView tv_time = dialogView.findViewById(R.id.tv_time);
        TextView tv_edit_profile = dialogView.findViewById(R.id.tv_edit_profile);
        tv_username.setText(Utils.unescapeJava(username));
        tv_time.setText(timezone);
        Picasso.with(mContext).load(userImage).into(user_image);

        tv_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EditProfileActivity.class);
                mContext.startActivity(intent);
            }
        });
        dialogBuilder.show();
    }

    public static void otherUserDialog(final Context mContext, final String user_id, String username, String userImage, String timezone, String backColor, String FontColor) {


        final Dialog dialogBuilder = new Dialog(mContext);

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_other_user_profile, null);
        dialogBuilder.setContentView(dialogView);
        CircleImageView user_image = dialogView.findViewById(R.id.user_image);
        TextView tv_username = dialogView.findViewById(R.id.tv_username);
        TextView tv_time = dialogView.findViewById(R.id.tv_time);
        TextView tv_have_seen = dialogView.findViewById(R.id.tv_have_seen);
        TextView tv_edit_profile = dialogView.findViewById(R.id.tv_edit_profile);
        TextView tvPing = dialogView.findViewById(R.id.tv_ping_other_user_dialog);
        View v_line = dialogView.findViewById(R.id.v_line);
        tv_username.setText(username);
        tv_time.setText(timezone);
        if (userImage != null && !userImage.equals("")) {
            Picasso.with(mContext).load(userImage).into(user_image);
        } else {
            user_image.setImageDrawable(imageLatter(Utils.word(username), backColor, FontColor));
        }


        tv_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, EditProfileActivity.class);
                mContext.startActivity(intent);
            }
        });

        tvPing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isStringValid(user_id)) {
                    Intent intent = new Intent(mContext, PingActivity.class);
                    intent.putExtra("user_id", user_id);
                    mContext.startActivity(intent);
                    dialogBuilder.dismiss();
                } else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.somethingwentwrong), Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (!user_id.equals(ShardPreferences.get(mContext, ShardPreferences.share_current_UserId_key))) {
            tv_edit_profile.setVisibility(View.GONE);
        } else {
            tvPing.setVisibility(View.GONE);
        }
        dialogBuilder.show();
    }

    private static TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(300)  // width in px
                .height(300)
                .fontSize(150)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }
}
