package com.peerbuckets.peerbucket.ApiController;

/**
 * Created by HP on 25-02-2016.
 */
public class ApiConfigs {

    /* BASE URL's*/
    //public static String BASE_URL = "http://peerbuckets.tk/WebService/";
    public static String BASE_URL = "http://dev.peerbuckets.com/WebService/";
    public static String DEVELOPMENT_URL = "http://dev.peerbuckets.com/";
    public static String DEVELOPMENT = "http://dev.peerbuckets/";


    public static String CHECK_EMAIL = "checkEmailExists";
    public static String USER_PASSWORD = "userPassword";
    public static String USER_REGISTER = "userRegister";
    public static String USER_LOGIN = "login";
    public static String INSERT_COMPANY = "updateCompany";
    public static String INSERT_TEAM_WITHOUT_DESC = "teamRegister";
    public static String INSERT_PROJECT_WITHOUT_DESC = "projectRegister";
    public static String FORGOT_PASSWORD = "forgot_password";
    public static String INVITE_COWORKER = "inviteCoworker";
    public static String HOMEAPI = "homeApi";
    public static String COMPANY_LIST = "CompanyList";
    public static String INSERT_SINGLE_TEAM = "insertTeam";
    public static String INSERT_SINGLE_PROJECT = "insertProject";
    public static String FETCH_EMPLOYEE_DETAILS = "employeeDetailDialog";
    public static String GET_EMPLOYEE_PROFILE = "getEmployeeProfile";
    public static String ADD_PEOPLE = "addPeople";
    public static String REMOVE_PEOPLE = "removePepole";
    public static String TRASH_CPT = "trash";
    public static String TRASH_DETAIL = "trashDetail";
    public static String TRASH_MODULE = "trashModule";
    public static String LATEST_ACTIVITIES = "latestActivityList";
    public static String UPLOAD_IMAGE = "updateProfileImage";
    public static String GET_MESSAGE_BOARD_DETAILS = "get_message_board";
    public static String ADD_MESSAGE_BOARD = "post_message_action";
    public static String EDIT_MESSAGE_BOARD = "message_board_update_action";
    public static String DELETE_MESSAGE_BOARD = "delete_messagboard_message";
    public static String ADD_COMMENT = "add_comment";
    public static String EDIT_COMMENT = "edit_comment";
    public static String DELETE_COMMENT = "delete_comment";
    public static String FETCH_SINGLE_MESSGAGE_BOARD = "get_single_message";
    public static String FETCH_ALL_TODO = "get_all_todo_list";
    public static String FETCH_SINGLE_TODO = "get_single_todo_detail";
    public static String ADD_TODO = "saveNewTodo";
    public static String UPDATE_TODO = "todo_update";
    public static String DELETE_TODO = "delete_todo_list";
    public static String ADD_TODO_TASK = "saveNewTodoTask";
    public static String UPDATE_TODO_TASK = "subtodo_update";
    public static String DELETE_TODO_TASK = "delete_subtodo_task";
    public static String FETCH_SINGLE_TODO_SUB_TASK = "get_single_subtodo_task_detail";
    public static String FETCH_COMMENT_LIST = "getCommentList";
    public static String MASK_SUB_TO_DO_COMPLETE = "markSubToDosCompletedOn";
    public static String GET_INVITED_USERS = "get_invited_users";
    public static String GET_USER_DETAIL_BY_ID = "SingleUserDetails";
    public static String UPDATE_PROFILE = "updateUserDetail";
    public static String CONNECTED_DEVICE = "connected_devices";
    public static String SCHEDULE_LIST_MONTH = "get_schdeule_list_by_month";
    public static String ADD_CHECK_IN = "add_checkin";
    public static String GET_CHECK_IN_LIST = "get_checkin_list";
    public static String SCHEDULE_LIST_BY_DATE = "get_schdeule_list_by_date";
    public static String DOCUMENT_LIST = "get_document_list";
    public static String UPDATE_CHECK_IN = "update_checkin";
    public static String GET_CHECK_IN_DETAIL = "get_checkin_detail";
    public static String GET_CHECK_IN_REPLY_LIST = "get_checkin_reply_list";
    public static String ADD_CHECK_IN_REPLY = "add_checkin_reply";
    public static String UPDATE_CHECK_IN_REPLY = "update_checkin_reply";
    public static String DELETE_CHECK_IN_REPLY = "delete_checkin_reply";
    public static String GET_CHECKIN_REPLY_DETAIL = "get_checkin_reply_detail";
    public static String DELETE_CHECK_IN = "delete_checkin";
    public static String ADD_NEW_SCHEDULE = "add_new_schedule";
    public static String UPLOAD_DOCUMENT = "upload_document";
    public static String GET_DOCUMENT_DETAIL = "get_single_file_detail";
    public static String CREATE_FOLDER = "create_folder";
    public static String ADD_TO_DRIVE = "add_drive";
    public static String UPDATE_SCHEDULE = "update_schedule";
    public static String GET_SCHEDULE_DETAIL = "get_schedule_detail";
    public static String DELETE_SCHEDULE = "delete_schedule";
    public static String DELETE_DOCUMENT = "delete_document";
    public static String UPDATE_DOCUMENT = "update_document";
    public static String GET_CHANNEL_ID = "getChannelId";
    public static String FETCH_EMPLOYEE_ANDROID = "employeeDetailAndroid"; // User_image contain url
    public static String ADD_REMOVE_NOTIFIED = "addNotifyPeople"; // User_image contain url
    public static String UPDATE_COMPANY_LOGO = "updateCompanyLogo"; // User_image contain url
    public static String DELETE_DEVICE = "deleteDevice"; // User_image contain url
    public static String SEND_NOTIFICATION = "sendTestPushNotification";
    public static String APPLAUSE = "addApplause";
    public static String SUBSCRIBE = "subscribeModule";
    public static String HEY_LIST = "heyList";
    public static String WORK_CAN_WAIT = "notification_settings_update";
    public static String GET_DEFAULT_NOTIFICATION_SETTING = "get_default_settings";
    public static String READ_UNREAD_NOTIFICATION = "readunreadNotification";
    public static String TUEN_ON_NOTIFICTION = "turnNotificationAction";

}
