package com.peerbuckets.peerbucket.Adapter;
import android.content.Context;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.AddEditMessageBoardActivity;
import com.peerbuckets.peerbucket.POJO.MessageTypePOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import java.util.List;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class MessageTypeAdapter extends RecyclerView.Adapter<MessageTypeAdapter.ViewHolder> implements IApiResponse {

    private Context context;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    public int globalPosition=0;

    //List to store all mMessageTypeList
    List<MessageTypePOJO> mMessageTypeList;
    String adapterType;
    String mcompanyId="", muserId="", memployeeId="", mCureentSelectedMessageId="",mCurrentuserId="";

    //Constructor of this class
    public MessageTypeAdapter(List<MessageTypePOJO> mMessageTypeList, Context context, String adaptertype){
        super();
        mApiRequest= new ApiRequest(context, (IApiResponse) this);
        //Getting all mMessageTypeList
        this.mMessageTypeList = mMessageTypeList;
        this.context = context;
        this.adapterType= adaptertype;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_message_type, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final MessageTypePOJO MessageTypeList=  mMessageTypeList.get(position);


        //Loading image from url
        holder.mdialog_message_type_icon.setTypeface(fontTF);
        //holder.mdialog_message_type_icon.setText(MessageTypeList.getMessageIconCode().replace("//","/"));
        holder.mdialog_message_type_text .setText(MessageTypeList.getName());

        holder.mdialog_message_typeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCureentSelectedMessageId=MessageTypeList.getMessageTypeId();
                if (adapterType.equals("MessageBoard")){
                    ((AddEditMessageBoardActivity)context).hideMessageTypeDialog();
                }
                if (adapterType.equals("AddEdit")){
                    ((AddEditMessageBoardActivity)context).hideMessageTypeDialog();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMessageTypeList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public String getSelectedMessageType(){
        return mCureentSelectedMessageId;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        //Views

        TextView mdialog_message_type_text ,mdialog_message_type_icon;
        LinearLayout mdialog_message_typeLL;
        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mCureentSelectedMessageId=mMessageTypeList.get(0).getMessageTypeId();
            mcompanyId=ShardPreferences.get(context,share_Company_Id_key);
            muserId=ShardPreferences.get(context,share_UserId_key);
            mCurrentuserId=ShardPreferences.get(context,share_current_UserId_key);
            memployeeId=ShardPreferences.get(context,share_employee_Id_key);
            mdialog_message_type_text = (TextView) itemView.findViewById(R.id.dialog_message_type_text);
            mdialog_message_type_icon = (TextView) itemView.findViewById(R.id.dialog_message_type_icon);
            mdialog_message_typeLL=  itemView.findViewById(R.id.dialog_message_typeLL);
            fontTF=Typeface.createFromAsset(context.getAssets(), "font/fawsmsolid.ttf");
        }
    }


}