package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.AddRemovePeopleActivity;
import com.peerbuckets.peerbucket.POJO.addRemoveResponse.AddEmployeeListNew;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.ADD_PEOPLE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.REMOVE_PEOPLE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_company_name;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

/**
 * Created by Belal on 11/9/2015.
 */
public class AddRemoveAdapter extends RecyclerView.Adapter<AddRemoveAdapter.ViewHolder> implements IApiResponse {

    String[] COLORArray = {"red", "blue", "green", "black", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    private Context context;
    private ApiRequest mApiRequest;
    public int globalPosition = 0;
    Typeface fontTF;
    String mcompanyId = "", muserId = "", memployeeId = "", maddremovetype = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "";
    String firstName = "", lastName = "", imgName = "";
    //List to store all mProjectList
    List<AddEmployeeListNew> mEmployeeList;


    //Constructor of this class

    public AddRemoveAdapter(Context mContext, ArrayList<AddEmployeeListNew> list, String projectTeamId) {
        this.mEmployeeList = list;
        this.context = mContext;
        this.mcurrentProjectTeamId = projectTeamId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_employee_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        mApiRequest = new ApiRequest(context, (IApiResponse) this);

        final AddEmployeeListNew EmployeeList = mEmployeeList.get(position);
        holder.memployee_email.setText(Utils.unescapeJava(EmployeeList.getEmployeeEmail()));

        if (EmployeeList.getEmployeeJobTitle().equals("")) {
            holder.memployee_designation.setVisibility(View.GONE);
        } else {
            holder.memployee_designation.setText(Utils.unescapeJava(EmployeeList.getEmployeeJobTitle()));
        }

        holder.memployee_name.setText(Utils.unescapeJava(EmployeeList.getEmployeeName()));

        holder.mreomoveemployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globalPosition = position;
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.remove))
                        .setMessage(context.getResources().getString(R.string.remove_user))
                        .setPositiveButton(context.getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Map<String, String> paramsReq = new HashMap<>();
                                paramsReq.put("projectUserRelationId", EmployeeList.getProjectUserRelationId());
                                mApiRequest.postRequest(BASE_URL + REMOVE_PEOPLE, REMOVE_PEOPLE, paramsReq, Request.Method.POST);
                            }
                        }).setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                alertDialog.show();
            }
        });
        holder.addEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globalPosition = position;
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.add))
                        .setMessage(context.getResources().getString(R.string.Add_user))
                        .setPositiveButton(context.getResources().getString(R.string.sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Map<String, String> paramsReq = new HashMap<>();
                                EmployeeList.setIsUserAdded("1");
                                notifyDataSetChanged();
                                paramsReq.put("projectTeamId", mcurrentProjectTeamId);
                                paramsReq.put("employeeEmail", EmployeeList.getEmployeeEmail());
                                paramsReq.put("employeeName", EmployeeList.getEmployeeName());
                                paramsReq.put("companyId", mcompanyId);
                                paramsReq.put("employeeJobTitle", EmployeeList.getEmployeeJobTitle());
                                paramsReq.put("employeeCompanyName", ShardPreferences.get(context, ShardPreferences.share_current_module));
                                paramsReq.put("type", maddremovetype);
                                paramsReq.put("userId", ShardPreferences.get(context, share_UserId_key));
                                mApiRequest.postRequest(BASE_URL + ADD_PEOPLE, ADD_PEOPLE, paramsReq, Request.Method.POST);
                                // mApiRequest.postRequestBackground(BASE_URL + ADD_PEOPLE, ADD_PEOPLE, paramsReq, Request.Method.POST);

                            }
                        }).setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                alertDialog.show();
            }
        });
        imgName = Utils.word(EmployeeList.getEmployeeName());
        if (!EmployeeList.getUserImage().equals("") && EmployeeList.getUserImage() != null) {
            Picasso.with(context).load(EmployeeList.getUserImage()).into(holder.userImage);
        } else {
            holder.userImage.setImageDrawable(imageLatter(imgName, EmployeeList.getBgColor(), EmployeeList.getFontColor()));
        }

    }


    private TextDrawable imageLatter(String name, String backColor, String FontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(30)
                .textColor(Color.parseColor("#" + FontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return mEmployeeList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if (tag_json_obj.equals(REMOVE_PEOPLE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    mEmployeeList.get(globalPosition).setIsUserAdded("0");
                    notifyDataSetChanged();
                    ((Activity) context).finish();
                    //   Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    //    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tag_json_obj.equals(ADD_PEOPLE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    // Intent intent = new Intent(context, HomeActivity.class);
                    //  context.startActivity(intent);
                    mEmployeeList.get(globalPosition).setIsUserAdded("1");
                    notifyDataSetChanged();
                    //     Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    //   Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public TextView memployee_email, memployee_designation, memployee_name, mreomoveemployee, addEmployee;
        ImageView userImage;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            maddremovetype = ShardPreferences.get(context, share_addd_remove_current_type);
            mcurrentCompanyName = ShardPreferences.get(context, share_current_company_name);
            memployee_email = (TextView) itemView.findViewById(R.id.employee_email);
            memployee_designation = (TextView) itemView.findViewById(R.id.employee_designation);
            addEmployee = itemView.findViewById(R.id.addEmployee);
            memployee_name = itemView.findViewById(R.id.employee_name);
            mreomoveemployee = itemView.findViewById(R.id.reomoveemployee);
            userImage = itemView.findViewById(R.id.userImage);

        }
    }


}