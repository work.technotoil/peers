package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

public class InvitedUserAdapter extends RecyclerView.Adapter<InvitedUserAdapter.MyViewHolder> {

    private Context context;
    private List<User> list;

    public InvitedUserAdapter(List<User> mList, Context mContext) {
        context = mContext;
        list = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_invited_users_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tvName.setText(Utils.checkStringWithHash(Utils.unescapeJava(list.get(position).getEmployeeName())));
        if (Utils.isStringValid(list.get(position).getUserImage())) {
            Picasso.with(context).load(DEVELOPMENT_URL + list.get(position).getUserImage()).into(holder.imgUser);
        } else {
            holder.imgUser.setImageDrawable(imageLatter(Utils.word(list.get(position).getEmployeeName()), list.get(position).getBgColor(), list.get(position).getFontColor()));
            /*if (Utils.isStringValid(list.get(position).getEmployeeName())) {

                //String s = list.get(position).getEmployeeName().substring(0, 1);
                //Picasso.with(context).load(DEVELOPMENT_URL + "image/PreviewImages/" + s.toLowerCase() + ".png").into(holder.imgUser);
            }*/
        }

        holder.imgChecked.setVisibility(list.get(position).getSelected() ? View.VISIBLE : View.INVISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getSelected()) {
                    list.get(position).setSelected(false);
                    holder.imgChecked.setVisibility(View.INVISIBLE);
                } else {
                    list.get(position).setSelected(true);
                    holder.imgChecked.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public List<User> getSelectedUsers() {
        List<User> selectedUserList = new ArrayList<>();

        for (User selectedUser : list) {
            if (selectedUser.getSelected()) {
                selectedUserList.add(selectedUser);
            }
        }

        return selectedUserList;
    }

    public void clearAllSelections() {
        if (list != null && list.size() > 0) {
            for (User user : list) {
                user.setSelected(false);
            }
            notifyDataSetChanged();
        }
    }

    public void updateSelectedUser(List<User> selectedUserList) {
        if (selectedUserList != null && selectedUserList.size() > 0 && list != null && list.size() > 0) {
            for (User selectedUser : selectedUserList) {
                for (User originalList : list) {
                    if (selectedUser.getUserId().equals(originalList.getUserId())) {
                        originalList.setSelected(true);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        CircleImageView imgUser;
        ImageView imgChecked;

        MyViewHolder(View v) {
            super(v);
            tvName = v.findViewById(R.id.tv_name_bottom_sheet);
            imgUser = v.findViewById(R.id.img_user_bottom_sheet);
            imgChecked = v.findViewById(R.id.img_checked_bottom_sheet);
        }
    }
}