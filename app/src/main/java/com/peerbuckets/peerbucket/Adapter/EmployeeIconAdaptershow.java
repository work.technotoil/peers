package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Hpojo.UserListing;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.UserDialogNew;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

/**
 * Created by Belal on 11/9/2015.
 */
public class EmployeeIconAdaptershow extends RecyclerView.Adapter<EmployeeIconAdaptershow.ViewHolder> implements IApiResponse {

    private Context ctx;
    private ApiRequest mApiRequest;

    int value;

    Typeface fontTF;
    String mcompanyId = "", muserId = "", memployeeId = "", maddremovetype = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", firstName = "", lastName = "";
    String[] COLORArray = {"black", "blue", "green", "black", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    public int selected = 0;
    int extra = 0;
    int lSize=0;

    ArrayList<UserListing> listSize;
    //List to store all mProjectList
    /*List<UserListing> mEmployeeList;*/

    //Constructor of this class
    public EmployeeIconAdaptershow(Context context, ArrayList<UserListing> mEmployeeList, int i) {
        super();
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        ctx = context;
        listSize = mEmployeeList;
        lSize=mEmployeeList.size();
        value = i;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_employee_icons, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final UserListing EmployeeList = listSize.get(position);
        String firstLetter = EmployeeList.getEmployeeName();
        holder.tv_memberName.setText(listSize.get(position).getEmployeeName());
        if (value == 0) {
            holder.tv_memberName.setVisibility(View.VISIBLE);
        } else {
            holder.tv_memberName.setVisibility(View.GONE);
        }

        if (position >= 3) {
            firstLetter = "+" + (lSize - 3);
            String mColor = COLORArray[0];
            holder.drawable = TextDrawable.builder().buildRound(firstLetter.toUpperCase(), Color.parseColor(mColor));
            holder.memployee_icon_iv.setImageDrawable(imageLatter(firstLetter,"000000","ffffff"));

        } else {
            if (!EmployeeList.getUserImage().equals("") && EmployeeList.getUserImage() != null) {
                selected = holder.random.nextInt(COLORArray.length);
                Picasso.with(ctx).load(EmployeeList.getUserImage()).into(holder.memployee_icon_iv);
            } else {
                holder.memployee_icon_iv.setImageDrawable(imageLatter(Utils.word(EmployeeList.getEmployeeName()), EmployeeList.getBgColor(), EmployeeList.getFontColor()));
                selected = holder.random.nextInt(COLORArray.length);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDialogNew.otherUserDialog(ctx, listSize.get(position).getUserId(), Utils.unescapeJava(listSize.get(position).getEmployeeName()), listSize.get(position).getUserImage(), "GMT", listSize.get(position).getBgColor(), listSize.get(position).getFontColor());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listSize.size() > 3) {
            return 4;
        } else {
            return listSize.size();
        }
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public ImageView memployee_icon_iv;
        TextDrawable drawable;
        TextView tv_memberName;
        Random random;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            random = new Random();
            memployee_icon_iv = itemView.findViewById(R.id.employee_icon_iv);
            tv_memberName = itemView.findViewById(R.id.tv_memberName);

        }
    }

    private TextDrawable imageLatter(String name, String backColor, String FontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + FontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    public List<EmployeeDetalPOJO> getSelectedUserList(List<EmployeeDetalPOJO> mList) {
        List<EmployeeDetalPOJO> selectedUsersList = new ArrayList<>();
        if (mList != null && mList.size() > 0) {
            if (mList != null && mList.size() > 0) {
                for (EmployeeDetalPOJO userId : mList) {
                    if (userId.getSelected()) {
                        if (userId.getUserImage().equals("")) {
                            String s = userId.getEmployeeName().substring(0, 1);
                            String userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                            userId.setUserImage(DEVELOPMENT_URL + "" + userImage);
                        } else {
                            userId.setUserImage(DEVELOPMENT_URL + "" + userId.getEmployeeName());
                        }
                        selectedUsersList.add(userId);
                    }
                }
                return selectedUsersList;
            } else {
                return selectedUsersList;
            }
        }
        return selectedUsersList;
    }
    public void updateSelectedUser(List<UserListing> selectedUserList) {
        if (listSize != null) {
            listSize.addAll(selectedUserList);
            notifyDataSetChanged();
        }
    }
}