package com.peerbuckets.peerbucket.Adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchDialogAdapter extends RecyclerView.Adapter<SearchDialogAdapter.SearchDialogHolder> {
    @NonNull
    @Override
    public SearchDialogAdapter.SearchDialogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchDialogAdapter.SearchDialogHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class SearchDialogHolder extends RecyclerView.ViewHolder {

        public SearchDialogHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
