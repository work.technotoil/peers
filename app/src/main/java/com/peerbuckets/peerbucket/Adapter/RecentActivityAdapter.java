package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.POJO.MeActivitiListResponse.Activity;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecentActivityAdapter extends RecyclerView.Adapter<RecentActivityAdapter.RecentActivityHolder> {
    private Context context;
    private ArrayList<Activity> mList;

    public RecentActivityAdapter(Context ctx, ArrayList<Activity> list) {
        mList = list;
        context = ctx;
    }

    @NonNull
    @Override
    public RecentActivityAdapter.RecentActivityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_activity_screen, parent, false);
        return new RecentActivityHolder(v);
    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(30)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onBindViewHolder(@NonNull RecentActivityAdapter.RecentActivityHolder holder, int position) {
        holder.activity_title.setText(Utils.unescapeJava(mList.get(position).getProjectTeamName().substring(0, 1).toUpperCase() + mList.get(position).getProjectTeamName().substring(1)));
        holder.activity_time.setText(mList.get(position).getDate() + " " + mList.get(position).getTime());
        if (mList.get(position).getUserImage() != null && !mList.get(position).getUserImage().equals("")) {
            Picasso.with(context).load(mList.get(position).getUserImage()).into(holder.imgUser);
        } else {
            holder.imgUser.setImageDrawable(imageLatter(Utils.word(mList.get(position).getEmployeeName()), mList.get(position).getBgColor(), mList.get(position).getFontColor()));
        }
        showStickyHeaderDate(position, holder.activity_date);

        String title = "", name = "";
        name = Utils.unescapeJava(mList.get(position).getEmployeeName());
        if (mList.get(position).getProjectTeamType().equals("test_hq") ||
                mList.get(position).getProjectTeamType().equals("team") ||
                mList.get(position).getProjectTeamType().equals("project") ||
                mList.get(position).getProjectTeamType().equals("company") ||
                mList.get(position).getProjectTeamType().equals("checkin")) {

            title = Utils.unescapeJava(mList.get(position).getEmployeeName()) + context.getResources().getString(R.string.started_a_new);

            if (mList.get(position).getProjectTeamType().equals("test_hq")) {
                holder.activity_text.setText(title + " company called");
            } else {
                holder.activity_text.setText(title + (Utils.unescapeJava(mList.get(position).getProjectTeamType())) + context.getResources().getString(R.string.called));
            }
        } else if (mList.get(position).getProjectTeamType().equals("timezone")) {
            holder.activity_text.setText(name + context.getResources().getString(R.string.change_our_timezone));

        } else if (mList.get(position).getProjectTeamType().equals("message")) {
            holder.activity_text.setText(name + context.getResources().getString(R.string.post_a_new_message));

        } else if (mList.get(position).getProjectTeamType().equals("to-dos")) {
            holder.activity_text.setText(name + " " + Utils.unescapeJava(mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("schedule")) {
            holder.activity_text.setText(name + " " + Utils.unescapeJava(mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("checkin")) {
            holder.activity_text.setText(name + " " + Utils.unescapeJava(mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("comment") || mList.get(position).getProjectTeamType().equals("checkin_reply")) {
            holder.activity_text.setText(name + " " + Utils.unescapeJava(mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("sub-todo")) {
            holder.activity_text.setText(name + " " + Utils.unescapeJava(mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("sub-to-do-mark-as-done")) {
            holder.activity_text.setText(mList.get(position).getTitle());
        } else {
            holder.activity_text.setText(name + " " + Utils.unescapeJava(mList.get(position).getDescription()));
        }
        if (mList.get(position).getProjectTeamType().equals("checkin")) {
            holder.activity_text.setVisibility(View.VISIBLE);
            holder.activity_content.setVisibility(View.GONE);
        } else {
            holder.activity_text.setVisibility(View.VISIBLE);
            holder.activity_content.setVisibility(View.VISIBLE);

            String newString = Utils.unescapeJava(mList.get(position).getDescription());

            Spanned spanned = Html.fromHtml(newString);
            char[] chars = new char[spanned.length()];
            TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
            String plainText = new String(chars);

            newString = plainText.replace("\"", "'");

            Utils.setDescription(newString, holder.activity_content, context);

            holder.activity_content.loadData("<style>img{display: inline;height: auto;max-width: 100%;}</style>" + newString, "text/html; charset=utf-8", null);

        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class RecentActivityHolder extends RecyclerView.ViewHolder {
        TextView activity_date, activity_text, activity_time, activity_title;
        WebView activity_content;
        ImageView imgUser;

        public RecentActivityHolder(@NonNull View itemView) {
            super(itemView);
            activity_text = itemView.findViewById(R.id.activity_text);
            activity_time = itemView.findViewById(R.id.activity_time);
            activity_title = itemView.findViewById(R.id.activity_title);
            activity_date = itemView.findViewById(R.id.activity_date);
            activity_content = itemView.findViewById(R.id.activity_content);
            imgUser = itemView.findViewById(R.id.imgUser);
            activity_content.getSettings().setJavaScriptEnabled(true);
            activity_content.setLayerType(WebView.LAYER_TYPE_NONE, null);
        }
    }

    private void showStickyHeaderDate(int position, TextView tvLabel) {
        if (position > 0) {
            String cIsRead = mList.get(position).getDate();
            String previouscIsRead = mList.get(position - 1).getDate();
            if (cIsRead.equalsIgnoreCase(previouscIsRead)) {
                tvLabel.setVisibility(View.GONE);
            } else {
                tvLabel.setText(mList.get(position).getDate());
                tvLabel.setVisibility(View.VISIBLE);
            }
        } else {
            boolean isNewLabelDispay = false;
            if (mList != null && mList.size() > 0) {
                for (int i = 0; i < mList.size(); i++) {
                    if (mList.get(i).getDate().equals("")) {
                        isNewLabelDispay = true;
                        break;
                    }
                }
            }
            tvLabel.setVisibility(View.VISIBLE);
            if (isNewLabelDispay) {
                tvLabel.setText(mList.get(position).getDate());
            } else {
                tvLabel.setText(mList.get(position).getDate());
            }
        }
    }
}
