package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.CreateTeamActivity;
import com.peerbuckets.peerbucket.Activity.HomeActivity;
import com.peerbuckets.peerbucket.Hpojo.HomeModule;
import com.peerbuckets.peerbucket.Hpojo.ModuleList;
import com.peerbuckets.peerbucket.POJO.AddModulePojo;
import com.peerbuckets.peerbucket.POJO.deleteTrash.TrashDeleteResponse;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeAdapterHolder> implements Filterable {

    private Context ctx;
    private ArrayList<ModuleList> mList;
    private ArrayList<ModuleList> mDisplayedValues;

    IteamShowAdapter iteamShowAdapter;
    private EditText et_newModule;
    int pos = -1;
    PopupMenu popup;

    public HomeAdapter(Context context, ArrayList<ModuleList> list) {
        ctx = context;
        mList = list;
        mDisplayedValues = list;
    }

    @NonNull
    @Override
    public HomeAdapter.HomeAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.latout_home_page, parent, false);
        return new HomeAdapterHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeAdapter.HomeAdapterHolder holder, final int position) {
        holder.tv_teamProjectName.setText(Utils.unescapeJava(mList.get(position).getModuleName()));
        module(mList.get(position).getModuleList(), holder.rv_listAddProjrct);
        holder.tv_project_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, CreateTeamActivity.class);
                intent.putExtra("id", mList.get(position).getModuleIdPK());
                intent.putExtra("moduleName", mList.get(position).getModuleName());
                ((HomeActivity) ctx).startActivityForResult(intent, 102);
            }
        });
        holder.tv_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                showPopup(view);
            }
        });
        if (mList.get(position).getModuleList().size() == 0) {
            holder.tv_emptyHint.setVisibility(View.VISIBLE);
            holder.tv_emptyHint.setText(ctx.getResources().getString(R.string.There_s_no_cards_for) + " " + mList.get(position).getModuleName());
        } else {
            holder.tv_emptyHint.setVisibility(View.GONE);
        }


    }

    public void refreshHomeScreen() {
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mList = (ArrayList<ModuleList>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ModuleList> FilteredArrList = new ArrayList<ModuleList>();

                if (mDisplayedValues == null) {
                    mDisplayedValues = new ArrayList<ModuleList>(mList); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mDisplayedValues.size();
                    results.values = mDisplayedValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mDisplayedValues.size(); i++) {
                        String data = mDisplayedValues.get(i).getModuleName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            ModuleList mModel = new ModuleList();
                            mModel.setModuleName(data);
                            mModel.setSelected(mDisplayedValues.get(i).getSelected());
                            mModel.setModuleIdPK(mDisplayedValues.get(i).getModuleIdPK());
                            mModel.setModuleList(mDisplayedValues.get(i).getModuleList());
                            FilteredArrList.add(mModel);

                        }
                    }

                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public void check() {
        notifyDataSetChanged();
    }

    public class HomeAdapterHolder extends RecyclerView.ViewHolder {
        TextView tv_teamProjectName, tv_icon, cv_icon_vert, tv_project_name, tv_emptyHint;
        RecyclerView rv_listAddProjrct;
        CardView cv_icon;
        RecyclerView.LayoutManager rv_listAddProjrctLayoutManager;

        public HomeAdapterHolder(@NonNull View itemView) {
            super(itemView);


            tv_icon = itemView.findViewById(R.id.tv_icon);
            tv_emptyHint = itemView.findViewById(R.id.tv_emptyHint);

            cv_icon_vert = itemView.findViewById(R.id.cv_icon_vert);
            cv_icon = itemView.findViewById(R.id.cv_icon);


            rv_listAddProjrct = itemView.findViewById(R.id.rv_listAddProjrct);
            tv_teamProjectName = itemView.findViewById(R.id.tv_teamProjectName);

            tv_project_name = itemView.findViewById(R.id.tv_project_name);
            rv_listAddProjrct.setHasFixedSize(true);
            rv_listAddProjrctLayoutManager = new GridLayoutManager(ctx, 2);
            rv_listAddProjrct.setLayoutManager(rv_listAddProjrctLayoutManager);
        }
    }

    private void module(List<HomeModule> list, RecyclerView rv_list) {
        if (list != null && list.size() > 0) {
            iteamShowAdapter = new IteamShowAdapter(ctx, (ArrayList<HomeModule>) list);
            rv_list.setAdapter(iteamShowAdapter);
        } else {
            list = new ArrayList<>();
            iteamShowAdapter = new IteamShowAdapter(ctx, (ArrayList<HomeModule>) list);
            rv_list.setAdapter(iteamShowAdapter);
        }
    }

    public void showPopup(View v) {
        popup = new PopupMenu(ctx, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.add_module_popup, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit:
                        editModule();
                        return true;
                    case R.id.trash:
                        DeleteModule();
                        return true;
                    case R.id.add_module:
                        Intent intent = new Intent(ctx, CreateTeamActivity.class);
                        intent.putExtra("id", mList.get(pos).getModuleIdPK());
                        intent.putExtra("moduleName", mList.get(pos).getModuleName());
                        ((HomeActivity) ctx).startActivityForResult(intent, 102);
                        return true;
                    default:

                        return false;
                }
            }
        });
    }

    private void DeleteModule() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(ctx, share_current_UserId_key));
        map.put("module_id", mList.get(pos).getModuleIdPK());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TrashDeleteResponse> resultCall = apiInterface.callSetting(map);
        resultCall.enqueue(new Callback<TrashDeleteResponse>() {

            @Override
            public void onResponse(Call<TrashDeleteResponse> call, Response<TrashDeleteResponse> response) {

                if (response.body().getStatus()) {
                    mList.remove(pos);
                    notifyDataSetChanged();
                } else {
                }
            }

            @Override
            public void onFailure(Call<TrashDeleteResponse> call, Throwable t) {
            }
        });
    }

    private void editModule() {
        AlertDialog.Builder ModuleDialogBuilder = new AlertDialog.Builder(ctx);
        View ModuleView = LayoutInflater.from(ctx).inflate(R.layout.dialog_add_new_module, null);
        ModuleDialogBuilder.setView(ModuleView);

        ImageView imgClose = ModuleView.findViewById(R.id.imgClose);
        TextView tv_addModuleDialog = ModuleView.findViewById(R.id.tv_addModuleDialog);
        et_newModule = ModuleView.findViewById(R.id.et_newModule);
        Button btnClose = ModuleView.findViewById(R.id.btnClose);
        Button btnAdd = ModuleView.findViewById(R.id.btnAdd);
        et_newModule.setText(mList.get(pos).getModuleName());
        final AlertDialog addDialog = ModuleDialogBuilder.create();
        addDialog.show();
        tv_addModuleDialog.setText(ctx.getResources().getString(R.string.Update_module));
        btnAdd.setText(ctx.getResources().getString(R.string.update));
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDialog.dismiss();
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDialog.dismiss();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", ShardPreferences.get(ctx, share_current_UserId_key));
                map.put("company_id", ShardPreferences.get(ctx, ShardPreferences.key_company_Id));
                map.put("title", et_newModule.getText().toString());
                map.put("module_id", mList.get(pos).getModuleIdPK());

                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddModulePojo> resultCall = apiInterface.CallCreateModule(map);
                resultCall.enqueue(new Callback<AddModulePojo>() {

                    @Override
                    public void onResponse(Call<AddModulePojo> call, Response<AddModulePojo> response) {

                        if (response.body().getStatus().equals("true")) {
                            mList.get(pos).setModuleName(et_newModule.getText().toString());
                            notifyDataSetChanged();
                            addDialog.dismiss();
                        } else {
                        }
                    }

                    @Override
                    public void onFailure(Call<AddModulePojo> call, Throwable t) {
                    }
                });
            }
        });
    }
}

