package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.message.ExpandMessageBoardActivity;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.POJO.MessageList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> implements IApiResponse {


    private Context context;
    Typeface fontTF;
    ApiRequest mApiRequest;
    public int globalPosition = 0;
    //List to store all mMessageTypeList
    List<MessageList> mMessageList;
    String mcompanyId = "", muserId = "", memployeeId = "", mtimezone = "",
            mcurrentProjectTeamId = "", mcurrentCompanyName = "",
            maddremovetype = "test_hq", mCurrentuserId = "", firstName = "", lastName = "";

    //Constructor of this class
    public MessageListAdapter(List<MessageList> mMessageTypeList, Context context) {
        super();
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        //Getting all mMessageTypeList
        this.mMessageList = mMessageTypeList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_message_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final MessageList mmMessageListData = mMessageList.get(position);
        String titles = Utils.unescapeJava(mmMessageListData.getMessageTitle());
        holder.title.setText(Utils.unescapeJava(titles));
        holder.username.setText(Utils.unescapeJava(mmMessageListData.getEmployeeName()) + " "+context.getResources().getString(R.string.at)+" " + mmMessageListData.getDate());

        if (mmMessageListData.getUserImage() != null && !mmMessageListData.getUserImage().equals("")) {
            Picasso.with(context).load(DEVELOPMENT_URL + mmMessageListData.getUserImage()).into(holder.image_view);
        } else {
            holder.image_view.setImageDrawable(imageLatter(Utils.word(mmMessageListData.getEmployeeName()), mmMessageListData.getBgColor(), mmMessageListData.getFontColor()));
            /*String s = mmMessageListData.getEmployeeName().substring(0, 1);
            Picasso.with(context).load(DEVELOPMENT_URL + "image/PreviewImages/" + s.toLowerCase() + ".png").into(holder.image_view); */
        }

        holder.image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mmMessageListData.getUserImage() != null && !mmMessageListData.getUserImage().equals("")) {
                    Utils.showNameDialog(context, DEVELOPMENT_URL + mmMessageListData.getUserImage(), mMessageList.get(position).getEmployeeName(), mMessageList.get(position).getEmail(), mMessageList.get(position).getBgColor(), mMessageList.get(position).getFontColor());
                } else {
                    //  String s = mmMessageListData.getEmployeeName().substring(0, 1);
                    Utils.showNameDialog(context, DEVELOPMENT_URL + mmMessageListData.getUserImage(), mMessageList.get(position).getEmployeeName(), mMessageList.get(position).getEmail(), mMessageList.get(position).getBgColor(), mMessageList.get(position).getFontColor());

                }
            }
        });

        if (mmMessageListData.getTotalMessages().equals("0")) {
            holder.totalCount.setVisibility(View.GONE);
        } else {
            holder.totalCount.setVisibility(View.VISIBLE);
            holder.totalCount.setText(mmMessageListData.getTotalMessages());
        }

        holder.mmessage_list_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExpandMessageBoardActivity.class);
                intent.putExtra("message_id", mmMessageListData.getMessageId());
                intent.putExtra("message_title", mmMessageListData.getMessageTitle());
                ((Activity_MessageBoard) context).startActivityForResult(intent, Activity_MessageBoard.REQ_CODE_EXPAND_MESSAGE);
            }
        });


    }

    private TextDrawable imageLatter(String name, String backColor, String FontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(30)
                .textColor(Color.parseColor("#" + FontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public TextView title, username, totalCount;
        CircleImageView image_view;
        LinearLayout mmessage_list_ll;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);
            title = (TextView) itemView.findViewById(R.id.title);
            username = (TextView) itemView.findViewById(R.id.username);
            totalCount = (TextView) itemView.findViewById(R.id.totalCount);
            image_view = itemView.findViewById(R.id.image_view);
            mmessage_list_ll = itemView.findViewById(R.id.mmessage_list_ll);
            fontTF = Typeface.createFromAsset(context.getAssets(), "font/fawsmsolid.ttf");
        }
    }

    public void removeMessageAndUpdateComment(Boolean isDelete, String messageId, String commentCount,String title) {
        if (messageId != null && !messageId.equals("")) {
            for (MessageList list : mMessageList) {
                if (list.getMessageId().equals(messageId)) {
                    if (isDelete) {
                        mMessageList.remove(list);
                    } else {
                        list.setTotalMessages(commentCount);
                        list.setMessageTitle(title);
                    }
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    public void setNotifyData(List<MessageList> mList) {
        mMessageList.addAll(mList);
        notifyDataSetChanged();
    }
}