package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.UserDialog;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

/**
 * Created by Belal on 11/9/2015.
 */
public class EmployeeIconAdapter extends RecyclerView.Adapter<EmployeeIconAdapter.ViewHolder> implements IApiResponse {

    private Context context;
    private ApiRequest mApiRequest;
    Typeface fontTF;
    String mcompanyId = "", muserId = "", memployeeId = "",
            lastName = "", firstName = "",
            maddremovetype = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "";
    String[] COLORArray = {"black", "blue", "green", "black", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    public int selected = 0;
    int extra = 0;
    int listSize = 0;
    //List to store all mProjectList
    List<EmployeeDetalPOJO> mEmployeeList;

    public EmployeeIconAdapter(List<EmployeeDetalPOJO> mEmployeeList, Context context) {
        super();
        //Getting all mProjectList
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        this.mEmployeeList = mEmployeeList;
        this.context = context;
        listSize = mEmployeeList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_employee_icons, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final EmployeeDetalPOJO EmployeeList = mEmployeeList.get(position);
        holder.tv_memberName.setText(mEmployeeList.get(position).getEmployeeName());
        //  holder.tv_memberName.setVisibility(View.VISIBLE);
        selected = holder.random.nextInt(COLORArray.length);

//        if (EmployeeList.getUserImage() != null && !EmployeeList.getUserImage().equals("")) {
//            Picasso.with(context).load(DEVELOPMENT_URL + EmployeeList.getUserImage()).into(holder.memployee_icon_iv);
//        } else {
//            holder.memployee_icon_iv.setImageDrawable(imageLatter(Utils.word(Utils.unescapeJava(EmployeeList.getEmployeeName())), EmployeeList.getBgColor(), EmployeeList.getFontColor()));
//        }

        if (!EmployeeList.getUserImage().equals("") && EmployeeList.getUserImage() != null) {
            Picasso.with(context).load(EmployeeList.getUserImage()).into(holder.employee_icon_iv);
        } else {
            holder.employee_icon_iv.setImageDrawable(imageLatter(Utils.word(EmployeeList.getEmployeeName()), EmployeeList.getBgColor(), EmployeeList.getFontColor()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDialog.otherUserDialogs(context, mEmployeeList.get(position).getUserId(), mEmployeeList.get(position).getEmployeeName(), mEmployeeList.get(position).getUserImage(), "GMT", mEmployeeList.get(position).getBgColor(), mEmployeeList.get(position).getFontColor());
            }
        });
    }

    public TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(75)  // width in px
                .height(75)
                .fontSize(30)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public int getItemCount() {
        if (mEmployeeList.size() > 3) {
            return 4;
        } else {
            return mEmployeeList.size();
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public ImageView memployee_icon_iv;
        public ImageView employee_icon_iv;
        TextDrawable drawable;
        TextView tv_memberName;
        Random random;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            random = new Random();
            memployee_icon_iv = itemView.findViewById(R.id.employee_icon_iv);
            employee_icon_iv = itemView.findViewById(R.id.employee_icon_iv);
            tv_memberName = itemView.findViewById(R.id.tv_memberName);
        }
    }

    public List<EmployeeDetalPOJO> getSelectedUserList(List<EmployeeDetalPOJO> mList) {
        List<EmployeeDetalPOJO> selectedUsersList = new ArrayList<>();
        if (mList != null && mList.size() > 0) {
            if (mList != null && mList.size() > 0) {
                for (EmployeeDetalPOJO userId : mList) {
                    if (userId.getSelected()) {
                        if (userId.getUserImage().equals("")) {
                            String s = userId.getEmployeeName().substring(0, 1);
                            String userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                            userId.setUserImage(DEVELOPMENT_URL + "" + userImage);
                        } else {
                            userId.setUserImage(DEVELOPMENT_URL + "" + userId.getEmployeeName());
                        }
                        selectedUsersList.add(userId);
                    }
                }

                return selectedUsersList;
            } else {
                return selectedUsersList;
            }
        }
        return selectedUsersList;
    }

    public void updateSelectedUser(List<EmployeeDetalPOJO> selectedUserList) {
        if (mEmployeeList != null) {
            mEmployeeList.addAll(selectedUserList);
            notifyDataSetChanged();
        }
    }
}