package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.Activity.PingActivity;
import com.peerbuckets.peerbucket.Hpojo.HomeModule;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.view_holder.ChatSenderTextViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

public class ProiortyEmployeeIconAdapter extends RecyclerView.Adapter<ProiortyEmployeeIconAdapter.ProiortyEmployeeIconViewHolder> {
    private Context ctx;
    private ArrayList<User> mList;
    int value = 0;
    String firstName = "", lastName = "", imgName = "";

    public ProiortyEmployeeIconAdapter(Context context, ArrayList<User> teamMembersList, int i) {
        ctx = context;
        mList = teamMembersList;
        value = i;
    }

    @NonNull
    @Override
    public ProiortyEmployeeIconAdapter.ProiortyEmployeeIconViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_chating_user_list, parent, false);
        return new ProiortyEmployeeIconViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProiortyEmployeeIconAdapter.ProiortyEmployeeIconViewHolder holder, int position) {

        if (!mList.get(position).getUserImage().equals("")) {
            Picasso.with(ctx).load(DEVELOPMENT_URL + mList.get(position).getUserImage()).into(holder.employee_icon_iv);
        } else {
            imgName = Utils.word(mList.get(position).getEmployeeName());
            //  imageLatter(imgName);
            holder.employee_icon_iv.setImageDrawable(imageLatter(imgName, mList.get(position).getBgColor(), mList.get(position).getFontColor()));
            // Picasso.with(ctx).load(R.drawable.contact_support1).into(holder.employee_icon_iv);
        }

        if (value == 1) {
            holder.tv_memberName.setVisibility(View.VISIBLE);
            holder.tv_memberName.setText(Utils.unescapeJava(mList.get(position).getEmployeeName()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, PingActivity.class);
                intent.putExtra("user_id", mList.get(position).getEmployeeId());
                ctx.startActivity(intent);
            }
        });
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(40)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ProiortyEmployeeIconViewHolder extends RecyclerView.ViewHolder {

        ImageView employee_icon_iv;
        TextView tv_memberName;

        public ProiortyEmployeeIconViewHolder(@NonNull View itemView) {
            super(itemView);
            employee_icon_iv = itemView.findViewById(R.id.employee_icon_iv);
            tv_memberName = itemView.findViewById(R.id.tv_memberName);
        }
    }
}