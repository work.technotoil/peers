package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.peerbuckets.peerbucket.POJO.Day;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class CheckInDayAdapter extends RecyclerView.Adapter<CheckInDayAdapter.MyViewHolder> {
    Context context;
    List<Day> list;
    private int TYPE;
    private Boolean isMultipleSelection;

    public CheckInDayAdapter(Context ctx, List<Day> mList, int type, Boolean isMultiple) {
        context = ctx;
        list = mList;
        TYPE = type;
        isMultipleSelection = isMultiple;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_check_in_day_list, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (list.get(position).getSelected()) {
            holder.tvDay.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_tv_day_selected));
        } else {
            holder.tvDay.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_tv_day_unselected));
        }
        holder.tvDay.setText(list.get(position).getDayName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMultipleSelection) {
                    list.get(position).setSelected(!list.get(position).getSelected());
                } else {
                    for (Day day : list) {
                        day.setSelected(false);
                    }
                    list.get(position).setSelected(true);
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDay;

        MyViewHolder(View v) {
            super(v);
            tvDay = v.findViewById(R.id.tv_day_check_id_day_list);
        }
    }


    public JSONArray getSelectedArray() {
        JSONArray daysArray = new JSONArray();
        if (list != null && list.size() > 0) {
            List<String> selectedDays = new ArrayList<>();
            for (Day days : list) {
                if (days.getSelected()) {
                    selectedDays.add(days.getDayName().toLowerCase());
                }
            }
            if (selectedDays != null && selectedDays.size() > 0) {
                daysArray = new JSONArray(selectedDays);
            }
        }
        return daysArray;
    }

    public void updateDaysList(String selectedDay) {
        for (Day originalList : list) {
            if (selectedDay.contains(originalList.getDayName().toLowerCase())) {
                originalList.setSelected(true);
            }
        }
        notifyDataSetChanged();
    }
}
