package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.TimezoneListActivity;
import com.peerbuckets.peerbucket.POJO.TimezoneListModel;
import com.peerbuckets.peerbucket.R;
import java.util.List;


public class TimezoneAdapter extends RecyclerView.Adapter<TimezoneAdapter.ViewHolder> {

    private Context context;

    List<TimezoneListModel> mTimezoneListModel;

    //Constructor of this class
    public TimezoneAdapter(List<TimezoneListModel> mTeamList, Context context) {
        super();
        //Getting all mTeamList
        this.mTimezoneListModel = mTeamList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_timezone, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TimezoneListModel mList = mTimezoneListModel.get(position);
        holder.tv_timezone.setText(mList.getTimezoneName());
        holder.tv_timezone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = ((TimezoneListActivity)context).getIntent();
                i.putExtra("value",mList.getTimezoneValue());
                i.putExtra("timezoneName",mList.getTimezoneName());
                ((TimezoneListActivity)context).setResult(Activity.RESULT_OK,i);
                ((TimezoneListActivity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTimezoneListModel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_timezone;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_timezone = (TextView) itemView.findViewById(R.id.tv_timezone);
        }
    }


}