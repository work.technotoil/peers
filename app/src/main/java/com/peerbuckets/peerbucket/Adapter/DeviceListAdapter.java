package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.TimezoneListActivity;
import com.peerbuckets.peerbucket.Activity.TrashActivity;
import com.peerbuckets.peerbucket.POJO.DeviceDatum;
import com.peerbuckets.peerbucket.POJO.TimezoneListModel;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_DEVICE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.SEND_NOTIFICATION;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.TRASH_CPT;


public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.ViewHolder> implements IApiResponse {

    private Context context;

    List<DeviceDatum> mDeviceDatumListModel;
    private ApiRequest mApiRequest;
    public int globalPosition=0;

    //Constructor of this class
    public DeviceListAdapter(List<DeviceDatum> mTeamList, Context context) {
        super();
        mApiRequest= new ApiRequest(context, (IApiResponse) this);
        //Getting all mTeamList
        this.mDeviceDatumListModel = mTeamList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_reigister_device, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final DeviceDatum mList = mDeviceDatumListModel.get(position);

        if(mList.getType().equalsIgnoreCase("iOS")){
            Picasso.with(context).load(DEVELOPMENT_URL+"uploads/apple-logo.jpg").into(holder.imgLogo);
        }else{
            Picasso.with(context).load(DEVELOPMENT_URL+"uploads/team-work1.png").into(holder.imgLogo);
        }
        holder.device_name.setText(mList.getDeviceName());
        holder.r_date.setText(context.getResources().getString(R.string.Registered_on)+Utils.parseDateChange(mList.getCreatedDate()));
        holder.device_id.setText(context.getResources().getString(R.string.Device_id)+mList.getDeviceId());
        holder.testDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globalPosition=position;
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("device_token",mList.getDeviceToken());
                mApiRequest.postRequest(BASE_URL + SEND_NOTIFICATION, SEND_NOTIFICATION, paramsReq, Request.Method.POST);

            }
        });

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globalPosition=position;
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("device_id",mList.getDeviceId());
                mApiRequest.postRequest(BASE_URL + DELETE_DEVICE, DELETE_DEVICE, paramsReq, Request.Method.POST);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mDeviceDatumListModel.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equals(DELETE_DEVICE)){
            try {
                JSONObject jsonObject=new JSONObject(response);
                if(jsonObject.getBoolean("status")){
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    mDeviceDatumListModel.remove(globalPosition);
                    notifyDataSetChanged();

                }else{
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView device_name,r_date,device_id,testDeviceButton,btnRemove;
        ImageView imgLogo;
        public ViewHolder(View itemView) {
            super(itemView);
            device_name = (TextView) itemView.findViewById(R.id.device_name);
            r_date = (TextView) itemView.findViewById(R.id.r_date);
            device_id = (TextView) itemView.findViewById(R.id.device_id);
            testDeviceButton = (TextView) itemView.findViewById(R.id.testDeviceButton);
            btnRemove = (TextView) itemView.findViewById(R.id.btnRemove);
            imgLogo = itemView.findViewById(R.id.imgLogo);
        }
    }


}