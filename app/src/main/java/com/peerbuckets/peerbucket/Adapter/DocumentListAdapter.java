package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocumentDetailActivity;
import com.peerbuckets.peerbucket.POJO.AlldocumentPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.ViewHolder> {

    private Context context;

    List<AlldocumentPOJO> mAlldocumentPOJOModel;
    public static int REQUST_CODE = 342;
    String parent_id;

    //Constructor of this class
    public DocumentListAdapter(String parent_id, List<AlldocumentPOJO> mTeamList, Context context) {
        super();
        //Getting all mTeamList
        this.mAlldocumentPOJOModel = mTeamList;
        this.context = context;
        this.parent_id = parent_id;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_document_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final AlldocumentPOJO mList = mAlldocumentPOJOModel.get(position);
        if (mList.getType().equals("folder")) {
            holder.tv_folder_name.setVisibility(View.VISIBLE);
            holder.tv_file_name.setVisibility(View.GONE);
            holder.tv_desc.setVisibility(View.GONE);
            holder.vv_view.setVisibility(View.GONE);
            holder.tv_folder_name.setText(mList.getDocumentTitle());
            Picasso.with(context).load(mList.getFolderImage()).into(holder.img_folder);
        } else {
            holder.vv_view.setVisibility(View.VISIBLE);
            holder.tv_folder_name.setVisibility(View.GONE);
            holder.tv_file_name.setVisibility(View.VISIBLE);
            holder.tv_desc.setVisibility(View.VISIBLE);
            Picasso.with(context).load(mList.getFolderImage()).into(holder.img_folder);
        }

        holder.tv_file_name.setText(Utils.unescapeJava(mList.getDocumentTitle()));
        holder.tv_desc.setText(Utils.unescapeJava(mList.getDocumentDescription()));

        holder.img_folder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mList.getType().equals("folder")) {
                    Intent i = new Intent(context, DocsAndFilesActivity.class);
                    i.putExtra("parent_id", mList.getDocumentId());
                    context.startActivity(i);
                } else {
                    Intent i = new Intent(context, DocumentDetailActivity.class);
                    i.putExtra("document_id", mList.getDocumentId());
                    i.putExtra("user_id", mList.getUserId());
                    i.putExtra("message_title", mList.getDocumentTitle());
                    i.putExtra("desc", mList.getDocumentDescription());
                    i.putExtra("class_type", "add");
                    i.putExtra("parent_id", parent_id);
                    ((DocsAndFilesActivity) context).startActivityForResult(i, REQUST_CODE);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return mAlldocumentPOJOModel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_folder_name, tv_file_name, tv_desc;
        ImageView img_folder;
        View vv_view;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_folder_name = (TextView) itemView.findViewById(R.id.tv_folder_name);
            tv_file_name = (TextView) itemView.findViewById(R.id.tv_file_name);
            tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);
            img_folder = itemView.findViewById(R.id.img_folder);
            vv_view = itemView.findViewById(R.id.vv_view);
        }
    }

    public void addUpdatedList(ArrayList<AlldocumentPOJO> mList) {
        mAlldocumentPOJOModel.add(0, mList.get(0));
        notifyDataSetChanged();
    }


    public void removeMessage(String messageId, String filename) {
        if (messageId != null && !messageId.equals("")) {
            for (AlldocumentPOJO list : mAlldocumentPOJOModel) {
                if (list.getDocumentId().equals(messageId)) {
                    mAlldocumentPOJOModel.remove(list);
                    list.setDocumentTitle(filename);
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }


}