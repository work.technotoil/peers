package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Typeface;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.TrashActivity;
import com.peerbuckets.peerbucket.POJO.ProjectListPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.TRASH_CPT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class TrashProjectListAdapter extends RecyclerView.Adapter<TrashProjectListAdapter.ViewHolder> implements IApiResponse {

    MinEmployeeIconAdapter memployee_icon_recycler_view_adapter;
    private Context context;
    boolean projectTypeInt;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    public int globalPosition = 0;
    //List to store all mTrashProjectList
    List<ProjectListPOJO> mTrashProjectList;
    String mcompanyId = "", muserId = "", memployeeId = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", maddremovetype = "test_hq", mCurrentuserId = "";

    //Constructor of this class
    public TrashProjectListAdapter(List<ProjectListPOJO> mTrashProjectList, Context context, boolean projectTypeInt) {
        super();
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        //Getting all mTrashProjectList
        this.mTrashProjectList = mTrashProjectList;
        this.context = context;
        this.projectTypeInt = projectTypeInt;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_trash_archeived, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final ProjectListPOJO TrashProjectList = mTrashProjectList.get(position);
        holder.mrestoreLL.setVisibility(View.GONE);
        holder.mtrash_desc_tv.setVisibility(View.VISIBLE);
        holder.mtrash_name_tv.setVisibility(View.VISIBLE);
        //Loading image from url
        holder.mtrash_desc_tv.setText(Utils.unescapeJava(TrashProjectList.getProjectTeamDescription()));
        holder.mtrash_name_tv.setText(Utils.unescapeJava(TrashProjectList.getProjectTeamName()));
        holder.mmain_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.mrestoreLL.setVisibility(View.VISIBLE);
                holder.mtrash_desc_tv.setVisibility(View.INVISIBLE);
                holder.mtrash_name_tv.setVisibility(View.INVISIBLE);
                holder.employee_icon_recycler_view.setVisibility(View.INVISIBLE);
            }
        });

        holder.mrestoreLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globalPosition = position;
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("companyId", mcompanyId);
                paramsReq.put("status", "active");
                paramsReq.put("type", mTrashProjectList.get(position).getProject_team_type());
                paramsReq.put("userId", mCurrentuserId);
                paramsReq.put("projectTeamId", mTrashProjectList.get(position).getProjectTeamId());
                mApiRequest.postRequest(BASE_URL + TRASH_CPT, TRASH_CPT, paramsReq, Request.Method.POST);
            }
        });


        memployee_icon_recycler_view_adapter = new MinEmployeeIconAdapter(TrashProjectList.getUserListing(), context, 0, 0);
        holder.employee_icon_recycler_view.setAdapter(memployee_icon_recycler_view_adapter);


    }

    @Override
    public int getItemCount() {
        return mTrashProjectList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(TRASH_CPT)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String code = jsonObject.getString("code");
                if (code.equals("200")) {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    ((TrashActivity) context).RefreshProjectAdapter(globalPosition, projectTypeInt);

                } else {
                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public TextView mtrash_name_tv, mtrash_desc_tv;
        LinearLayout mrestoreLL;
        CardView mmain_cv;
        RecyclerView employee_icon_recycler_view;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);
            mtrash_name_tv = (TextView) itemView.findViewById(R.id.trash_name_tv);
            mtrash_desc_tv = (TextView) itemView.findViewById(R.id.trash_desc_tv);
            mrestoreLL = itemView.findViewById(R.id.restoreLL);
            mmain_cv = itemView.findViewById(R.id.main_cv);

            employee_icon_recycler_view = itemView.findViewById(R.id.employee_icon_recycler_view);
            LinearLayoutManager mLayputManager = new LinearLayoutManager(context);
            mLayputManager.setOrientation(RecyclerView.HORIZONTAL);
            employee_icon_recycler_view.setLayoutManager(mLayputManager);
        }
    }
}