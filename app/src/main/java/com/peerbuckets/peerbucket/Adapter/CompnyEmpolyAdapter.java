package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Hpojo.UserListing;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.UserDialog;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

/**
 * Created by Belal on 11/9/2015.
 */
public class CompnyEmpolyAdapter extends RecyclerView.Adapter<CompnyEmpolyAdapter.ViewHolder> implements IApiResponse {

    private Context ctx;
    private ApiRequest mApiRequest;
    int value;
    public static int vItem = 0;
    Typeface fontTF;
    String mcompanyId = "", muserId = "", memployeeId = "", maddremovetype = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "";
    String[] COLORArray = {"black", "blue", "green", "white", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    public int selected = 0;
    int extra = 0;
    int listSize = 0;

    //List to store all mProjectList
    List<UserListing> mEmployeeList;

    //Constructor of this class
    public CompnyEmpolyAdapter(Context context, ArrayList<UserListing> list) {
        mEmployeeList = list;
        ctx = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_employee_icons, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final UserListing EmployeeList = mEmployeeList.get(position);
        selected = holder.random.nextInt(COLORArray.length);
        String COLORString = COLORArray[selected];
        String firstLetter = EmployeeList.getEmployeeName();


        if (position >= 3) {
            firstLetter = "+" + (listSize - 3);

            String mColor = COLORArray[0];
            holder.drawable = TextDrawable.builder()
                    .buildRound(firstLetter.toUpperCase(), Color.parseColor(mColor));

            holder.memployee_icon_iv.setImageDrawable(holder.drawable);
        } else {
            if (EmployeeList.getUserImage() != null && !EmployeeList.getUserImage().equals("")) {
                Picasso.with(ctx).load(EmployeeList.getUserImage()).into(holder.memployee_icon_iv);
            } else {
                holder.memployee_icon_iv.setImageDrawable(imageLatter(Utils.word(EmployeeList.getEmployeeName()), EmployeeList.getBgColor(), EmployeeList.getFontColor()));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (value == 0) {
                    UserDialog.otherUserDialog(ctx, mEmployeeList.get(position).getUserId(), mEmployeeList.get(position).getEmployeeName(), mEmployeeList.get(position).getUserImage(), "GMT", mEmployeeList.get(position).getBgColor(), mEmployeeList.get(position).getFontColor());
                } else {
                    ShardPreferences.save(ctx, ShardPreferences.key_EmpName, mEmployeeList.get(position).getEmployeeName());
                }
            }
        });
        if (value == 1) {
            holder.tv_memberName.setVisibility(View.VISIBLE);
            holder.tv_memberName.setText(EmployeeList.getEmployeeName());
        } else {
            holder.tv_memberName.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        if (mEmployeeList.size() > 3) {
            return 4;
        } else {
            return mEmployeeList.size();
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public ImageView memployee_icon_iv;
        public TextView tv_memberName;
        TextDrawable drawable;
        Random random;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            random = new Random();
            memployee_icon_iv = itemView.findViewById(R.id.employee_icon_iv);
            tv_memberName = itemView.findViewById(R.id.tv_memberName);

        }
    }

    public List<EmployeeDetalPOJO> getSelectedUserList(List<EmployeeDetalPOJO> mList) {
        List<EmployeeDetalPOJO> selectedUsersList = new ArrayList<>();
        if (mList != null && mList.size() > 0) {
            if (mList != null && mList.size() > 0) {
                for (EmployeeDetalPOJO userId : mList) {
                    if (userId.getSelected()) {
                        if (userId.getUserImage().equals("")) {
                            String s = userId.getEmployeeName().substring(0, 1);
                            String userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                            userId.setUserImage(DEVELOPMENT_URL + "" + userImage);
                        } else {
                            userId.setUserImage(DEVELOPMENT_URL + "" + userId.getEmployeeName());
                        }
                        selectedUsersList.add(userId);
                    }
                }
                return selectedUsersList;
            } else {
                return selectedUsersList;
            }
        }
        return selectedUsersList;
    }


    public void updateSelectedUser(List<UserListing> selectedUserList) {
        if (mEmployeeList != null) {
            mEmployeeList.addAll(selectedUserList);
            notifyDataSetChanged();
        }

    }


}