package com.peerbuckets.peerbucket.Adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.peerbuckets.peerbucket.Activity.PlayVideoActivity;
import com.peerbuckets.peerbucket.Activity.ZoomImagePagerActivity;
import com.peerbuckets.peerbucket.POJO.Chat;
import com.peerbuckets.peerbucket.POJO.OneToOneChatResponse.ReceiverName;
import com.peerbuckets.peerbucket.POJO.OneToOneChatResponse.SenderName;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Common;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverFileViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverImageGiphyViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverImageGroupViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverImageViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverTextGroupViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverVideoViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderFileViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderGIphyViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderImageViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderVideoViewHolder;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

public class PingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Chat> list;
    private Context mContext;
    private static final int TYPE_RECEIVE_TEXT = 1, TYPE_SEND_TEXT = 2, TYPE_DEFAULT = 0, TYPE_SEND_IMAGE = 4, TYPE_RECEIVER_IMAGE = 5,
            TYPE_RECEIVER_GIPHY = 3, TYPE_SEND_GIPHY = 6, TYPE_SEND_VIDEO = 7, TYPE_RECEIVER_VIDEO = 8, TYPE_SEND_FILE = 9, TYPE_RECEIVER_FILE = 10;

    private SenderName senderNames;
    private ReceiverName receiverNames;
    String firstName = "", lastName = "", imgName = "";

    public PingAdapter(Context context, List<Chat> chatList, SenderName senderNameList, ReceiverName receiverNameList) {
        mContext = context;
        list = chatList;
        senderNames = senderNameList;
        receiverNames = receiverNameList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = null;
        switch (i) {
            case TYPE_SEND_TEXT: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_text_chat, viewGroup, false);
                return new ChatSenderTextViewHolder(view, mContext);
            }
            case TYPE_RECEIVE_TEXT: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_text_group_chat, viewGroup, false);
                return new ChatReceiverTextGroupViewHolder(view);
            }
            case TYPE_SEND_IMAGE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_image_chat, viewGroup, false);
                return new ChatSenderImageViewHolder(view, mContext);
            }
            case TYPE_SEND_GIPHY: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_giphy_chat, viewGroup, false);
                return new ChatSenderGIphyViewHolder(view, mContext);
            }
            case TYPE_SEND_VIDEO: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_video_chat, viewGroup, false);
                return new ChatSenderVideoViewHolder(view, mContext);
            }
            case TYPE_SEND_FILE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_file_chat, viewGroup, false);
                return new ChatSenderFileViewHolder(view, mContext);
            }

            case TYPE_RECEIVER_IMAGE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_image_group_chat, viewGroup, false);
                return new ChatReceiverImageGroupViewHolder(view, mContext);
            }
            case TYPE_RECEIVER_GIPHY: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_giphy_group_chat, viewGroup, false);
                return new ChatReceiverImageGiphyViewHolder(view, mContext);
            }
            case TYPE_RECEIVER_VIDEO: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_video_group_chat, viewGroup, false);
                return new ChatReceiverVideoViewHolder(view, mContext);
            }
            case TYPE_RECEIVER_FILE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_file_chat, viewGroup, false);
                return new ChatReceiverFileViewHolder(view, mContext);
            }
            default: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_default_chat, viewGroup, false);
                return new ChatReceiverTextViewHolder(view, mContext);
            }
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        int itemViewType = getItemViewType(i);
        switch (itemViewType) {
            case TYPE_SEND_TEXT:
                if (!list.get(i).getMessage().equals("") && list.get(i).getMessage() != null) {
                    ((ChatSenderTextViewHolder) viewHolder).tvSendMessage.setText(Html.fromHtml(list.get(i).getMessage()));
                }
                ((ChatSenderTextViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderTextViewHolder) viewHolder).tvName.setText(mContext.getResources().getString(R.string.me));
                showStickyHeaderDate(i, ((ChatSenderTextViewHolder) viewHolder).tvStickyDate);
                imgName = Utils.word(senderNames.getEmployeeName());
                ((ChatSenderTextViewHolder) viewHolder).cl_sendChat.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        setClipboard(list.get(i).getMessage());
                        return true;
                    }
                });
                if (senderNames.getUserImage() != null && !senderNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(senderNames.getUserImage()).into(((ChatSenderTextViewHolder) viewHolder).img_user_receiver_text_chat);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderTextViewHolder) viewHolder).img_user_receiver_text_chat.setImageDrawable(imageLatter(imgName, senderNames.getBgColor(), senderNames.getFontColor()));
                }
                break;
            case TYPE_RECEIVE_TEXT:
                ((ChatReceiverTextGroupViewHolder) viewHolder).tvReceiveMessage.setText(Html.fromHtml(list.get(i).getMessage()));
                ((ChatReceiverTextGroupViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatReceiverTextGroupViewHolder) viewHolder).tvUserName.setText(receiverNames.getEmployeeName());
                imgName = Utils.word(receiverNames.getEmployeeName());
                showStickyHeaderDate(i, ((ChatReceiverTextGroupViewHolder) viewHolder).tvStickyDate);
                ((ChatReceiverTextGroupViewHolder) viewHolder).rl_receiveChat.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        setClipboard(list.get(i).getMessage());
                        return true;
                    }
                });
                if (receiverNames.getUserImage() != null && !receiverNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(receiverNames.getUserImage()).into(((ChatReceiverTextGroupViewHolder) viewHolder).img_user_reciver_text_chat);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverTextGroupViewHolder) viewHolder).img_user_reciver_text_chat.setImageDrawable(imageLatter(imgName, receiverNames.getBgColor(), receiverNames.getFontColor()));
                }
                break;
            case TYPE_SEND_IMAGE:
                ((ChatSenderImageViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Picasso.with(mContext).load(list.get(i).getMessage()).into(((ChatSenderImageViewHolder) viewHolder).imgSend);
                //  Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderImageViewHolder) viewHolder).imgSend);

                ((ChatSenderImageViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderImageViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                imgName = Utils.word(senderNames.getEmployeeName());
                ((ChatSenderImageViewHolder) viewHolder).imgSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ZoomImagePagerActivity.class);
                        intent.putExtra("type", ZoomImagePagerActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatSenderImageViewHolder) viewHolder).tvStickyDate);
                if (senderNames.getUserImage() != null && !senderNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(senderNames.getUserImage()).into(((ChatSenderImageViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderImageViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName, senderNames.getBgColor(), senderNames.getFontColor()));
                }
                break;
            case TYPE_SEND_VIDEO:
                ((ChatSenderVideoViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Glide.with(mContext).load(list.get(i).getMessage()).into(((ChatSenderVideoViewHolder) viewHolder).vv_send);
                ((ChatSenderVideoViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderVideoViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                imgName = Utils.word(senderNames.getEmployeeName());
                ((ChatSenderVideoViewHolder) viewHolder).vv_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, PlayVideoActivity.class);
                        intent.putExtra("type", PlayVideoActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatSenderVideoViewHolder) viewHolder).tvStickyDate);
                if (senderNames.getUserImage() != null && !senderNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(senderNames.getUserImage()).into(((ChatSenderVideoViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderVideoViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName, senderNames.getBgColor(), senderNames.getFontColor()));
                }
                break;
            case TYPE_SEND_GIPHY:
                ((ChatSenderGIphyViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderGIphyViewHolder) viewHolder).imgSend);
                imgName = Utils.word(senderNames.getEmployeeName());
                ((ChatSenderGIphyViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderGIphyViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                showStickyHeaderDate(i, ((ChatSenderGIphyViewHolder) viewHolder).tvStickyDate);
                if (senderNames.getUserImage() != null && !senderNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(senderNames.getUserImage()).into(((ChatSenderGIphyViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderGIphyViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName, senderNames.getBgColor(), senderNames.getFontColor()));
                }
                break;
            case TYPE_SEND_FILE:
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                ((ChatSenderFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderFileViewHolder) viewHolder).tv_fileName.setText(list.get(i).getFile_name());
                //   Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderFileViewHolder) viewHolder).imgSend);
                imgName = Utils.word(senderNames.getEmployeeName());
                ((ChatSenderFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderFileViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                showStickyHeaderDate(i, ((ChatSenderFileViewHolder) viewHolder).tvStickyDate);
                if (senderNames.getUserImage() != null && !senderNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(senderNames.getUserImage()).into(((ChatSenderFileViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderFileViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName, senderNames.getBgColor(), senderNames.getFontColor()));
                }
                ((ChatSenderFileViewHolder) viewHolder).imgFile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DownloadFileFromURL().execute(list.get(i).getMessage(), list.get(i).getFile_name());
                    }
                });
                break;
            case TYPE_RECEIVER_IMAGE:
                ((ChatReceiverImageGroupViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatReceiverImageGroupViewHolder) viewHolder).tvUserName.setText(receiverNames.getEmployeeName());
                imgName = Utils.word(receiverNames.getEmployeeName());
                Picasso.with(mContext).load(list.get(i).getMessage()).into(((ChatReceiverImageGroupViewHolder) viewHolder).imgReceiver);
                ((ChatReceiverImageGroupViewHolder) viewHolder).imgReceiver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ZoomImagePagerActivity.class);
                        intent.putExtra("type", ZoomImagePagerActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatReceiverImageGroupViewHolder) viewHolder).tvStickyDate);
                if (receiverNames.getUserImage() != null && !receiverNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(receiverNames.getUserImage()).into(((ChatReceiverImageGroupViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverImageGroupViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName, receiverNames.getBgColor(), receiverNames.getFontColor()));
                }
                break;

            case TYPE_RECEIVER_GIPHY:
                imgName = Utils.word(receiverNames.getEmployeeName());
                ((ChatReceiverImageGiphyViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatReceiverImageGiphyViewHolder) viewHolder).tvUserName.setText(receiverNames.getEmployeeName());
                Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatReceiverImageGiphyViewHolder) viewHolder).imgReceiver);
                showStickyHeaderDate(i, ((ChatReceiverImageGiphyViewHolder) viewHolder).tvStickyDate);
                if (receiverNames.getUserImage() != null && !receiverNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(receiverNames.getUserImage()).into(((ChatReceiverImageGiphyViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverImageGiphyViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName, receiverNames.getBgColor(), receiverNames.getFontColor()));
                }
                break;

            case TYPE_RECEIVER_VIDEO:
                ((ChatReceiverVideoViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Glide.with(mContext).load(list.get(i).getMessage()).into(((ChatReceiverVideoViewHolder) viewHolder).vv_receiver);
                ((ChatReceiverVideoViewHolder) viewHolder).tvUserName.setText(receiverNames.getEmployeeName());
                imgName = Utils.word(receiverNames.getEmployeeName());
                ((ChatReceiverVideoViewHolder) viewHolder).vv_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, PlayVideoActivity.class);
                        intent.putExtra("type", PlayVideoActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatReceiverVideoViewHolder) viewHolder).tvStickyDate);
                if (receiverNames.getUserImage() != null && !receiverNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(receiverNames.getUserImage()).into(((ChatReceiverVideoViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverVideoViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName, receiverNames.getBgColor(), receiverNames.getFontColor()));
                }
                break;

            case TYPE_RECEIVER_FILE:
                StrictMode.ThreadPolicy policyReceiver = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policyReceiver);
                ((ChatReceiverFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                //   Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderFileViewHolder) viewHolder).imgSend);
                ((ChatReceiverFileViewHolder) viewHolder).tvUserName.setText(receiverNames.getEmployeeName());
                imgName = Utils.word(receiverNames.getEmployeeName());
                ((ChatReceiverFileViewHolder) viewHolder).tv_fileName.setText(list.get(i).getFile_name());
                ((ChatReceiverFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                showStickyHeaderDate(i, ((ChatReceiverFileViewHolder) viewHolder).tvStickyDate);
                if (receiverNames.getUserImage() != null && !receiverNames.getUserImage().equals("")) {
                    Picasso.with(mContext).load(receiverNames.getUserImage()).into(((ChatReceiverFileViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverFileViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName, receiverNames.getBgColor(), receiverNames.getFontColor()));
                }
                ((ChatReceiverFileViewHolder) viewHolder).imgReceiver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DownloadFileFromURL().execute(list.get(i).getMessage(), list.get(i).getFile_name());
                    }
                });
                break;
        }

    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(35)
                .textColor(Color.parseColor("#" +fontColor ))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    public void updateSenderList(SenderName memberList) {
        senderNames = memberList;
    }

    public void updateReceiverList(ReceiverName memberList) {
        receiverNames = memberList;
    }

    @SuppressLint("StaticFieldLeak")
    public class DownloadFileFromURL extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog = new ProgressDialog(mContext);
        String filename = "";
        String urlll = "";

        @Override
        protected String doInBackground(String... strings) {
            int count;
            try {
                filename = strings[1];
                URL url = new URL(strings[0]);
                urlll = strings[0];
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(
                        url.openStream(), 8192);
                File mFile = Utils.getOutputMediaFile(filename, mContext);
                OutputStream output = new FileOutputStream(mFile);
                byte data[] = new byte[2048];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress((int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage(mContext.getResources().getString(R.string.Downloading_Please_wait));
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            if (pDialog != null && !pDialog.isShowing()) {
                pDialog.show();
            }
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
            pDialog.setProgress(progress[0]);
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {
                String downloaded = Environment.getExternalStorageDirectory()
                        .toString() + "/" + mContext.getResources().getString(R.string.app_name) + "/" + filename;

                //  Toast.makeText(mContext, "Downloading completed.", Toast.LENGTH_SHORT).show();
                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                Intent newIntent = new Intent(Intent.ACTION_VIEW);
                String mimeType = myMime.getMimeTypeFromExtension(fileExt(urlll).substring(1));
                newIntent.setDataAndType(Uri.parse(urlll), mimeType);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                try {
                    mContext.startActivity(newIntent);
                } catch (ActivityNotFoundException e) {
                    //      Toast.makeText(mContext, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setClipboard(String text) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getResources().getString(R.string.CopyText), text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, text + " " + mContext.getResources().getString(R.string.is_copy), Toast.LENGTH_SHORT).show();
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public void setData(List<Chat> dailyDoseLists) {
        list = dailyDoseLists;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0) {
            if (list.get(position).getSenderId().equals(ShardPreferences.get(mContext, ShardPreferences.share_UserId_key))) {
                if (list.get(position).getMessageType().equals(Common.TYPE_TEXT)) {
                    return TYPE_SEND_TEXT;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_IMAGE)) {
                    return TYPE_SEND_IMAGE;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_GIPHY)) {
                    return TYPE_SEND_GIPHY;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_VIDEO)) {
                    return TYPE_SEND_VIDEO;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_FILE)) {
                    return TYPE_SEND_FILE;
                }
            } else {
                if (list.get(position).getMessageType().equals(Common.TYPE_TEXT)) {
                    return TYPE_RECEIVE_TEXT;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_IMAGE)) {
                    return TYPE_RECEIVER_IMAGE;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_GIPHY)) {
                    return TYPE_RECEIVER_GIPHY;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_VIDEO)) {
                    return TYPE_RECEIVER_VIDEO;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_FILE)) {
                    return TYPE_RECEIVER_FILE;
                }
            }

        } else {
            return TYPE_DEFAULT;
        }
        return TYPE_DEFAULT;

    }

    private void showStickyHeaderDate(int position, TextView tvDate) {
        if (position > 0) {
            String cDate = getDate(list.get(position).getTimestampCreatedLong());
            String previousDate = getDate(list.get(position - 1).getTimestampCreatedLong());
            if (cDate.equalsIgnoreCase(previousDate)) {
                tvDate.setVisibility(View.GONE);
            } else {
                tvDate.setText(getDate(list.get(position).getTimestampCreatedLong()));
                tvDate.setVisibility(View.VISIBLE);
            }
        } else {
            tvDate.setVisibility(View.VISIBLE);
            tvDate.setText(getDate(list.get(position).getTimestampCreatedLong()));
        }

    }

    private String getTime(long time) {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return sdf.format(date);
    }

    private String getDate(long time) {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        return sdf.format(date);
    }
}