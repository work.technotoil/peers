package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_name_key;

public class AddRemoveUserAdapter extends RecyclerView.Adapter<AddRemoveUserAdapter.MyViewHolder> {

    private Context context;
    private List<EmployeeDetalPOJO> list;
    String all_users_id;


    public AddRemoveUserAdapter(String all_users_id, List<EmployeeDetalPOJO> mList, Context mContext) {
        context = mContext;
        list = mList;
        this.all_users_id = all_users_id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_remove_users_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tvName.setText(Utils.checkStringWithHash(Utils.unescapeJava(list.get(position).getEmployeeName())));
        if (Utils.isStringValid(list.get(position).getUserImage())) {
            Picasso.with(context).load( list.get(position).getUserImage()).into(holder.imgUser);
        } else {
            holder.imgUser.setImageDrawable(imageLatter(Utils.word(list.get(position).getEmployeeName()), list.get(position).getBgColor(), list.get(position).getFontColor()));
        }

        if (position == 0) {
            holder.tv_username.setVisibility(View.VISIBLE);
            holder.tv_username.setText(Utils.unescapeJava(ShardPreferences.get(context, share_user_name_key)));
        } else if (position == 1) {
            holder.tv_username.setVisibility(View.VISIBLE);
            holder.tv_username.setText(context.getResources().getString(R.string.Everyone_else));
        } else if (position > 1) {
            holder.tv_username.setVisibility(View.GONE);
        }

        if (all_users_id != null && all_users_id.contains(list.get(position).getUserId())) {
            list.get(position).setSelected(true);
        } else {
            list.get(position).setSelected(false);
        }

        holder.chkUser.setChecked(list.get(position).getSelected() ? true : false);

        holder.chkUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getSelected()) {
                    list.get(position).setSelected(false);
                    holder.chkUser.setChecked(false);
                } else {
                    list.get(position).setSelected(true);
                    holder.chkUser.setChecked(true);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public List<EmployeeDetalPOJO> getSelectedUsers() {
        List<EmployeeDetalPOJO> selectedUserList = new ArrayList<>();

        for (EmployeeDetalPOJO selectedUser : list) {
            if (selectedUser.getSelected()) {
                selectedUserList.add(selectedUser);
            }
        }
        return selectedUserList;
    }

    public void clearAllSelections() {
        if (list != null && list.size() > 0) {
            for (EmployeeDetalPOJO user : list) {
                user.setSelected(false);
            }
            notifyDataSetChanged();
        }
    }

    public void selectAllSelections() {
        if (list != null && list.size() > 0) {
            for (EmployeeDetalPOJO user : list) {
                user.setSelected(true);
            }
            notifyDataSetChanged();
        }
    }

    public String getAssignedUserString() {
        if (list != null && list.size() > 0) {
            List<String> usersIdList = new ArrayList<>();
            if (list != null && list.size() > 0) {
                for (EmployeeDetalPOJO userId : list) {
                    if (userId.getSelected()) {
                        usersIdList.add(userId.getUserId());
                    }
                }
                return android.text.TextUtils.join(",", usersIdList);

            } else {
                return "";
            }
        }
        return "";
    }


    public void updateSelectedUser(List<EmployeeDetalPOJO> selectedUserList) {
        if (selectedUserList != null && selectedUserList.size() > 0 && list != null && list.size() > 0) {
            for (EmployeeDetalPOJO selectedUser : selectedUserList) {
                for (EmployeeDetalPOJO originalList : list) {
                    if (selectedUser.getUserId().equals(originalList.getUserId())) {
                        originalList.setSelected(true);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tv_username;
        CircleImageView imgUser;
        CheckBox chkUser;

        MyViewHolder(View v) {
            super(v);
            tv_username = v.findViewById(R.id.tv_username);
            tvName = v.findViewById(R.id.tv_name_bottom_sheet);
            imgUser = v.findViewById(R.id.img_user);
            chkUser = v.findViewById(R.id.chkUser);

        }
    }


}