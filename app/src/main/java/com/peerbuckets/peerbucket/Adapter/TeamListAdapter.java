package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.AddRemovePeopleActivity;
import com.peerbuckets.peerbucket.Activity.ChatActivity;
import com.peerbuckets.peerbucket.Activity.CompanyHQExpandActivity;
import com.peerbuckets.peerbucket.Activity.HomeActivity;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInListActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleActivity;
import com.peerbuckets.peerbucket.Activity.todo.ToDoActivity;
import com.peerbuckets.peerbucket.POJO.TeamListPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;

import java.util.List;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

/**
 * Created by Belal on 11/9/2015.
 */
public class TeamListAdapter extends RecyclerView.Adapter<TeamListAdapter.ViewHolder> {

    private Context context;
    Typeface fontTF;
    MinEmployeeIconAdapter memployee_icon_recycler_view_adapter;

    //List to store all mTeamList
    List<TeamListPOJO> mTeamList;

    //Constructor of this class
    public TeamListAdapter(List<TeamListPOJO> mTeamList, Context context) {
        super();
        //Getting all mTeamList
        this.mTeamList = mTeamList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_home_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final TeamListPOJO TeamList = mTeamList.get(position);
        holder.mlist_name.setText(TeamList.getTeamName());
        holder.description.setText(TeamList.getProjectTeamDescription());

        holder.cv_company_hq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tittle = TeamList.getTeamName();
                String desc = TeamList.getProjectTeamDescription();
                String id = TeamList.getProjectTeamId();
                createDialog(tittle, id, desc, position);
            }
        });

       // memployee_icon_recycler_view_adapter = new MinEmployeeIconAdapter(TeamList.getUserListing(), context, 0);
        holder.employee_icon_recycler_view.setAdapter(memployee_icon_recycler_view_adapter);
    }

    @Override
    public int getItemCount() {
        return mTeamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public TextView  mlist_name,description;
        RecyclerView employee_icon_recycler_view;
        CardView cv_company_hq;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);

            cv_company_hq = itemView.findViewById(R.id.cv_company_hq);
            mlist_name = (TextView) itemView.findViewById(R.id.list_name);
            description = (TextView) itemView.findViewById(R.id.description);
            employee_icon_recycler_view =  itemView.findViewById(R.id.employee_icon_recycler_view);
            LinearLayoutManager mLayputManager = new LinearLayoutManager(context);
            mLayputManager.setOrientation(RecyclerView.HORIZONTAL);
            employee_icon_recycler_view.setLayoutManager(mLayputManager);

        }
    }

    public void createDialog(final String tittle, String id, String desc, final int pos) {
        ShardPreferences.save(context, share_current_project_team_id, tittle);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_tcp_expand, null);
        dialogBuilder.setView(dialogView);
        ImageView expandIv = dialogView.findViewById(R.id.expand_company_hq);
        TextView add_remove_tv = dialogView.findViewById(R.id.add_remove);
        TextView mcompany_expand_name_tv = dialogView.findViewById(R.id.company_expand_name_tv);
        TextView mcompany_desc_expand_tv = dialogView.findViewById(R.id.company_desc_expand_tv);

        LinearLayout linearTodo = dialogView.findViewById(R.id.dialog_tcp_todos);
        LinearLayout linearChat = dialogView.findViewById(R.id.dialog_tcp_chat);
        LinearLayout linearMessageBoard = dialogView.findViewById(R.id.dialog_tcp_message_board);
        LinearLayout linearSchedule = dialogView.findViewById(R.id.dialog_tcp_schedule);
        LinearLayout linearAutomaticCheclin = dialogView.findViewById(R.id.dialog_tcp_automatic_checkins);
        LinearLayout linearDOcs = dialogView.findViewById(R.id.dialog_tcp_docs_and_files);

        mcompany_expand_name_tv.setText(tittle);
        mcompany_desc_expand_tv.setText(desc);


        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();


        /*Omclick dialog Listeners*/

        expandIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent expandIntent = new Intent(context, CompanyHQExpandActivity.class);

                context.startActivity(expandIntent);
            }
        });

        add_remove_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent arIntent = new Intent(context, AddRemovePeopleActivity.class);
                ((HomeActivity) context).startActivityForResult(arIntent, 22);
            }
        });

        linearTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent intent = new Intent(context, ToDoActivity.class);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        linearChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent intent = new Intent(context, ChatActivity.class);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        linearMessageBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent intent = new Intent(context, Activity_MessageBoard.class);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        linearSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent intent = new Intent(context, ScheduleActivity.class);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        linearAutomaticCheclin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent intent = new Intent(context, CheckInListActivity.class);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });

        linearDOcs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShardPreferences.save(context, share_addd_remove_current_type, "team");
                ShardPreferences.save(context, share_current_project_team_id, mTeamList.get(pos).getProjectTeamId());
                ShardPreferences.save(context, share_current_module, mTeamList.get(pos).getTeamName());
                Intent intent = new Intent(context, DocsAndFilesActivity.class);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });
    }

}