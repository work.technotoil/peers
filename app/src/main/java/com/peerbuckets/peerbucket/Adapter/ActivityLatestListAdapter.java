package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.POJO.ActivityListResponce.Activity;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

public class ActivityLatestListAdapter extends RecyclerView.Adapter<ActivityLatestListAdapter.ActivityLatestListAdapterHolder> {
    private Context ctx;
    private ArrayList<Activity> mList;
    String mcompanyId = "", muserId = "", memployeeId = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", maddremovetype = "test_hq", mCurrentuserId = "";

    public ActivityLatestListAdapter(Context context, ArrayList<Activity> list) {
        mList = list;
        ctx = context;
    }

    @NonNull
    @Override
    public ActivityLatestListAdapter.ActivityLatestListAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_activity_screen, parent, false);
        return new ActivityLatestListAdapterHolder(v);
    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityLatestListAdapter.ActivityLatestListAdapterHolder holder, final int position) {
        holder.mactivity_title.setText(Utils.unescapeJava(mList.get(position).getProjectTeamName().substring(0, 1).toUpperCase() + mList.get(position).getProjectTeamName().substring(1)));
        holder.mactivity_time.setText(mList.get(position).getDate() + " " + mList.get(position).getTime());
        if (mList.get(position).getUserImage() != null && !mList.get(position).getUserImage().equals("")) {
            Picasso.with(ctx).load(mList.get(position).getUserImage()).into(holder.imgUser);
        } else {
            holder.imgUser.setImageDrawable(imageLatter(Utils.word(mList.get(position).getEmployeeName()), mList.get(position).getBgColor(), mList.get(position).getFontColor()));
        }
        showStickyHeaderDate(position, holder.mactivity_date);

        holder.rl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        String title = "";
        if (mList.get(position).getProjectTeamType().equals("test_hq") ||
                mList.get(position).getProjectTeamType().equals("team") ||
                mList.get(position).getProjectTeamType().equals("project") ||
                mList.get(position).getProjectTeamType().equals("company") ||
                mList.get(position).getProjectTeamType().equals("checkin")) {

            title = mList.get(position).getEmployeeName() +ctx.getResources().getString(R.string.started_a_new);

            if (mList.get(position).getProjectTeamType().equals("test_hq")) {
                holder.activity_text.setText(Utils.unescapeJava(title + ctx.getResources().getString(R.string.company_called)));
            } else {
                holder.activity_text.setText(Utils.unescapeJava(title + (mList.get(position).getProjectTeamType()) + ctx.getResources().getString(R.string.called)));
            }
        } else if (mList.get(position).getProjectTeamType().equals("timezone")) {
            holder.activity_text.setText(Utils.unescapeJava((mList.get(position).getEmployeeName()) +ctx.getResources().getString(R.string.change_our_timezone)));

        } else if (mList.get(position).getProjectTeamType().equals("message")) {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() +ctx.getResources().getString(R.string.post_a_new_message)));


        } else if (mList.get(position).getProjectTeamType().equals("to-dos")) {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() + " " + mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("schedule")) {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() + " " + mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("checkin")) {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() + " " + mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("comment") || mList.get(position).getProjectTeamType().equals("checkin_reply")) {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() + " " + mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("sub-todo")) {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() + " " + mList.get(position).getTitle()));

        } else if (mList.get(position).getProjectTeamType().equals("sub-to-do-mark-as-done")) {
            holder.activity_text.setText(mList.get(position).getTitle());
        } else {
            holder.activity_text.setText(Utils.unescapeJava(mList.get(position).getEmployeeName() + " " + mList.get(position).getDescription()));
        }
        if (mList.get(position).getProjectTeamType().equals("checkin")) {
            holder.activity_text.setVisibility(View.VISIBLE);
            holder.mActivity_content.setVisibility(View.GONE);
        } else {
            holder.activity_text.setVisibility(View.VISIBLE);
            holder.mActivity_content.setVisibility(View.VISIBLE);

            String newString = mList.get(position).getDescription();

            Spanned spanned = Html.fromHtml(newString);
            char[] chars = new char[spanned.length()];
            TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
            String plainText = new String(chars);

            newString = plainText.replace("\"", "'");

            Utils.setDescription(Utils.unescapeJava(newString), holder.mActivity_content, ctx);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ActivityLatestListAdapterHolder extends RecyclerView.ViewHolder {
        public TextView mactivity_date, mactivity_title, mactivity_time, activity_text;
        WebView mActivity_content;
        ImageView imgUser;
        RelativeLayout rl_click;

        public ActivityLatestListAdapterHolder(@NonNull View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(ctx, share_Company_Id_key);
            muserId = ShardPreferences.get(ctx, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(ctx, share_current_UserId_key);
            memployeeId = ShardPreferences.get(ctx, share_employee_Id_key);
            mactivity_date = itemView.findViewById(R.id.activity_date);
            mactivity_title = itemView.findViewById(R.id.activity_title);
            mActivity_content = itemView.findViewById(R.id.activity_content);
            mActivity_content.getSettings().setJavaScriptEnabled(true);
            mActivity_content.setLayerType(WebView.LAYER_TYPE_NONE, null);
            mactivity_time = itemView.findViewById(R.id.activity_time);
            activity_text = itemView.findViewById(R.id.activity_text);
            imgUser = itemView.findViewById(R.id.imgUser);
            rl_click = itemView.findViewById(R.id.rl_click);
        }
    }

    private void showStickyHeaderDate(int position, TextView tvLabel) {
        if (position > 0) {
            String cIsRead = mList.get(position).getDate();
            String previouscIsRead = mList.get(position - 1).getDate();
            if (cIsRead.equalsIgnoreCase(previouscIsRead)) {
                tvLabel.setVisibility(View.GONE);
            } else {
                tvLabel.setText(mList.get(position).getDate());
                tvLabel.setVisibility(View.VISIBLE);
            }
        } else {
            boolean isNewLabelDispay = false;
            if (mList != null && mList.size() > 0) {
                for (int i = 0; i < mList.size(); i++) {
                    if (mList.get(i).getDate().equals("")) {
                        isNewLabelDispay = true;
                        break;
                    }
                }
            }
            tvLabel.setVisibility(View.VISIBLE);
            if (isNewLabelDispay) {
                tvLabel.setText(mList.get(position).getDate());
            } else {
                tvLabel.setText(mList.get(position).getDate());
            }
        }
    }
}
