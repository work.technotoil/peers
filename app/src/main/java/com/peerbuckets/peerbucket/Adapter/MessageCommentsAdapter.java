package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.AddEditCommentActivity;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInReplyDetailActivity;
import com.peerbuckets.peerbucket.Activity.message.ExpandMessageBoardActivity;
import com.peerbuckets.peerbucket.ApiController.ApiConfigs;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.POJO.MessageCommentPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.APPLAUSE;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_COMMENT;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class MessageCommentsAdapter extends RecyclerView.Adapter<MessageCommentsAdapter.ViewHolder> implements IApiResponse {

    Context context;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    //List to store all mCommentDetailList
    List<MessageCommentPOJO> mCommentDetailList;
    String mcompanyId = "", muserId = "", memployeeId = "", updatedComment = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", maddremovetype = "test_hq", mCurrentuserId = "";
    String[] COLORArray = {"red", "blue", "green", "black", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    String COLORString, firstLetter;
    int selected = 0;
    int getGlobalPosition = 0;
    String mTtitle = "", message_id = "";
    Activity activity;
    RefreshCommentInterface mRefreshCommentInterface;
    ViewHolder mView = null;
    ProgressBar pb_progress;
    boolean isApplause = false;
    int isSeduale;

    //Constructor of context class
    public MessageCommentsAdapter(List<MessageCommentPOJO> mCommentDetailList, Context context, String mTtitle, String message_id, RefreshCommentInterface mRefreshCommentInterface, int a) {
        super();
        mApiRequest = new ApiRequest(context, this);
        //Getting all mCommentDetailList
        this.mCommentDetailList = mCommentDetailList;
        this.context = context;
        this.isSeduale = a;
        this.mTtitle = mTtitle;
        this.activity = (Activity) context;
        this.message_id = message_id;
        this.mRefreshCommentInterface = mRefreshCommentInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_comments, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (mCommentDetailList.get(position).getUserId().equals(ShardPreferences.get(context, share_UserId_key))) {
            holder.mmore_option_icon.setVisibility(View.VISIBLE);
        } else {
            holder.mmore_option_icon.setVisibility(View.GONE);
        }


        //Getting the particular item from the list
        final MessageCommentPOJO CommentDetailList = mCommentDetailList.get(position);

        //applause(CommentDetailList);
        //Loading image from url

        selected = holder.random.nextInt(COLORArray.length);
        COLORString = COLORArray[selected];
        firstLetter = CommentDetailList.getEmployeeName();

        holder.user_name.setText(Utils.unescapeJava(CommentDetailList.getEmployeeName()));
        String newString = CommentDetailList.getCommentDescription();

        Spanned spanned = Html.fromHtml(newString);
        char[] chars = new char[spanned.length()];
        TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
        String plainText = new String(chars);

        newString = plainText.replace("\"", "'");


        final int random = new Random().nextInt(100000) + 20; // [0, 60] + 20 => [20, 80]
//        String tributeCssPath = ApiConfigs.DEVELOPMENT_URL + "public/trixeditor/tribute.css?" + random;
        String tributeCssPath = "file:///android_asset/tribute.css";
        String basecampCSS = ApiConfigs.DEVELOPMENT_URL + "public/css/trixeditor.css?" + random;

        String pish = "<html><head><link rel='stylesheet' media='all' href='" + basecampCSS + "' data-turbolinks-track='reload'/>" +
                "<link rel='stylesheet' type='text/css' href='" + tributeCssPath + "'>" +
                "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                "<style type='text/css'> .boady { font-size: 15.0px; color:#000000;    background-color: #ffffff;} </style>" +
                "</head><body class='boady'>";
        String pas = "</body></html>";
        String myHtmlString = pish + newString + pas;
        String comment = Utils.unescapeJava(CommentDetailList.getCommentDescription());
        holder.muser_comment.loadData("<style>img{display: inline;height: auto;max-width: 100%;}</style>" + comment, "text/html; charset=utf-8", null);
        holder.muser_comment.setBackgroundColor(Color.TRANSPARENT);

       /* holder.muser_comment.getSettings().setLoadWithOverviewMode(false);
        holder.muser_comment.getSettings().setUseWideViewPort(true);*/

        String finalNewString = newString;
        holder.muser_comment.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(context.getResources().getString(R.string.CopyText), finalNewString);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, finalNewString + context.getResources().getString(R.string.is_copy), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        holder.mdate_created.setText(CommentDetailList.getCommentAt());
        holder.mmore_option_icon.setTypeface(fontTF);
        holder.mmore_option_icon.setText("\uF141");

        if (CommentDetailList.getUser_image() != null && !CommentDetailList.getUser_image().equals("")) {
            Picasso.with(context).load(DEVELOPMENT_URL + CommentDetailList.getUser_image()).into(holder.muser_image);
        } else {
            holder.muser_image.setImageDrawable(imageLatter(Utils.word(CommentDetailList.getEmployeeName()), CommentDetailList.getBgColor(), CommentDetailList.getFontColor()));
        }

        holder.muser_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommentDetailList.getUser_image() != null && !CommentDetailList.getUser_image().equals("")) {
                    Utils.showNameDialog(context, DEVELOPMENT_URL + CommentDetailList.getUser_image(), mCommentDetailList.get(position).getEmployeeName(), mCommentDetailList.get(position).getEmail(), mCommentDetailList.get(position).getBgColor(), mCommentDetailList.get(position).getFontColor());
                } else {
                    Utils.showNameDialog(context, DEVELOPMENT_URL + CommentDetailList.getUser_image(), mCommentDetailList.get(position).getEmployeeName(), mCommentDetailList.get(position).getEmail(), mCommentDetailList.get(position).getBgColor(), mCommentDetailList.get(position).getFontColor());

                }

            }
        });


        holder.mmore_option_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.meud_ll.setVisibility(View.VISIBLE);
                holder.ll_edit_menu.setVisibility(View.GONE);
            }
        });

        holder.mcancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.meud_ll.setVisibility(View.GONE);
                holder.ll_edit_menu.setVisibility(View.VISIBLE);
            }
        });


        holder.medit_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddEditCommentActivity.class);
                i.putExtra("comment_description", Utils.unescapeJava(CommentDetailList.getCommentDescription()));
                i.putExtra("comment_id", CommentDetailList.getCommentId());
                i.putExtra("record_id", message_id);
                i.putExtra("message_title", Utils.unescapeJava(mTtitle));
                i.putExtra("position", position + "");
                i.putExtra("user_image", CommentDetailList.getUser_image());
                i.putExtra("back", CommentDetailList.getBgColor());
                i.putExtra("fount", CommentDetailList.getFontColor());
                i.putExtra("name", CommentDetailList.getEmployeeName());
                ((Activity) context).startActivityForResult(i, 5);
                getGlobalPosition = position;
            }
        });

        holder.mdelete_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGlobalPosition = position;
                pb_progress.setVisibility(View.VISIBLE);
                holder.meud_ll.setVisibility(View.GONE);
                holder.ll_edit_menu.setVisibility(View.VISIBLE);
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("comment_id", CommentDetailList.getCommentId());
                paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                mApiRequest.postRequest(BASE_URL + DELETE_COMMENT, DELETE_COMMENT, paramsReq, Request.Method.POST);

            }
        });

        List<EmployeeDetalPOJO> mGetApplauseArray = CommentDetailList.getmApplauseEmployeeList();
        if (mGetApplauseArray != null && mGetApplauseArray.size() > 0) {
            for (int i = 0; i < mGetApplauseArray.size(); i++) {
                mGetApplauseArray.get(i).setUserImage(mGetApplauseArray.get(i).getUserImage());
                mGetApplauseArray.get(i).setEmployeeName(mGetApplauseArray.get(i).getEmployeeName());
                mGetApplauseArray.get(i).setBgColor(mGetApplauseArray.get(i).getBgColor());
                mGetApplauseArray.get(i).setFontColor(mGetApplauseArray.get(i).getFontColor());
            }
            EmployeeIconAdapter mNotifiedUserAdapter = new EmployeeIconAdapter(mGetApplauseArray, context);
            holder.comment_applause_rv.setAdapter(mNotifiedUserAdapter);
            mNotifiedUserAdapter.notifyDataSetChanged();
        }

        holder.imgCommentApplause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGlobalPosition = position;
                mView = holder;
                if (isApplause) {
                    isApplause = false;
                } else {
                    isApplause = true;
                }
                addApplause(CommentDetailList);
            }
        });

        holder.edit_discuss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CheckInReplyDetailActivity.class);
                intent.putExtra("check_in_reply_id", mCommentDetailList.get(position));
                context.startActivity(intent);
            }
        });


        if (isSeduale == 1) {
            holder.edit_discuss.setVisibility(View.VISIBLE);
        } else {
            holder.edit_discuss.setVisibility(View.GONE);
        }
    }

    private int getScale(WebView mWebView) {
        int PIC_WIDTH = mWebView.getRight() - mWebView.getLeft();

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width) / new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;

    }

    public void addApplause(MessageCommentPOJO mCommentListObj) {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("record_id", mCommentListObj.getCommentId());
        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
        paramsReq.put("user_id", ShardPreferences.get(context, share_UserId_key));
        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
        paramsReq.put("notification_type", "comment");
        paramsReq.put("sub_module", mCommentListObj.getRecordType());

        if (isApplause) {
            paramsReq.put("isApplause", "1");
        } else {
            paramsReq.put("isApplause", "0");
        }
        mApiRequest.postRequestBackground(BASE_URL + APPLAUSE, APPLAUSE, paramsReq, Request.Method.POST);

    }

    @Override
    public int getItemCount() {
        return mCommentDetailList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(DELETE_COMMENT)) {
            Log.d("DELETE_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                pb_progress.setVisibility(View.GONE);
                if (status == 1) {

                    mCommentDetailList.remove(getGlobalPosition);
                    mRefreshCommentInterface.setCommentData(mCommentDetailList.size());
                    notifyDataSetChanged();
                }
            } catch (Exception e) {
                pb_progress.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }

        if (tag_json_obj.equals(APPLAUSE)) {
            Log.d("EDIT_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    JSONArray mGetApplauseArray = jsonObject.getJSONArray("getlike");

                    if (mGetApplauseArray != null && mGetApplauseArray.length() > 0) {

                        if (mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList() == null) {
                            ArrayList<EmployeeDetalPOJO> list = new ArrayList<>();
                            mCommentDetailList.get(getGlobalPosition).setmApplauseEmployeeList(list);
                        }
                        if (mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList() != null) {
                            mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList().clear();

                            for (int i = 0; i < mGetApplauseArray.length(); i++) {
                                JSONObject jsonObject2 = mGetApplauseArray.getJSONObject(i);
                                EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                                mModel.setUserId(jsonObject2.getString("user_id"));
                                mModel.setEmployeeName(jsonObject2.getString("employee_name"));
                                mModel.setBgColor(jsonObject2.getString("bgColor"));
                                mModel.setFontColor(jsonObject2.getString("fontColor"));
                                mModel.setUserImage(jsonObject2.getString("user_image"));

                                mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList().add(mModel);
                            }
                            EmployeeIconAdapter mNotifiedUserAdapter = new EmployeeIconAdapter(mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList(), context);
                            mView.comment_applause_rv.setAdapter(mNotifiedUserAdapter);
                        }
                    } else {
                        if (mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList() != null) {
                            mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList().clear();
                            EmployeeIconAdapter mNotifiedUserAdapter = new EmployeeIconAdapter(mCommentDetailList.get(getGlobalPosition).getmApplauseEmployeeList(), context);
                            mView.comment_applause_rv.setAdapter(mNotifiedUserAdapter);
                        }
                    }
                }
            } catch (Exception e) {
                Log.d("EXCPTN_EC", e.toString());
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        //Views
        TextView user_name, mdate_created, mmore_option_icon, mdelete_comment, medit_comment, mcancel_layout;
        WebView muser_comment;
        CircleImageView muser_image;
        LinearLayout meud_ll, mcomment_sectoin_LL, ll_edit_menu, linear1;
        Random random;
        TextView edit_discuss;
        ImageView imgCommentApplause;
        RecyclerView comment_applause_rv;
        RecyclerView.LayoutManager mCommentApplauseManager;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);

            fontTF = Typeface.createFromAsset(context.getAssets(), "font/fawsmsolid.ttf");
            random = new Random();
            muser_image = itemView.findViewById(R.id.user_image);
            user_name = itemView.findViewById(R.id.user_name);
            imgCommentApplause = itemView.findViewById(R.id.imgCommentApplause);
            comment_applause_rv = itemView.findViewById(R.id.comment_applause_rv);
            mCommentApplauseManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            comment_applause_rv.setLayoutManager(mCommentApplauseManager);

            pb_progress = itemView.findViewById(R.id.pb_progress);
            muser_comment = itemView.findViewById(R.id.user_comment);
            muser_comment.getSettings().setJavaScriptEnabled(true);
            muser_comment.setLayerType(WebView.LAYER_TYPE_NONE, null);


            mdate_created = itemView.findViewById(R.id.date_created);
            mmore_option_icon = itemView.findViewById(R.id.more_option_icon);
            meud_ll = itemView.findViewById(R.id.eud_ll);
            mdelete_comment = itemView.findViewById(R.id.delete_comment);
            medit_comment = itemView.findViewById(R.id.edit_comment);
            linear1 = itemView.findViewById(R.id.linear1_comment_list);
            mcancel_layout = itemView.findViewById(R.id.cancel_layout);
            mcomment_sectoin_LL = itemView.findViewById(R.id.comment_sectoin_LL);
            ll_edit_menu = itemView.findViewById(R.id.ll_edit_menu);
            edit_discuss = itemView.findViewById(R.id.edit_discuss);

        }
    }

    public void updateData(int position, String data) {
        if (position != -1) {
            String updatedData = TextUtils.htmlEncode(data);
            mCommentDetailList.get(position).setCommentDescription(updatedData);
            notifyItemChanged(position);
        }
    }

    public void setNotifyData(List<MessageCommentPOJO> mList) {
        mCommentDetailList.addAll(mList);
        notifyDataSetChanged();
    }

    public void addCommentSize() {
        if (mRefreshCommentInterface != null && mCommentDetailList != null) {
            mRefreshCommentInterface.setCommentData(mCommentDetailList.size());
        }
    }
}