package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.ZoomImagePagerActivity;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInDetailActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocumentDetailActivity;
import com.peerbuckets.peerbucket.Activity.message.ExpandMessageBoardActivity;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleDetailActivity;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoActivity;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoSubTaskActivity;
import com.peerbuckets.peerbucket.POJO.Chat;
import com.peerbuckets.peerbucket.POJO.HeyListPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Common;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverImageGroupViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverTextGroupViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderImageViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.ReadNotificationTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.UnreadNotificationTextViewHolder;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_TODO;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.READ_UNREAD_NOTIFICATION;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class HeyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IApiResponse {

    private List<HeyListPOJO> list;
    private Context mContext;
    private static final int TYPE_READ_TEXT = 1, TYPE_UNREAD_TEXT = 2, TYPE_DEFAULT = 0;
    ApiRequest mApiRequest;

    public HeyListAdapter(List<HeyListPOJO> chatList, Context context) {
        mContext = context;
        list = chatList;
        mApiRequest = new ApiRequest(mContext, this);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = null;
        switch (i) {
            case TYPE_READ_TEXT: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.new_notification_layout, viewGroup, false);
                return new ReadNotificationTextViewHolder(view, mContext);
            }
            case TYPE_UNREAD_TEXT: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.new_notification_layout, viewGroup, false);
                return new UnreadNotificationTextViewHolder(view, mContext);
            }
            default: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_default_chat, viewGroup, false);
                return new ChatReceiverTextViewHolder(view, mContext);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int i) {
        int itemViewType = getItemViewType(i);

        String newString = list.get(i).getMsg();

        Spanned spanned = Html.fromHtml(newString);
        char[] chars = new char[spanned.length()];
        TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
        String plainText = new String(chars);
        newString = plainText.replace("\"", "'");

        switch (itemViewType) {

            case TYPE_READ_TEXT:
                ((ReadNotificationTextViewHolder) viewHolder).textViewOptions.setVisibility(View.VISIBLE);
                ((ReadNotificationTextViewHolder) viewHolder).imgType.setVisibility(View.VISIBLE);
                ((ReadNotificationTextViewHolder) viewHolder).imgCLose.setVisibility(View.GONE);
                ((ReadNotificationTextViewHolder) viewHolder).module_type.setText(list.get(i).getProjectTeamName());
                ((ReadNotificationTextViewHolder) viewHolder).module_date.setText(list.get(i).getDate());


                String msgType = "";
                if (list.get(i).getType().equals("checkin")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_checkin);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_automatic_checkins);
                } else if (list.get(i).getType().equals("checkin_reply")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_checkin_reply);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_automatic_checkins);
                } else if (list.get(i).getType().equals("message")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_message);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_message_board);
                } else if (list.get(i).getType().equals("schedule")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_schedule);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_calendar);
                } else if (list.get(i).getType().equals("todo_list")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_todo);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_to_dos);
                } else if (list.get(i).getType().equals("doc")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_document);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_docs_files);
                } else if (list.get(i).getType().equals("comment")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_comment);

                    //subtype set
                    if (list.get(i).getSubtype().equals("message")) {
                        ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_message_board);
                    } else if (list.get(i).getSubtype().equals("todo_list")) {
                        ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_to_dos);
                    } else if (list.get(i).getSubtype().equals("checkin")) {
                        ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_automatic_checkins);
                    } else if (list.get(i).getSubtype().equals("schedule")) {
                        ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_calendar);
                    } else if (list.get(i).getSubtype().equals("document")) {
                        ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_docs_files);
                    } else if (list.get(i).getSubtype().equals("sub-todo")) {
                        ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_to_dos);
                    }

                } else if (list.get(i).getType().contains("message_mention")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_message);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_message_board);
                } else if (list.get(i).getType().contains("todo_mention")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_todo);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_to_dos);
                } else if (list.get(i).getType().contains("comment_mention")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_comment);
                } else if (list.get(i).getType().contains("checkin_reply_mention")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_check_in_reply);
                } else if (list.get(i).getType().contains("sub-todo")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_sub_todo);
                    ((ReadNotificationTextViewHolder) viewHolder).imgType.setImageResource(R.drawable.ic_global_to_dos);
                } else if (list.get(i).getType().contains("sub-to-do-mark-as-done")) {
                    msgType = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.Completed) + list.get(i).getTitle();
                }

                ((ReadNotificationTextViewHolder) viewHolder).txtTitle.setText(Utils.unescapeJava(msgType));

                ((ReadNotificationTextViewHolder) viewHolder).textViewOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popup = new PopupMenu(mContext, ((ReadNotificationTextViewHolder) viewHolder).textViewOptions);
                        //inflating menu from xml resource
                        popup.inflate(R.menu.menu_pop);
                        //adding click listener
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.mark_unread:
                                        list.get(i).setIsRead("0");
                                        notifyDataSetChanged();
                                        Map<String, String> paramsReq = new HashMap<>();
                                        paramsReq.put("notification_id", list.get(i).getNotification_id());
                                        paramsReq.put("isRead", "0");
                                        paramsReq.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
                                        mApiRequest.postRequestBackground(BASE_URL + READ_UNREAD_NOTIFICATION, READ_UNREAD_NOTIFICATION, paramsReq, Request.Method.POST);

                                        break;
                                }
                                return false;
                            }
                        });
                        popup.show();
                    }
                });

                showStickyHeaderDate(i, ((ReadNotificationTextViewHolder) viewHolder).tv_read_lable);

                ((ReadNotificationTextViewHolder) viewHolder).ffView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendToSpecificScreen(list.get(i).getType(), list.get(i).getId(), list.get(i).getMsg(),
                                ShardPreferences.get(mContext, share_UserId_key), list.get(i).getProjectTeamId(), list.get(i).getSubtype());
                    }
                });


                break;

            case TYPE_UNREAD_TEXT:
                ((UnreadNotificationTextViewHolder) viewHolder).imgCLose.setVisibility(View.VISIBLE);
                ((UnreadNotificationTextViewHolder) viewHolder).imagType.setVisibility(View.VISIBLE);
                ((UnreadNotificationTextViewHolder) viewHolder).textViewOptions.setVisibility(View.GONE);
                ((UnreadNotificationTextViewHolder) viewHolder).module_type.setText(list.get(i).getProjectTeamName());
                ((UnreadNotificationTextViewHolder) viewHolder).module_date.setText(list.get(i).getDate());

                String msgTypeee = "";
                if (list.get(i).getType().equals("checkin")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_checkin);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_automatic_checkins);
                } else if (list.get(i).getType().equals("checkin_reply")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_checkin_reply);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_automatic_checkins);
                } else if (list.get(i).getType().equals("message")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_message);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_message_board);
                } else if (list.get(i).getType().equals("schedule")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_schedule);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_calendar);
                } else if (list.get(i).getType().equals("todo_list")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_todo);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_to_dos);
                } else if (list.get(i).getType().equals("doc")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_document);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_docs_files);
                } else if (list.get(i).getType().equals("comment")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_comment);

                    //subtype set
                    if (list.get(i).getSubtype().equals("message")) {
                        ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_message_board);
                    } else if (list.get(i).getSubtype().equals("todo_list")) {
                        ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_to_dos);
                    } else if (list.get(i).getSubtype().equals("checkin")) {
                        ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_automatic_checkins);
                    } else if (list.get(i).getSubtype().equals("schedule")) {
                        ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_calendar);
                    } else if (list.get(i).getSubtype().equals("document")) {
                        ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_docs_files);
                    } else if (list.get(i).getSubtype().equals("sub-todo")) {
                        ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_to_dos);
                    }

                } else if (list.get(i).getType().contains("message_mention")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_message);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_message_board);
                } else if (list.get(i).getType().contains("todo_mention")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_todo);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_to_dos);
                } else if (list.get(i).getType().contains("comment_mention")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_comment);
                } else if (list.get(i).getType().contains("checkin_reply_mention")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.mentioned_you_in_check_in_reply);
                } else if (list.get(i).getType().contains("sub-todo")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.added_new_sub_todo);
                    ((UnreadNotificationTextViewHolder) viewHolder).imagType.setImageResource(R.drawable.ic_global_to_dos);
                } else if (list.get(i).getType().contains("sub-to-do-mark-as-done")) {
                    msgTypeee = list.get(i).getEmployee_name() + mContext.getResources().getString(R.string.Completed) + list.get(i).getTitle();
                }
                ((UnreadNotificationTextViewHolder) viewHolder).txtTitle.setText(Utils.unescapeJava(msgTypeee));

                showStickyHeaderDate(i, ((UnreadNotificationTextViewHolder) viewHolder).tv_read_lable);

                ((UnreadNotificationTextViewHolder) viewHolder).imgCLose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(i).setIsRead("1");
                        notifyDataSetChanged();

                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("notification_id", list.get(i).getNotification_id());
                        paramsReq.put("isRead", "1");
                        paramsReq.put("user_id", ShardPreferences.get(mContext, share_current_UserId_key));
                        mApiRequest.postRequestBackground(BASE_URL + READ_UNREAD_NOTIFICATION, READ_UNREAD_NOTIFICATION, paramsReq, Request.Method.POST);
                    }
                });

                ((UnreadNotificationTextViewHolder) viewHolder).ffView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendToSpecificScreen(list.get(i).getType(), list.get(i).getId(), list.get(i).getMsg(),
                                ShardPreferences.get(mContext, share_UserId_key), list.get(i).getProjectTeamId(), list.get(i).getSubtype());
                    }
                });

                break;
        }

    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public void setData(List<HeyListPOJO> dailyDoseLists) {
        list = dailyDoseLists;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0) {
            if (list.get(position).getIsRead().equals("1")) {
                return TYPE_READ_TEXT;
            } else if (list.get(position).getIsRead().equals("0")) {
                return TYPE_UNREAD_TEXT;
            }
        }
        return TYPE_DEFAULT;

    }


    private void showStickyHeaderDate(int position, TextView tvLabel) {
        if (position > 0) {
            String cIsRead = list.get(position).getIsRead();
            String previouscIsRead = list.get(position - 1).getIsRead();
            if (cIsRead.equalsIgnoreCase(previouscIsRead)) {
                tvLabel.setVisibility(View.GONE);
            } else {
                if (list.get(position).getIsRead().equals("1")) {
                    tvLabel.setText(mContext.getResources().getString(R.string.Previous_Notifications));
                    tvLabel.setVisibility(View.VISIBLE);
                } else {
                    tvLabel.setText(mContext.getResources().getString(R.string.New_for_you));
                    tvLabel.setVisibility(View.VISIBLE);
                }
            }
        } else {
            boolean isNewLabelDispay = false;
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getIsRead().equals("0")) {
                        isNewLabelDispay = true;
                    }
                }
            }
            tvLabel.setVisibility(View.VISIBLE);
            if (isNewLabelDispay) {
                tvLabel.setText(mContext.getResources().getString(R.string.New_for_you));
            } else {
                tvLabel.setText(mContext.getResources().getString(R.string.Previous_Notifications));
            }
        }
    }

    public void setNotifyData(List<HeyListPOJO> mList) {
        list.addAll(mList);
        notifyDataSetChanged();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    public void sendToSpecificScreen(String notificationType, String record_id, String title, String user_id, String projectID, String subType) {
        ShardPreferences.save(mContext, ShardPreferences.share_current_project_team_id, projectID);

        if (notificationType.equals("checkin")) {
            Intent i1 = new Intent(mContext, CheckInDetailActivity.class);
            i1.putExtra("checkInId", record_id);
            i1.putExtra("checkin_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("checkin_reply")) {
            Intent i1 = new Intent(mContext, CheckInDetailActivity.class);
            i1.putExtra("check_in_reply_id", record_id);
            i1.putExtra("check_in_question", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("message")) {
            Intent i1 = new Intent(mContext, ExpandMessageBoardActivity.class);
            i1.putExtra("message_id", record_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("schedule")) {
            Intent i1 = new Intent(mContext, ScheduleDetailActivity.class);
            i1.putExtra("schedule_id", record_id);
            i1.putExtra("schedule_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("todo_list")) {
            Intent i1 = new Intent(mContext, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);

            mContext.startActivity(i1);
        } else if (notificationType.equals("doc")) {
            Intent i1 = new Intent(mContext, DocumentDetailActivity.class);
            i1.putExtra("document_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        }

        //comment with subType
        else if (notificationType.equals("comment") && subType.equals("message")) {
            Intent i1 = new Intent(mContext, ExpandMessageBoardActivity.class);
            i1.putExtra("message_id", record_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("sub-todo")) {
            Intent i1 = new Intent(mContext, ExpandToDoSubTaskActivity.class);
            i1.putExtra("check_in_reply_id", record_id);
            i1.putExtra("check_in_question", title);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("checkin")) {
            Intent i1 = new Intent(mContext, CheckInDetailActivity.class);
            i1.putExtra("checkInId", record_id);
            i1.putExtra("checkin_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("document")) {
            Intent i1 = new Intent(mContext, DocumentDetailActivity.class);
            i1.putExtra("document_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("todo_list")) {
            Intent i1 = new Intent(mContext, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("schedule")) {
            Intent i1 = new Intent(mContext, ScheduleDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        }

        //contents intent
        else if (notificationType.equals("message_mention")) {
            Intent i1 = new Intent(mContext, ExpandMessageBoardActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("todo_mention")) {
            Intent i1 = new Intent(mContext, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("checkin_reply_mention")) {
            Intent i1 = new Intent(mContext, CheckInDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("sub-todo")) {
            Intent i1 = new Intent(mContext, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("sub-to-do-mark-as-done")) {
            Intent i1 = new Intent(mContext, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        }

        //contents comment mention with subType
        else if (notificationType.equals("comment_mention") && subType.equals("message")) {
            Intent i1 = new Intent(mContext, ExpandMessageBoardActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("sub-todo")) {
            Intent i1 = new Intent(mContext, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("checkin")) {
            Intent i1 = new Intent(mContext, CheckInDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("document")) {
            Intent i1 = new Intent(mContext, DocumentDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("todo_list")) {
            Intent i1 = new Intent(mContext, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("schedule")) {
            Intent i1 = new Intent(mContext, ScheduleDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("todo_list_task")) {
            Intent i1 = new Intent(mContext, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            mContext.startActivity(i1);
        }

    }
}