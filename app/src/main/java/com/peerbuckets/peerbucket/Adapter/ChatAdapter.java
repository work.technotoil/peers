package com.peerbuckets.peerbucket.Adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.peerbuckets.peerbucket.Activity.PlayVideoActivity;
import com.peerbuckets.peerbucket.Activity.ZoomImagePagerActivity;
import com.peerbuckets.peerbucket.POJO.Chat;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Common;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverFileViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverImageGiphyViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverImageGroupViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverTextGroupViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatReceiverVideoViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderFileViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderGIphyViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderImageViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderTextViewHolder;
import com.peerbuckets.peerbucket.view_holder.ChatSenderVideoViewHolder;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Chat> list;
    private Context mContext;
    private static final int TYPE_RECEIVE_TEXT = 1, TYPE_SEND_TEXT = 2, TYPE_DEFAULT = 0, TYPE_SEND_IMAGE = 4, TYPE_RECEIVER_IMAGE = 5,
            TYPE_RECEIVER_GIPHY = 3, TYPE_SEND_GIPHY = 6, TYPE_SEND_VIDEO = 7, TYPE_RECEIVER_VIDEO = 8, TYPE_SEND_FILE = 9, TYPE_RECEIVER_FILE = 10;
    private List<User> teamMemberList;
    String firstName = "", lastName = "", imgName = "";

    public ChatAdapter(Context context, List<Chat> chatList, List<User> memberList) {
        mContext = context;
        list = chatList;
        teamMemberList = memberList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = null;
        switch (i) {
            case TYPE_SEND_TEXT: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_text_chat, viewGroup, false);
                return new ChatSenderTextViewHolder(view, mContext);
            }
            case TYPE_RECEIVE_TEXT: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_text_group_chat, viewGroup, false);
                return new ChatReceiverTextGroupViewHolder(view);
            }
            case TYPE_SEND_IMAGE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_image_chat, viewGroup, false);
                return new ChatSenderImageViewHolder(view, mContext);
            }
            case TYPE_SEND_GIPHY: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_giphy_chat, viewGroup, false);
                return new ChatSenderGIphyViewHolder(view, mContext);
            }
            case TYPE_SEND_VIDEO: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_video_chat, viewGroup, false);
                return new ChatSenderVideoViewHolder(view, mContext);
            }
            case TYPE_SEND_FILE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_send_file_chat, viewGroup, false);
                return new ChatSenderFileViewHolder(view, mContext);
            }

            case TYPE_RECEIVER_IMAGE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_image_group_chat, viewGroup, false);
                return new ChatReceiverImageGroupViewHolder(view, mContext);
            }
            case TYPE_RECEIVER_GIPHY: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_giphy_group_chat, viewGroup, false);
                return new ChatReceiverImageGiphyViewHolder(view, mContext);
            }
            case TYPE_RECEIVER_VIDEO: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_video_group_chat, viewGroup, false);
                return new ChatReceiverVideoViewHolder(view, mContext);
            }
            case TYPE_RECEIVER_FILE: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_receive_file_chat, viewGroup, false);
                return new ChatReceiverFileViewHolder(view, mContext);
            }

            default: {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_view_holder_default_chat, viewGroup, false);
                return new ChatReceiverTextViewHolder(view, mContext);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        int itemViewType = getItemViewType(i);
        switch (itemViewType) {
            case TYPE_SEND_TEXT:
                if (!list.get(i).getMessage().equals("") && list.get(i).getMessage() != null) {
                    ((ChatSenderTextViewHolder) viewHolder).tvSendMessage.setText(Html.fromHtml(list.get(i).getMessage()));
                    ((ChatSenderTextViewHolder) viewHolder).cl_sendChat.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            setClipboard(list.get(i).getMessage());
                            return true;
                        }
                    });
                }

                ((ChatSenderTextViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderTextViewHolder) viewHolder).tvName.setText(mContext.getResources().getString(R.string.me));
                showStickyHeaderDate(i, ((ChatSenderTextViewHolder) viewHolder).tvStickyDate);
                Object sendTextPath = checkUserImage(list.get(i).getSenderId());
                String userNameText = checkUserName(list.get(i).getSenderId());
                String userTextBColor = checkUserBackName(list.get(i).getSenderId());
                String userTextFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(userNameText)) {
                    imgName = Utils.word(userNameText);
                }
                if (sendTextPath != null && !sendTextPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + sendTextPath).into(((ChatSenderTextViewHolder) viewHolder).img_user_receiver_text_chat);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderTextViewHolder) viewHolder).img_user_receiver_text_chat.setImageDrawable(imageLatter(imgName,userTextBColor, userTextFColor));
                }
                break;

            case TYPE_RECEIVE_TEXT:

                ((ChatReceiverTextGroupViewHolder) viewHolder).tvReceiveMessage.setText(Html.fromHtml(list.get(i).getMessage()));
                ((ChatReceiverTextGroupViewHolder) viewHolder).rl_receiveChat.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        setClipboard(list.get(i).getMessage());
                        return true;
                    }
                });
                ((ChatReceiverTextGroupViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                String userName = checkUserName(list.get(i).getSenderId());
                String receiverTextBColor = checkUserBackName(list.get(i).getSenderId());
                String receiverTextFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(userName)) {
                    imgName = Utils.word(userName);
                    ((ChatReceiverTextGroupViewHolder) viewHolder).tvUserName.setText(userName);
                } else {
                    ((ChatReceiverTextGroupViewHolder) viewHolder).tvUserName.setVisibility(View.VISIBLE);
                }
                showStickyHeaderDate(i, ((ChatReceiverTextGroupViewHolder) viewHolder).tvStickyDate);
                Object receiveTextPath = checkUserImage(list.get(i).getSenderId());
                if (receiveTextPath != null && !receiveTextPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + receiveTextPath).into(((ChatReceiverTextGroupViewHolder) viewHolder).img_user_reciver_text_chat);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverTextGroupViewHolder) viewHolder).img_user_reciver_text_chat.setImageDrawable(imageLatter(imgName,receiverTextBColor, receiverTextFColor));

                    //   Picasso.with(mContext).load(R.drawable.contact_support1).into(((ChatReceiverTextGroupViewHolder) viewHolder).img_user_reciver_text_chat);
                }
                break;
            case TYPE_SEND_IMAGE:
                ((ChatSenderImageViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Picasso.with(mContext).load(list.get(i).getMessage()).into(((ChatSenderImageViewHolder) viewHolder).imgSend);
                //  Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderImageViewHolder) viewHolder).imgSend);

                ((ChatSenderImageViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderImageViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                ((ChatSenderImageViewHolder) viewHolder).imgSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ZoomImagePagerActivity.class);
                        intent.putExtra("type", ZoomImagePagerActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatSenderImageViewHolder) viewHolder).tvStickyDate);
                String userNameImag = checkUserName(list.get(i).getSenderId());
                String userImagBColor = checkUserBackName(list.get(i).getSenderId());
                String userImagFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(userNameImag)) {
                    imgName = Utils.word(userNameImag);
                }
                Object sendPath = checkUserImage(list.get(i).getSenderId());
                if (sendPath != null && !sendPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + sendPath).into(((ChatSenderImageViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderImageViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName,userImagBColor, userImagFColor));
                }
                break;
            case TYPE_SEND_VIDEO:
                ((ChatSenderVideoViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Glide.with(mContext).load(list.get(i).getMessage()).into(((ChatSenderVideoViewHolder) viewHolder).vv_send);
                ((ChatSenderVideoViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderVideoViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                ((ChatSenderVideoViewHolder) viewHolder).vv_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, PlayVideoActivity.class);
                        intent.putExtra("type", PlayVideoActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatSenderVideoViewHolder) viewHolder).tvStickyDate);
                String userNameVideo = checkUserName(list.get(i).getSenderId());
                String userVideoBColor = checkUserBackName(list.get(i).getSenderId());
                String userVideoFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(userNameVideo)) {
                    imgName = Utils.word(userNameVideo);
                }
                Object sendVideoPath = checkUserImage(list.get(i).getSenderId());
                if (sendVideoPath != null && !sendVideoPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + sendVideoPath).into(((ChatSenderVideoViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderVideoViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName,userVideoBColor, userVideoFColor));
                }
                break;
            case TYPE_SEND_GIPHY:
                ((ChatSenderGIphyViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderGIphyViewHolder) viewHolder).imgSend);

                ((ChatSenderGIphyViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderGIphyViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                showStickyHeaderDate(i, ((ChatSenderGIphyViewHolder) viewHolder).tvStickyDate);
                String userNameGiphy = checkUserName(list.get(i).getSenderId());
                String userGiphyBColor = checkUserBackName(list.get(i).getSenderId());
                String userGiphyFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(userNameGiphy)) {
                    imgName = Utils.word(userNameGiphy);
                }
                Object sendGiphyPath = checkUserImage(list.get(i).getSenderId());
                if (sendGiphyPath != null && !sendGiphyPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + sendGiphyPath).into(((ChatSenderGIphyViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderGIphyViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName,userGiphyBColor, userGiphyFColor));
                }
                break;

            case TYPE_SEND_FILE:
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                ((ChatSenderFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderFileViewHolder) viewHolder).tv_fileName.setText(list.get(i).getFile_name());
                //   Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderFileViewHolder) viewHolder).imgSend);

                ((ChatSenderFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                ((ChatSenderFileViewHolder) viewHolder).tv_user_name_sender_text_group_chat.setText(mContext.getResources().getString(R.string.me));
                showStickyHeaderDate(i, ((ChatSenderFileViewHolder) viewHolder).tvStickyDate);
                String userNameFile = checkUserName(list.get(i).getSenderId());
                String userFileBColor = checkUserBackName(list.get(i).getSenderId());
                String userFileFColor = checkUserFontName(list.get(i).getSenderId());

                if (Utils.isStringValid(userNameFile)) {
                    imgName = Utils.word(userNameFile);
                }
                Object sendFilePath = checkUserImage(list.get(i).getSenderId());
                if (sendFilePath != null && !sendFilePath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + sendFilePath).into(((ChatSenderFileViewHolder) viewHolder).img_user_sender_image_png);
                } else {
                    //  imageLatter(imgName);
                    ((ChatSenderFileViewHolder) viewHolder).img_user_sender_image_png.setImageDrawable(imageLatter(imgName,userFileBColor, userFileFColor));
                }
                ((ChatSenderFileViewHolder) viewHolder).imgFile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DownloadFileFromURL().execute(list.get(i).getMessage(), list.get(i).getFile_name());
                    }
                });
                break;

            case TYPE_RECEIVER_IMAGE:
                ((ChatReceiverImageGroupViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                String userName1 = checkUserName(list.get(i).getSenderId());
                String receiverImgBColor = checkUserBackName(list.get(i).getSenderId());
                String receiverImgFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(userName1)) {
                    imgName = Utils.word(userName1);
                    ((ChatReceiverImageGroupViewHolder) viewHolder).tvUserName.setText(userName1);
                } else {
                    ((ChatReceiverImageGroupViewHolder) viewHolder).tvUserName.setVisibility(View.VISIBLE);
                }

                Picasso.with(mContext).load(list.get(i).getMessage()).into(((ChatReceiverImageGroupViewHolder) viewHolder).imgReceiver);

                ((ChatReceiverImageGroupViewHolder) viewHolder).imgReceiver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ZoomImagePagerActivity.class);
                        intent.putExtra("type", ZoomImagePagerActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatReceiverImageGroupViewHolder) viewHolder).tvStickyDate);
                Object receivedPath = checkUserImage(list.get(i).getSenderId());
                if (receivedPath != null && !receivedPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + receivedPath).into(((ChatReceiverImageGroupViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverImageGroupViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName,receiverImgBColor, receiverImgFColor));
                }
                break;

            case TYPE_RECEIVER_GIPHY:
                ((ChatReceiverImageGiphyViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                String receiverGiphyPath = checkUserName(list.get(i).getSenderId());
                String receiverGiphyPathBColor = checkUserBackName(list.get(i).getSenderId());
                String receiverGiphyPathFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(receiverGiphyPath)) {
                    imgName = Utils.word(receiverGiphyPath);
                    ((ChatReceiverImageGiphyViewHolder) viewHolder).tvUserName.setText(receiverGiphyPath);
                } else {
                    ((ChatReceiverImageGiphyViewHolder) viewHolder).tvUserName.setVisibility(View.VISIBLE);
                }
                Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatReceiverImageGiphyViewHolder) viewHolder).imgReceiver);
                showStickyHeaderDate(i, ((ChatReceiverImageGiphyViewHolder) viewHolder).tvStickyDate);
                Object giphyPathObject = checkUserImage(list.get(i).getSenderId());
                if (giphyPathObject != null && !giphyPathObject.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + giphyPathObject).into(((ChatReceiverImageGiphyViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverImageGiphyViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName,receiverGiphyPathBColor, receiverGiphyPathFColor));
                }
                break;

            case TYPE_RECEIVER_VIDEO:
                ((ChatReceiverVideoViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                String receiverPathVideo = checkUserName(list.get(i).getSenderId());
                String receiverPathVideoBColor = checkUserBackName(list.get(i).getSenderId());
                String receiverPathVideoFColor = checkUserFontName(list.get(i).getSenderId());
                Glide.with(mContext).load(list.get(i).getMessage()).into(((ChatReceiverVideoViewHolder) viewHolder).vv_receiver);
                if (Utils.isStringValid(receiverPathVideo)) {
                    imgName = Utils.word(receiverPathVideo);
                    ((ChatReceiverVideoViewHolder) viewHolder).tvUserName.setText(receiverPathVideo);
                } else {
                    ((ChatReceiverVideoViewHolder) viewHolder).tvUserName.setVisibility(View.VISIBLE);
                }
                ((ChatReceiverVideoViewHolder) viewHolder).vv_play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, PlayVideoActivity.class);
                        intent.putExtra("type", PlayVideoActivity.TYPE_SINGLE);
                        intent.putExtra("image", list.get(i).getMessage());
                        intent.putExtra("imagePath", list.get(i).getFile_name());
                        mContext.startActivity(intent);
                    }
                });
                showStickyHeaderDate(i, ((ChatReceiverVideoViewHolder) viewHolder).tvStickyDate);
                Object receiverVideoPath = checkUserImage(list.get(i).getSenderId());
                if (receiverVideoPath != null && !receiverVideoPath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + receiverVideoPath).into(((ChatReceiverVideoViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverVideoViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName,receiverPathVideoBColor, receiverPathVideoFColor));
                }
                break;

            case TYPE_RECEIVER_FILE:
                StrictMode.ThreadPolicy policyReceiver = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policyReceiver);
                ((ChatReceiverFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                //   Glide.with(mContext).asGif().load(list.get(i).getMessage()).into(((ChatSenderFileViewHolder) viewHolder).imgSend);
                String receiverFile = checkUserName(list.get(i).getSenderId());
                String receiverFileBColor = checkUserBackName(list.get(i).getSenderId());
                String receiverFileFColor = checkUserFontName(list.get(i).getSenderId());
                if (Utils.isStringValid(receiverFile)) {
                    imgName = Utils.word(receiverFile);
                    ((ChatReceiverFileViewHolder) viewHolder).tvUserName.setText(receiverFile);
                } else {
                    ((ChatReceiverFileViewHolder) viewHolder).tvUserName.setVisibility(View.VISIBLE);
                }
                ((ChatReceiverFileViewHolder) viewHolder).tv_fileName.setText(list.get(i).getFile_name());
                ((ChatReceiverFileViewHolder) viewHolder).tvTime.setText(getTime(list.get(i).getTimestampCreatedLong()));
                showStickyHeaderDate(i, ((ChatReceiverFileViewHolder) viewHolder).tvStickyDate);
                Object ReceiverFilePath = checkUserImage(list.get(i).getSenderId());
                if (ReceiverFilePath != null && !ReceiverFilePath.equals("")) {
                    Picasso.with(mContext).load(DEVELOPMENT_URL + ReceiverFilePath).into(((ChatReceiverFileViewHolder) viewHolder).img_user_receiver_image_ping);
                } else {
                    //  imageLatter(imgName);
                    ((ChatReceiverFileViewHolder) viewHolder).img_user_receiver_image_ping.setImageDrawable(imageLatter(imgName, receiverFileBColor, receiverFileFColor));
                }
                ((ChatReceiverFileViewHolder) viewHolder).imgReceiver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DownloadFileFromURL().execute(list.get(i).getMessage(), list.get(i).getFile_name());
                    }
                });
                break;
        }
    }

    private TextDrawable imageLatter(String name, String backcolor, String FontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(35) /* size in px */
                .bold()// height in px
                .textColor(Color.parseColor("#" + FontColor))
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backcolor));

        return drawable;
    }

    private void setClipboard(String text) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getResources().getString(R.string.CopyText), text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, text + " " + mContext.getResources().getString(R.string.is_copy), Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("StaticFieldLeak")
    public class DownloadFileFromURL extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog = new ProgressDialog(mContext);
        String filename = "";
        String urlll = "";

        @Override
        protected String doInBackground(String... strings) {
            int count;
            try {
                filename = strings[1];
                URL url = new URL(strings[0]);
                urlll = strings[0];
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(
                        url.openStream(), 8192);
                File mFile = Utils.getOutputMediaFile(filename, mContext);
                OutputStream output = new FileOutputStream(mFile);
                byte data[] = new byte[2048];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress((int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage(mContext.getResources().getString(R.string.Downloading_Please_wait));
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            if (pDialog != null && !pDialog.isShowing()) {
                pDialog.show();
            }
        }
        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
            pDialog.setProgress(progress[0]);
        }


        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {
                String downloaded = Environment.getExternalStorageDirectory()
                        .toString() + "/" + mContext.getResources().getString(R.string.app_name) + "/" + filename;

                //     Toast.makeText(mContext, "Downloading completed.", Toast.LENGTH_SHORT).show();
                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                Intent newIntent = new Intent(Intent.ACTION_VIEW);
                String mimeType = myMime.getMimeTypeFromExtension(fileExt(urlll).substring(1));
                newIntent.setDataAndType(Uri.parse(urlll), mimeType);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                try {
                    mContext.startActivity(newIntent);
                } catch (ActivityNotFoundException e) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }


    @Override
    public int getItemCount() {
        return list.size();

    }

    public void setData(List<Chat> dailyDoseLists) {
        list = dailyDoseLists;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0) {
            if (list.get(position).getSenderId().equals(ShardPreferences.get(mContext, ShardPreferences.share_UserId_key))) {
                if (list.get(position).getMessageType().equals(Common.TYPE_TEXT)) {
                    return TYPE_SEND_TEXT;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_IMAGE)) {
                    return TYPE_SEND_IMAGE;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_GIPHY)) {
                    return TYPE_SEND_GIPHY;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_VIDEO)) {
                    return TYPE_SEND_VIDEO;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_FILE)) {
                    return TYPE_SEND_FILE;
                }
            } else {
                if (list.get(position).getMessageType().equals(Common.TYPE_TEXT)) {
                    return TYPE_RECEIVE_TEXT;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_IMAGE)) {
                    return TYPE_RECEIVER_IMAGE;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_GIPHY)) {
                    return TYPE_RECEIVER_GIPHY;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_VIDEO)) {
                    return TYPE_RECEIVER_VIDEO;
                } else if (list.get(position).getMessageType().equals(Common.TYPE_FILE)) {
                    return TYPE_RECEIVER_FILE;
                }
            }

        }
        return TYPE_DEFAULT;

    }

    public void updateTeamMemberList(List<User> memberList) {
        teamMemberList = memberList;
    }

    private String checkUserName(String senderId) {
        if (teamMemberList != null && teamMemberList.size() > 0) {
            for (User member : teamMemberList) {
                if (member.getUserId().equals(senderId)) {
                    return member.getEmployeeName();
                } else {
//                    return "";
                }
            }
        } else {
            return "";
        }
        return "";
    }

    private String checkUserBackName(String senderId) {
        if (teamMemberList != null && teamMemberList.size() > 0) {
            for (User member : teamMemberList) {
                if (member.getUserId().equals(senderId)) {
                    return member.getBgColor();
                } else {
//                    return "";
                }
            }
        } else {
            return "";
        }
        return "";
    }

    private String checkUserFontName(String senderId) {
        if (teamMemberList != null && teamMemberList.size() > 0) {
            for (User member : teamMemberList) {
                if (member.getUserId().equals(senderId)) {
                    return member.getFontColor();

                } else {
//                    return "";
                }
            }
        } else {
            return "";
        }
        return "";
    }

    private Object checkUserImage(String senderId) {
        if (teamMemberList != null && teamMemberList.size() > 0) {
            for (User member : teamMemberList) {
                if (member.getUserId().equals(senderId)) {
                    return member.getUserImage();
                }
            }
        } else {
            return "";
        }
        return "";
    }

    private void showStickyHeaderDate(int position, TextView tvDate) {
        if (position > 0) {
            String cDate = getDate(list.get(position).getTimestampCreatedLong());
            String previousDate = getDate(list.get(position - 1).getTimestampCreatedLong());
            if (cDate.equalsIgnoreCase(previousDate)) {
                tvDate.setVisibility(View.GONE);
            } else {
                tvDate.setText(getDate(list.get(position).getTimestampCreatedLong()));
                tvDate.setVisibility(View.VISIBLE);
            }
        } else {
            tvDate.setVisibility(View.VISIBLE);
            tvDate.setText(getDate(list.get(position).getTimestampCreatedLong()));
        }

    }

    private String getTime(long time) {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return sdf.format(date);
    }

    private String getDate(long time) {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        return sdf.format(date);
    }
}