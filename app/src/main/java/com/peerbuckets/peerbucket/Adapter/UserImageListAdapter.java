package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.Hpojo.UserListing;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

public class UserImageListAdapter extends RecyclerView.Adapter<UserImageListAdapter.MyViewHolder> {
    Context context;
    EmployeeIconAdaptershow userImageListAdapterShow;
    List<User> list;

    public UserImageListAdapter(Context ctx, List<User> mList) {
        context = ctx;
        list = mList;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_image_list, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String userImage = list.get(position).getUserImage();
        String userName = list.get(position).getEmployeeName();

//        if (Utils.isStringValid(userName)) {
//            holder.imageView.setImageDrawable(imageLatter(Utils.word(list.get(position).getEmployeeName()), list.get(position).getBgColor(), list.get(position).getFontColor()));
//        } else {
//            Picasso.with(context).load(DEVELOPMENT_URL + userImage).into(holder.imageView);
//
//        }

        if (!userImage.equals("") && userImage!= null) {
            Picasso.with(context).load( userImage).into(holder.imageView);
        } else {
            holder.imageView.setImageDrawable(imageLatter(Utils.word(userName), list.get(position).getBgColor(), list.get(position).getFontColor()));
        }
    }





    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40)
                .fontSize(20)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imageView;

        MyViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.img_user_image_list);

        }
    }

}