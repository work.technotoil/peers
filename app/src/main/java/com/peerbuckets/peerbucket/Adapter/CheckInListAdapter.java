package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.checkin.CheckInDetailActivity;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInListActivity;
import com.peerbuckets.peerbucket.POJO.Checkin;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;

import java.util.List;

public class CheckInListAdapter extends RecyclerView.Adapter<CheckInListAdapter.MyViewHolder> {
    Context context;
    List<Checkin> list;

    public CheckInListAdapter(Context ctx, List<Checkin> mList) {
        context = ctx;
        list = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_check_in_list, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvTime.setText(Utils.unescapeJava(list.get(position).getAskingPeople()));
        holder.tvQuestion.setText(Utils.unescapeJava(list.get(position).getQuestion()));

        if (ShardPreferences.get(context, ShardPreferences.share_language).equals("") || ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
            holder.tvQuestion.setGravity(Gravity.LEFT);

        } else {
        }
        if (list.get(position).getUsers() != null && list.get(position).getUsers().size() > 0) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            layoutManager.setOrientation(RecyclerView.HORIZONTAL);
            holder.recyclerUsers.setLayoutManager(layoutManager);
            holder.recyclerUsers.setHasFixedSize(true);
            UserImageListAdapter adapter = new UserImageListAdapter(context, list.get(position).getUsers());
            holder.recyclerUsers.setAdapter(adapter);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CheckInDetailActivity.class);
                intent.putExtra("checkInId", list.get(position).getCheckinId());
                intent.putExtra("user_id", list.get(position).getUserId());
                intent.putExtra("checkin_title", list.get(position).getQuestion());
                ((Activity) context).startActivityForResult(intent, CheckInListActivity.REQUEST_CODE_DELETE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTime, tvQuestion;
        RecyclerView recyclerUsers;

        MyViewHolder(View v) {
            super(v);
            tvTime = v.findViewById(R.id.tv_time_check_in_list);
            tvQuestion = v.findViewById(R.id.tv_question_check_in_list);
            recyclerUsers = v.findViewById(R.id.recycler_assigned_check_in_list);
        }
    }
}