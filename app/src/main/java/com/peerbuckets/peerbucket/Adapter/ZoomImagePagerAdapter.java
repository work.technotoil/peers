package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.peerbuckets.peerbucket.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ZoomImagePagerAdapter extends PagerAdapter {

    private List<String> list;
    private List<String> mImageFullList;
    private LayoutInflater inflater;
    private Context mContext;

    public ZoomImagePagerAdapter(Context context, List<String> mList) {
        this.list = mList;
        this.mImageFullList = mImageFullList;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        ImageView image;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.layout_zoom_image, container,
                false);

        image = itemView.findViewById(R.id.img_show_image);
        (container).addView(itemView);

        if (!list.get(position).equals("")) {

            Picasso.with(mContext).load(list.get(position))
                    .into(image);
        }
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        (container).removeView((RelativeLayout) object);

    }


}