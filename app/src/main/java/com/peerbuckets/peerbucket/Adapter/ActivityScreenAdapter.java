package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.VolleyError;

import com.peerbuckets.peerbucket.Activity.checkin.CheckInDetailActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocumentDetailActivity;
import com.peerbuckets.peerbucket.Activity.message.ExpandMessageBoardActivity;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleDetailActivity;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoActivity;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoSubTaskActivity;
import com.peerbuckets.peerbucket.POJO.LatestActivityPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;

import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.util.List;


import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class ActivityScreenAdapter extends RecyclerView.Adapter<ActivityScreenAdapter.ViewHolder> implements IApiResponse {

    private Context context;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    public int globalPosition = 0;
    //List to store all mLatestActivityList
    List<LatestActivityPOJO> list;
    String mcompanyId = "", muserId = "", memployeeId = "", mcurrentProjectTeamId = "",
            mcurrentCompanyName = "", maddremovetype = "test_hq", mCurrentuserId = "", firstName = "", lastName = "";

    //Constructor of this class
    public ActivityScreenAdapter(List<LatestActivityPOJO> mLatestActivityList, Context context) {
        super();
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        //Getting all mLatestActivityList
        this.list = mLatestActivityList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_activity_screen, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final LatestActivityPOJO mlist = list.get(position);

        //Loading image from url
        //  holder.mactivity_date.setText(LatestActivityList.getDate());
        holder.mactivity_title.setText(Utils.unescapeJava(mlist.getProjectTeamName().substring(0, 1).toUpperCase() + mlist.getProjectTeamName().substring(1)));
        holder.mactivity_time.setText(mlist.getDate() + " " + mlist.getTime());
        if (mlist.getUser_image() != null && !mlist.getUser_image().equals("")) {
            Picasso.with(context).load(mlist.getUser_image()).into(holder.imgUser);
        } else {

            holder.imgUser.setImageDrawable(imageLatter(Utils.word(mlist.getEmployee_name()), mlist.getBgColor(), mlist.getFontColor()));
        }

        showStickyHeaderDate(position, holder.mactivity_date);

        holder.rl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToSpecificScreen(mlist.getProjectTeamType(), mlist.getProjectTeamId(), mlist.getTitle(), mlist.getUserId(), mlist.getProjectTeamId(), mlist.getTitle());

            }
        });

        String title = "";
        if (list.get(position).getProjectTeamType().equals("test_hq") ||
                list.get(position).getProjectTeamType().equals("team") ||
                list.get(position).getProjectTeamType().equals("project") ||
                list.get(position).getProjectTeamType().equals("company") ||
                list.get(position).getProjectTeamType().equals("checkin")) {

            title = list.get(position).getEmployee_name() +context.getResources().getString(R.string.started_a_new);

            if (list.get(position).getProjectTeamType().equals("test_hq")) {
                holder.activity_text.setText(Utils.unescapeJava(title + context.getResources().getString(R.string.company_called)));
            } else {
                holder.activity_text.setText(Utils.unescapeJava(title + (list.get(position).getProjectTeamType()) +context.getResources().getString(R.string.called)));
            }
        } else if (list.get(position).getProjectTeamType().equals("timezone")) {
            holder.activity_text.setText(Utils.unescapeJava((list.get(position).getEmployee_name()) +context.getResources().getString(R.string.change_our_timezone)));

        } else if (list.get(position).getProjectTeamType().equals("message")) {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + context.getResources().getString(R.string.post_a_new_message)));

        } else if (list.get(position).getProjectTeamType().equals("to-dos")) {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + " " + list.get(position).getTitle()));

        } else if (list.get(position).getProjectTeamType().equals("schedule")) {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + " " + list.get(position).getTitle()));

        } else if (list.get(position).getProjectTeamType().equals("checkin")) {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + " " + list.get(position).getTitle()));

        } else if (list.get(position).getProjectTeamType().equals("comment") || list.get(position).getProjectTeamType().equals("checkin_reply")) {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + " " + list.get(position).getTitle()));

        } else if (list.get(position).getProjectTeamType().equals("sub-todo")) {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + " " + list.get(position).getTitle()));

        } else if (list.get(position).getProjectTeamType().equals("sub-to-do-mark-as-done")) {
            holder.activity_text.setText(list.get(position).getTitle());
        } else {
            holder.activity_text.setText(Utils.unescapeJava(list.get(position).getEmployee_name() + " " + list.get(position).getDescription()));
        }
        if (mlist.getProjectTeamType().equals("checkin")) {
            holder.activity_text.setVisibility(View.VISIBLE);
            holder.mActivity_content.setVisibility(View.GONE);
        } else {
            holder.activity_text.setVisibility(View.VISIBLE);
            holder.mActivity_content.setVisibility(View.VISIBLE);

            String newString = mlist.getDescription();

            Spanned spanned = Html.fromHtml(newString);
            char[] chars = new char[spanned.length()];
            TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
            String plainText = new String(chars);

            newString = plainText.replace("\"", "'");
            Utils.setDescription(Utils.unescapeJava(newString), holder.mActivity_content, context);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private TextDrawable imageLatter(String name, String backColor, String FontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(90)  // width in px
                .height(90)
                .fontSize(30)
                .textColor(Color.parseColor("#" + FontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public TextView mactivity_date, mactivity_title, mactivity_time, activity_text;
        WebView mActivity_content;
        ImageView imgUser;
        RelativeLayout rl_click;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);
            mactivity_date = itemView.findViewById(R.id.activity_date);
            mactivity_title = itemView.findViewById(R.id.activity_title);
            mActivity_content = itemView.findViewById(R.id.activity_content);
            mActivity_content.getSettings().setJavaScriptEnabled(true);
            mActivity_content.setLayerType(WebView.LAYER_TYPE_NONE, null);
            mactivity_time = itemView.findViewById(R.id.activity_time);
            activity_text = itemView.findViewById(R.id.activity_text);
            imgUser = itemView.findViewById(R.id.imgUser);
            rl_click = itemView.findViewById(R.id.rl_click);
        }
    }

    private void showStickyHeaderDate(int position, TextView tvLabel) {
        if (position > 0) {
            String cIsRead = list.get(position).getDate();
            String previouscIsRead = list.get(position - 1).getDate();
            if (cIsRead.equalsIgnoreCase(previouscIsRead)) {
                tvLabel.setVisibility(View.GONE);
            } else {
                tvLabel.setText(list.get(position).getDate());
                tvLabel.setVisibility(View.VISIBLE);
            }
        } else {
            boolean isNewLabelDispay = false;
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getDate().equals("")) {
                        isNewLabelDispay = true;
                        break;
                    }
                }
            }
            tvLabel.setVisibility(View.VISIBLE);
            if (isNewLabelDispay) {
                tvLabel.setText(list.get(position).getDate());
            } else {
                tvLabel.setText(list.get(position).getDate());
            }
        }
    }

    public void setNotofyData(List<LatestActivityPOJO> mList) {
        list.addAll(mList);
        notifyDataSetChanged();
    }

    public void sendToSpecificScreen(String notificationType, String record_id, String title, String user_id, String projectID, String subType) {
        ShardPreferences.save(context, ShardPreferences.share_current_project_team_id, projectID);

        if (notificationType.equals("checkin")) {
            Intent i1 = new Intent(context, CheckInDetailActivity.class);
            i1.putExtra("checkInId", record_id);
            i1.putExtra("checkin_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("checkin_reply")) {
            Intent i1 = new Intent(context, CheckInDetailActivity.class);
            i1.putExtra("check_in_reply_id", record_id);
            i1.putExtra("check_in_question", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("message")) {
            Intent i1 = new Intent(context, ExpandMessageBoardActivity.class);
            i1.putExtra("message_id", record_id);
            context.startActivity(i1);
        } else if (notificationType.equals("schedule")) {
            Intent i1 = new Intent(context, ScheduleDetailActivity.class);
            i1.putExtra("schedule_id", record_id);
            i1.putExtra("schedule_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("todo_list")) {
            Intent i1 = new Intent(context, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);

            context.startActivity(i1);
        } else if (notificationType.equals("doc")) {
            Intent i1 = new Intent(context, DocumentDetailActivity.class);
            i1.putExtra("document_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        }

        //comment with subType
        else if (notificationType.equals("comment") && subType.equals("message")) {
            Intent i1 = new Intent(context, ExpandMessageBoardActivity.class);
            i1.putExtra("message_id", record_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("sub-todo")) {
            Intent i1 = new Intent(context, ExpandToDoSubTaskActivity.class);
            i1.putExtra("check_in_reply_id", record_id);
            i1.putExtra("check_in_question", title);
            context.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("checkin")) {
            Intent i1 = new Intent(context, CheckInDetailActivity.class);
            i1.putExtra("checkInId", record_id);
            i1.putExtra("checkin_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("document")) {
            Intent i1 = new Intent(context, DocumentDetailActivity.class);
            i1.putExtra("document_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("todo_list")) {
            Intent i1 = new Intent(context, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment") && subType.equals("schedule")) {
            Intent i1 = new Intent(context, ScheduleDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        }

        //contents intent
        else if (notificationType.equals("message_mention")) {
            Intent i1 = new Intent(context, ExpandMessageBoardActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("todo_mention")) {
            Intent i1 = new Intent(context, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("checkin_reply_mention")) {
            Intent i1 = new Intent(context, CheckInDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("sub-todo")) {
            Intent i1 = new Intent(context, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("sub-to-do-mark-as-done")) {
            Intent i1 = new Intent(context, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        }

        //contents comment mention with subType
        else if (notificationType.equals("comment_mention") && subType.equals("message")) {
            Intent i1 = new Intent(context, ExpandMessageBoardActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("sub-todo")) {
            Intent i1 = new Intent(context, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("checkin")) {
            Intent i1 = new Intent(context, CheckInDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("document")) {
            Intent i1 = new Intent(context, DocumentDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("todo_list")) {
            Intent i1 = new Intent(context, ExpandToDoActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("schedule")) {
            Intent i1 = new Intent(context, ScheduleDetailActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        } else if (notificationType.equals("comment_mention") && subType.equals("todo_list_task")) {
            Intent i1 = new Intent(context, ExpandToDoSubTaskActivity.class);
            i1.putExtra("to_do_id", record_id);
            i1.putExtra("to_do_title", title);
            i1.putExtra("user_id", user_id);
            context.startActivity(i1);
        }
    }

}