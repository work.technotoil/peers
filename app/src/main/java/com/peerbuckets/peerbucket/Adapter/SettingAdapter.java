package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.HomeActivity;
import com.peerbuckets.peerbucket.Activity.SettingActivity;
import com.peerbuckets.peerbucket.POJO.SettingListResponce.ModuleListing;
import com.peerbuckets.peerbucket.POJO.SettingListResponce.SettingListResponse;
import com.peerbuckets.peerbucket.POJO.SettingUpdateResponse.SettingUpdateResponse;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.SettingAdapterHolder> {
    private Context context;
    private ArrayList<ModuleListing> mList;
    int pos = -1;
    ProgressBar pb_progress;

    public SettingAdapter(Context ctx, ArrayList<ModuleListing> list) {
        mList = list;
        context = ctx;
    }

    @NonNull
    @Override
    public SettingAdapter.SettingAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_setting, parent, false);
        return new SettingAdapterHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SettingAdapter.SettingAdapterHolder holder, final int position) {
        holder.tv_activity.setText(mList.get(position).getModuleName());
        holder.tv_settingContent.setText(mList.get(position).getDescription());

        if (mList.get(position).getSelected()) {
            holder.ll_btnOn.setVisibility(View.VISIBLE);
            holder.ll_btnOff.setVisibility(View.GONE);
        } else {
            holder.ll_btnOff.setVisibility(View.VISIBLE);
            holder.ll_btnOn.setVisibility(View.GONE);
        }

        if (mList.get(position).getIsVisible().equals("0")) {
            holder.ll_btnOff.setVisibility(View.VISIBLE);
            holder.ll_btnOn.setVisibility(View.GONE);
        } else {
            holder.ll_btnOn.setVisibility(View.VISIBLE);
            holder.ll_btnOff.setVisibility(View.GONE);
        }



       /* holder.fl_Off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                holder.ll_btnOn.setVisibility(View.VISIBLE);
                holder.ll_btnOff.setVisibility(View.GONE);
                callApiOffOn(1);
            }
        });*/

        holder.ll_btnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                mList.get(position).setSelected(false);
                mList.get(position).setIsVisible("0");
       /*         holder.ll_btnOn.setVisibility(View.VISIBLE);
                holder.ll_btnOff.setVisibility(View.GONE);*/
                callApiOffOn(1);
                notifyDataSetChanged();
            }
        });

       /* holder.fl_On.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                holder.ll_btnOff.setVisibility(View.VISIBLE);
                holder.ll_btnOn.setVisibility(View.GONE);
                callApiOffOn(0);
            }
        });*/

        holder.ll_btnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                mList.get(position).setSelected(true);
                mList.get(position).setIsVisible("1");
          /*       holder.ll_btnOff.setVisibility(View.VISIBLE);
                holder.ll_btnOn.setVisibility(View.GONE);*/
                callApiOffOn(0);
                notifyDataSetChanged();


            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class SettingAdapterHolder extends RecyclerView.ViewHolder {
        TextView tv_activity, tv_settingContent;
        TextView btn_Off, btn_On;
        RelativeLayout ll_btnOn, ll_btnOff;
        FrameLayout fl_Off, fl_On;


        public SettingAdapterHolder(@NonNull View itemView) {
            super(itemView);

           /* fl_Off = itemView.findViewById(R.id.fl_Off);
            fl_On = itemView.findViewById(R.id.fl_On);*/
            pb_progress = itemView.findViewById(R.id.pb_progress);
            ll_btnOn = itemView.findViewById(R.id.ll_btnOn);
            ll_btnOff = itemView.findViewById(R.id.ll_btnOff);
            btn_Off = itemView.findViewById(R.id.btn_Off);
            btn_On = itemView.findViewById(R.id.btn_On);
            tv_activity = itemView.findViewById(R.id.tv_activity);
            tv_settingContent = itemView.findViewById(R.id.tv_settingContent);
        }
    }

    private void callApiOffOn(final int i) {
        pb_progress.setVisibility(View.VISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("user_id", ShardPreferences.get(context, ShardPreferences.share_current_UserId_key));
        map.put("module_id", mList.get(pos).getModuleIdPK());
        map.put("isVisible", i + "");

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SettingUpdateResponse> resultCall = apiInterface.callSettingUpdate(map);
        resultCall.enqueue(new Callback<SettingUpdateResponse>() {

            @Override
            public void onResponse(Call<SettingUpdateResponse> call, Response<SettingUpdateResponse> response) {
                pb_progress.setVisibility(View.GONE);
                if (response.body().getStatus()) {
                    pb_progress.setVisibility(View.GONE);
                    mList.get(pos).setIsVisible(i + "");
                    notifyDataSetChanged();
                } else {
                    pb_progress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<SettingUpdateResponse> call, Throwable t) {
                pb_progress.setVisibility(View.GONE);
            }
        });
    }
}
