package com.peerbuckets.peerbucket.Adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peerbuckets.peerbucket.R;

public class DateEventAdapter extends RecyclerView.Adapter<DateEventAdapter.MyViewHolder> {

    public DateEventAdapter() {

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_date_events, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        MyViewHolder(View v) {
            super(v);
            cardView = v.findViewById(R.id.card_date_event);
        }

    }
}