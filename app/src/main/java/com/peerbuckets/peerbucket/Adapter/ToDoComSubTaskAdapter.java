package com.peerbuckets.peerbucket.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoActivity;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoSubTaskActivity;
import com.peerbuckets.peerbucket.POJO.ToDoSubTaskPOJO;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.MASK_SUB_TO_DO_COMPLETE;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class ToDoComSubTaskAdapter extends RecyclerView.Adapter<ToDoComSubTaskAdapter.ViewHolder> implements IApiResponse {

    private Context context;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    public int globalPosition = 0;
    private List<ToDoSubTaskPOJO> mToDoSubTaskList;
    String mcompanyId = "", muserId = "", memployeeId = "",
            mtimezone = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "",
            maddremovetype = "test_hq", mCurrentuserId = "";

    //Constructor of this class
    public ToDoComSubTaskAdapter(List<ToDoSubTaskPOJO> mToDoSubTaskList, Context context) {
        super();
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        //Getting all mToDoSubTaskList
        this.mToDoSubTaskList = mToDoSubTaskList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_com_sub_task, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String userData = "";
        //Getting the particular item from the list
        final ToDoSubTaskPOJO ToDoSubTaskList = mToDoSubTaskList.get(position);
        if (ToDoSubTaskList.getUsers() != null && ToDoSubTaskList.getUsers().size() > 0) {

            for (User userList : ToDoSubTaskList.getUsers()) {
                userData = userData + Utils.createUserHtml(userList.getEmployeeName(), userList.getUserImage(), userList.getBgColor(), userList.getFontColor());
            }
        }
        holder.webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(context, ExpandToDoSubTaskActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("sub_task_title", ToDoSubTaskList.getTodoListTaskTitle());
                intent.putExtra("to_do_title", ((ExpandToDoActivity) context).getTo_do_title());
                intent.putExtra("sub_task_desc", mToDoSubTaskList.get(position).getTodoListTaskDescription());
                intent.putExtra("sub_task_start_date", mToDoSubTaskList.get(position).getTodoListTaskStartDate());
                intent.putExtra("sub_task_selector", mToDoSubTaskList.get(position).getTodoListTaskStartDate());
                intent.putExtra("sub_task_id", mToDoSubTaskList.get(position).getTodoListTaskId());
                intent.putExtra("email", ((ExpandToDoActivity) context).getEmail());
                intent.putExtra("users", ((ExpandToDoActivity) context).getAll_users_id());
                intent.putExtra("checked_type", true);
                intent.putExtra("user_id", mToDoSubTaskList.get(position).getUserId());
                context.startActivity(intent);
                return true;
            }
        });
        Utils.setDescriptionWithDate(Utils.unescapeJava(ToDoSubTaskList.getTodoListTaskTitle()), ToDoSubTaskList.getTodoListTaskStartDate(), userData, holder.webView, context);
        holder.mselction_cb.setChecked(true);
        holder.mselction_cb.setEnabled(true);
        holder.mselction_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (!isChecked) {
                        holder.mselction_cb.setEnabled(false);
                        Map<String, String> paramsReq = new HashMap<>();
                        paramsReq.put("todo_task_id", ToDoSubTaskList.getTodoListTaskId());
                        paramsReq.put("status", "0");
                        paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                        paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                        paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                        mApiRequest.postRequestBackground(BASE_URL + MASK_SUB_TO_DO_COMPLETE, MASK_SUB_TO_DO_COMPLETE, paramsReq, Request.Method.POST);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mToDoSubTaskList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(MASK_SUB_TO_DO_COMPLETE)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    ((ExpandToDoActivity) context).refreshUnCompleteTask(mToDoSubTaskList.get(globalPosition));
                    mToDoSubTaskList.remove(globalPosition);
                    notifyItemRemoved(globalPosition);
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        LinearLayout msub_task_section_ll;
        CheckBox mselction_cb;
        WebView webView;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);
            webView = itemView.findViewById(R.id.sub_task_title);
            msub_task_section_ll = itemView.findViewById(R.id.sub_task_section_ll);
            mselction_cb = itemView.findViewById(R.id.selction_cb);

            fontTF = Typeface.createFromAsset(context.getAssets(), "font/fawsmsolid.ttf");
        }
    }

    public void addItem(ToDoSubTaskPOJO taskPOJO) {
        mToDoSubTaskList.add(0, taskPOJO);
        notifyDataSetChanged();
    }

    public void setItems(ArrayList<ToDoSubTaskPOJO> items) {
        //mToDoSubTaskList.clear();
        //mToDoSubTaskList.addAll(items);
        notifyDataSetChanged();
    }
}