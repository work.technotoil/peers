package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.Activity.AddRemovePeopleActivity;
import com.peerbuckets.peerbucket.Activity.ChatActivity;
import com.peerbuckets.peerbucket.Activity.CompanyHQExpandActivity;
import com.peerbuckets.peerbucket.Activity.CreateTeamActivity;
import com.peerbuckets.peerbucket.Activity.HomeActivity;
import com.peerbuckets.peerbucket.Activity.TrashActivity;
import com.peerbuckets.peerbucket.Activity.checkin.CheckInListActivity;
import com.peerbuckets.peerbucket.Activity.docs.DocsAndFilesActivity;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleActivity;
import com.peerbuckets.peerbucket.Activity.todo.ToDoActivity;
import com.peerbuckets.peerbucket.Hpojo.HomeModule;
import com.peerbuckets.peerbucket.Hpojo.ModuleList;
import com.peerbuckets.peerbucket.Hpojo.UserListing;
import com.peerbuckets.peerbucket.POJO.ProjectTeamTrash.NewTrashTeamResponse;
import com.peerbuckets.peerbucket.POJO.deleteTrash.TrashDeleteResponse;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.retrofit.ApiClient;
import com.peerbuckets.peerbucket.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_addd_remove_current_type;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_module;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;

public class IteamShowAdapter extends RecyclerView.Adapter<IteamShowAdapter.IteamShowAdapterHolder> implements Filterable {
    EmployeeIconAdaptershow userImageListAdapterShow;
    private Context Mcontext;
    private ArrayList<HomeModule> mList;
    private ArrayList<HomeModule> mDisplayedValues;
    private LinearLayout mdialog_tcp_chat, mdialog_tcp_message_board, mdialog_tcp_automatic_checkins, mdialog_tcp_docs_and_files,
            mdialog_tcp_todos, mdialog_tcp_schedule;
    PopupMenu popup;
    int pos = -1;

    public IteamShowAdapter(Context ctx, ArrayList<HomeModule> list) {
        Mcontext = ctx;
        mDisplayedValues = list;
        mList = list;
    }

    @NonNull
    @Override
    public IteamShowAdapter.IteamShowAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_show_homedetails, parent, false);
        return new IteamShowAdapterHolder(v);
    }

    public void subCheck() {
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull final IteamShowAdapter.IteamShowAdapterHolder holder, final int position) {
        holder.tv_nameModule.setText(Utils.unescapeJava(mList.get(position).getProjectTeamName()));
        holder.tv_shortDisc.setText(Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));

        holder.tv_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                showPopup(view);
            }
        });

        module(mList.get(position).getUserListing(), holder.user_icon);

        holder.cv_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Mcontext);
                View dialogView = LayoutInflater.from(Mcontext).inflate(R.layout.dialog_tcp_expand, null);
                dialogBuilder.setView(dialogView);

                ImageView imageView = dialogView.findViewById(R.id.expand_company_hq);
                TextView addorRemtextView = dialogView.findViewById(R.id.add_remove);
                TextView mcompany_expand_name_tv = dialogView.findViewById(R.id.company_expand_name_tv);
                TextView mcompany_desc_expand_tv = dialogView.findViewById(R.id.company_desc_expand_tv);

                mcompany_expand_name_tv.setText(Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                mcompany_desc_expand_tv.setText(Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));

                mdialog_tcp_chat = dialogView.findViewById(R.id.dialog_tcp_chat);
                mdialog_tcp_message_board = dialogView.findViewById(R.id.dialog_tcp_message_board);
                mdialog_tcp_automatic_checkins = dialogView.findViewById(R.id.dialog_tcp_automatic_checkins);
                mdialog_tcp_docs_and_files = dialogView.findViewById(R.id.dialog_tcp_docs_and_files);
                mdialog_tcp_todos = dialogView.findViewById(R.id.dialog_tcp_todos);
                mdialog_tcp_schedule = dialogView.findViewById(R.id.dialog_tcp_schedule);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();

                addorRemtextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_current_module, Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        Intent intent = new Intent(Mcontext, AddRemovePeopleActivity.class);
                        intent.putExtra("Directions", Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        intent.putExtra("ProjectTeamId", mList.get(position).getProjectTeamId());
                        ((Activity) Mcontext).startActivityForResult(intent, 11);
                        alertDialog.dismiss();
                    }
                });


                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(Mcontext, share_current_module, mList.get(position).getProjectTeamName());
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        Intent intent = new Intent(Mcontext, CompanyHQExpandActivity.class);
                        intent.putExtra("description", Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));
                        intent.putExtra("Directions", Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        intent.putExtra("projectId", mList.get(position).getProjectTeamId());
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_current_module, Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        Intent intent = new Intent(Mcontext, ChatActivity.class);
                        intent.putExtra("description", Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));
                        intent.putExtra("projectId", mList.get(position).getProjectTeamId());
                        intent.putExtra("name", Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_message_board.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Mcontext, Activity_MessageBoard.class);
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_module, mList.get(position).getProjectTeamName());
                        intent.putExtra("description", Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_automatic_checkins.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Toast.makeText(Mcontext, Mcontext.getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Mcontext, CheckInListActivity.class);
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_module, mList.get(position).getProjectTeamName());
                        intent.putExtra("description", mList.get(position).getProjectTeamDescription());
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_docs_and_files.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                   //     Toast.makeText(Mcontext, Mcontext.getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(Mcontext, DocsAndFilesActivity.class);
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_module, mList.get(position).getProjectTeamName());
                        intent.putExtra("description", mList.get(position).getProjectTeamDescription());
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_todos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_module, Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        Intent intent = new Intent(Mcontext, ToDoActivity.class);
                        intent.putExtra("description", Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });

                mdialog_tcp_schedule.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(Mcontext, Mcontext.getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();

                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_project_team_id, mList.get(position).getProjectTeamId());
                        ShardPreferences.save(Mcontext, share_addd_remove_current_type, "test_hq");
                        ShardPreferences.save(Mcontext, share_current_module, mList.get(position).getProjectTeamName());
                        Intent intent = new Intent(Mcontext, ScheduleActivity.class);
                        intent.putExtra("description", Utils.unescapeJava(mList.get(position).getProjectTeamDescription()));
                        intent.putExtra("projectId", mList.get(position).getProjectTeamId());
                        intent.putExtra("name", Utils.unescapeJava(mList.get(position).getProjectTeamName()));
                        Mcontext.startActivity(intent);
                        alertDialog.dismiss();

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mList = (ArrayList<HomeModule>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<HomeModule> FilteredArrList = new ArrayList<HomeModule>();

                if (mDisplayedValues == null) {
                    mDisplayedValues = new ArrayList<HomeModule>(mList); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mDisplayedValues.size();
                    results.values = mDisplayedValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mDisplayedValues.size(); i++) {
                        String data = mDisplayedValues.get(i).getProjectTeamName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            HomeModule mModel = new HomeModule();
                            mModel.setProjectTeamName(data);
                            mModel.setProjectTeamDescription(mDisplayedValues.get(i).getProjectTeamDescription());
                            mModel.setProjectTeamId(mDisplayedValues.get(i).getProjectTeamId());
                            mModel.setUserListing(mDisplayedValues.get(i).getUserListing());
                            FilteredArrList.add(mModel);

                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class IteamShowAdapterHolder extends RecyclerView.ViewHolder {
        RecyclerView user_icon;
        RecyclerView.LayoutManager user_iconProjrctLayoutManager;
        CardView cv_project, cv_icon;
        ImageView img_creator;
        TextView tv_shortDisc, tv_nameModule, tv_icon, cv_icon_vert, tv_edit, tv_trash;

        public IteamShowAdapterHolder(@NonNull View itemView) {
            super(itemView);
            user_icon = itemView.findViewById(R.id.user_icon);
            tv_edit = itemView.findViewById(R.id.tv_edit);
            tv_trash = itemView.findViewById(R.id.tv_trash);
            cv_icon_vert = itemView.findViewById(R.id.cv_icon_vert);
            cv_icon = itemView.findViewById(R.id.cv_icon);
            cv_project = itemView.findViewById(R.id.cv_project);
            tv_icon = itemView.findViewById(R.id.tv_icon);
            img_creator = itemView.findViewById(R.id.img_creator);
            tv_shortDisc = itemView.findViewById(R.id.tv_shortDisc);
            tv_nameModule = itemView.findViewById(R.id.tv_nameModule);

            user_icon.setHasFixedSize(true);
            user_iconProjrctLayoutManager = new LinearLayoutManager(Mcontext, LinearLayoutManager.HORIZONTAL, false);
            user_icon.setLayoutManager(user_iconProjrctLayoutManager);
        }
    }

    private void module(List<UserListing> list, RecyclerView rv_list) {
        if (list != null && list.size() > 0) {
            userImageListAdapterShow = new EmployeeIconAdaptershow(Mcontext, (ArrayList<UserListing>) list, 1);
            rv_list.setAdapter(userImageListAdapterShow);
        } else {
            list = new ArrayList<>();
            userImageListAdapterShow = new EmployeeIconAdaptershow(Mcontext, (ArrayList<UserListing>) list, 1);
            rv_list.setAdapter(userImageListAdapterShow);
        }
    }

    public void showPopup(View v) {
        popup = new PopupMenu(Mcontext, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_trash_popup, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.edit:
                        editProjectTeam();
                        return true;
                    case R.id.trash:
                        DeleteProject();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void editProjectTeam() {
        Intent intent = new Intent(Mcontext, CreateTeamActivity.class);
        intent.putExtra("name", Utils.unescapeJava(mList.get(pos).getProjectTeamName()));
        intent.putExtra("description", Utils.unescapeJava(mList.get(pos).getProjectTeamDescription()));
        intent.putExtra("projectTeamId", mList.get(pos).getProjectTeamId());
        ((HomeActivity) Mcontext).startActivityForResult(intent, 101);
    }

    private void DeleteProject() {
        Map<String, String> paramsReq = new HashMap<>();
        paramsReq.put("companyId", ShardPreferences.get(Mcontext, ShardPreferences.key_company_Id));
        paramsReq.put("status", "trash");
        paramsReq.put("type", "team");
        paramsReq.put("projectTeamId", mList.get(pos).getProjectTeamId());
        paramsReq.put("userId", ShardPreferences.get(Mcontext, share_current_UserId_key));
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NewTrashTeamResponse> resultCall = apiInterface.callTrashTeam(paramsReq);
        resultCall.enqueue(new Callback<NewTrashTeamResponse>() {

            @Override
            public void onResponse(Call<NewTrashTeamResponse> call, Response<NewTrashTeamResponse> response) {

                if (response.body().getStatus().equals("true")) {
                    mList.remove(pos);
                    notifyDataSetChanged();
                    Intent intent = new Intent(Mcontext, TrashActivity.class);
                    ((Activity) Mcontext).startActivityForResult(intent, 101);
                } else {
                }
            }

            @Override
            public void onFailure(Call<NewTrashTeamResponse> call, Throwable t) {
            }
        });
    }

}
