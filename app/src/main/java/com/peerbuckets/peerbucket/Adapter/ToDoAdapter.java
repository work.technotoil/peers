package com.peerbuckets.peerbucket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoActivity;
import com.peerbuckets.peerbucket.Activity.todo.ToDoActivity;
import com.peerbuckets.peerbucket.POJO.ToDoList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.MyViewHolder> {

    private Context context;
    private List<ToDoList> list = new ArrayList<>();

    public ToDoAdapter(List<ToDoList> mList, Context mContext) {
        context = mContext;
        list = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_todo, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (list.get(position).getTodoListTitle() != null && !list.get(position).getTodoListTitle().equals("")) {
            holder.tvTitle.setText(Utils.unescapeJava(list.get(position).getTodoListTitle()));
        }
        if (ShardPreferences.get(context, ShardPreferences.share_language).equals("2")) {
            holder.webViewDesc.loadData("<body dir=\"rtl\"><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + Utils.unescapeJava(list.get(position).getTodo_list_desc_mobile()), "text/html; charset=utf-8", null);
        }else {
            holder.webViewDesc.loadData("<body dir=\"ltr\" ><style>img{display: inline;height: auto;max-width: 100%;}</style></body>" + Utils.unescapeJava(list.get(position).getTodo_list_desc_mobile()), "text/html; charset=utf-8", null);
        }
         holder.tvCompletedWork.setText(list.get(position).getTotalTask());
        if (list.get(position).getTotalTask() != null && list.get(position).getCompletedTask() != null) {
            holder.tvCompletedWork.setText(list.get(position).getCompletedTask() + " / " + list.get(position).getTotalTask() + " Completed");
        }
        holder.layout_constraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExpandToDoActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                intent.putExtra("to_do_id", list.get(position).getTodoListId());
                intent.putExtra("to_do_title", list.get(position).getTodoListTitle());
                intent.putExtra("to_do_desc", list.get(position).getTodoListDesc());
                intent.putExtra("position", position);
                ((Activity) context).startActivityForResult(intent, ToDoActivity.REQUEST_CODE_DELETE);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExpandToDoActivity.class);
                intent.putExtra("user_id", list.get(position).getUser_id());
                intent.putExtra("to_do_id", list.get(position).getTodoListId());
                intent.putExtra("to_do_title", list.get(position).getTodoListTitle());
                intent.putExtra("to_do_desc", list.get(position).getTodoListDesc());
                intent.putExtra("position", position);
                ((Activity) context).startActivityForResult(intent, ToDoActivity.REQUEST_CODE_DELETE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setNotifyData(List<ToDoList> mList) {
        list.addAll(mList);
        notifyDataSetChanged();
    }

    public void addNewItem(ToDoList data) {
        if (data != null) {
            list.add(0, data);
            notifyDataSetChanged();

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvCompletedWork;
        WebView webViewDesc;
        ConstraintLayout layout_constraint;

        MyViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_title_todo_list);
            layout_constraint = v.findViewById(R.id.layout_constraint);
            webViewDesc = v.findViewById(R.id.tv_message_todo_list);
            tvCompletedWork = v.findViewById(R.id.tv_completed_todo_list);
        }
    }
}