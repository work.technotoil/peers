package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.UserDialog;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;

/**
 * Created by Belal on 11/9/2015.
 */
public class MinEmployeeIconAdapter extends RecyclerView.Adapter<MinEmployeeIconAdapter.ViewHolder> implements IApiResponse {

    private Context context;
    private ApiRequest mApiRequest;
    int value;
    public static int vItem = 0;
    Typeface fontTF;
    String mcompanyId = "", muserId = "", memployeeId = "", maddremovetype = "",
            mcurrentProjectTeamId = "", mcurrentCompanyName = "", firstName = "", lastName = "";
    String[] COLORArray = {"black", "blue", "green", "white", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    public int selected = 0;
    int extra = 0;

    int listSize = 0;

    //List to store all mProjectList
    List<EmployeeDetalPOJO> mEmployeeList;

    //Constructor of this class
    public MinEmployeeIconAdapter(List<EmployeeDetalPOJO> mEmployeeList, Context context, int i, int j) {
        super();
        //Getting all mProjectList
        mApiRequest = new ApiRequest(context, (IApiResponse) this);
        this.mEmployeeList = mEmployeeList;
        this.context = context;
        this.value = i;
        listSize = mEmployeeList.size();
        value = i;
        extra = j;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_employee_icons, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final EmployeeDetalPOJO EmployeeList = mEmployeeList.get(position);
        selected = holder.random.nextInt(COLORArray.length);
        String COLORString = COLORArray[selected];
        String firstLetter = EmployeeList.getEmployeeName();


        if (position >= 3) {
            firstLetter = "+" + (listSize - 3);

            String mColor = COLORArray[0];
            holder.drawable = TextDrawable.builder().buildRound(firstLetter.toUpperCase(), Color.parseColor(mColor));
            holder.memployee_icon_iv.setImageDrawable(imageLatter(firstLetter, "000000", "ffffff"));
        } else {
            if (!EmployeeList.getUserImage().equals("") && EmployeeList.getUserImage() != null) {
                Picasso.with(context).load(EmployeeList.getUserImage()).into(holder.memployee_icon_iv);
            } else {
                holder.memployee_icon_iv.setImageDrawable(imageLatter(Utils.word(EmployeeList.getEmployeeName()), EmployeeList.getBgColor(), EmployeeList.getFontColor()));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (value == 0) {
                    UserDialog.otherUserDialog(context, mEmployeeList.get(position).getUserId(), mEmployeeList.get(position).getEmployeeName(), mEmployeeList.get(position).getUserImage(), "GMT", mEmployeeList.get(position).getBgColor(), mEmployeeList.get(position).getFontColor());
                } else {
                    ShardPreferences.save(context, ShardPreferences.key_EmpName, mEmployeeList.get(position).getEmployeeName());
                }
            }
        });
        if (value == 1) {
            holder.tv_memberName.setVisibility(View.VISIBLE);
            holder.tv_memberName.setText(EmployeeList.getEmployeeName());
        } else {
            holder.tv_memberName.setVisibility(View.GONE);
        }
        if (extra == 1) {
            holder.memployee_icon_iv.getLayoutParams().height = 100;
            holder.memployee_icon_iv.getLayoutParams().width = 100;
        }
    }

    @Override
    public int getItemCount() {
        if (mEmployeeList.size() > 3) {
            return 4;
        } else {
            return mEmployeeList.size();
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views

        public ImageView memployee_icon_iv;
        public TextView tv_memberName;
        TextDrawable drawable;
        Random random;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            random = new Random();
            memployee_icon_iv = itemView.findViewById(R.id.employee_icon_iv);
            tv_memberName = itemView.findViewById(R.id.tv_memberName);

        }

    }


    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(60)  // width in px
                .height(60)
                .fontSize(30)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    public List<EmployeeDetalPOJO> getSelectedUserList(List<EmployeeDetalPOJO> mList) {
        List<EmployeeDetalPOJO> selectedUsersList = new ArrayList<>();
        if (mList != null && mList.size() > 0) {
            if (mList != null && mList.size() > 0) {
                for (EmployeeDetalPOJO userId : mList) {
                    if (userId.getSelected()) {
                        if (userId.getUserImage().equals("")) {
                            String s = userId.getEmployeeName().substring(0, 1);
                            String userImage = "image/PreviewImages/" + s.toLowerCase() + ".png";
                            userId.setUserImage(DEVELOPMENT_URL + "" + userImage);
                        } else {
                            userId.setUserImage(DEVELOPMENT_URL + "" + userId.getEmployeeName());
                        }
                        selectedUsersList.add(userId);
                    }
                }
                return selectedUsersList;
            } else {
                return selectedUsersList;
            }
        }
        return selectedUsersList;
    }


    public void updateSelectedUser(List<EmployeeDetalPOJO> selectedUserList) {
        if (mEmployeeList != null) {
            mEmployeeList.addAll(selectedUserList);
            notifyDataSetChanged();
        }

    }


}