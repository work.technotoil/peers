package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.Activity.PingActivity;
import com.peerbuckets.peerbucket.POJO.EmployeeDetalPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DEVELOPMENT_URL;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_user_name_key;

public class UserListChatAdapter extends RecyclerView.Adapter<UserListChatAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<EmployeeDetalPOJO> mList;
    private List<EmployeeDetalPOJO> mDisplayedValues;


    public UserListChatAdapter(List<EmployeeDetalPOJO> mList, Context mContext) {
        context = mContext;
        this.mList = mList;
        this.mDisplayedValues = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chat_user_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tvName.setText(Utils.checkStringWithHash(mDisplayedValues.get(position).getEmployeeName()));
        if (mDisplayedValues.get(position).getUserImage() != null && !mDisplayedValues.get(position).getUserImage().equals("")) {
            Picasso.with(context).load(mDisplayedValues.get(position).getUserImage()).into(holder.imgUser);
        } else {
            holder.imgUser.setImageDrawable(imageLatter(Utils.word(mDisplayedValues.get(position).getCompanyName()), mDisplayedValues.get(position).getBgColor(), mDisplayedValues.get(position).getFontColor()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PingActivity.class);
                intent.putExtra("user_id", mDisplayedValues.get(position).getEmployeeId());
                context.startActivity(intent);
            }
        });

    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(40)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<EmployeeDetalPOJO>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<EmployeeDetalPOJO> FilteredArrList = new ArrayList<EmployeeDetalPOJO>();

                if (mList == null) {
                    mList = new ArrayList<EmployeeDetalPOJO>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mList.size();
                    results.values = mList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mList.size(); i++) {
                        String data = mList.get(i).getEmployeeName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            EmployeeDetalPOJO mModel = new EmployeeDetalPOJO();
                            mModel.setEmployeeName(data);
                            mModel.setSelected(mList.get(i).getSelected());
                            mModel.setUserImage(mList.get(i).getUserImage());
                            mModel.setEmployeeId(mList.get(i).getEmployeeId());
                            FilteredArrList.add(mModel);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        CircleImageView imgUser;

        MyViewHolder(View v) {
            super(v);
            tvName = v.findViewById(R.id.tv_name_bottom_sheet);
            imgUser = v.findViewById(R.id.img_user);

        }
    }


}