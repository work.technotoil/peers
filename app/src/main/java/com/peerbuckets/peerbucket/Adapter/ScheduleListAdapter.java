package com.peerbuckets.peerbucket.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.message.Activity_MessageBoard;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleActivity;
import com.peerbuckets.peerbucket.Activity.schedule.ScheduleDetailActivity;
import com.peerbuckets.peerbucket.POJO.MessageList;
import com.peerbuckets.peerbucket.POJO.ScheduleListModel;
import com.peerbuckets.peerbucket.POJO.User;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.peerbuckets.peerbucket.interfaces.RefreshCommentInterface;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_COMMENT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.ViewHolder> implements IApiResponse {

    Context context;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    public int globalPosition = 0;
    //List to store all mCommentDetailList
    List<ScheduleListModel> mCommentDetailList;
    String mcompanyId = "", muserId = "", memployeeId = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", mCurrentuserId = "";
    int getGlobalPosition = 0;
    String mTtitle = "", message_id = "";
    Activity activity;
    RefreshCommentInterface mRefreshCommentInterface;

    //Constructor of context class
    public ScheduleListAdapter(List<ScheduleListModel> mCommentDetailList, Context context) {
        super();
        mApiRequest = new ApiRequest(context, this);
        //Getting all mCommentDetailList
        this.mCommentDetailList = mCommentDetailList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_schedule, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ScheduleListModel scheduleList = mCommentDetailList.get(position);

        if (scheduleList.getCompletedOn() != null) {
            scheduleList.setTaskSelected(true);
        } else {
            scheduleList.setTaskSelected(false);
        }

        if (scheduleList.getTaskType().equals("schedule")) {
            holder.check_todo.setVisibility(View.GONE);
        } else {
            holder.check_todo.setVisibility(View.VISIBLE);
            holder.check_todo.setChecked(scheduleList.getTaskSelected());
        }


        String newString = scheduleList.getTodoListTaskTitle();

        Spanned spanned = Html.fromHtml(newString);
        char[] chars = new char[spanned.length()];
        TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
        String plainText = new String(chars);
        newString = plainText.replace("\"", "'");

        String userData = "";

        for (User userList : scheduleList.getUsers()) {
            userData = userData + Utils.createUserHtml(userList.getEmployeeName(), userList.getUserImage(), userList.getBgColor(), userList.getFontColor());
        }

        Utils.setDescriptionWithDate(Utils.unescapeJava(newString), scheduleList.getDateRange(), userData, holder.schedule_data, context);

        holder.linear1_comment_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGlobalPosition = position;
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                mApiRequest.postRequest(BASE_URL + DELETE_COMMENT, DELETE_COMMENT, paramsReq, Request.Method.POST);
            }
        });

        holder.schedule_data.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(context, ScheduleDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("schedule_title", scheduleList.getTodoListTaskTitle());
                intent.putExtra("schedule_id", scheduleList.getTodoListTaskId());
                intent.putExtra("user_id", scheduleList.getUser_id());
                intent.putExtra("title", scheduleList.getTodoListTaskTitle());
                ((ScheduleActivity) context).startActivityForResult(intent, ScheduleActivity.REQ_CODE_EXPAND_MESSAGESss);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCommentDetailList.size();
    }

    public void removeMessageAndUpdateComment(Boolean isDelete, String messageId) {
        if (messageId != null && !messageId.equals("")) {
            for (ScheduleListModel list : mCommentDetailList) {
                if (list.getTodoListTaskId().equals(messageId)) {
                    if (isDelete) {
                        mCommentDetailList.remove(list);
                    } else {

                    }
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equals(DELETE_COMMENT)) {
            Log.d("DELETE_COMMENT", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                int status = jsonObject.getInt("status");
                if (status == 1) {
                    mCommentDetailList.remove(globalPosition);
                    mRefreshCommentInterface.setCommentData(mCommentDetailList.size());
                    notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView date_created;
        WebView schedule_data;
        LinearLayout linear1_comment_list;
        CheckBox check_todo;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);

            fontTF = Typeface.createFromAsset(context.getAssets(), "font/fawsmsolid.ttf");
            date_created = itemView.findViewById(R.id.date_created);
            schedule_data = itemView.findViewById(R.id.schedule_data);
            linear1_comment_list = itemView.findViewById(R.id.linear1_comment_list);
            check_todo = itemView.findViewById(R.id.check_todo);
        }
    }

}