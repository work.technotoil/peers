package com.peerbuckets.peerbucket.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peerbuckets.peerbucket.R;

public class OldNotificationAdapter extends RecyclerView.Adapter<OldNotificationAdapter.MyViewHolder>  {

    public  OldNotificationAdapter () {

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.old_notification_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {



        MyViewHolder (View v) {
            super(v);
        }

    }
}