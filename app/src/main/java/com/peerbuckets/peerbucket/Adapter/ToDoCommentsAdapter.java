package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.peerbuckets.peerbucket.Activity.todo.ExpandToDoActivity;
import com.peerbuckets.peerbucket.POJO.ToDoCommentsPOJO;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.SharedPreference.ShardPreferences;
import com.peerbuckets.peerbucket.volley.ApiRequest;
import com.peerbuckets.peerbucket.volley.IApiResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.BASE_URL;
import static com.peerbuckets.peerbucket.ApiController.ApiConfigs.DELETE_COMMENT;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_Company_Id_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_UserId_key;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_current_project_team_id;
import static com.peerbuckets.peerbucket.SharedPreference.ShardPreferences.share_employee_Id_key;

/**
 * Created by Belal on 11/9/2015.
 */
public class ToDoCommentsAdapter extends RecyclerView.Adapter<ToDoCommentsAdapter.ViewHolder> implements IApiResponse {

    Context context;
    Typeface fontTF;
    private ApiRequest mApiRequest;
    public int globalPosition = 0;
    //List to store all mCommentDetailList
    List<ToDoCommentsPOJO> mCommentDetailList;
    String mcompanyId = "", muserId = "", memployeeId = "", updatedComment = "", mcurrentProjectTeamId = "", mcurrentCompanyName = "", maddremovetype = "test_hq", mCurrentuserId = "";
    String[] COLORArray = {"red", "blue", "green", "black", "gray", "cyan", "magenta", "lightgray", "darkgray"};
    String COLORString, firstLetter;
    int selected = 0;
    int getGlobalPosition = 0;

    //Constructor of context class
    public ToDoCommentsAdapter(List<ToDoCommentsPOJO> mCommentDetailList, Context context) {
        super();
        mApiRequest = new ApiRequest(context, (IApiResponse) context);
        //Getting all mCommentDetailList
        this.mCommentDetailList = mCommentDetailList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_comments, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //Getting the particular item from the list
        final ToDoCommentsPOJO CommentDetailList = mCommentDetailList.get(position);

        //Loading image from url
        selected = holder.random.nextInt(COLORArray.length);
        COLORString = COLORArray[selected];
        firstLetter = CommentDetailList.getEmployeeName();

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            holder.muser_comment.setText(Html.fromHtml(CommentDetailList.getCommentDescription(), Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            holder.muser_comment.setText(Html.fromHtml(CommentDetailList.getCommentDescription()));
//        }
        holder.mdate_created.setText(CommentDetailList.getCreatedAt());
        holder.mmore_option_icon.setTypeface(fontTF);
        holder.mmore_option_icon.setText("\uF141");


        if (CommentDetailList.getEmployeePic() == null || CommentDetailList.getEmployeePic().equals("null") || CommentDetailList.getEmployeePic().equals("")) {
            holder.drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(firstLetter.charAt(0)), Color.parseColor(COLORString));
            holder.muser_image.setImageDrawable(holder.drawable);
//            holder.edit_user_image.setImageDrawable(holder.drawable);
        } else {
            holder.drawable = TextDrawable.builder()
                    .buildRound(String.valueOf(firstLetter.charAt(0)), Color.parseColor(COLORString));
            holder.muser_image.setImageDrawable(holder.drawable);
//            holder.edit_user_image.setImageDrawable(holder.drawable);
            /*byte[] decodedString = Base64.decode(MessageBoardList.getEmployeePic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.muser_iv.setImageBitmap(decodedByte);
            Log.d("Decoded_url","run");*/
        }

        holder.mmore_option_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.meud_ll.setVisibility(View.VISIBLE);
            }
        });
        holder.mcancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.meud_ll.setVisibility(View.GONE);
            }
        });

        holder.medit_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mcomment_sectoin_LL.setVisibility(View.GONE);
                holder.medit_comment_ll.setVisibility(View.VISIBLE);
                holder.meud_ll.setVisibility(View.GONE);
                holder.mtemp_et.setText(CommentDetailList.getCommentDescription());
                getGlobalPosition = position;
            }
        });

//        holder.mdiscard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                holder.mcomment_sectoin_LL.setVisibility(View.VISIBLE);
//                holder.medit_comment_ll.setVisibility(View.GONE);
//            }
//        });

        holder.msave_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatedComment = holder.mtemp_et.getText().toString();
                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("comment", holder.mtemp_et.getText().toString());
                paramsReq.put("comment_id", CommentDetailList.getCommentId());
                paramsReq.put("record_id", CommentDetailList.getRecordId());
                paramsReq.put("record_type", "todo_list");
                paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                paramsReq.put("project_team_id", ShardPreferences.get(context, share_current_project_team_id));
                paramsReq.put("users", ((ExpandToDoActivity) context).getAll_users_id());
/*
                mApiRequest.postRequest(BASE_URL + EDIT_COMMENT, EDIT_COMMENT, paramsReq, Request.Method.POST);
*/
                holder.mcomment_sectoin_LL.setVisibility(View.VISIBLE);
                holder.medit_comment_ll.setVisibility(View.GONE);
            }
        });

        holder.mdelete_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Map<String, String> paramsReq = new HashMap<>();
                paramsReq.put("comment_id", CommentDetailList.getCommentId());
                paramsReq.put("user_id", ShardPreferences.get(context, share_current_UserId_key));
                paramsReq.put("company_id", ShardPreferences.get(context, share_Company_Id_key));
                mApiRequest.postRequest(BASE_URL + DELETE_COMMENT, DELETE_COMMENT, paramsReq, Request.Method.POST);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mCommentDetailList.size();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        //Views
        TextView mdate_created, mmore_option_icon, mdelete_comment, medit_comment, mcancel_layout, msave_tv;
        TextView mdiscard;
        WebView muser_comment;
        ImageView muser_image;
        //        ImageView edit_user_image;
        LinearLayout meud_ll, mcomment_sectoin_LL, medit_comment_ll;
        TextDrawable drawable;
        Random random;
        EditText mtemp_et;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            mcompanyId = ShardPreferences.get(context, share_Company_Id_key);
            muserId = ShardPreferences.get(context, share_UserId_key);
            mCurrentuserId = ShardPreferences.get(context, share_current_UserId_key);
            memployeeId = ShardPreferences.get(context, share_employee_Id_key);

            fontTF = Typeface.createFromAsset(context.getAssets(), "font/fawsmsolid.ttf");
            random = new Random();
            muser_image = itemView.findViewById(R.id.user_image);
            muser_comment = itemView.findViewById(R.id.user_comment);
            mdate_created = itemView.findViewById(R.id.date_created);
            mmore_option_icon = itemView.findViewById(R.id.more_option_icon);
            meud_ll = itemView.findViewById(R.id.eud_ll);
            mdelete_comment = itemView.findViewById(R.id.delete_comment);
            medit_comment = itemView.findViewById(R.id.edit_comment);
            mcancel_layout = itemView.findViewById(R.id.cancel_layout);
            mcomment_sectoin_LL = itemView.findViewById(R.id.comment_sectoin_LL);


        }
    }


}