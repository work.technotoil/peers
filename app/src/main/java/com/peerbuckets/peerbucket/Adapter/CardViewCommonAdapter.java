package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.NetworkImageView;
import com.peerbuckets.peerbucket.POJO.HoursModel;
import com.peerbuckets.peerbucket.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 8/10/16.
 */
public class CardViewCommonAdapter extends RecyclerView.Adapter<CardViewCommonAdapter.DataObjectHolder> {
    private String LOG_TAG = "MyRecyclerViewAdapter";
    Context context;
    private List<HoursModel> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name;
        LinearLayout linearMaster;
        RadioButton RadioButton;

        public DataObjectHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            RadioButton = (RadioButton) itemView.findViewById(R.id.RadioButton);
            RadioButton.setVisibility(View.GONE);
            linearMaster= (LinearLayout) itemView.findViewById(R.id.linearMaster);
        }
    }

    public CardViewCommonAdapter(List<HoursModel> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_dialog, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        holder.name.setText(list.get(position).getHours());
        holder.linearMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

