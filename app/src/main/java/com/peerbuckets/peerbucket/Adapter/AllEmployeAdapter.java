package com.peerbuckets.peerbucket.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.peerbuckets.peerbucket.Activity.PingActivity;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;
import com.peerbuckets.peerbucket.R;
import com.peerbuckets.peerbucket.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AllEmployeAdapter extends RecyclerView.Adapter<AllEmployeAdapter.AllEmployeViewHolder> implements Filterable {
    private Context ctx;
    private ArrayList<EmployeeList> mList;
    private ArrayList<EmployeeList> mDisplayedValues;
    String firstName = "", lastName = "";

    public AllEmployeAdapter(Context context, ArrayList<EmployeeList> list) {
        mList = list;
        mDisplayedValues = list;
        ctx = context;
    }

    @NonNull
    @Override
    public AllEmployeAdapter.AllEmployeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chat_user_list, parent, false);
        return new AllEmployeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AllEmployeAdapter.AllEmployeViewHolder holder, int position) {
        holder.tv_name_bottom_sheet.setText(Utils.unescapeJava(mDisplayedValues.get(position).getEmployeeName()));
        if (!mDisplayedValues.get(position).getUserImage().equals("") && mDisplayedValues.get(position).getUserImage() != null) {
            Picasso.with(ctx).load(mDisplayedValues.get(position).getUserImage()).into(holder.img_user);
        } else {
            holder.img_user.setImageDrawable(imageLatter(Utils.word(mDisplayedValues.get(position).getEmployeeName()),
                    mDisplayedValues.get(position).getBgColor(), mDisplayedValues.get(position).getFontColor()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, PingActivity.class);
                intent.putExtra("user_id", mDisplayedValues.get(position).getEmployeeId());
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<EmployeeList>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<EmployeeList> FilteredArrList = new ArrayList<EmployeeList>();

                if (mList == null) {
                    mList = new ArrayList<EmployeeList>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mList.size();
                    results.values = mList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mList.size(); i++) {
                        String data = mList.get(i).getEmployeeName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            EmployeeList mModel = new EmployeeList();
                            mModel.setEmployeeName(data);
                            mModel.setSelected(mList.get(i).getSelected());
                            mModel.setUserImage(mList.get(i).getUserImage());
                            mModel.setEmployeeId(mList.get(i).getEmployeeId());
                            mModel.setBgColor(mList.get(i).getBgColor());
                            mModel.setFontColor(mList.get(i).getFontColor());
                            FilteredArrList.add(mModel);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }

    public class AllEmployeViewHolder extends RecyclerView.ViewHolder {
        ImageView img_user;
        TextView tv_name_bottom_sheet;

        public AllEmployeViewHolder(@NonNull View itemView) {
            super(itemView);
            img_user = itemView.findViewById(R.id.img_user);
            tv_name_bottom_sheet = itemView.findViewById(R.id.tv_name_bottom_sheet);
        }
    }

    private TextDrawable imageLatter(String name, String backColor, String fontColor) {
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(80)  // width in px
                .height(80)
                .fontSize(35)
                .textColor(Color.parseColor("#" + fontColor))/* size in px */
                .bold()// height in px
                .endConfig()
                .buildRect(name, Color.parseColor("#" + backColor));
        return drawable;
    }
}
