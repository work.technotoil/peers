package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.luseen.autolinklibrary.AutoLinkTextView;
import com.peerbuckets.peerbucket.R;

import org.w3c.dom.Text;

import java.security.PublicKey;

public class ChatReceiverTextGroupViewHolder extends RecyclerView.ViewHolder {
    public TextView tvUserName, tvTime, tvStickyDate;
    public ImageView img_user_reciver_text_chat;
    public RelativeLayout rl_receiveChat;
    public AutoLinkTextView tvReceiveMessage;

    public ChatReceiverTextGroupViewHolder(@NonNull View itemView) {
        super(itemView);
        rl_receiveChat = itemView.findViewById(R.id.rl_receiveChat);
        img_user_reciver_text_chat = itemView.findViewById(R.id.img_user_reciver_text_chat);
        tvReceiveMessage = itemView.findViewById(R.id.tv_message_receive_text_chat);
        tvUserName = itemView.findViewById(R.id.tv_user_name_receive_text_group_chat);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);
    }
}
