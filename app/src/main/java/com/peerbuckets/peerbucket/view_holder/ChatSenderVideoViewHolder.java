package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class ChatSenderVideoViewHolder extends RecyclerView.ViewHolder {
    public ImageView img_user_sender_image_png,vv_play;
    public ImageView vv_send;
    public TextView tvTime,tvStickyDate,tv_user_name_sender_text_group_chat;

    public ChatSenderVideoViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        tv_user_name_sender_text_group_chat = itemView.findViewById(R.id.tv_user_name_sender_text_giphy_chat);
        img_user_sender_image_png = itemView.findViewById(R.id.img_user_sender_image_png);
        vv_send = itemView.findViewById(R.id.img_message_send_video);
        vv_play = itemView.findViewById(R.id.vv_play);

        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);
    }
}
