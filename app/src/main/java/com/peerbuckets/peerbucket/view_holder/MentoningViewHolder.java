package com.peerbuckets.peerbucket.view_holder;


import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.peerbuckets.peerbucket.R;
import com.teamwork.autocomplete.view.AutoCompleteViewHolder;


public class MentoningViewHolder extends AutoCompleteViewHolder {

    public TextView tv_memberName;
    public ImageView employee_icon_iv;
    public RelativeLayout rel_masrter;

    public MentoningViewHolder(@NonNull View view) {
        super(view);
        employee_icon_iv = view.findViewById(R.id.employee_icon_iv);
        tv_memberName = view.findViewById(R.id.tv_memberName);
        rel_masrter = view.findViewById(R.id.rel_masrter);
    }
}

