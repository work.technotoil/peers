package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class ChatReceiverFileViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgReceiver, img_user_receiver_image_ping;
    public TextView tvUserName, tvTime, tvStickyDate,tv_fileName,tv_downloadReceiver;

    public ChatReceiverFileViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        imgReceiver = itemView.findViewById(R.id.img_message_receive_file_ping);
        img_user_receiver_image_ping = itemView.findViewById(R.id.img_user_receiver_image_ping);
        tvUserName = itemView.findViewById(R.id.tv_user_name_receive_image_group_chat);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);
        tv_fileName = itemView.findViewById(R.id.tv_fileName);
        tv_downloadReceiver = itemView.findViewById(R.id.tv_downloadReceiver);
    }
}
