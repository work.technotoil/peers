package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.peerbuckets.peerbucket.R;

import org.w3c.dom.Text;

public class ChatSenderTextViewHolder extends RecyclerView.ViewHolder {
    public TextView tvTime, tvStickyDate, tvName;
    public ImageView img_user_receiver_text_chat;
    public ConstraintLayout cl_sendChat;
    public AutoLinkTextView tvSendMessage;

    public ChatSenderTextViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        cl_sendChat = itemView.findViewById(R.id.cl_sendChat);
        img_user_receiver_text_chat = itemView.findViewById(R.id.img_user_receiver_text_chat);
        tvSendMessage = itemView.findViewById(R.id.tv_message_send_text_chat);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);
        tvName = itemView.findViewById(R.id.tv_user_name_receive_text_group_chat);

    }
}
