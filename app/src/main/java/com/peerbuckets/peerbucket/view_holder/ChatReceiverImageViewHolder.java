package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class ChatReceiverImageViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgReceiver;
    public TextView tvTime,tvStickyDate;
    public ChatReceiverImageViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        imgReceiver = itemView.findViewById(R.id.img_message_receive_image_ping);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);


    }
}
