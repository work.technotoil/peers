package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class UnreadNotificationTextViewHolder extends RecyclerView.ViewHolder {
    public TextView tv_read_lable;
    public TextView module_type,module_date;
    public ImageView imagType;
    public ImageView imgCLose;
    public TextView textViewOptions,txtTitle;
    public LinearLayout llMaster;
    public FrameLayout ffView;

    public UnreadNotificationTextViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        tv_read_lable = itemView.findViewById(R.id.tv_read_lable);
        module_type = itemView.findViewById(R.id.module_type);
        module_date = itemView.findViewById(R.id.module_date);
        imagType = itemView.findViewById(R.id.imageType);
        imgCLose = itemView.findViewById(R.id.imgCLose);
        txtTitle = itemView.findViewById(R.id.txtTitle);
        textViewOptions = itemView.findViewById(R.id.textViewOptions);
        llMaster = itemView.findViewById(R.id.llMaster);
        ffView = itemView.findViewById(R.id.ffView);
    }
}
