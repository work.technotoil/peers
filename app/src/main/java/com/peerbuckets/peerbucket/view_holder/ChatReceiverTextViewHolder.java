package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class ChatReceiverTextViewHolder extends RecyclerView.ViewHolder {
    public TextView tvReceiveMessage;
    public TextView tvTime,tvStickyDate;

    public ChatReceiverTextViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        tvReceiveMessage = itemView.findViewById(R.id.tv_message_receive_text_chat);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);
    }
}
