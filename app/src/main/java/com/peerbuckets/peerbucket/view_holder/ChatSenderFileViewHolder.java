package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class ChatSenderFileViewHolder extends RecyclerView.ViewHolder {
    public ImageView imgFile,img_user_sender_image_png;
    public TextView tvTime,tvStickyDate,tv_user_name_sender_text_group_chat,tv_fileName;

    public ChatSenderFileViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        tv_user_name_sender_text_group_chat = itemView.findViewById(R.id.tv_user_name_sender_text_file_chat);
        img_user_sender_image_png = itemView.findViewById(R.id.img_user_sender_image_png);
        imgFile = itemView.findViewById(R.id.img_message_send_file_ping);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);
        tv_fileName = itemView.findViewById(R.id.tv_fileName);
    }
}
