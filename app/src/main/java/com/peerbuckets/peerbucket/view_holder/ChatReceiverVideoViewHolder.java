package com.peerbuckets.peerbucket.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.peerbuckets.peerbucket.R;

public class ChatReceiverVideoViewHolder extends RecyclerView.ViewHolder {

    public ImageView img_user_receiver_image_ping, vv_play;
    public ImageView vv_receiver;
    public TextView tvUserName, tvTime, tvStickyDate;

    public ChatReceiverVideoViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        vv_play = itemView.findViewById(R.id.vv_play);
        vv_receiver = itemView.findViewById(R.id.img_message_receive_video_ping);
        img_user_receiver_image_ping = itemView.findViewById(R.id.img_user_receiver_image_ping);
        tvUserName = itemView.findViewById(R.id.tv_user_name_receive_image_group_chat);
        tvTime = itemView.findViewById(R.id.text_message_time);
        tvStickyDate = itemView.findViewById(R.id.tv_sticky_date);


    }
}