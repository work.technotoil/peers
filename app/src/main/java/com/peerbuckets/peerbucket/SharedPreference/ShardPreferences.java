package com.peerbuckets.peerbucket.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;


public class ShardPreferences {

    public static final String APP_PREF = "PeerBucketPref";
    public static SharedPreferences sp;
    public static String share_UserId_key="share_user_Id_key" ;
    public static String share_ISFLOW="share_isflow" ;
    public static String share_current_UserId_key="share_current_user_Id_key" ;
    public static String share_Company_Id_key="share_company_key";
    public static String share_employee_Id_key="share_employee_key";
    public static String share_user_email_key="share_email_key";
    public static String share_user_image_key="share_image_key";
    public static String share_user_name_key="share_user_name_key";
    public static String share_addd_remove_current_type="share_add_remove_type_key";
    public static String share_current_project_team_id="share_current_project_teamId";
    public static String share_current_company_name="share_current_company_name";
    public static String share_current_profile_url="share_current_profile_url";
    public static String share_current_user_token="share_current_user_token";
    public static String share_current_module="share_current_module";
    public static String share_language="share_language";
    public static String key_company_logo="key_company_logo";
    public static String key_company_Id="company_id";
    public static String key_EmpName="key_EmpName";
    public static String key_userSpinner_id="key_userSpinner_id";
    public static String key_cardListingSpinner_id="key_cardListingSpinner_id";
    public static String key_backColor="key_backColor";
    public static String key_fontColor="key_fontColor";


    public static void save(Context context, String key, String value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String get(Context context , String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        String userId = sp.getString(key, "");
        return userId;
    }

    public static void saveInt(Context context, String key, int value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public static int getInt(Context context , String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        int userId = sp.getInt(key,0);
        return userId;
    }


    public static void saveBool(Context context, String key, Boolean value) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static Boolean getBool(Context context , String key) {
        sp = context.getSharedPreferences(APP_PREF, 0);
        return sp.getBoolean(key,false);
    }

    public static void clearPreference(Context context) {
        sp = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.apply();
    }


}
