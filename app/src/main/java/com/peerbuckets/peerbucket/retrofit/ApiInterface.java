package com.peerbuckets.peerbucket.retrofit;

import com.peerbuckets.peerbucket.Hpojo.HomePojo;
import com.peerbuckets.peerbucket.POJO.ActivityListResponce.CompanyActivityListResponse;
import com.peerbuckets.peerbucket.POJO.AddModulePojo;
import com.peerbuckets.peerbucket.POJO.MeActivitiListResponse.RecentLatestActivityResponse;
import com.peerbuckets.peerbucket.POJO.OneToOneChatResponse.OneToOneNotification;
import com.peerbuckets.peerbucket.POJO.PicUpdateResponse;
import com.peerbuckets.peerbucket.POJO.ProjectTeamTrash.NewTrashTeamResponse;
import com.peerbuckets.peerbucket.POJO.SearchResponse.SearchResponse;
import com.peerbuckets.peerbucket.POJO.SettingListResponce.SettingListResponse;
import com.peerbuckets.peerbucket.POJO.SettingUpdateResponse.SettingUpdateResponse;
import com.peerbuckets.peerbucket.POJO.addRemoveResponse.AddRemoveLIstNew;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.AllEmployeeList;
import com.peerbuckets.peerbucket.POJO.deleteTrash.TrashDeleteResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


public interface ApiInterface {

    @POST("homeApi")
    @FormUrlEncoded
    Call<HomePojo> Callhome(@FieldMap Map<String, String> params);

    @POST("create_module")
    @FormUrlEncoded
    Call<AddModulePojo> CallCreateModule(@FieldMap Map<String, String> params);

    @POST("companyActivityList")
    @FormUrlEncoded
    Call<CompanyActivityListResponse> callCompanyActivityList(@FieldMap Map<String, String> params);

    @POST("trashModule")
    @FormUrlEncoded
    Call<TrashDeleteResponse> callSetting(@FieldMap Map<String, String> params);

    @POST("recentActivityList")
    @FormUrlEncoded
    Call<RecentLatestActivityResponse> callRecentLatestActivity(@FieldMap Map<String, String> params);

    @POST("trash")
    @FormUrlEncoded
    Call<NewTrashTeamResponse> callTrashTeam(@FieldMap Map<String, String> params);

    @POST("searchDataList")
    @FormUrlEncoded
    Call<SearchResponse> callSearch(@FieldMap Map<String, String> params);

    @POST("getModuleListSetting")
    @FormUrlEncoded
    Call<SettingListResponse> callSettingList(@FieldMap Map<String, String> params);

    @POST("saveLastMessageInChat")
    @FormUrlEncoded
    Call<OneToOneNotification> callPingNotification(@FieldMap Map<String, String> params);

    @POST("updateModuleSetting")
    @FormUrlEncoded
    Call<SettingUpdateResponse> callSettingUpdate(@FieldMap Map<String, String> params);

    @POST("allEmployeeList")
    @FormUrlEncoded
    Call<AllEmployeeList> callAllEmployeeList(@FieldMap Map<String, String> params);

    @POST("employeeDetailForInvitation")
    @FormUrlEncoded
    Call<AddRemoveLIstNew> callAddRemoveList(@FieldMap Map<String, String> params);

    @POST("updateProfileImage")
    @Multipart
    Call<PicUpdateResponse> CallPicUpdateResponse(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part filePart);

}