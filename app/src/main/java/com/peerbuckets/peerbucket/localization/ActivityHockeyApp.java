package com.peerbuckets.peerbucket.localization;

import android.app.Activity;
import android.content.Context;

import com.peerbuckets.peerbucket.localization.LocaleHelper;

public class ActivityHockeyApp extends Activity {
    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(LocaleHelper.setLocale(newBase, LocaleHelper.getLanguage(newBase)));
    }
}