package com.peerbuckets.peerbucket.POJO.OneToOneChatResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiverName {

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("user_image")
    @Expose
    private String userImage;

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    @SerializedName("bgColor")
    @Expose
    private String bgColor;
    @SerializedName("fontColor")
    @Expose
    private String fontColor;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

}
