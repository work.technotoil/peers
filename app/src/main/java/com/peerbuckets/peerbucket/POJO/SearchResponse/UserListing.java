package com.peerbuckets.peerbucket.POJO.SearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserListing {

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
