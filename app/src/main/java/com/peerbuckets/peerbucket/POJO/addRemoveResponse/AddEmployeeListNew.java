package com.peerbuckets.peerbucket.POJO.addRemoveResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddEmployeeListNew {

    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_job_title")
    @Expose
    private String employeeJobTitle;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("project_user_relation_id")
    @Expose
    private String projectUserRelationId;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("bgColor")
    @Expose
    private String bgColor;
    @SerializedName("fontColor")
    @Expose
    private String fontColor;
    @SerializedName("isUserAdded")
    @Expose
    private String isUserAdded;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeJobTitle() {
        return employeeJobTitle;
    }

    public void setEmployeeJobTitle(String employeeJobTitle) {
        this.employeeJobTitle = employeeJobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getProjectUserRelationId() {
        return projectUserRelationId;
    }

    public void setProjectUserRelationId(String projectUserRelationId) {
        this.projectUserRelationId = projectUserRelationId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getIsUserAdded() {
        return isUserAdded;
    }

    public void setIsUserAdded(String isUserAdded) {
        this.isUserAdded = isUserAdded;
    }

}
