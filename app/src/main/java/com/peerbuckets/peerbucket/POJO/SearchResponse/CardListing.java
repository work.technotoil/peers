package com.peerbuckets.peerbucket.POJO.SearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardListing {

    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("project_team_name")
    @Expose
    private String projectTeamName;

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getProjectTeamName() {
        return projectTeamName;
    }

    public void setProjectTeamName(String projectTeamName) {
        this.projectTeamName = projectTeamName;
    }

}
