package com.peerbuckets.peerbucket.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CheckinsNewPojo implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("checkin_id")
    @Expose
    private String checkinId;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("question_date")
    @Expose
    private String questionDate;
    @SerializedName("answere_date")
    @Expose
    private String answereDate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("bgColor")
    @Expose
    private String bgColor;

    @SerializedName("applause")
    @Expose
    private List<EmployeeDetalPOJO> mApplauseEmployeeList;

    public List<EmployeeDetalPOJO> getmApplauseEmployeeList() {
        return mApplauseEmployeeList;
    }

    public void setmApplauseEmployeeList(List<EmployeeDetalPOJO> mApplauseEmployeeList) {
        this.mApplauseEmployeeList = mApplauseEmployeeList;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    @SerializedName("fontColor")
    @Expose
    private String fontColor;
    @SerializedName("users")
    @Expose
    private List<User> users = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(String checkinId) {
        this.checkinId = checkinId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getQuestionDate() {
        return questionDate;
    }

    public void setQuestionDate(String questionDate) {
        this.questionDate = questionDate;
    }

    public String getAnswereDate() {
        return answereDate;
    }

    public void setAnswereDate(String answereDate) {
        this.answereDate = answereDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
