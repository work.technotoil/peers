package com.peerbuckets.peerbucket.POJO.addRemoveResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.peerbuckets.peerbucket.POJO.allEmployeeListResponse.EmployeeList;

public class AddRemoveLIstNew {

    @SerializedName("employeeList")
    @Expose
    private List<AddEmployeeListNew> employeeList = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public List<AddEmployeeListNew> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<AddEmployeeListNew> employeeList) {
        this.employeeList = employeeList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
