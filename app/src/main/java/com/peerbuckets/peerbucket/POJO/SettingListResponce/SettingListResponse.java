package com.peerbuckets.peerbucket.POJO.SettingListResponce;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingListResponse {

    @SerializedName("module_listing")
    @Expose
    private List<ModuleListing> moduleListing = null;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<ModuleListing> getModuleListing() {
        return moduleListing;
    }

    public void setModuleListing(List<ModuleListing> moduleListing) {
        this.moduleListing = moduleListing;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
