package com.peerbuckets.peerbucket.POJO.OneToOneChatResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OneToOneChatResponse {

    @SerializedName("channel_id")
    @Expose
    private String channelId;
    @SerializedName("receiver_name")
    @Expose
    private ReceiverName receiverName;
    @SerializedName("sender_name")
    @Expose
    private SenderName senderName;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public ReceiverName getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(ReceiverName receiverName) {
        this.receiverName = receiverName;
    }

    public SenderName getSenderName() {
        return senderName;
    }

    public void setSenderName(SenderName senderName) {
        this.senderName = senderName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
