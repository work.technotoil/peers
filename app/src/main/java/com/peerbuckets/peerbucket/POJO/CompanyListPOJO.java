package com.peerbuckets.peerbucket.POJO;
import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyListPOJO implements Serializable
{

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("company_logo")
    @Expose
    private String company_logo;

    @SerializedName("project_team_description")
    @Expose
    private String projectTeamDescription;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("userListing")
    @Expose
    private List<UserListingPOJO> userListing = null;
    private final static long serialVersionUID = -6931342678392854088L;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProjectTeamDescription() {
        return projectTeamDescription;
    }

    public void setProjectTeamDescription(String projectTeamDescription) {
        this.projectTeamDescription = projectTeamDescription;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public List<UserListingPOJO> getUserListing() {
        return userListing;
    }

    public void setUserListing(List<UserListingPOJO> userListing) {
        this.userListing = userListing;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }
}