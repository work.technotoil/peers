
package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageList implements Serializable {

    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("message_title")
    @Expose
    private String messageTitle;
    @SerializedName("message_description")
    @Expose
    private String messageDescription;
    @SerializedName("message_type_id")
    @Expose
    private String messageTypeId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("activation_code")
    @Expose
    private String activationCode;
    @SerializedName("remember_token")
    @Expose
    private Object rememberToken;
    @SerializedName("expire_date")
    @Expose
    private String expireDate;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("isActive")
    @Expose
    private String isActive;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_job_title")
    @Expose
    private String employeeJobTitle;
    @SerializedName("employee_pic")
    @Expose
    private Object employeePic;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("employee_company_name")
    @Expose
    private String employeeCompanyName;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("timezone_update_status")
    @Expose
    private String timezoneUpdateStatus;
    @SerializedName("shortbio")
    @Expose
    private String shortbio;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("postedDate")
    @Expose
    private String postedDate;
    @SerializedName("total_messages")
    @Expose
    private String totalMessages;
    @SerializedName("bgColor")
    @Expose
    private String bgColor;

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    @SerializedName("fontColor")
    @Expose
    private String fontColor;
    @SerializedName("date")
    @Expose
    private String date;
    private final static long serialVersionUID = -1994436566381758942L;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageDescription() {
        return messageDescription;
    }

    public void setMessageDescription(String messageDescription) {
        this.messageDescription = messageDescription;
    }

    public String getMessageTypeId() {
        return messageTypeId;
    }

    public void setMessageTypeId(String messageTypeId) {
        this.messageTypeId = messageTypeId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Object getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeJobTitle() {
        return employeeJobTitle;
    }

    public void setEmployeeJobTitle(String employeeJobTitle) {
        this.employeeJobTitle = employeeJobTitle;
    }

    public Object getEmployeePic() {
        return employeePic;
    }

    public void setEmployeePic(Object employeePic) {
        this.employeePic = employeePic;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getEmployeeCompanyName() {
        return employeeCompanyName;
    }

    public void setEmployeeCompanyName(String employeeCompanyName) {
        this.employeeCompanyName = employeeCompanyName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezoneUpdateStatus() {
        return timezoneUpdateStatus;
    }

    public void setTimezoneUpdateStatus(String timezoneUpdateStatus) {
        this.timezoneUpdateStatus = timezoneUpdateStatus;
    }

    public String getShortbio() {
        return shortbio;
    }

    public void setShortbio(String shortbio) {
        this.shortbio = shortbio;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getTotalMessages() {
        return totalMessages;
    }

    public void setTotalMessages(String totalMessages) {
        this.totalMessages = totalMessages;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
