package com.peerbuckets.peerbucket.POJO;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

public class Chat {

    private String message, messageType, receiverId, sender_profile_image, senderId, primaryKey, file_name;
    Object timestamp;


    public Chat() {
    }

    public Chat(String message, String messageType, String receiverId, String sender_profile_image, String senderId, String file_name) {
        this.message = message;
        this.messageType = messageType;
        this.receiverId = receiverId;
        this.timestamp = ServerValue.TIMESTAMP;
        this.sender_profile_image = sender_profile_image;
        this.senderId = senderId;
        this.file_name = file_name;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSender_profile_image() {
        return sender_profile_image;
    }

    public void setSender_profile_image(String sender_profile_image) {
        this.sender_profile_image = sender_profile_image;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) timestamp;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public String getSenderId() {
        return senderId;
    }
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }
}
