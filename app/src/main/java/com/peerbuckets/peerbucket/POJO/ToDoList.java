
package com.peerbuckets.peerbucket.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ToDoList implements Serializable {

    @SerializedName("todo_list_id")
    @Expose
    private String todoListId;
    @SerializedName("todo_list_title")
    @Expose
    private String todoListTitle;
    @SerializedName("todo_list_desc")
    @Expose
    private String todoListDesc;
    @SerializedName("completedTask")
    @Expose
    private String completedTask;
    @SerializedName("TotalTask")
    @Expose
    private String totalTask;
    @SerializedName("commentCount")
    @Expose
    private Integer commentCount;

    public String getTodo_list_desc_mobile() {
        return todo_list_desc_mobile;
    }

    public void setTodo_list_desc_mobile(String todo_list_desc_mobile) {
        this.todo_list_desc_mobile = todo_list_desc_mobile;
    }


    @SerializedName("todo_list_desc_mobile")
    @Expose
    private String todo_list_desc_mobile;
    @SerializedName("user_id")
    @Expose
    private String user_id = "";

    public String getTodoListId() {
        return todoListId;
    }

    public void setTodoListId(String todoListId) {
        this.todoListId = todoListId;
    }

    public String getTodoListTitle() {
        return todoListTitle;
    }

    public void setTodoListTitle(String todoListTitle) {
        this.todoListTitle = todoListTitle;
    }

    public String getTodoListDesc() {
        return todoListDesc;
    }

    public void setTodoListDesc(String todoListDesc) {
        this.todoListDesc = todoListDesc;
    }

    public String getCompletedTask() {
        return completedTask;
    }

    public void setCompletedTask(String completedTask) {
        this.completedTask = completedTask;
    }

    public String getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(String totalTask) {
        this.totalTask = totalTask;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
