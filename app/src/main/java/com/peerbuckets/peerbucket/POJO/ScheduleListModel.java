
package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScheduleListModel implements Serializable
{
    @SerializedName("isSelected")
    @Expose
    private Boolean isTaskSelected;

    @SerializedName("todo_list_task_id")
    @Expose
    private String todoListTaskId;
    @SerializedName("todo_list_task_title")
    @Expose
    private String todoListTaskTitle;
    @SerializedName("task_type")
    @Expose
    private String taskType;
    @SerializedName("todo_list_task_start_date")
    @Expose
    private String todoListTaskStartDate;
    @SerializedName("todo_list_task_end_date")
    @Expose
    private String todoListTaskEndDate;
    @SerializedName("completed_on")
    @Expose
    private Object completedOn;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("users")
    @Expose
    private List<User> users = null;
    @SerializedName("date_range")
    @Expose
    private String dateRange;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    private final static long serialVersionUID = 1411363365524265196L;

    public String getTodoListTaskId() {
        return todoListTaskId;
    }

    public void setTodoListTaskId(String todoListTaskId) {
        this.todoListTaskId = todoListTaskId;
    }

    public String getTodoListTaskTitle() {
        return todoListTaskTitle;
    }

    public void setTodoListTaskTitle(String todoListTaskTitle) {
        this.todoListTaskTitle = todoListTaskTitle;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTodoListTaskStartDate() {
        return todoListTaskStartDate;
    }

    public void setTodoListTaskStartDate(String todoListTaskStartDate) {
        this.todoListTaskStartDate = todoListTaskStartDate;
    }

    public String getTodoListTaskEndDate() {
        return todoListTaskEndDate;
    }

    public void setTodoListTaskEndDate(String todoListTaskEndDate) {
        this.todoListTaskEndDate = todoListTaskEndDate;
    }

    public Object getCompletedOn() {
        return completedOn;
    }

    public void setCompletedOn(Object completedOn) {
        this.completedOn = completedOn;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getDateRange() {
        return dateRange;
    }


    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public Boolean getTaskSelected() {
        return isTaskSelected;
    }

    public void setTaskSelected(Boolean taskSelected) {
        isTaskSelected = taskSelected;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
