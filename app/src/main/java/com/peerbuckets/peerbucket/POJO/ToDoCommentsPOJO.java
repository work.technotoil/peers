
package com.peerbuckets.peerbucket.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToDoCommentsPOJO {

    @SerializedName("comment_id")
    @Expose
    private String commentId;
    @SerializedName("record_type")
    @Expose
    private String recordType;
    @SerializedName("record_id")
    @Expose
    private String recordId;
    @SerializedName("comment_description")
    @Expose
    private String commentDescription;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("activation_code")
    @Expose
    private String activationCode;
    @SerializedName("remember_token")
    @Expose
    private Object rememberToken;
    @SerializedName("expire_date")
    @Expose
    private String expireDate;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("isActive")
    @Expose
    private String isActive;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_job_title")
    @Expose
    private String employeeJobTitle;
    @SerializedName("employee_pic")
    @Expose
    private Object employeePic;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("employee_company_name")
    @Expose
    private String employeeCompanyName;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("country_code")
    @Expose
    private Object countryCode;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("lat")
    @Expose
    private Object lat;
    @SerializedName("lon")
    @Expose
    private Object lon;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("timezone_update_status")
    @Expose
    private String timezoneUpdateStatus;
    @SerializedName("shortbio")
    @Expose
    private String shortbio;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("comment_at")
    @Expose
    private String commentAt;
    @SerializedName("currentTime")
    @Expose
    private String currentTime;
    @SerializedName("users")
    @Expose
    private List<Object> users = null;
    @SerializedName("applause")
    @Expose
    private List<Object> applause = null;
    @SerializedName("attachement")
    @Expose
    private List<Object> attachement = null;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCommentDescription() {
        return commentDescription;
    }

    public void setCommentDescription(String commentDescription) {
        this.commentDescription = commentDescription;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Object getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeJobTitle() {
        return employeeJobTitle;
    }

    public void setEmployeeJobTitle(String employeeJobTitle) {
        this.employeeJobTitle = employeeJobTitle;
    }

    public Object getEmployeePic() {
        return employeePic;
    }

    public void setEmployeePic(Object employeePic) {
        this.employeePic = employeePic;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getEmployeeCompanyName() {
        return employeeCompanyName;
    }

    public void setEmployeeCompanyName(String employeeCompanyName) {
        this.employeeCompanyName = employeeCompanyName;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Object countryCode) {
        this.countryCode = countryCode;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLon() {
        return lon;
    }

    public void setLon(Object lon) {
        this.lon = lon;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezoneUpdateStatus() {
        return timezoneUpdateStatus;
    }

    public void setTimezoneUpdateStatus(String timezoneUpdateStatus) {
        this.timezoneUpdateStatus = timezoneUpdateStatus;
    }

    public String getShortbio() {
        return shortbio;
    }

    public void setShortbio(String shortbio) {
        this.shortbio = shortbio;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCommentAt() {
        return commentAt;
    }

    public void setCommentAt(String commentAt) {
        this.commentAt = commentAt;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public List<Object> getUsers() {
        return users;
    }

    public void setUsers(List<Object> users) {
        this.users = users;
    }

    public List<Object> getApplause() {
        return applause;
    }

    public void setApplause(List<Object> applause) {
        this.applause = applause;
    }

    public List<Object> getAttachement() {
        return attachement;
    }

    public void setAttachement(List<Object> attachement) {
        this.attachement = attachement;
    }

}
