
package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AlldocumentPOJO implements Serializable
{

    @SerializedName("document_id")
    @Expose
    private String documentId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("document_title")
    @Expose
    private String documentTitle;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("name_on_disk")
    @Expose
    private String nameOnDisk;
    @SerializedName("document_description")
    @Expose
    private String documentDescription;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("folder_image")
    @Expose
    private String folderImage;
    private final static long serialVersionUID = -992665070096148986L;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getNameOnDisk() {
        return nameOnDisk;
    }

    public void setNameOnDisk(String nameOnDisk) {
        this.nameOnDisk = nameOnDisk;
    }

    public String getDocumentDescription() {
        return documentDescription;
    }

    public void setDocumentDescription(String documentDescription) {
        this.documentDescription = documentDescription;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFolderImage() {
        return folderImage;
    }

    public void setFolderImage(String folderImage) {
        this.folderImage = folderImage;
    }

}
