package com.peerbuckets.peerbucket.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmpPOJO {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_pic")
    @Expose
    private String employeePic;
    @SerializedName("timezone")
    @Expose
    private String timezone;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeePic() {
        return employeePic;
    }

    public void setEmployeePic(String employeePic) {
        this.employeePic = employeePic;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

}
