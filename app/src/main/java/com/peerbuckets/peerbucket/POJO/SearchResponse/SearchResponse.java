package com.peerbuckets.peerbucket.POJO.SearchResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResponse {

    @SerializedName("user_listing")
    @Expose
    private List<UserListing> userListing = null;
    @SerializedName("card_listing")
    @Expose
    private List<CardListing> cardListing = null;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<UserListing> getUserListing() {
        return userListing;
    }

    public void setUserListing(List<UserListing> userListing) {
        this.userListing = userListing;
    }

    public List<CardListing> getCardListing() {
        return cardListing;
    }

    public void setCardListing(List<CardListing> cardListing) {
        this.cardListing = cardListing;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
