package com.peerbuckets.peerbucket.POJO;

public class HoursModel {

    String HourFormat="";
    String hours = "";


    public String getHourFormat() {
        return HourFormat;
    }

    public void setHourFormat(String hourFormat) {
        HourFormat = hourFormat;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }
}
