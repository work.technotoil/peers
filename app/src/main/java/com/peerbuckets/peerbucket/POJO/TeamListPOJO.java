package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class TeamListPOJO implements Serializable
{

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("teamName")
    @Expose
    private String teamName;
    @SerializedName("project_team_description")
    @Expose
    private String projectTeamDescription;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("userListing")
    @Expose
    private List<EmployeeDetalPOJO> userListing = null;
    private final static long serialVersionUID = 2976721072810248563L;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getProjectTeamDescription() {
        return projectTeamDescription;
    }

    public void setProjectTeamDescription(String projectTeamDescription) {
        this.projectTeamDescription = projectTeamDescription;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public List<EmployeeDetalPOJO> getUserListing() {
        return userListing;
    }

    public void setUserListing(List<EmployeeDetalPOJO> userListing) {
        this.userListing = userListing;
    }

}