package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class AllCompanyListPOJO implements Serializable
{

    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_email")
    @Expose
    private String companyEmail;
    @SerializedName("company_admin_name")
    @Expose
    private String companyAdminName;
    @SerializedName("company_admin_job_title")
    @Expose
    private String companyAdminJobTitle;
    @SerializedName("expire_date")
    @Expose
    private Object expireDate;

    @SerializedName("isCompanyExpired")
    @Expose
    private String isCompanyExpired;


    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    private final static long serialVersionUID = -1068671209709891127L;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyAdminName() {
        return companyAdminName;
    }

    public void setCompanyAdminName(String companyAdminName) {
        this.companyAdminName = companyAdminName;
    }

    public String getCompanyAdminJobTitle() {
        return companyAdminJobTitle;
    }

    public void setCompanyAdminJobTitle(String companyAdminJobTitle) {
        this.companyAdminJobTitle = companyAdminJobTitle;
    }

    public Object getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Object expireDate) {
        this.expireDate = expireDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsCompanyExpired() {
        return isCompanyExpired;
    }

    public void setIsCompanyExpired(String isCompanyExpired) {
        this.isCompanyExpired = isCompanyExpired;
    }
}