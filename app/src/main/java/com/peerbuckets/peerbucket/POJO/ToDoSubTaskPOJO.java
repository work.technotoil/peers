package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToDoSubTaskPOJO implements Serializable {

    @SerializedName("todo_list_task_id")
    @Expose
    private String todoListTaskId;
    @SerializedName("todo_list_task_title")
    @Expose
    private String todoListTaskTitle;
    @SerializedName("start_date")
    @Expose
    private String todoListTaskStartDate;

    @SerializedName("todo_list_task_description")
    @Expose
    private String todoListTaskDescription;
    @SerializedName("list_id")
    @Expose
    private String listId;
    @SerializedName("task_type")
    @Expose
    private String taskType;
    @SerializedName("completed_on")
    @Expose
    private Object completedOn;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("selector")
    @Expose
    private String selector;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("users")
    @Expose
    private List<User> users = null;
    private final static long serialVersionUID = 6016053573852216594L;

    public String getTodoListTaskId() {
        return todoListTaskId;
    }

    public void setTodoListTaskId(String todoListTaskId) {
        this.todoListTaskId = todoListTaskId;
    }

    public String getTodoListTaskTitle() {
        return todoListTaskTitle;
    }

    public void setTodoListTaskTitle(String todoListTaskTitle) {
        this.todoListTaskTitle = todoListTaskTitle;
    }

    public String getTodoListTaskStartDate() {
        return todoListTaskStartDate;
    }

    public void setTodoListTaskStartDate(String todoListTaskStartDate) {
        this.todoListTaskStartDate = todoListTaskStartDate;
    }



    public String getTodoListTaskDescription() {
        return todoListTaskDescription;
    }

    public void setTodoListTaskDescription(String todoListTaskDescription) {
        this.todoListTaskDescription = todoListTaskDescription;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public Object getCompletedOn() {
        return completedOn;
    }

    public void setCompletedOn(Object completedOn) {
        this.completedOn = completedOn;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}