package com.peerbuckets.peerbucket.POJO;
import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectListPOJO implements Serializable
{

    @SerializedName("employee_name")
    @Expose
    private String employeeName;

    @SerializedName("project_team_type")
    @Expose
    private String project_team_type;

    @SerializedName("project_team_name")
    @Expose
    private String projectTeamName;
    @SerializedName("project_team_description")
    @Expose
    private String projectTeamDescription;
    @SerializedName("project_team_id")
    @Expose
    private String projectTeamId;
    @SerializedName("userListing")
    @Expose
    private List<EmployeeDetalPOJO> userListing = null;
    private final static long serialVersionUID = 1533375961542336681L;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getProjectTeamName() {
        return projectTeamName;
    }

    public void setProjectTeamName(String projectTeamName) {
        this.projectTeamName = projectTeamName;
    }

    public String getProjectTeamDescription() {
        return projectTeamDescription;
    }

    public void setProjectTeamDescription(String projectTeamDescription) {
        this.projectTeamDescription = projectTeamDescription;
    }

    public String getProjectTeamId() {
        return projectTeamId;
    }

    public void setProjectTeamId(String projectTeamId) {
        this.projectTeamId = projectTeamId;
    }

    public List<EmployeeDetalPOJO> getUserListing() {
        return userListing;
    }

    public void setUserListing(List<EmployeeDetalPOJO> userListing) {
        this.userListing = userListing;
    }

    public String getProject_team_type() {
        return project_team_type;
    }

    public void setProject_team_type(String project_team_type) {
        this.project_team_type = project_team_type;
    }
}