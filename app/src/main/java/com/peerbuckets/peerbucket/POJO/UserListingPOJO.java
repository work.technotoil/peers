package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserListingPOJO implements Serializable {

    @SerializedName("employee_name")
    @Expose
    private String employeeName;

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    @SerializedName("user_image")
    @Expose
    private String userImage;
    private final static long serialVersionUID = 4728883164704823024L;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

}
