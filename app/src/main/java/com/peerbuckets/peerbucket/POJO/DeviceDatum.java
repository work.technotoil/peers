
package com.peerbuckets.peerbucket.POJO;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceDatum implements Serializable
{

    @SerializedName("device_id_PK")
    @Expose
    private String deviceIdPK;
    @SerializedName("user_id_FK")
    @Expose
    private String userIdFK;
    @SerializedName("device_name")
    @Expose
    private String deviceName;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("os_version")
    @Expose
    private String osVersion;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    private final static long serialVersionUID = -7919845208626926660L;

    public String getDeviceIdPK() {
        return deviceIdPK;
    }

    public void setDeviceIdPK(String deviceIdPK) {
        this.deviceIdPK = deviceIdPK;
    }

    public String getUserIdFK() {
        return userIdFK;
    }

    public void setUserIdFK(String userIdFK) {
        this.userIdFK = userIdFK;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}
