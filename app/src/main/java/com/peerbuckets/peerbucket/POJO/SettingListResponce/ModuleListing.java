package com.peerbuckets.peerbucket.POJO.SettingListResponce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModuleListing {

    @SerializedName("module_id_PK")
    @Expose
    private String moduleIdPK;
    @SerializedName("module_name")
    @Expose
    private String moduleName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isVisible")
    @Expose
    private String isVisible;

    @SerializedName("isSelected")
    @Expose
    private Boolean isSelected = false;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModuleIdPK() {
        return moduleIdPK;
    }

    public void setModuleIdPK(String moduleIdPK) {
        this.moduleIdPK = moduleIdPK;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(String isVisible) {
        this.isVisible = isVisible;
    }

}
