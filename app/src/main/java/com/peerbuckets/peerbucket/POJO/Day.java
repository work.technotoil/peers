package com.peerbuckets.peerbucket.POJO;

public class Day {
    private String dayName;
    private Boolean isSelected = false;

    public Day() {
    }

    public Day(String dayName) {
        this.dayName = dayName;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}
